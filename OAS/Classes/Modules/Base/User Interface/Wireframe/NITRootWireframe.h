//
//  NITRootWireframe.h
//  OAS
//
//  Created by Eugene Parafiynyk on 10/18/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

typedef CF_ENUM (NSUInteger, StoryboardType) {
    StoryboardTypeMain      = 0,
    StoryboardTypeCatalog   = 1,
    StoryboardTypeMap       = 2,
    StoryboardTypeFavorite  = 3,
    StoryboardTypePharmacy  = 4,
    StoryboardTypeBasket    = 5
};

@interface NITRootWireframe : NSObject

- (void)showRootViewController:(UIViewController *)viewController inWindow:(UIWindow *)window;

- (id)createViewControllerWithKey:(NSString *)key storyboardType:(StoryboardType)storyboardType;
- (id)createViewControllerFromXib:(NSString *)xib;

@end
