//
//  NITRootPresenter.m
//  OAS
//
//  Created by Eugene Parafiynyk on 3/24/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITRootPresenter.h"

@implementation NITRootPresenter

#pragma mark - Life cyrcle
- (instancetype)init
{
    if (self = [super init])
    {
        [self setup];
    }
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Private
- (void)setup
{
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRestoreNetworkConnection) name:kNetworkConnectionRestored object:nil];
}

- (void)didRestoreNetworkConnection
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateViewNotification object:nil];
}

@end
