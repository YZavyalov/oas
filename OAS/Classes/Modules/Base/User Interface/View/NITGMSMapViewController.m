//
//  NITGMSMapViewController.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/22/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITGMSMapViewController.h"
#import "HSDemoClusterRenderer.h"
#import "HSClusterMarker.h"

@interface NITGMSMapViewController ()
<
GMSMapViewDelegate
>

@end

@implementation NITGMSMapViewController

#pragma mark - Life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.mapView = [[HSClusterMapView alloc] initWithRenderer:[HSDemoClusterRenderer new]];
    
    UIView * mapContainer = [self.view viewWithTag:123];
    self.mapView.frame = mapContainer.bounds;
    self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [mapContainer addSubview:self.mapView];
    [mapContainer sendSubviewToBack:self.mapView];
    
    self.mapView.minimumMarkerCountPerCluster = 2;
    self.mapView.myLocationEnabled = true;
    self.mapView.delegate = self;
    
    [LOCATION_MANAGER updateLocationWithHandler:nil];
}

#pragma mark - Public
- (UIView *)mapViewMarkerInfoWindow:(GMSMarker *)marker
{
    return [UIView new];
}

- (void)showMarkerInfoWindowForMarker:(GMSMarker *)marker
{

}

- (void)hideMarkerInfoWindow
{

}

#pragma mark - Private
- (void)didChangeMapCameraPosition
{
    
}

- (void)didSelectMarker:(GMSMarker *)marker
{
    
}

- (void)didTapMap
{
    
}

- (void)fixCenterPositionForPoint:(CGPoint)point
{
    if(IS_IPHONE_5)
        point.y += 100;
    else
        point.y += 150;
    GMSCameraUpdate * camera = [GMSCameraUpdate setTarget:[self.mapView.projection coordinateForPoint:point]];
    [self.mapView animateWithCameraUpdate:camera];
}

- (CGPoint)pointFromCoord:(CLLocationCoordinate2D)coordinate
{
    return [self.mapView.projection pointForCoordinate:coordinate];
}

- (CGPoint)roundPoint:(CGPoint)point
{
    point.x = [[NSString stringWithFormat:@"%.2f", point.x] floatValue];
    point.y = [[NSString stringWithFormat:@"%.2f", point.y] floatValue];
    
    return point;
}

#pragma mark - GMSMapViewDelegate
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    if(![marker isKindOfClass:[HSClusterMarker class]])
    {
        [self didSelectMarker:marker];
    }
    else
    {
        GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithLatitude:marker.position.latitude
                                                                        longitude:marker.position.longitude
                                                                             zoom:self.mapView.camera.zoom * 1.1];
        
        [self.mapView animateToCameraPosition:cameraPosition];
    }
    
    return true;
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture
{
    
}

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    //[self didChangeMapCameraPosition];
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    if(self.fixCenterPosition)
    {
        self.fixCenterPosition = false;
        
        CGPoint cameraPoint = [self pointFromCoord:self.mapView.camera.target];
        [self fixCenterPositionForPoint:cameraPoint];
    }
    
    if(!self.cameraZoom) self.cameraZoom = @(mapView.camera.zoom);
    self.mapView.clusterSize = 0.1;
    [self.mapView cluster];
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [self didTapMap];
}

@end
