//
//  NITBaseViewController.h
//  OAS
//
//  Created by Eugene Parafiynyk on 10/18/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITSearchView.h"
#import "NITEmptySearchView.h"

@interface NITBaseViewController : UIViewController

#pragma mark - Action
- (IBAction)backAction:(id)sender;
- (IBAction)dismissAction:(id)sender;

#pragma mark - Keyboard
@property (nonatomic) IBInspectable BOOL keyboardActiv;
@property (nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray * keyboardShiftConstraints;
@property (nonatomic, weak) IBOutlet UIScrollView * keyboardScrollViewBottomInset;
@property (nonatomic) UIViewController * backViewController;

@property (nonatomic) IBOutlet UISearchBar * searchBar;
@property (nonatomic) IBOutlet NITSearchView * searchView;
@property (nonatomic) IBOutlet NITEmptySearchView * emptySearchView;
@property (nonatomic) UIView * mainTitle;
@property (nonatomic) UIBarButtonItem * leftBarButtonItem;
@property (nonatomic) UIBarButtonItem * rightBarButtonItem;

- (void)initSearchBar;
- (void)showSearchBar;
- (void)hideSearchBar;
- (void)enableSearchBarCanelButton;
- (void)keyboardWasShown:(NSNotification *)aNotification;
- (void)keyboardWillBeHidden:(NSNotification *)aNotification;
- (CGFloat)keyboardHeight:(NSNotification*)notification;

@end
