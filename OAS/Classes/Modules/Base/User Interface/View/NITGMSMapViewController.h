//
//  NITGMSMapViewController.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/22/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITShopItemModel.h"

@interface NITGMSMapViewController : NITBaseViewController

@property (nonatomic) IBOutlet HSClusterMapView * mapView;
@property (nonatomic) NSNumber * cameraZoom;
@property (nonatomic) BOOL fixCenterPosition;

- (UIView *)mapViewMarkerInfoWindow:(GMSMarker *)marker;
- (void)showMarkerInfoWindowForMarker:(GMSMarker *)marker;
- (void)hideMarkerInfoWindow;

@end
