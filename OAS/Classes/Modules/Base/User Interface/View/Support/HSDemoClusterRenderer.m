//
//  HSDemoClusterRenderer.m
//  HSClusterMapView
//
//  Created by Alan Bouzek on 8/25/15.
//  Copyright (c) 2015 abouzek. All rights reserved.
//

#import "HSDemoClusterRenderer.h"

@implementation HSDemoClusterRenderer

- (UIImage *)imageForClusterWithCount:(NSUInteger)count
{
    NSString *countString = [NSString stringWithFormat:@"%d", (int)count];
    
    CGFloat widthAndHeight = 25 + (5 * countString.length);
    UIImageView * clusterView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, widthAndHeight, widthAndHeight)];
    clusterView.image = [UIImage imageNamed:@"marker_big"];
    
    UILabel *numberLabel = [[UILabel alloc] initWithFrame:clusterView.frame];
    numberLabel.font = [UIFont systemFontOfSize:17];
    numberLabel.textColor = [UIColor whiteColor];
    numberLabel.text = countString;
    numberLabel.textAlignment = NSTextAlignmentCenter;
    [clusterView addSubview:numberLabel];
    
    UIGraphicsBeginImageContextWithOptions(clusterView.bounds.size, NO, 0.0);
    [clusterView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *icon = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
        
    return icon;
}

@end
