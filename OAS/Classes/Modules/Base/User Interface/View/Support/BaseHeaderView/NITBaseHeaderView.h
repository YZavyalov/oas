//
//  NITBaseHeaderView.h
//  OAS
//
//  Created by Eugene Parafiynyk on 6/7/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITBaseHeaderView : UIView

+ (instancetype)viewWithTitle:(NSString *)title;

@end
