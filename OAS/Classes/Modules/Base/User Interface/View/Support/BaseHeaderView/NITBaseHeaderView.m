//
//  NITBaseHeaderView.m
//  OAS
//
//  Created by Eugene Parafiynyk on 6/7/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseHeaderView.h"

@interface NITBaseHeaderView()

@property (nonatomic) IBOutlet UILabel * title;

@end

@implementation NITBaseHeaderView

+ (instancetype)viewWithTitle:(NSString *)title
{
    NITBaseHeaderView * view = [[[NSBundle mainBundle] loadNibNamed:_s(self) owner:self options:nil] firstObject];
    view.title.text = title;
    
    return view;
}

@end
