//
//  NITBaseViewController.m
//  OAS
//
//  Created by Eugene Parafiynyk on 10/18/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseViewController.h"
#import "NITCatalogTopSearchTableViewCell.h"

@interface NITBaseViewController ()

@end

@implementation NITBaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = false;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self enableSearchBarCanelButton];
    [self.tabBarController showTabBarAnimated:false];

    self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
    [self.navigationController.interactivePopGestureRecognizer setEnabled:true];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:true];
    [HELPER stopLoading];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Custom Accessors
- (void)setKeyboardActiv:(BOOL)keyboardActiv
{
    if (_keyboardActiv != keyboardActiv)
    {
        _keyboardActiv = keyboardActiv;
        if (_keyboardActiv)
        {
            [self registerForKeyboardNotifications];
            [self addKeyboardHideTouch];
        }
        else
        {
            [self unregisterForKeyboardNotifications];
        }
    }
}

#pragma mark - Action
- (IBAction)backAction:(id)sender
{
    if (self.backViewController)
    {
        [self dismissViewControllerAnimated:true completion:^{}];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:true];
    }
}

- (IBAction)dismissAction:(id)sender
{
    [self dismissViewControllerAnimated:true completion:^{}];
}

#pragma mark - Search
- (void)initSearchBar
{
    
    id barButtonAppearanceInSearchBar = [UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]];
    [barButtonAppearanceInSearchBar setTitle:NSLocalizedString(@"Отменить", nil)];
    [barButtonAppearanceInSearchBar setTintColor:RGB(88, 160, 144)];
    self.searchBar.placeholder = NSLocalizedString(@"Поиск", nil);
    
    // nav bar
    self.mainTitle = self.navigationItem.titleView;
    self.leftBarButtonItem = self.navigationItem.leftBarButtonItem;
    self.rightBarButtonItem = self.navigationItem.rightBarButtonItem;
}

- (void)showSearchBar
{
    [UIView animateWithDuration:0.1 animations:^{} completion:^(BOOL finished) {
        self.searchBar.hidden = false;
        self.searchBar.showsCancelButton = true;
        [self enableSearchBarCanelButton];
        self.navigationItem.rightBarButtonItems = nil;
        self.navigationItem.leftBarButtonItem  = nil;
        self.navigationItem.titleView = self.searchBar;
        self.navigationItem.hidesBackButton = true;
        
        [self.searchBar becomeFirstResponder];
    }];
}

- (void)hideSearchBar
{
    [UIView animateWithDuration:0.1f animations:^{
        [self.searchBar endEditing:true];
        self.searchBar.text = @"";
        self.searchBar.hidden = true;
        self.searchBar.showsCancelButton = false;
        self.searchView.hidden = true;
        self.emptySearchView.hidden = true;
    } completion:^(BOOL finished) {
        self.navigationItem.titleView = self.mainTitle;
        self.navigationItem.rightBarButtonItem = self.rightBarButtonItem;
        self.navigationItem.leftBarButtonItem = self.leftBarButtonItem;
        self.navigationItem.hidesBackButton = false;
    }];
}

- (void)enableSearchBarCanelButton
{
    for (UIView * view in self.searchBar.subviews)
    {
        for (id subview in view.subviews)
        {
            if ( [subview isKindOfClass:[UIButton class]] )
            {
                [subview setEnabled:true];
                return;
            }
        }
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    self.searchBar.showsCancelButton = true;
}

#pragma mark - Keyboard
- (void)registerForKeyboardNotifications
{
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self
               selector:@selector(keyboardWasShown:)
                   name:UIKeyboardWillShowNotification object:nil];
    
    [center addObserver:self
               selector:@selector(keyboardWillBeHidden:)
                   name:UIKeyboardWillHideNotification object:nil];
}

- (void)unregisterForKeyboardNotifications
{
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    
    [center removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [center removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIViewAnimationCurve animationCurve;
    NSTimeInterval animationDuration;
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    for (NSLayoutConstraint *c in self.keyboardShiftConstraints)
    {
        CGFloat devider = 1.f;
        c.constant = kbSize.height/devider;
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    [self.view layoutIfNeeded];
    
    if (self.keyboardScrollViewBottomInset)
    {
        UIEdgeInsets insets = UIEdgeInsetsZero;
        insets.bottom += kbSize.height;
        self.keyboardScrollViewBottomInset.contentInset = insets;
    }
    
    [UIView commitAnimations];
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    UIViewAnimationCurve animationCurve;
    NSTimeInterval animationDuration;
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    for (NSLayoutConstraint *c in self.keyboardShiftConstraints)
    {
        CGFloat value = 0.f;
        c.constant = value;
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    [self.view layoutIfNeeded];
    
    if (self.keyboardScrollViewBottomInset)
    {
        self.keyboardScrollViewBottomInset.contentInset = UIEdgeInsetsZero;
    }
    
    [UIView commitAnimations];
}

- (void)addKeyboardHideTouch
{
    UIGestureRecognizer * tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = false;
    
    [self.view addGestureRecognizer:tapper];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:true];
}

- (CGFloat)keyboardHeight:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    return keyboardFrameBeginRect.size.height;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

@end
