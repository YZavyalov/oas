//
//  NITLaunchScreenAPIDataManager.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/23/2017.
//  Copyright © 2017 NapoleonIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITLaunchScreenProtocols.h"

@interface NITLaunchScreenAPIDataManager : NSObject <NITLaunchScreenAPIDataManagerInputProtocol>

@end
