//
//  NITLaunchScreenAPIDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/23/2017.
//  Copyright © 2017 NapoleonIT. All rights reserved.
//

#import "NITLaunchScreenAPIDataManager.h"

@implementation NITLaunchScreenAPIDataManager

#pragma mark - NITLaunchScreenDataManagerInputProtocol
//registration
- (void)registrationWithType:(NSString *)userType withLogin:(NSString *)userLogin withPassowrd:(NSString *)userPassword withhandler:(void(^)(SLSignupResponse *output))handler
{
    SLSignupQuery * query = [SLSignupQuery new];
    query.value     = userLogin;
    query.password  = userPassword;
    query.type      = userType;
    
    [[SLAuthApi new] postAuthSignupPasswordWithBody:query completionHandler:^(SLSignupResponse *output, NSError *error) {
        if(error)
        {
            [ERROR_MESSAGE errorMessage:error_on_splash];
            [HELPER logError:error method:METHOD_NAME];
        }
        if (output)
        {
            USER.login      = userLogin;
            USER.password   = userPassword;
            
            handler (output);
        }
    }];
}

- (void)authorizationUserWithHandler:(void(^)(SLSignin *result))handler
{   
    SLPasswordQuery * query = [SLPasswordQuery new];
    query.value     = USER.login;
    query.password  = USER.password;
    query.type      = @"login";

    [[SLAuthApi new] postAuthSigninPasswordWithBody:query completionHandler:^(SLSignin *output, NSError *error) {
        if (error)
        {
            [ERROR_MESSAGE errorMessage:error_service_unavailable];
            [HELPER logError:error method:METHOD_NAME];
        }
        if (output) handler (output);
    }];
}

//infosystems
- (void)getInfossytemConstantWithHandler:(void(^)(NSArray <SLInfosystem *> *output))handler
{
    [[SLInfosystemsApi new] getStaticInfosystemsWithCompletionHandler:^(NSArray<SLInfosystem> *output, NSError *error) {
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (output) handler (output);
    }];
}

//Mobile constants
- (void)getDocumentsWithHandler:(void(^)(SLDocumentResponse *output))handler
{
    [[SLDocumentsApi new] getStaticDocumentsWithCompletionHandler:^(SLDocumentResponse *output, NSError *error) {
        if(error) [HELPER logError:error method:METHOD_NAME];
        if (output) handler (output);
    }];
}

- (void)getMobileConstantWithHandler:(void(^)(NSDictionary *output))handler
{
    [[SLDocumentsApi new] getStaticDocumentsItemsByDocumentidBodyWithDocumentId:[API mobileConstantID] completionHandler:^(NSURL *output, NSError *error) {
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:output cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30];
        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
            if(data)
            {
                NSDictionary * jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
                if (error) [HELPER logError:error method:METHOD_NAME];
                if (data) handler (jsonData);
            }
            else
            {
                [HELPER logError:error method:METHOD_NAME];
            }
        }];
        [task resume];
    }];
}

@end
