//
//  NITLaunchScreenInteractor.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/23/2017.
//  Copyright © 2017 NapoleonIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITLaunchScreenProtocols.h"

@interface NITLaunchScreenInteractor : NSObject <NITLaunchScreenInteractorInputProtocol>

@property (nonatomic, weak) id <NITLaunchScreenInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITLaunchScreenAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITLaunchScreenLocalDataManagerInputProtocol> localDataManager;

@end
