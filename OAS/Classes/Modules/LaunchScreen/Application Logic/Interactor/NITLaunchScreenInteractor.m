//
//  NITLaunchScreenInteractor.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/23/2017.
//  Copyright © 2017 NapoleonIT. All rights reserved.
//

#import "NITLaunchScreenInteractor.h"

@implementation NITLaunchScreenInteractor

#pragma mark - NITLaunchScreenInteractorInputProtocol
//registration
- (void)registrationWithType:(NSString *)userType withLogin:(NSString *)userLogin withPassowrd:(NSString *)userPassword
{
    weaken(self);
    [self.APIDataManager registrationWithType:userType withLogin:userLogin withPassowrd:userPassword withhandler:^(SLSignupResponse *output) {
        [weakSelf.presenter updateCidUid:output];
    }];
}

- (void)authorization
{
    weaken(self);
    [self.APIDataManager authorizationUserWithHandler:^(SLSignin *result) {
        [weakSelf.presenter updateCidUidToken:result];
    }];
}

//infosystems
- (void)getInfosystems
{
    weaken(self);
    [self.APIDataManager getInfossytemConstantWithHandler:^(NSArray<SLInfosystem *> *output) {
        [weakSelf.presenter updateInfosystems:output];
    }];
}

//documents
- (void)getDocuments
{
    weaken(self);
    [self.APIDataManager getDocumentsWithHandler:^(SLDocumentResponse *output) {
        [output.documents bk_each:^(SLDocument *obj) {
            if([obj.name isEqualToString:@"mobile_constants"])
            {
                [[NITMobileConstantsModel new] setMobileCosntantsID:obj._id];
            }
        }];
        [weakSelf.APIDataManager getMobileConstantWithHandler:^(NSDictionary *output) {
            [weakSelf.presenter updateDocuments:output];
        }];
    }];
}


@end
