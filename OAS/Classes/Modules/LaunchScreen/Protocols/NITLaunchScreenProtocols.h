//
//  NITLaunchScreenProtocols.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/23/2017.
//  Copyright © 2017 NapoleonIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SLAuthApi.h"
#import "SLInfosystemsApi.h"
#import "SLDocumentsApi.h"
#import "NITMobileConstantsModel.h"
#import "SLUsersApi.h"

@protocol NITLaunchScreenInteractorOutputProtocol;
@protocol NITLaunchScreenInteractorInputProtocol;
@protocol NITLaunchScreenViewProtocol;
@protocol NITLaunchScreenPresenterProtocol;
@protocol NITLaunchScreenLocalDataManagerInputProtocol;
@protocol NITLaunchScreenAPIDataManagerInputProtocol;

@class NITLaunchScreenWireFrame;

// Defines the public interface that something else can use to drive the user interface
@protocol NITLaunchScreenViewProtocol
@required
@property (nonatomic, strong) id <NITLaunchScreenPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

@protocol NITLaunchScreenWireFrameProtocol
@required
+ (id)createNITLaunchScreenModule;
- (void)presentNITLaunchScreenModuleFromWindow:(UIWindow *)window;
- (void)presentGeolocationFrom:(UIWindow *)window;
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
@end

@protocol NITLaunchScreenPresenterProtocol
@required
@property (nonatomic, weak) id <NITLaunchScreenViewProtocol> view;
@property (nonatomic, strong) id <NITLaunchScreenInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITLaunchScreenWireFrameProtocol> wireFrame;
@property (nonatomic) UIWindow * window;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
@end

@protocol NITLaunchScreenInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateCidUid:(SLSignupResponse *)data;
- (void)updateCidUidToken:(SLSignin *)data;
- (void)updateInfosystems:(NSArray <SLInfosystem *> *)data;
- (void)updateDocuments:(NSDictionary *)data;
@end

@protocol NITLaunchScreenInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITLaunchScreenInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITLaunchScreenAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITLaunchScreenLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)registrationWithType:(NSString *)userType withLogin:(NSString *)userLogin withPassowrd:(NSString *)userPassword;
- (void)authorization;
- (void)getInfosystems;
- (void)getDocuments;
@end


@protocol NITLaunchScreenDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITLaunchScreenAPIDataManagerInputProtocol <NITLaunchScreenDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)registrationWithType:(NSString *)userType withLogin:(NSString *)userLogin withPassowrd:(NSString *)userPassword withhandler:(void(^)(SLSignupResponse *output))handler;
- (void)authorizationUserWithHandler:(void(^)(SLSignin *result))handler;
- (void)getInfossytemConstantWithHandler:(void(^)(NSArray <SLInfosystem *> *output))handler;
- (void)getDocumentsWithHandler:(void(^)(SLDocumentResponse *output))handler;
- (void)getMobileConstantWithHandler:(void(^)(NSDictionary *output))handler;
@end

@protocol NITLaunchScreenLocalDataManagerInputProtocol <NITLaunchScreenDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
