//
//  NITLaunchScreenPresenter.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/23/2017.
//  Copyright © 2017 NapoleonIT. All rights reserved.
//

#import "NITLaunchScreenPresenter.h"
#import "NITLaunchScreenWireframe.h"
#import "NITInfosystems.h"
#import "NITApplyingFilterModel.h"
#import <Crashlytics/Crashlytics.h>

@interface NITLaunchScreenPresenter()

@property (nonatomic) dispatch_group_t group;

@end

@implementation NITLaunchScreenPresenter

#pragma mark - NITLaunchScreenPresenterProtocol
- (void)initData
{
    [self addObservers];
    [self updateData];
}

- (void)showGeolocation
{
    [self.wireFrame presentGeolocationFrom:self.window];
}

#pragma mark - NITLaunchScreenInteractorOutputProtocol
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kUpdateInternetConnect object:nil];
}

- (void)updateData
{
    [NOTIFICATION clearNoptificationData];
    [[NITApplyingFilterModel new] clearFilter];
    
    //TODO: в конце подключить KeyChain
    //[self getKaychainUserdata];
    
    if(!(USER.login.length > 0) && !(USER.password.length > 0))
    {
        NSString * userLogin    = [self randomString];
        NSString * userPassword = [self randomString];
        NSString * userType     = @"login";
        
        [self.interactor registrationWithType:userType withLogin:userLogin withPassowrd:userPassword];
    }
    else
    {
        [self.interactor authorization];
    }
}

- (void)updateCidUid:(SLSignupResponse *)data
{
    NSString * userData = [[USER.login stringByAppendingString:@" "] stringByAppendingString:USER.password];
    [SAMKeychain setPassword:userData forService:kKeychainServiceKey account:kKeychainAccountKey];

    USER.company    = data.cid;
    USER.identifier = data.uid;
    SLDefaultConfiguration *apiConfig = [SLDefaultConfiguration sharedConfig];
    [apiConfig setApiKey:USER.company forApiKeyIdentifier:@"x-cid"];
    
    [self.interactor authorization];
}

- (void)updateCidUidToken:(SLSignin *)data
{
    [CrashlyticsKit setUserIdentifier:USER.identifier];
    [CrashlyticsKit setUserEmail:USER.email];
    [CrashlyticsKit setUserName:USER.name];
    
    USER.company     = data.xCid;
    USER.identifier  = data.xUid;
    USER.accessToken = data.xToken;
    SLDefaultConfiguration *apiConfig = [SLDefaultConfiguration sharedConfig];
    [apiConfig setApiKey:USER.company forApiKeyIdentifier:@"x-cid"];
    [apiConfig setApiKey:USER.accessToken forApiKeyIdentifier:@"x-token"];
    
    self.group = dispatch_group_create();
    dispatch_group_enter(self.group);
    dispatch_group_enter(self.group);
    [self.interactor getInfosystems];
    [self.interactor getDocuments];

    weaken(self);
    dispatch_group_notify(self.group, dispatch_get_main_queue(), ^{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [API syncUserData];
        });
        [weakSelf showGeolocation];
    });
}

- (void)updateInfosystems:(NSArray <SLInfosystem *> *)data
{
    __unused NITInfosystems * infosystems = [[NITInfosystems alloc] initWithModel:data];
    dispatch_group_leave(self.group);
}

- (void)updateDocuments:(NSDictionary *)data
{
    __unused NITMobileConstantsModel * mobileConstants = [[NITMobileConstantsModel alloc] initWithDictionary:data];
    dispatch_group_leave(self.group);
}

#pragma mark - Private
- (NSString *)randomString
{
    int len = arc4random_uniform(8) + 3;
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    for (int i = 0; i < len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((int)[letters length])]];
    }
    
    return randomString;
}

- (void)getKaychainUserdata
{
    NSError *error = nil;
    SAMKeychainQuery *query = [[SAMKeychainQuery alloc] init];
    query.service = kKeychainServiceKey;
    query.account = kKeychainAccountKey;
    [query fetch:&error];
    
    if(!error)
    {
        NSArray <NSString *> * userData = [[SAMKeychain passwordForService:kKeychainServiceKey account:kKeychainAccountKey] componentsSeparatedByString:@" "];
        USER.login      = [userData objectAtIndex:0].length > 0 ? [userData objectAtIndex:0] : nil;
        USER.password   = [userData objectAtIndex:1].length > 0 ? [userData objectAtIndex:1] : nil;
    }
}

@end
