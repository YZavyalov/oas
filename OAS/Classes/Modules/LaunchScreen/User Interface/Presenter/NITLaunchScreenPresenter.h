//
//  NITLaunchScreenPresenter.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/23/2017.
//  Copyright © 2017 NapoleonIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITLaunchScreenProtocols.h"

@class NITLaunchScreenWireFrame;

@interface NITLaunchScreenPresenter : NSObject <NITLaunchScreenPresenterProtocol, NITLaunchScreenInteractorOutputProtocol>

@property (nonatomic, weak) id <NITLaunchScreenViewProtocol> view;
@property (nonatomic, strong) id <NITLaunchScreenInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITLaunchScreenWireFrameProtocol> wireFrame;

@property (nonatomic) UIWindow * window;

@end
