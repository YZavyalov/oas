//
//  NITLaunchScreenViewController.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/23/2017.
//  Copyright © 2017 NapoleonIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITLaunchScreenProtocols.h"
#import "NITBaseViewController.h"

@interface NITLaunchScreenViewController : NITBaseViewController <NITLaunchScreenViewProtocol>

@property (nonatomic, strong) id <NITLaunchScreenPresenterProtocol> presenter;

@end
