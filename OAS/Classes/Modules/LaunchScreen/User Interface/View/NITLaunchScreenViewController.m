//
//  NITLaunchScreenViewController.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/23/2017.
//  Copyright © 2017 NapoleonIT. All rights reserved.
//

#import "NITLaunchScreenViewController.h"

@implementation NITLaunchScreenViewController

#pragma mark - ViewController Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

#pragma mark - Private
- (void)initUI
{
    [self.navigationController setNavigationBarHidden:true animated:false];
}

@end
