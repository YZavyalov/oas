//
//  NITLaunchScreenWireFrame.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/23/2017.
//  Copyright © 2017 NapoleonIT. All rights reserved.
//

#import "NITLaunchScreenWireFrame.h"
#import "NITGeolocationWireFrame.h"

@implementation NITLaunchScreenWireFrame

#pragma mark - Private
+ (id)createNITLaunchScreenModule
{
    // Generating module components
    id <NITLaunchScreenPresenterProtocol, NITLaunchScreenInteractorOutputProtocol> presenter = [NITLaunchScreenPresenter new];
    id <NITLaunchScreenInteractorInputProtocol> interactor = [NITLaunchScreenInteractor new];
    id <NITLaunchScreenAPIDataManagerInputProtocol> APIDataManager = [NITLaunchScreenAPIDataManager new];
    id <NITLaunchScreenLocalDataManagerInputProtocol> localDataManager = [NITLaunchScreenLocalDataManager new];
    id <NITLaunchScreenWireFrameProtocol> wireFrame= [NITLaunchScreenWireFrame new];
    id <NITLaunchScreenViewProtocol> view = [(NITLaunchScreenWireFrame *)wireFrame createViewControllerWithKey:_s(NITLaunchScreenViewController) storyboardType:StoryboardTypeMain];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

- (void)presentNITLaunchScreenModuleFromWindow:(UIWindow *)window
{
    NITLaunchScreenViewController * vc = [NITLaunchScreenWireFrame createNITLaunchScreenModule];
    vc.presenter.window = window;
    [self showRootViewController:vc inWindow:window];
}

- (void)presentGeolocationFrom:(UIWindow *)window
{
    NITGeolocationWireFrame * geolocationWireFrame = [NITGeolocationWireFrame new];
    [geolocationWireFrame presentNITGeolocationModuleFromWindow:window];
}


@end
