//
//  NITLaunchScreenWireFrame.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/23/2017.
//  Copyright © 2017 NapoleonIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITLaunchScreenProtocols.h"
#import "NITLaunchScreenViewController.h"
#import "NITLaunchScreenLocalDataManager.h"
#import "NITLaunchScreenAPIDataManager.h"
#import "NITLaunchScreenInteractor.h"
#import "NITLaunchScreenPresenter.h"
#import "NITLaunchScreenWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITLaunchScreenWireFrame : NITRootWireframe <NITLaunchScreenWireFrameProtocol>

@end
