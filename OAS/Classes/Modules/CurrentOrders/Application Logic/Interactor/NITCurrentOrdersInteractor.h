//
//  NITCurrentOrdersInteractor.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/19/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCurrentOrdersProtocols.h"

@interface NITCurrentOrdersInteractor : NSObject <NITCurrentOrdersInteractorInputProtocol>

@property (nonatomic, weak) id <NITCurrentOrdersInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCurrentOrdersAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCurrentOrdersLocalDataManagerInputProtocol> localDataManager;

@end
