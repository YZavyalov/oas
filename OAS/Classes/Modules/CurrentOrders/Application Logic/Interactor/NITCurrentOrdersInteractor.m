//
//  NITCurrentOrdersInteractor.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/19/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCurrentOrdersInteractor.h"

@implementation NITCurrentOrdersInteractor

#pragma mark - NITCurrentOrdersInteractorInputProtocol
- (void)getOrders
{
    NSMutableArray * sortedModels = [NITOrderItemModel orderItemsModelsFromOrders:[self.localDataManager getOrders]].mutableCopy;
    [sortedModels sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:keyPath(NITOrderItemModel.createdAt) ascending:false]]];
    [self.presenter updateDataByModels:sortedModels];
}

@end
