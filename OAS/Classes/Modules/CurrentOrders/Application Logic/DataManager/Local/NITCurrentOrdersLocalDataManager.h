//
//  NITCurrentOrdersLocalDataManager.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/19/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCurrentOrdersProtocols.h"

@interface NITCurrentOrdersLocalDataManager : NSObject <NITCurrentOrdersLocalDataManagerInputProtocol>

@end
