//
//  NITCurrentOrdersLocalDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/19/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCurrentOrdersLocalDataManager.h"

@implementation NITCurrentOrdersLocalDataManager

#pragma mark - NITCurrentOrdersDataManagerInputProtocol
- (NSArray <NITOrderItem *> *)getOrders
{
    return [NITOrderItem allOrderItems];
}

@end
