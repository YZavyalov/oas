//
//  NITCurrentOrdersViewController.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/19/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCurrentOrdersViewController.h"
#import "NITOrderPriceTableViewCell.h"
#import "NITOrderInfoTableViewCell.h"

@interface NITCurrentOrdersViewController()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic) IBOutlet UITableView * tableView;

@property (nonatomic) IBOutlet UIView * emptyView;
@property (nonatomic) IBOutlet UILabel * emptyTitleLbl;
@property (nonatomic) IBOutlet UILabel * emptySubtitleLbl;
@property (nonatomic) IBOutlet UIButton * showCatalogBtn;

@property (nonatomic) NSArray <NITOrderItemModel *> * models;

@end

@implementation NITCurrentOrdersViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self.presenter initData];
}

#pragma mark - IBAction
- (IBAction)showCatalogAction:(id)sender
{
    [self.presenter showCatalog];
}

#pragma mark - Private
- (void)initData
{    
    //Table view
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITOrderPriceTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITOrderPriceTableViewCell)];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITOrderInfoTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITOrderInfoTableViewCell)];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)updateEmptyView
{
    self.emptyView.hidden = false;
    self.tableView.hidden = true;
}

#pragma mark - NITCurrentOrdersPresenterProtocol
- (void)reloadDataByModels:(NSArray <NITOrderItemModel *> *)models
{
    self.tableView.hidden = models.count > 0 ? false : true;
    self.emptyView.hidden = models.count > 0 ? true : false;
    self.models = models;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.models.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return section == 0 ? 0 : 15;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CurrentOrderRowType rowType = indexPath.row;
    switch (rowType)
    {
        case CurrentOrderRowTypePrice:          return 44;
        case CurrentOrderRowTypeDescription:    return 114;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CurrentOrderRowType rowType = indexPath.row;
    switch (rowType)
    {
        case CurrentOrderRowTypePrice:
        {
            NITOrderPriceTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITOrderPriceTableViewCell) forIndexPath:indexPath];
            cell.orderNumber = self.models.count - indexPath.section;
            cell.orderPrice = [self.presenter orderPrice:self.models[indexPath.section]];
            return cell;
        } break;
            
        case CurrentOrderRowTypeDescription:
        {
            NITOrderInfoTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITOrderInfoTableViewCell) forIndexPath:indexPath];
            cell.model = self.models[indexPath.section];
            
            return cell;
        } break;
    }
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.presenter showCurrentOrderDetails:indexPath.section withModel:self.models[indexPath.section]];
}

@end
