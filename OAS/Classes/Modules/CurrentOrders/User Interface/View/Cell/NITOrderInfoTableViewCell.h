//
//  NITOrderInfoTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 19.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITOrderItemModel.h"

@interface NITOrderInfoTableViewCell : UITableViewCell

@property (nonatomic) NITOrderItemModel * model;

@end
