//
//  NITOrderInfoTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 19.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderInfoTableViewCell.h"
#import "NITMobileConstantsModel.h"
#import "NITPharmacyItem.h"
#import "NITShopItemModel.h"
#import "SLShopsApi.h"

@interface NITOrderInfoTableViewCell()

@property (nonatomic) IBOutlet UILabel * dateLbl;
@property (nonatomic) IBOutlet UILabel * addressLbl;
@property (nonatomic) IBOutlet UIImageView * byOrderImg;

@property (nonatomic) IBOutlet NSLayoutConstraint * addressTrailing;

@end

@implementation NITOrderInfoTableViewCell

#pragma mark - Life cycle
- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initData];
}

#pragma mark - Custom accessors
- (void)setModel:(NITOrderItemModel *)model
{
    _model = model;
    
    [self updateDate];
}

#pragma mark - Private
- (void)initData
{
    self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_next"]];
}

- (void)updateDate
{
    self.addressTrailing.constant = self.model.underOrder ? 70 : 0;
    self.dateLbl.text = self.model.times;
    self.addressLbl.text = self.model.address;
    self.byOrderImg.hidden = !self.model.underOrder;
}

@end
