//
//  NITOrderPriceTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 19.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITOrderPriceTableViewCell : UITableViewCell

@property (nonatomic) NSInteger orderNumber;
@property (nonatomic) double orderPrice;

@end
