//
//  NITOrderPriceTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 19.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderPriceTableViewCell.h"

@interface NITOrderPriceTableViewCell()

@property (nonatomic) IBOutlet UILabel * orderNumberLbl;
@property (nonatomic) IBOutlet UILabel * priceLbl;

@property (nonatomic) IBOutlet NSLayoutConstraint * orderNumberLblW;

@end

@implementation NITOrderPriceTableViewCell

#pragma mark - Custom accessors
- (void)setOrderNumber:(NSInteger)orderNumber
{
    _orderNumber = orderNumber;
    
    [self updateData];
}

- (void)setOrderPrice:(double)orderPrice
{
    _orderPrice = orderPrice;
    
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.orderNumberLbl.text = [NSString stringWithFormat:@"Заказ №%ld на сумму:", (long)self.orderNumber];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:[NSLocale currentLocale]];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    NSString * priceFormatter = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:self.orderPrice]];
    self.priceLbl.text = [priceFormatter stringByAppendingString:@" ₽"];
}

@end
