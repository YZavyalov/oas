//
//  NITCurrentOrdersViewController.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/19/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCurrentOrdersProtocols.h"
#import "NITBaseViewController.h"

@interface NITCurrentOrdersViewController : NITBaseViewController <NITCurrentOrdersViewProtocol>

@property (nonatomic, strong) id <NITCurrentOrdersPresenterProtocol> presenter;

@end
