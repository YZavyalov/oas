//
//  NITCurrentOrdersWireFrame.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/19/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCurrentOrdersWireFrame.h"
#import "NITCatalogWireFrame.h"
#import "NITCurrentOrderDetailsWireFrame.h"

@implementation NITCurrentOrdersWireFrame

#pragma mark - Private
+ (id)createNITCurrentOrdersModule
{
    // Generating module components
    id <NITCurrentOrdersPresenterProtocol, NITCurrentOrdersInteractorOutputProtocol> presenter = [NITCurrentOrdersPresenter new];
    id <NITCurrentOrdersInteractorInputProtocol> interactor = [NITCurrentOrdersInteractor new];
    id <NITCurrentOrdersAPIDataManagerInputProtocol> APIDataManager = [NITCurrentOrdersAPIDataManager new];
    id <NITCurrentOrdersLocalDataManagerInputProtocol> localDataManager = [NITCurrentOrdersLocalDataManager new];
    id <NITCurrentOrdersWireFrameProtocol> wireFrame= [NITCurrentOrdersWireFrame new];
    id <NITCurrentOrdersViewProtocol> view = [(NITCurrentOrdersWireFrame *)wireFrame createViewControllerWithKey:_s(NITCurrentOrdersViewController) storyboardType:StoryboardTypeBasket];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITCurrentOrdersWireFrameProtocol
+ (void)presentNITCurrentOrdersModuleFrom:(UIViewController *)fromViewController
{
    NITCurrentOrdersViewController * vc = [NITCurrentOrdersWireFrame createNITCurrentOrdersModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)showCatalogFromViewController:(UIViewController *)fromViewController
{
    [fromViewController.tabBarController setSelectedIndex:0];
}

- (void)showCurrentOrderDetails:(UIViewController *)fromViewController withOrderNumber:(NSInteger)orderNumber witModel:(NITOrderItemModel *)model withType:(NSString *)type
{
    [NITCurrentOrderDetailsWireFrame presentNITCurrentOrderDetailsModuleFrom:fromViewController withOrderNumber:orderNumber withModel:model withType:type];
}

@end
