//
//  NITCurrentOrdersWireFrame.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/19/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCurrentOrdersProtocols.h"
#import "NITCurrentOrdersViewController.h"
#import "NITCurrentOrdersLocalDataManager.h"
#import "NITCurrentOrdersAPIDataManager.h"
#import "NITCurrentOrdersInteractor.h"
#import "NITCurrentOrdersPresenter.h"
#import "NITCurrentOrdersWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITCurrentOrdersWireFrame : NITRootWireframe <NITCurrentOrdersWireFrameProtocol>

@end
