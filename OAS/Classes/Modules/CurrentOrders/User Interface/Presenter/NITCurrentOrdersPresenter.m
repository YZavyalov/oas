//
//  NITCurrentOrdersPresenter.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/19/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCurrentOrdersPresenter.h"
#import "NITCurrentOrdersWireframe.h"

@implementation NITCurrentOrdersPresenter

#pragma mark - NITCurrentOrdersPresenterProtocol
- (void)initData
{
    [self addObservers];
    [self updateData];
}

- (void)showCatalog
{
    [self.wireFrame showCatalogFromViewController:self.view];
}

- (void)showCurrentOrderDetails:(NSInteger)orderNumber withModel:(NITOrderItemModel *)model
{
    [self.wireFrame showCurrentOrderDetails:self.view withOrderNumber:orderNumber witModel:model withType:model.pick_up];
}

- (double)orderPrice:(NITOrderItemModel *)model
{
    float price = 0;
    for(NITCatalogItemModel * item in model.items)
    {
        price += [item.price doubleValue] * item.orderCount;
    }
    
    return price;
}

#pragma mark - NITCurrentOrdersInteractorOutputProtocol
- (void)updateDataByModels:(NSArray <NITOrderItemModel *> *)models
{
    [self.view reloadDataByModels:models];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kSaveOrderItemNotification object:nil];
}

- (void)updateData
{
    [self.interactor getOrders];
}

@end
