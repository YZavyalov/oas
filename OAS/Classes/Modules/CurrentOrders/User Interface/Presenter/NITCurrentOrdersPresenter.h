//
//  NITCurrentOrdersPresenter.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/19/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCurrentOrdersProtocols.h"

@class NITCurrentOrdersWireFrame;

@interface NITCurrentOrdersPresenter : NSObject <NITCurrentOrdersPresenterProtocol, NITCurrentOrdersInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCurrentOrdersViewProtocol> view;
@property (nonatomic, strong) id <NITCurrentOrdersInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCurrentOrdersWireFrameProtocol> wireFrame;

@end
