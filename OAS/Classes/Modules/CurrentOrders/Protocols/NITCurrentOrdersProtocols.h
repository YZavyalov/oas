//
//  NITCurrentOrdersProtocols.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/19/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITOrderItem.h"
#import "NITOrderItemModel.h"

@protocol NITCurrentOrdersInteractorOutputProtocol;
@protocol NITCurrentOrdersInteractorInputProtocol;
@protocol NITCurrentOrdersViewProtocol;
@protocol NITCurrentOrdersPresenterProtocol;
@protocol NITCurrentOrdersLocalDataManagerInputProtocol;
@protocol NITCurrentOrdersAPIDataManagerInputProtocol;

@class NITCurrentOrdersWireFrame;

typedef CF_ENUM (NSUInteger, CurrentOrderRowType) {
    CurrentOrderRowTypePrice        = 0,
    CurrentOrderRowTypeDescription  = 1
};

// Defines the public interface that something else can use to drive the user interface
@protocol NITCurrentOrdersViewProtocol
@required
@property (nonatomic, strong) id <NITCurrentOrdersPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModels:(NSArray <NITOrderItemModel *> *)models;
@end

@protocol NITCurrentOrdersWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITCurrentOrdersModuleFrom:(id)fromViewController;
- (void)showCatalogFromViewController:(id)fromViewController;
- (void)showCurrentOrderDetails:(id)fromViewController withOrderNumber:(NSInteger)orderNumber witModel:(NITOrderItemModel *)model withType:(NSString *)type;
@end

@protocol NITCurrentOrdersPresenterProtocol
@required
@property (nonatomic, weak) id <NITCurrentOrdersViewProtocol> view;
@property (nonatomic, strong) id <NITCurrentOrdersInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCurrentOrdersWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)showCatalog;
- (void)showCurrentOrderDetails:(NSInteger)orderNumber withModel:(NITOrderItemModel *)model;
- (double)orderPrice:(NITOrderItemModel *)model;
@end

@protocol NITCurrentOrdersInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateDataByModels:(NSArray <NITOrderItemModel *> *)models;
@end

@protocol NITCurrentOrdersInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCurrentOrdersInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCurrentOrdersAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCurrentOrdersLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getOrders;
@end


@protocol NITCurrentOrdersDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCurrentOrdersAPIDataManagerInputProtocol <NITCurrentOrdersDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITCurrentOrdersLocalDataManagerInputProtocol <NITCurrentOrdersDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NSArray <NITOrderItem *> *)getOrders;
@end
