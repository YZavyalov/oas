//
//  NITPharmacyProtocols.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "HSClusterMarker.h"
#import "NITPharmacySectionsModel.h"
#import "NITShopMarker.h"
#import "NITPharmacyItem.h"

@protocol NITPharmacyInteractorOutputProtocol;
@protocol NITPharmacyInteractorInputProtocol;
@protocol NITPharmacyViewProtocol;
@protocol NITPharmacyPresenterProtocol;
@protocol NITPharmacyLocalDataManagerInputProtocol;
@protocol NITPharmacyAPIDataManagerInputProtocol;

@class NITPharmacyWireFrame, NITShopItemModel;

typedef CF_ENUM (NSUInteger, PharmacyType) {
    PharmacyTypeAddress = 0,
    PharmacyTypeMap     = 1
};

typedef CF_ENUM (NSUInteger, PharmacyFilterType) {
    PharmacyFilterTypeDistance      = 0,
    PharmacyFilterTypeWorkingNow    = 1,
    PharmacyFilterTypeAvailable     = 2
};

typedef CF_ENUM (NSUInteger, PharmacyViewType) {
    PharmacyViewTypeDefault     = 0,
    PharmacyViewTypeAvailable   = 1,
    PharmacyViewTypePickup      = 2
};

@protocol NITPharmacyDelegate <NSObject>
- (void)didSelectAddress:(NITShopItemModel *)address;
@end

@protocol NITPharmacyViewProtocol
@required
@property (nonatomic, strong) id <NITPharmacyPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadTitle:(NSString *)title;
- (void)reloadCityTitle:(NSString *)title;
- (void)reloadFilterTitle:(NSString *)title;
- (void)reloadTableBySections:(NSArray <NITPharmacySectionsModel *> * )sections;
- (void)reloadCollectionByModels:(NSArray <NITShopItemModel *> *)models;
- (void)reloadMapByMarkers:(NSArray <NITShopMarker *> *)markers;
- (void)updateCameraCenter; 
@end

@protocol NITPharmacyWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (id)createNITPharmacyModule;
+ (void)presentNITPharmacyModuleFrom:(id)fromView;
+ (void)presentNITPharmacyAvailableModuleFrom:(id)fromView;
+ (void)selectAddressPickupModuleFrom:(id)fromView withDelegate:(id)delegate;
- (void)presentFeedbackModuleFrom:(id)fromView;
- (void)presentCitiesModuleFrom:(id)fromView;
- (void)closeViewController:(id)fromView;
@end

@protocol NITPharmacyPresenterProtocol
@required
@property (nonatomic, weak) id <NITPharmacyViewProtocol> view;
@property (nonatomic, strong) id <NITPharmacyInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPharmacyWireFrameProtocol> wireFrame;
@property (nonatomic, weak) id <NITPharmacyDelegate> delegate;
@property (nonatomic) PharmacyViewType viewType;
@property (nonatomic) PharmacyFilterType filterType;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)callAction;
- (void)showFeedback;
- (void)showCities;
- (void)showFilters;
- (void)selectAddress:(NITShopItemModel *)model;
- (void)selectShopModel:(NITShopItemModel *)model onMap:(HSClusterMapView *)mapView zoomed:(BOOL)zoomed;
- (void)createRoute:(NITShopItemModel *)model onMap:(HSClusterMapView *)mapView;
- (GMSCameraUpdate *)cameraUpdateByMarkers:(NSArray <GMSMarker *> *)markers;
@end

@protocol NITPharmacyInteractorOutputProtocol
@property (nonatomic) PharmacyViewType viewType;
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateShopSections:(NSArray <NITPharmacySectionsModel *> *)sections;
- (void)updateShopModels:(NSArray <NITShopItemModel *> *)models;
@end

@protocol NITPharmacyInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITPharmacyInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITPharmacyAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITPharmacyLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getPharmacySalonsWithFilterType:(PharmacyFilterType)filterType;
- (void)updateModels:(NSArray<NITShopItemModel *> *)models filterType:(PharmacyFilterType)filterType;
@end


@protocol NITPharmacyDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */

@end

@protocol NITPharmacyAPIDataManagerInputProtocol <NITPharmacyDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITPharmacyLocalDataManagerInputProtocol <NITPharmacyDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NSArray <NITPharmacyItem *> *)getPharmacies;
@end
