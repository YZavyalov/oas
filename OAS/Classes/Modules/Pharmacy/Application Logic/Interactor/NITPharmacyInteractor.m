//
//  NITPharmacyInteractor.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPharmacyInteractor.h"

@implementation NITPharmacyInteractor

#pragma mark - NITPharmacyInteractorInputProtocol
- (void)getPharmacySalonsWithFilterType:(PharmacyFilterType)filterType
{
    NSArray <NITShopItemModel *> * models = [[NITShopItemModel shopItemModelsFromPharmacyEntities:[self.localDataManager getPharmacies]] bk_select:^BOOL(NITShopItemModel *obj) {
        return (obj.remote_ia.length > 0);
    }];
    for(NITShopItemModel * model in models)
    {
        model.viewType = [self.presenter viewType];
    }
    
    [self updateModels:models filterType:filterType];
}

- (void)updateModels:(NSArray<NITShopItemModel *> *)models filterType:(PharmacyFilterType)filterType
{
    NSMutableArray * sections = [NSMutableArray new];
    
    switch (filterType)
    {
        case PharmacyFilterTypeDistance:
        {
            NSSet * distanceSet = [NSSet setWithArray:[[models bk_map:^id(NITShopItemModel *obj) {
                return @(obj.distance).stringValue;
            }] sortedArrayUsingSelector:@selector(localizedStandardCompare:)]];
            NSArray <NSString *> * distance = [distanceSet.allObjects sortedArrayUsingSelector:@selector(localizedStandardCompare:)];
    
            [sections addObjectsFromArray:[distance bk_map:^id(NSString *obj) {
                NSArray * shops = [models bk_select:^BOOL(NITShopItemModel *shop) {
                    return obj.integerValue == shop.distance;
                }];
                return [[NITPharmacySectionsModel alloc] initWithModels:shops title:[NSString stringWithFormat:@"%@ %@ %@",NSLocalizedString(@"НЕ ДАЛЕЕ", nil), obj, NSLocalizedString(@"КМ", nil)]];
            }]];
            
        } break;
            
        case PharmacyFilterTypeWorkingNow:
        {
            NSArray <NITShopItemModel *> * openedShops = [models bk_select:^BOOL(NITShopItemModel *obj) { return [obj isWorking] == true; }];
            NSArray <NITShopItemModel *> * closedShops = [models bk_select:^BOOL(NITShopItemModel *obj) { return [obj isWorking] == false; }];
            
            if (openedShops.count > 0)
            {
                NITPharmacySectionsModel * section = [[NITPharmacySectionsModel alloc] initWithModels:openedShops title:NSLocalizedString(@"РАБОТАЮТ СЕЙЧАС", nil)];
                [sections addObject:section];
            }
            
            if (closedShops.count > 0)
            {
                NITPharmacySectionsModel * section = [[NITPharmacySectionsModel alloc] initWithModels:closedShops title:NSLocalizedString(@"НЕ РАБОТАЮТ", nil)];
                [sections addObject:section];
            }
        } break;
            
        case PharmacyFilterTypeAvailable:
        {
            NSArray <NITShopItemModel *> * available = [models bk_select:^BOOL(NITShopItemModel *obj) { return [obj.is_hub isEqualToString:@"true"]; }];
            NSArray <NITShopItemModel *> * notAvailable = [models bk_select:^BOOL(NITShopItemModel *obj) { return ![obj.is_hub isEqualToString:@"true"]; }];
            if (available.count > 0)
            {
                NITPharmacySectionsModel * section = [[NITPharmacySectionsModel alloc] initWithModels:available title:NSLocalizedString(@"ЕСТЬ В НАЛИЧИИ", nil)];
                [sections addObject:section];
            }
            
            if (notAvailable.count > 0)
            {
                NITPharmacySectionsModel * section = [[NITPharmacySectionsModel alloc] initWithModels:notAvailable title:NSLocalizedString(@"НЕТ В НАЛИЧИИ", nil)];
                [sections addObject:section];
            }
        } break;
    }
        
    [self.presenter updateShopSections:sections];
    [self.presenter updateShopModels:models];
}

@end
