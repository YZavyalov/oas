//
//  NITPharmacyInteractor.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPharmacyProtocols.h"

@interface NITPharmacyInteractor : NSObject <NITPharmacyInteractorInputProtocol>

@property (nonatomic, weak) id <NITPharmacyInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITPharmacyAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITPharmacyLocalDataManagerInputProtocol> localDataManager;

@end
