//
//  NITShopMarker.h
//  OAS
//
//  Created by Eugene Parafiynyk on 6/7/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>
#import "HSClusterMarker.h"

@class NITShopItemModel;

@interface NITShopMarker : GMSMarker

@property (nonatomic) NITShopItemModel * model;

- (instancetype)initWithModel:(NITShopItemModel *)model;

@end
