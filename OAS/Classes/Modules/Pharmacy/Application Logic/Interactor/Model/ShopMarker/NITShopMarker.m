//
//  NITShopMarker.m
//  OAS
//
//  Created by Eugene Parafiynyk on 6/7/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITShopMarker.h"
#import "NITShopItemModel.h"

@implementation NITShopMarker

- (instancetype)initWithModel:(NITShopItemModel *)model
{
    if (self = [super init])
    {
        self.model = model;
    }
    
    return self;
}

#pragma mark - Custom accessors
- (void)setModel:(NITShopItemModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.position = self.model.location.coordinate;
    self.userData = self.model;
    self.icon     = !self.model.isSelected ? [UIImage imageNamed:@"marker_small"] : [UIImage imageWithImage:[UIImage imageNamed:@"map_marker"] withText:self.model.pharmacyNumber];
}

- (UIImage *)icon
{
    if (self.model)
    {
        return !self.model.isSelected ? [UIImage imageNamed:@"marker_small"] : [UIImage imageWithImage:[UIImage imageNamed:@"map_marker"] withText:self.model.pharmacyNumber];
    }
    
    return nil;
}

@end
