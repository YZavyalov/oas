//
//  NITShopItemModel.h
//  OAS
//
//  Created by Yaroslav on 30.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SLShopsResponse.h"
#import "SLIPropertyValue.h"

@class SLShop, SLIPropertyValue, NITPharmacyItem;

@interface NITShopItemModel : NITBaseModel

//main
@property (nonatomic) NSString * address;
@property (nonatomic) NSString * groupID;
@property (nonatomic) CLLocation * location;
//properties
@property (nonatomic) NSString * closing_time;
@property (nonatomic) NSString * opening_time;
@property (nonatomic) NSString * is_hub;
@property (nonatomic) NSString * remote_ia;

//Custom
@property (nonatomic) BOOL isWorking;
@property (nonatomic) BOOL isSelected;
@property (nonatomic) NSString * pharmacyNumber;
@property (nonatomic) NSString * storeTime;
@property (nonatomic) NSUInteger distance;
@property (nonatomic) NSInteger viewType;
@property (nonatomic) BOOL isFull;

- (instancetype)initWithModel:(SLShop *)model;
- (instancetype)initWithModel:(NITShopItemModel *)model withProperties:(NSArray<SLIPropertyValue *> *)properties;
- (instancetype)initWithPharmacyModel:(NITPharmacyItem *)model;
+ (NSArray <NITShopItemModel *> *)shopItemModelsFromEntities:(NSArray <SLShop *> *)entities;
+ (NSArray <NITShopItemModel *> *)shopItemModelsFromPharmacyEntities:(NSArray <NITPharmacyItem *> *)entities;

- (void)loadDetailsWithHandler:(void(^)(NITShopItemModel * model))handler;

@end
