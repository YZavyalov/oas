//
//  NITShopItemModel.m
//  OAS
//
//  Created by Yaroslav on 30.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITShopItemModel.h"
#import "SLShopsApi.h"
#import "NITPharmacyItem.h"

@implementation NITShopItemModel

#pragma mark - Life cycle
- (instancetype)initWithModel:(SLShop *)model
{
    if(self = [super init])
    {
        self.identifier = model._id;
        self.name       = model.name;
        self.address    = model.address;
        self.groupID    = model.group;
        self.location   = [[CLLocation alloc] initWithLatitude:[model.location.lat doubleValue] longitude:[model.location.lon doubleValue]];
    }
    
    return self;
}

- (instancetype)initWithModel:(NITShopItemModel *)model withProperties:(NSArray<SLIPropertyValue *> *)properties
{
    if (self = [super init])
    {
        self.identifier = model.identifier;
        self.name       = model.name;
        self.address    = model.address;
        self.groupID    = model.groupID;
        self.location   = model.location;
        self.isFull     = true;
        
        [properties bk_each:^(SLIPropertyValue * p) {
            
            if ([p.type isEqualToString:@"ipString"] && [self.allPropertyNames containsObject:p.key])
            {
                id value = p.value.firstObject ? : @"";
                [self setValue:value forKey:p.key];
            }
        }];
    }
    
    return self;
}

- (instancetype)initWithPharmacyModel:(NITPharmacyItem *)model
{
    if(self = [super init])
    {
        self.identifier     = model.identifier;
        self.name           = model.name;
        self.address        = model.address;
        self.groupID        = model.groupID;
        self.location       = [[CLLocation alloc] initWithLatitude:[model.locationLat doubleValue] longitude:[model.locationLon doubleValue]];
        self.isFull         = true;
        self.opening_time   = model.opening_time;
        self.closing_time   = model.closing_time;
        self.is_hub         = model.is_hub;
        self.remote_ia      = model.remote_ia;
    }
    
    return self;
}

#pragma mark - Custom accessors
- (NSUInteger)distance
{
    if([CLLocationManager locationServicesEnabled] &&  [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
    {
        return ([LOCATION_MANAGER.currentLocation distanceFromLocation:self.location] / 1000) + 1;
    }
    else
    {
        CLLocation * location = [[CLLocation alloc] initWithLatitude:USER.cityLat longitude:USER.cityLon];
        return ([location distanceFromLocation:self.location] / 1000) + 1;
    }
}

- (NSString *)storeTime
{
    return  self.opening_time && self.closing_time ? [NSString stringWithFormat:@"%@-%@", self.opening_time, self.closing_time] : NSLocalizedString(@"Время работы", nil);
}

- (NSString *)pharmacyNumber
{
    NSArray <NSString *> * splitString = [self.name componentsSeparatedByString:@" "];
    if([splitString objectAtIndex:2].length > 0)
    {
        return [NSString stringWithFormat:@"№%@", [splitString objectAtIndex:2]];
    }
    else
    {
        return nil;
    }
}

- (BOOL)isWorking
{
    NSInteger currentHour = [NSDate date].hour;
    if([self.opening_time integerValue] <= currentHour && [self.closing_time integerValue] > currentHour)
    {
        return true;
    }
    else
    {
        return false;
    }
}

#pragma mark - Public
+ (NSArray <NITShopItemModel *> *)shopItemModelsFromEntities:(NSArray <SLShop *> *)entities
{
    return [entities bk_map:^id(SLShop * obj) {
        return [[NITShopItemModel alloc] initWithModel:obj];
    }];
}

+ (NSArray <NITShopItemModel *> *)shopItemModelsFromPharmacyEntities:(NSArray<NITPharmacyItem *> *)entities
{
    return [entities bk_map:^id(NITPharmacyItem * obj) {
        return [[NITShopItemModel alloc] initWithPharmacyModel:obj];
    }];
}

- (void)loadDetailsWithHandler:(void(^)(NITShopItemModel * model))handler
{
    if (!self.isFull)
    {
        weaken(self);
        /*[[SLShopsApi new] getStaticShopsItemsByItemidWithXCid:USER.company xToken:USER.accessToken itemId:self.identifier completionHandler:^(SLShopWithRelations *output, NSError *error) {
            if (error)
            {
                [HELPER logError:error method:METHOD_NAME];
            }
            else
            {
                [output.properties bk_each:^(SLIPropertyValue * p) {
                    
                    if ([p.type isEqualToString:@"ipString"] && [weakSelf.allPropertyNames containsObject:p.key])
                    {
                        id value = p.value.firstObject ? : @"";
                        [weakSelf setValue:value forKey:p.key];
                    }
                }];
                
                weakSelf.isFull = true;
            }
            
            if (handler) handler (weakSelf);
        }];*/
        [[SLShopsApi new] getStaticShopsItemsByItemidWithItemId:self.identifier completionHandler:^(SLShopWithRelations *output, NSError *error) {
            if (error)
            {
                [HELPER logError:error method:METHOD_NAME];
            }
            else
            {
                [output.properties bk_each:^(SLIPropertyValue * p) {
                    
                    if ([p.type isEqualToString:@"ipString"] && [weakSelf.allPropertyNames containsObject:p.key])
                    {
                        id value = p.value.firstObject ? : @"";
                        [weakSelf setValue:value forKey:p.key];
                    }
                }];
                
                weakSelf.isFull = true;
            }
            
            if (handler) handler (weakSelf);
        }];
    }
    else
    {
        if (handler) handler (self);
    }
}

@end
