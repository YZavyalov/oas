//
//  NITPharmacySectionsModel.m
//  OAS
//
//  Created by Yaroslav on 31.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPharmacySectionsModel.h"
#import "NITPharmacyTableViewCell.h"
#import "NITBaseHeaderView.h"
#import "NITPharmacyInteractor.h"

@implementation NITPharmacySectionsModel

#pragma mark - Life cycle
- (instancetype)initWithModels:(NSArray<NITShopItemModel *> *)models title:(NSString *)title
{
    if (self = [super init])
    {
        self.models = models;
        self.title = title;
    }
    
    return self;
}

#pragma mark - Custom accessors
- (NSString *)cellID
{
    return _s(NITPharmacyTableViewCell);
}

- (CGFloat)sectionHeight
{
    return 36;
}

- (CGFloat)cellHeight
{
    switch ([self.models.firstObject viewType])
    {
        case PharmacyViewTypeDefault:   return 65;
        case PharmacyViewTypeAvailable: return 90;
        case PharmacyViewTypePickup:    return 90;
    }

    return 65;
}

- (UIView *)headerView
{
    return [NITBaseHeaderView viewWithTitle:self.title];
}

@end
