//
//  NITPharmacySectionsModel.h
//  OAS
//
//  Created by Yaroslav on 31.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITShopItemModel.h"

@class NITShopItemModel;

@interface NITPharmacySectionsModel : NSObject

@property (nonatomic) NSString * title;
@property (nonatomic) NSArray <NITShopItemModel *> * models;
@property (nonatomic) NSString * cellID;
@property (nonatomic) CGFloat sectionHeight;
@property (nonatomic) CGFloat cellHeight;
@property (nonatomic) UIView * headerView;

- (instancetype)initWithModels:(NSArray <NITShopItemModel *> *)models title:(NSString *)title;

@end
