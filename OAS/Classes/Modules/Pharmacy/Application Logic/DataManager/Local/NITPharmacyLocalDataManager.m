//
//  NITPharmacyLocalDataManager.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPharmacyLocalDataManager.h"

@implementation NITPharmacyLocalDataManager

#pragma mark - NITPharmacyDataManagerInputProtocol
- (NSArray <NITPharmacyItem *> *)getPharmacies
{
    return [NITPharmacyItem allPharmacyItems];
}

@end
