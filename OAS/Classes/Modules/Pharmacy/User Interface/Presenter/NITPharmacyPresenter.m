//
//  NITPharmacyPresenter.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPharmacyPresenter.h"
#import "NITPharmacyWireframe.h"
#import "NITMobileConstantsModel.h"

@interface NITPharmacyPresenter()

@property (nonatomic) GMSPolyline * rectangle;
@property (nonatomic) BOOL isShowRoute;

@property (nonatomic) NSArray <NITShopItemModel *> * models;
@property (nonatomic) NSMutableArray <NITPharmacySectionsModel *> * sections;

@property (nonatomic) CGRect selectedFrame;

@end

@implementation NITPharmacyPresenter

#pragma mark - NITPharmacyPresenterProtocol
- (void)initData
{
    [self addObservers];
    [self updateViewTitle];
    [self updateCity];
    [self updatePharmacies];
    [self.view reloadFilterTitle:[self filterTitle]];
}

// Action
- (void)callAction
{
    NSString * phone = [[NITMobileConstantsModel new] contact_phone];
    [HELPER callToPhoneNumber:phone];
}

- (void)showFeedback
{
    [self.wireFrame presentFeedbackModuleFrom:self.view];
}

- (void)showCities
{
    [self.wireFrame presentCitiesModuleFrom:self.view];
}

- (void)selectAddress:(NITShopItemModel *)model
{
    if ([self.delegate respondsToSelector:@selector(didSelectAddress:)])
    {
        [self.delegate didSelectAddress:model];
        [self.wireFrame closeViewController:self.view];
    }
}

- (void)showFilters
{
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{ }];
    
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"По расстоянию", nil) actionBlock:^{
        self.filterType = PharmacyFilterTypeDistance;
        [self.view reloadFilterTitle:[self filterTitle]];
        [self.interactor updateModels:self.models filterType:self.filterType];
    }];
    
    NITActionButton * action2;
    switch (self.viewType) {
        case PharmacyViewTypeDefault:
        case PharmacyViewTypePickup:
        {
            action2 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Работает сейчас", nil) actionBlock:^{
                self.filterType = PharmacyFilterTypeWorkingNow;
                [self.view reloadFilterTitle:[self filterTitle]];
                [self.interactor updateModels:self.models filterType:self.filterType];
            }];
        } break;
            
        case PharmacyViewTypeAvailable:
        {
            action2 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"По наличию", nil) actionBlock:^{
                self.filterType = PharmacyFilterTypeAvailable;
                [self.view reloadFilterTitle:[self filterTitle]];
                [self.interactor updateModels:self.models filterType:self.filterType];
            }];
        } break;
    }
    
    [HELPER showActionSheetFromView:self.view withTitle:NSLocalizedString(@"Сортировать аптеки", nil) actionButtons:@[action1, action2] cancelButton:cancel];
}

- (void)selectShopModel:(NITShopItemModel *)model onMap:(HSClusterMapView *)mapView zoomed:(BOOL)zoomed
{
    NITShopItemModel * selectedModel = [self.models bk_select:^BOOL(NITShopItemModel *obj) {
        return obj.isSelected;
    }].firstObject;
    
    if (selectedModel && [selectedModel.identifier isEqualToString:model.identifier])
    {
        return;
    }
    
    [self.models bk_each:^(NITShopItemModel *obj) {
        obj.isSelected = model && [obj.identifier isEqualToString:model.identifier];
    }];

    if (model)
    {
        [self moveCameraToLocation:model.location forMap:mapView zoomed:zoomed];
    }
    
    NSArray * markers = [self.models bk_map:^id(NITShopItemModel *obj) {
        return [[NITShopMarker alloc] initWithModel:obj];
    }];
    
    [self.view reloadMapByMarkers:markers];
}

// Map
- (GMSCameraUpdate *)cameraUpdateByMarkers:(NSArray <GMSMarker *> *)markers
{
    GMSMutablePath * rect = [GMSMutablePath path];
    [markers bk_each:^(GMSMarker *obj) {
        [rect addCoordinate:obj.position];
    }];

    GMSCameraUpdate * update;
    
    if (rect.count == 1)
    {
        CLLocationCoordinate2D coordinate = [rect coordinateAtIndex:0];
        GMSCameraPosition * camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude
                                                                 longitude:coordinate.longitude
                                                                      zoom:15];
        update = [GMSCameraUpdate setCamera:camera];
    }
    else
    {
        GMSPolyline * polyline = [GMSPolyline polylineWithPath:rect];
        GMSCoordinateBounds * bounds = [[GMSCoordinateBounds alloc] initWithPath:polyline.path];
        update = [GMSCameraUpdate fitBounds:bounds withPadding:15];
    }
    
    return update;
}

- (void)createRoute:(NITShopItemModel *)model onMap:(HSClusterMapView *)mapView
{
    if ([LOCATION_MANAGER currentLocation])
    {
        NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
        
        NSMutableArray * actions = [NSMutableArray new];
        
        NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"В приложении", nil) actionBlock:^{
            [self showRouteWithMap:mapView withModel:model];
        }];
        
        [actions addObject:action1];
        
        // apple
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"http://maps.apple.com/"]])
        {
            NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Карты Apple", nil) actionBlock:^{
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f",
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.latitude,
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.longitude,
                                                                                 model.location.coordinate.latitude,
                                                                                 model.location.coordinate.longitude]]];
            }];
            
            [actions addObject:action2];
        }
        
        // google
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]])
        {
            NITActionButton * action3 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Google карты", nil) actionBlock:^{
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f",
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.latitude,
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.longitude,
                                                                                 model.location.coordinate.latitude,
                                                                                 model.location.coordinate.longitude]]];
            }];
            
            [actions addObject:action3];
        }
        
        // yandex
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"yandexmaps://maps.yandex.ru/"]])
        {
            NITActionButton * action4 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Яндекс.Карты", nil) actionBlock:^{
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"yandexmaps://build_route_on_map/?lat_from=%f&lon_from=%f&lat_to=%f&lon_to=%f",
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.latitude,
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.longitude,
                                                                                 model.location.coordinate.latitude,
                                                                                 model.location.coordinate.longitude]]];
            }];
            
            [actions addObject:action4];
        }
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"dgis://"]])
        {
            NITActionButton * action5 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"2ГИС", nil) actionBlock:^{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"dgis://2gis.ru/routeSearch/rsType/%@/from/%f,%f/to/%f,%f",
                                                                                 @"car",
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.latitude,
                                                                                 [LOCATION_MANAGER currentLocation].coordinate.longitude,
                                                                                 model.location.coordinate.latitude,
                                                                                 model.location.coordinate.longitude]]];
            }];
            
            [actions addObject:action5];
        }
        
        //Uber
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"uber://"]])
        {
            NITActionButton * action6 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Uber", nil) actionBlock:^{
                NSString *getUberDetails=[NSString stringWithFormat:@"client_id=vbtCwkGyqvDupQhpCcgCMo4VE-gIRw42&action=setPickup&pickup[latitude]=%f&pickup[longitude]=%f&pickup[nickname]=OuttAPP&dropoff[latitude]=%f&dropoff[longitude]=%f&dropoff[nickname]=%@&product_id=%@",[LOCATION_MANAGER currentLocation].coordinate.latitude, [LOCATION_MANAGER currentLocation].coordinate.longitude, model.location.coordinate.latitude, model.location.coordinate.longitude, model.address, nil/*[[huberTableArr objectAtIndex:indexPath.row]objectForKey:@"productID"]*/];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"uber://?%@", getUberDetails]]];
            }];
            
            [actions addObject:action6];
        }
        
        if (actions.count == 1)
        {
            [self showRouteWithMap:mapView withModel:model];
            [ERROR_MESSAGE errorMessage:error_not_found_maps_app];
        }
        else
        {
            [HELPER showActionSheetFromView:self.view withTitle:nil actionButtons:actions cancelButton:cancel];
        }
    }
    else
    {
        NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
        NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Перейти к настройкам?", nil) actionBlock:^{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
        
        [HELPER showActionSheetFromView:self.view withTitle:NSLocalizedString(@"Для проложения маршрута необходимо включить доступ к геолокации!", nil) actionButtons:@[action1] cancelButton:cancel];
    }
}

#pragma mark - NITPharmacyInteractorOutputProtocol
- (void)updateShopSections:(NSArray<NITPharmacySectionsModel *> *)sections
{
    [self.view reloadTableBySections:sections];
}

- (void)updateShopModels:(NSArray <NITShopItemModel *> *)models
{
    [HELPER stopLoading];
    
    self.models = models;
    NSArray * markers = [models bk_map:^id(NITShopItemModel *obj) {
        return [[NITShopMarker alloc] initWithModel:obj];
    }];
    
    [self.view reloadMapByMarkers:markers];
    [self.view reloadCollectionByModels:models];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePharmacies) name:kUpdateCityNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCity) name:kChangeCityNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePharmacies) name:kUpdatePharmacyNotification object:nil];
}

- (void)updateCity
{
    [HELPER startLoading];
    [self.view reloadCityTitle:USER.cityName];
}

- (void)updatePharmacies
{
    [HELPER startLoading];
    [self.interactor getPharmacySalonsWithFilterType:self.filterType];
}

- (void)updateViewTitle
{
    NSString * title;
    switch (self.viewType) {
        case PharmacyViewTypeDefault:   title = NSLocalizedString(@"Аптеки", nil);      break;
        case PharmacyViewTypeAvailable: title = NSLocalizedString(@"Наличие", nil);     break;
        case PharmacyViewTypePickup:    title = NSLocalizedString(@"Самовывоз", nil);   break;
    }
    
    [self.view reloadTitle:title];
}

- (NSString *)filterTitle
{
    switch (self.filterType) {
        case PharmacyFilterTypeDistance:    return NSLocalizedString(@"По расстоянию", nil);
        case PharmacyFilterTypeWorkingNow:  return NSLocalizedString(@"Работает сейчас", nil);
        case PharmacyFilterTypeAvailable:   return NSLocalizedString(@"По наличию", nil);
    }
}

// Map
- (void)moveCameraToLocation:(CLLocation *)location forMap:(HSClusterMapView *)mapView zoomed:(BOOL)zoomed
{
    self.isShowRoute = false;
    GMSCameraPosition * cameraPosition = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                                     longitude:location.coordinate.longitude
                                                                          zoom:zoomed ? 13 : mapView.camera.zoom];
    
    [mapView animateToCameraPosition:cameraPosition];
    [self.view updateCameraCenter]; 
}

- (void)showRouteWithMap:(HSClusterMapView *)mapView withModel:(NITShopItemModel *)model
{
    weaken(self);
    [LOCATION_MANAGER googleFetchPolylineToDestination:model.location completionHandler:^(GMSPolyline * rectangle) {
        weakSelf.rectangle.map = nil;
        weakSelf.rectangle = rectangle;
        weakSelf.rectangle.strokeWidth = 2.f;
        weakSelf.rectangle.strokeColor = RGB(10, 88, 43);
        weakSelf.rectangle.map = mapView;
        weakSelf.isShowRoute = true;
        
        GMSCoordinateBounds * bounds = [[GMSCoordinateBounds alloc] initWithPath:self.rectangle.path];
        GMSCameraUpdate * update = [GMSCameraUpdate fitBounds:bounds withPadding:15.5];
        [mapView moveCamera:update];
    }];
}

@end
