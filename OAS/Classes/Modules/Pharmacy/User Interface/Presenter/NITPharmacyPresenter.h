//
//  NITPharmacyPresenter.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPharmacyProtocols.h"

@class NITPharmacyWireFrame;

@interface NITPharmacyPresenter : NITRootPresenter <NITPharmacyPresenterProtocol, NITPharmacyInteractorOutputProtocol>

@property (nonatomic, weak) id <NITPharmacyViewProtocol> view;
@property (nonatomic, strong) id <NITPharmacyInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPharmacyWireFrameProtocol> wireFrame;
@property (nonatomic, weak) id <NITPharmacyDelegate> delegate;

@property (nonatomic) PharmacyViewType viewType;
@property (nonatomic) PharmacyFilterType filterType;

@end
