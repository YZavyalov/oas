//
//  NITPharmacyViewController.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPharmacyViewController.h"
#import "HSDemoClusterRenderer.h"
#import "NITPharmacyTableViewCell.h"
#import "NITPharmacyCardCollectionViewCell.h"
#import "NITIndicator.h"
#import "CardLayout.h"
#import "NITGuideMapCardsView.h"

@interface NITPharmacyViewController()
<
CLLocationManagerDelegate,
UITableViewDelegate,
UITableViewDataSource,
UICollectionViewDataSource_Draggable,
UICollectionViewDelegate_Draggable,
NITPharmacyCardCollectionViewCellDelegate,
NITIndicatorDelegate
>

@property (nonatomic) IBOutlet UIBarButtonItem * backButton;
@property (nonatomic) IBOutlet NITIndicator * segmentControl;
@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UICollectionView * collectionView;
@property (nonatomic) IBOutlet UILabel * filterLbl;
@property (nonatomic) IBOutlet UILabel * cityLbl;
@property (nonatomic) IBOutlet UIButton * cityBtn;
@property (nonatomic) IBOutlet NSLayoutConstraint * filterLblW;
@property (nonatomic) IBOutlet NSLayoutConstraint * listSectionCenter;
@property (nonatomic) IBOutlet NSLayoutConstraint * listSectionBottom;
@property (nonatomic) IBOutlet NSLayoutConstraint * collectionViewBottom;

@property (nonatomic) NSArray <NITPharmacySectionsModel *> * sections;
@property (nonatomic) NSArray <NITShopItemModel *> * models;

@property (nonatomic, assign) CGPoint scrollingPoint, endPoint;
@property (nonatomic, strong) NSTimer *scrollingTimer;

@end

@implementation NITPharmacyViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter initData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateTabbarVisibilityAnimated:false];
}

- (void)dealloc
{
    [self.collectionView cardLayoutCleanup];
}

#pragma mark - IBAction
- (IBAction)callAction:(id)sender
{
    [self.presenter callAction];
}

- (IBAction)feedbackAction:(id)sender
{
    [self.presenter showFeedback];
}

- (IBAction)filterAction:(id)sender
{
    [self.presenter showFilters];
}

- (IBAction)cityAction:(id)sender
{
    [self.presenter showCities];
}

#pragma mark - Private
- (void)initUI
{    
    self.segmentControl.delegate = self;
    [self.tableView setUserInteractionEnabled:true];
    
    UISwipeGestureRecognizer *swipeFromLeftToRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [swipeFromLeftToRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.tableView addGestureRecognizer:swipeFromLeftToRight];

    switch (self.presenter.viewType) {
        case PharmacyViewTypeDefault: {
            self.backButton.enabled = false;
            self.backButton.image = nil;
        } break;
        case PharmacyViewTypeAvailable:
        {
            self.navigationItem.leftBarButtonItem = nil;
            self.navigationItem.rightBarButtonItem = nil;
        } break;
        case PharmacyViewTypePickup: {
            self.navigationItem.leftBarButtonItem = nil;
            self.navigationItem.rightBarButtonItem = nil;
            self.cityLbl.hidden = true;
            [self.cityBtn setEnabled:false];
        } break;
    }
    
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView registerNibArray:@[_s(NITPharmacyTableViewCell)]];
    [self.collectionView registerNibArray:@[_s(NITPharmacyCardCollectionViewCell)]];
}

- (void)updateTabbarVisibilityAnimated:(BOOL)animated
{
    if(self.presenter.viewType != PharmacyViewTypePickup)
    {
        switch (self.segmentControl.pharmacyType) {
            case PharmacyTypeMap: {
                [self.tabBarController hideTabBarAnimated:animated];
                self.listSectionBottom.constant = - 49;
            } break;
            case PharmacyTypeAddress: {
                [self.tabBarController showTabBarAnimated:animated];
                self.listSectionBottom.constant = 0;
            } break;
        }
    }
    else
    {
        [self.tabBarController hideTabBarAnimated:false];
        self.listSectionBottom.constant = -49;
        self.collectionViewBottom.constant = 49;
    }
}

- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight)
    {
        self.segmentControl.pharmacyType = PharmacyTypeMap;
    }
}

- (void)selectCollectionCardByModel:(NITShopItemModel *)model
{
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:[self.models indexOfObject:model] inSection:0];
    [self.collectionView showCardsWithComplition:^{
        [self.collectionView selectAndNotifyDelegate:indexPath];
    }];
}

#pragma mark - Map
- (void)didSelectMarker:(NITShopMarker *)marker
{
    [self selectCollectionCardByModel:marker.model];
}

#pragma mark - Guide
- (void)showMapCardsGuideIfNeed
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if ([NITGuideMapCardsView notYetShown] && [WINDOW.visibleController isEqual:self])
        {
            [NITGuideMapCardsView showWidthOverlayCollectionView:self.collectionView];
        }
    });
}

#pragma mark - NITPharmacyPresenterProtocol
- (void)reloadTitle:(NSString *)title
{
    self.title = title;
}

- (void)reloadCityTitle:(NSString *)title
{
    self.cityLbl.text = title;
}

- (void)reloadFilterTitle:(NSString *)title
{
    self.filterLbl.text = title;
    self.filterLblW.constant = [self.filterLbl sizeOfMultiLineLabel].width;
}

- (void)reloadTableBySections:(NSArray <NITPharmacySectionsModel *> *)sections
{
    [HELPER stopLoading];
    self.sections = sections;
    [self.tableView reloadData];
}

- (void)reloadCollectionByModels:(NSArray<NITShopItemModel *> *)models
{
    self.models = models;
    [self.collectionView reloadData];
    [self.mapView moveCamera:[self.presenter cameraUpdateByMarkers:self.mapView.unclusteredMarkers]];
}

- (void)reloadMapByMarkers:(NSArray<NITShopMarker *> *)markers
{
    [self.mapView clear];
    [self.mapView addMarkers:markers];
    [self.mapView cluster];
}

- (void)updateCameraCenter
{
    self.fixCenterPosition = true;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.sections[section].models.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return self.sections[section].sectionHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.sections[indexPath.section].cellHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.sections[section].headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITPharmacyTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITPharmacyTableViewCell) forIndexPath:indexPath];
    cell.model = self.sections[indexPath.section].models[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.segmentControl.pharmacyType = PharmacyTypeMap;
    [self selectCollectionCardByModel:self.sections[indexPath.section].models[indexPath.row]];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.models.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NITPharmacyCardCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:_s(NITPharmacyCardCollectionViewCell) forIndexPath:indexPath];
    [cell setModel:self.models[indexPath.row] delegate:self viewType:self.presenter.viewType];
    [cell.layer setMasksToBounds:NO];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    weaken(self);
    [self.collectionView setViewMode:MTCardLayoutViewModePresenting animated:true completion:^(BOOL success) {
        [weakSelf showMapCardsGuideIfNeed];
    }];
    [self.presenter selectShopModel:self.models[indexPath.row] onMap:self.mapView zoomed:true];
}

- (void)collectionView:(UITableView *)collectionView didDeselectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    [self.presenter selectShopModel:nil onMap:self.mapView zoomed:false];
}

#pragma mark - NITPharmacyCardCollectionView
- (void)didSelectModel:(NITShopItemModel *)model
{
    [self.presenter selectAddress:model];
}

- (void)didSelectRoute:(NITShopItemModel *)model
{
    [self.collectionView hideCards];
    [self.presenter createRoute:model onMap:self.mapView];
}

#pragma mark - NITIndicatorDelegate
- (void)didChangeSectionType:(PharmacyType)type
{
    self.listSectionCenter.constant = type == PharmacyTypeMap ? ViewWidth(self.view) : 0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self updateTabbarVisibilityAnimated:true];
    }];
}

@end
