//
//  NITPharmacyTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 18.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPharmacyTableViewCell.h"
#import "NITShopItemModel.h"
#import "NITMobileConstantsModel.h"

@interface NITPharmacyTableViewCell()

@property (nonatomic) IBOutlet UILabel * addressLbl;
@property (nonatomic) IBOutlet UILabel * timeLbl;
@property (nonatomic) IBOutlet UILabel * deliveryTimeLbl;

@end

#pragma mark - Custom accessors
@implementation NITPharmacyTableViewCell

- (void)setModel:(NITShopItemModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.addressLbl.text = self.model.address;
    self.timeLbl.text = self.model.storeTime;
    self.deliveryTimeLbl.text = [[NITMobileConstantsModel new] deliveryStringByModel:self.model];
}

@end
