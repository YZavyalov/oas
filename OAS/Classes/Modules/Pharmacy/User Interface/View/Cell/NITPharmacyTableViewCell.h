//
//  NITPharmacyTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 18.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NITShopItemModel;

@interface NITPharmacyTableViewCell : UITableViewCell

@property (nonatomic) NITShopItemModel * model;

@end
