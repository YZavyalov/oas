//
//  NITPharmacyCardCollectionViewCell.h
//  OAS
//
//  Created by Yaroslav on 14.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITShopItemModel.h"
#import "NITPharmacyProtocols.h"

@protocol NITPharmacyCardCollectionViewCellDelegate <NSObject>
@optional
- (void)didSelectRoute:(NITShopItemModel *)model;
- (void)didSelectModel:(NITShopItemModel *)model;
@end

@interface NITPharmacyCardCollectionViewCell : UICollectionViewCell

@property (nonatomic) NITShopItemModel * model;
@property (nonatomic) PharmacyViewType viewType;
@property (nonatomic, weak) id <NITPharmacyCardCollectionViewCellDelegate> delegate;

- (void)setModel:(id)model delegate:(id)delegate viewType:(PharmacyViewType)viewType;

@end
