//
//  NITPharmacyCardCollectionViewCell.m
//  OAS
//
//  Created by Yaroslav on 14.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPharmacyCardCollectionViewCell.h"
#import "NITMobileConstantsModel.h"

@interface NITPharmacyCardCollectionViewCell()

@property (nonatomic) IBOutlet UILabel * titleLbl;
@property (nonatomic) IBOutlet UILabel * addressLbl;
@property (nonatomic) IBOutlet UILabel * workTimeLbl;
@property (nonatomic) IBOutlet UILabel * deliveryTimeLbl;
@property (nonatomic) IBOutlet UIButton * actionBtn;

@end

@implementation NITPharmacyCardCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self initData];
}

#pragma mark - Custom accessors
- (void)setModel:(NITShopItemModel *)model
{
    _model = model;
    [self updateData];
}

- (void)setViewType:(PharmacyViewType)viewType
{
    _viewType = viewType;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)selectAction:(id)sender
{
    switch (self.model.viewType) {
        case PharmacyViewTypeAvailable:
        case PharmacyViewTypeDefault: {
            if ([self.delegate respondsToSelector:@selector(didSelectRoute:)])
            {
                [self.delegate didSelectRoute:self.model];
            }
        } break;
            
        case PharmacyViewTypePickup: {
            if ([self.delegate respondsToSelector:@selector(didSelectModel:)])
            {
                [self.delegate didSelectModel:self.model];
            }
        } break;
    }
}

#pragma mark - Public
- (void)setModel:(NITShopItemModel *)model delegate:(id)delegate viewType:(PharmacyViewType)viewType
{
    self.model = model;
    self.delegate = delegate;
    self.viewType = viewType;
}

#pragma mark - Private
- (void)initData
{
    CALayer *layer = [self layer];
    [layer setMasksToBounds:NO];
    [layer setRasterizationScale:[[UIScreen mainScreen] scale]];
    [layer setShouldRasterize:NO];
    [layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
    [layer setShadowOffset:CGSizeMake(0.0f,0.2f)];
    [layer setShadowRadius:2.0f];
    [layer setShadowOpacity:0.2f];
    [layer setShadowPath:[[UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, ViewWidth(self) + 30, ViewHeight(self)) cornerRadius:layer.cornerRadius] CGPath]];
}

- (void)updateData
{
    self.titleLbl.text = self.model.name;
    self.addressLbl.text = self.model.address;
    self.workTimeLbl.text = self.model.storeTime;
    self.deliveryTimeLbl.text = [[NITMobileConstantsModel new] deliveryStringByModel:self.model];
    self.deliveryTimeLbl.hidden = self.viewType == PharmacyViewTypeDefault ? true : false;
    
    NSString * buttonTitle;
    switch (self.model.viewType) {
        case PharmacyViewTypeAvailable:
        case PharmacyViewTypeDefault:   buttonTitle = NSLocalizedString(@"Проложить маршрут", nil); break;
        case PharmacyViewTypePickup:    buttonTitle = NSLocalizedString(@"Выбрать", nil);           break;
    }
    
    [self.actionBtn setTitle:buttonTitle forState:UIControlStateNormal];
}

@end
