//
//  NITIndicator.h
//  OAS
//
//  Created by Yaroslav on 01.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPharmacyProtocols.h"
#import "XibView.h"

@protocol NITIndicatorDelegate <NSObject>
- (void)didChangeSectionType:(PharmacyType)type;
@end

@interface NITIndicator : XibView

@property (nonatomic) PharmacyType pharmacyType;
@property (nonatomic, weak) id <NITIndicatorDelegate> delegate;

@end
