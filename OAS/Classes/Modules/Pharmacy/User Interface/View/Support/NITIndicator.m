//
//  NITIndicator.m
//  OAS
//
//  Created by Yaroslav on 01.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITIndicator.h"

@interface NITIndicator()

@property (nonatomic) IBOutlet UIButton * listBtn;
@property (nonatomic) IBOutlet UIButton * mapBtn;
@property (nonatomic) IBOutlet UIView * selectUnderline;
@property (nonatomic) IBOutlet NSLayoutConstraint * selectUnderlineLeading;

@end

@implementation NITIndicator

#pragma mark - Life cycle
- (void)awakeFromNib
{
    [super awakeFromNib];
}

#pragma mark - IBAction
- (IBAction)changeSectionAction:(UIButton *)sender
{
    self.pharmacyType = sender.tag;
}

#pragma mark - Custom accessors
- (void)setPharmacyType:(PharmacyType)pharmacyType
{
    BOOL changed = _pharmacyType != pharmacyType;
    
    _pharmacyType = pharmacyType;
    [self updateData];
    
    if (changed && [self.delegate respondsToSelector:@selector(didChangeSectionType:)])
    {
        [self.delegate didChangeSectionType:self.pharmacyType];
    }
}

#pragma mark - Private
- (void)updateData
{
    [self.listBtn setTitleColor:self.pharmacyType == PharmacyTypeAddress ? RGB(58, 160, 144) : RGB(0, 0, 0) forState:UIControlStateNormal];
    [self.mapBtn  setTitleColor:self.pharmacyType == PharmacyTypeAddress ? RGB(0, 0, 0) : RGB(58, 160, 144) forState:UIControlStateNormal];

    self.selectUnderlineLeading.constant = self.pharmacyType == PharmacyTypeAddress ? 0 : ViewWidth(self) * 0.5;
    [UIView animateWithDuration:0.5 animations:^{
        [self layoutIfNeeded];
    }];
}

@end
