//
//  NITPharmacyViewController.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITGMSMapViewController.h"
#import "NITPharmacyProtocols.h"

@interface NITPharmacyViewController : NITGMSMapViewController <NITPharmacyViewProtocol>

@property (nonatomic, strong) id <NITPharmacyPresenterProtocol> presenter;

@end
