//
//  NITPharmacyWireFrame.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPharmacyProtocols.h"
#import "NITPharmacyViewController.h"
#import "NITPharmacyLocalDataManager.h"
#import "NITPharmacyAPIDataManager.h"
#import "NITPharmacyInteractor.h"
#import "NITPharmacyPresenter.h"
#import "NITPharmacyWireframe.h"
#import <UIKit/UIKit.h>

@interface NITPharmacyWireFrame : NITRootWireframe <NITPharmacyWireFrameProtocol>

@end
