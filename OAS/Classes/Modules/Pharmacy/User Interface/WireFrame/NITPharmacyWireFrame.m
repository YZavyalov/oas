//
//  NITPharmacyWireFrame.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPharmacyWireFrame.h"
#import "NITFeedbackWireFrame.h"
#import "NITCitiesWireFrame.h"

@implementation NITPharmacyWireFrame

#pragma mark - NITPharmacyWireFrameProtocol
+ (id)createNITPharmacyModule
{
    // Generating module components
    id <NITPharmacyPresenterProtocol, NITPharmacyInteractorOutputProtocol> presenter = [NITPharmacyPresenter new];
    id <NITPharmacyInteractorInputProtocol> interactor = [NITPharmacyInteractor new];
    id <NITPharmacyAPIDataManagerInputProtocol> APIDataManager = [NITPharmacyAPIDataManager new];
    id <NITPharmacyLocalDataManagerInputProtocol> localDataManager = [NITPharmacyLocalDataManager new];
    id <NITPharmacyWireFrameProtocol> wireFrame= [NITPharmacyWireFrame new];
    id <NITPharmacyViewProtocol> view = [(NITPharmacyWireFrame *)wireFrame createViewControllerWithKey:_s(NITPharmacyViewController) storyboardType:StoryboardTypePharmacy];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITPharmacyModuleFrom:(UIViewController *)fromViewController
{
    NITPharmacyViewController * vc = [NITPharmacyWireFrame createNITPharmacyModule];
    vc.presenter.viewType = PharmacyViewTypeDefault;
    [fromViewController.navigationController pushViewController:vc animated:true];
}

+ (void)presentNITPharmacyAvailableModuleFrom:(UIViewController *)fromViewController
{
    NITPharmacyViewController * vc = [NITPharmacyWireFrame createNITPharmacyModule];
    vc.presenter.viewType = PharmacyViewTypeAvailable;
    [fromViewController.navigationController pushViewController:vc animated:true];
}

+ (void)selectAddressPickupModuleFrom:(UIViewController *)fromViewController withDelegate:(id)delegate
{
    NITPharmacyViewController * vc = [NITPharmacyWireFrame createNITPharmacyModule];
    vc.presenter.viewType = PharmacyViewTypePickup;
    vc.presenter.delegate = delegate;
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)presentFeedbackModuleFrom:(UIViewController *)fromViewController
{
    [NITFeedbackWireFrame presentNITFeedbackModuleFrom:fromViewController];
}

- (void)presentCitiesModuleFrom:(UIViewController *)fromViewController
{
    [NITCitiesWireFrame presentNITCitiesModuleFrom:fromViewController withType:CityViewTypeSelect withHandler:^{}];
}

- (void)closeViewController:(UIViewController *)fromViewController
{
    [fromViewController.navigationController popViewControllerAnimated:false];
}

@end
