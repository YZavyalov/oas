//
//  NITRegistrationCardInteractor.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITRegistrationCardInteractor.h"

@implementation NITRegistrationCardInteractor

#pragma mark - NITRegistrationCardInteractorInputProtocol
- (void)checkDiscountCard
{
    weaken(self);
    [self.APIDataManager checkDiscountCardWithHandler:^(NSError *error) {
        [weakSelf.presenter successRegistrationCard:error ? false : true];
    }];
}

@end
