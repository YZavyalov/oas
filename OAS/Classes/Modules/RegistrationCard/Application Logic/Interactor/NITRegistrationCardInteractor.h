//
//  NITRegistrationCardInteractor.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITRegistrationCardProtocols.h"

@interface NITRegistrationCardInteractor : NSObject <NITRegistrationCardInteractorInputProtocol>

@property (nonatomic, weak) id <NITRegistrationCardInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITRegistrationCardAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITRegistrationCardLocalDataManagerInputProtocol> localDataManager;

@end
