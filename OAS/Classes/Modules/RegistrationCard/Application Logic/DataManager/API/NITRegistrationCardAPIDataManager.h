//
//  NITRegistrationCardAPIDataManager.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITRegistrationCardProtocols.h"

@interface NITRegistrationCardAPIDataManager : NSObject <NITRegistrationCardAPIDataManagerInputProtocol>

@end
