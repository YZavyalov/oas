//
//  NITRegistrationCardAPIDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITRegistrationCardAPIDataManager.h"
#import "SLCollectionsApi.h"

@implementation NITRegistrationCardAPIDataManager

#pragma mark - NITRegistrationCardDataManagerInputProtocol
- (void)checkDiscountCardWithHandler:(void(^)(NSError *error))handler
{
    [HELPER startLoading];
    SLCollectionRow * body = [SLCollectionRow new];
    NITDiscountCardModel * model = [NITDiscountCardModel new];
    body.key = @"number"; body.value = model.cardNumber;
    [[SLCollectionsApi new] postStaticCollectionByCollectionkeyTupleSelectWithCollectionKey:@"cards" body:body completionHandler:^(NSArray<NSArray<SLCollectionRow> *> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
            [ERROR_MESSAGE errorMessage:notification_error_card];
            handler (error);
        }
        if(output.count > 0)
        {
            handler (nil);
        }
        else
        {
            [ERROR_MESSAGE errorMessage:notification_card_not_find];
        }
    }];

}

@end
