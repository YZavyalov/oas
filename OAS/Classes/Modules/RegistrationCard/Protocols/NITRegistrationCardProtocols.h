//
//  NITRegistrationCardProtocols.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITDiscountCardModel.h"

@protocol NITRegistrationCardInteractorOutputProtocol;
@protocol NITRegistrationCardInteractorInputProtocol;
@protocol NITRegistrationCardViewProtocol;
@protocol NITRegistrationCardPresenterProtocol;
@protocol NITRegistrationCardLocalDataManagerInputProtocol;
@protocol NITRegistrationCardAPIDataManagerInputProtocol;

@class NITRegistrationCardWireFrame;

// Defines the public interface that something else can use to drive the user interface
@protocol NITRegistrationCardViewProtocol
@required
@property (nonatomic, strong) id <NITRegistrationCardPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)updateNextBtn:(BOOL)update;
@end

@protocol NITRegistrationCardWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITRegistrationCardModuleFrom:(id)fromViewController;
- (void)presentNITSnapPhoneFromViewController:(id)fromViewController;
- (void)presentAlertFrom:(id)fromViewController withAlert:(id)alert;
@end

@protocol NITRegistrationCardPresenterProtocol
@required
@property (nonatomic, weak) id <NITRegistrationCardViewProtocol> view;
@property (nonatomic, strong) id <NITRegistrationCardInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITRegistrationCardWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)checkCardNumber:(NSString *)cardNumber;
- (void)snapCardNumber;
@end

@protocol NITRegistrationCardInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)successRegistrationCard:(BOOL)success;
@end

@protocol NITRegistrationCardInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITRegistrationCardInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITRegistrationCardAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITRegistrationCardLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)checkDiscountCard;
@end


@protocol NITRegistrationCardDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITRegistrationCardAPIDataManagerInputProtocol <NITRegistrationCardDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)checkDiscountCardWithHandler:(void(^)(NSError *error))handler;
@end

@protocol NITRegistrationCardLocalDataManagerInputProtocol <NITRegistrationCardDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
