//
//  NITRegistrationCardWireFrame.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITRegistrationCardProtocols.h"
#import "NITRegistrationCardViewController.h"
#import "NITRegistrationCardLocalDataManager.h"
#import "NITRegistrationCardAPIDataManager.h"
#import "NITRegistrationCardInteractor.h"
#import "NITRegistrationCardPresenter.h"
#import "NITRegistrationCardWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITRegistrationCardWireFrame : NITRootWireframe <NITRegistrationCardWireFrameProtocol>

@end
