//
//  NITRegistrationCardWireFrame.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITRegistrationCardWireFrame.h"
#import "NITSnapPhoneWireFrame.h"

@implementation NITRegistrationCardWireFrame

#pragma mark - Private
+ (id)createNITRegistrationCardModule
{
    // Generating module components
    id <NITRegistrationCardPresenterProtocol, NITRegistrationCardInteractorOutputProtocol> presenter = [NITRegistrationCardPresenter new];
    id <NITRegistrationCardInteractorInputProtocol> interactor = [NITRegistrationCardInteractor new];
    id <NITRegistrationCardAPIDataManagerInputProtocol> APIDataManager = [NITRegistrationCardAPIDataManager new];
    id <NITRegistrationCardLocalDataManagerInputProtocol> localDataManager = [NITRegistrationCardLocalDataManager new];
    id <NITRegistrationCardWireFrameProtocol> wireFrame= [NITRegistrationCardWireFrame new];
    id <NITRegistrationCardViewProtocol> view = [(NITRegistrationCardWireFrame *)wireFrame createViewControllerWithKey:_s(NITRegistrationCardViewController) storyboardType:StoryboardTypeMap];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITRegistrationCardWireFrameProtocol
+ (void)presentNITRegistrationCardModuleFrom:(UIViewController *)fromViewController
{
    NITRegistrationCardViewController * vc = [NITRegistrationCardWireFrame createNITRegistrationCardModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)presentNITSnapPhoneFromViewController:(UIViewController *)fromViewController
{
    [NITSnapPhoneWireFrame presentNITSnapPhoneModuleFrom:fromViewController];
}

- (void)presentAlertFrom:(UIViewController *)fromViewController withAlert:(UIAlertController *)alert
{
    [fromViewController presentViewController:alert animated:YES completion:nil];
}

@end
