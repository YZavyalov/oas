//
//  NITRegistrationCardPresenter.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITRegistrationCardPresenter.h"
#import "NITRegistrationCardWireframe.h"

@implementation NITRegistrationCardPresenter

#pragma mark - NITRegistrationCardPresenterProtocol
- (void)snapCardNumber
{
    [self.interactor checkDiscountCard];
}

- (void)checkCardNumber:(NSString *)cardNumber
{
    [[NITDiscountCardModel new] setCardNumber:cardNumber];
    if(cardNumber.length == 10)
    {
        [self.view updateNextBtn:true];
    }
    else
    {
        [self.view updateNextBtn:false];
    }  
}

#pragma mark - NITRegistrationCardInteractorOutputProtocol
- (void)successRegistrationCard:(BOOL)success
{
    if(success)
    {
        [self.wireFrame presentNITSnapPhoneFromViewController:self.view];
    }
    else
    {
        [self showAlert];
    }
}

#pragma mark - Private

- (void)showAlert
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Такой карты не существует", nil)
                                                                    message:nil
                                                             preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = RGB(58, 160, 144);
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ок", nil)
                                                      style:UIAlertActionStyleCancel
                                                    handler:^(UIAlertAction * action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                    }];
    
    [alert addAction:cancel];
    
    [self.wireFrame presentAlertFrom:self.view withAlert:alert];
}

@end
