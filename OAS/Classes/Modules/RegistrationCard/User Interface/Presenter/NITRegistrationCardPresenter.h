//
//  NITRegistrationCardPresenter.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITRegistrationCardProtocols.h"

@class NITRegistrationCardWireFrame;

@interface NITRegistrationCardPresenter : NSObject <NITRegistrationCardPresenterProtocol, NITRegistrationCardInteractorOutputProtocol>

@property (nonatomic, weak) id <NITRegistrationCardViewProtocol> view;
@property (nonatomic, strong) id <NITRegistrationCardInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITRegistrationCardWireFrameProtocol> wireFrame;

@end
