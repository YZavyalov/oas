//
//  NITRegistrationCardViewController.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITRegistrationCardProtocols.h"
#import "NITBaseViewController.h"

@interface NITRegistrationCardViewController : NITBaseViewController <NITRegistrationCardViewProtocol>

@property (nonatomic, strong) id <NITRegistrationCardPresenterProtocol> presenter;

@end
