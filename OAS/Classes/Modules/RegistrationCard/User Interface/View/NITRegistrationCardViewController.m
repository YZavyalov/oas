//
//  NITRegistrationCardViewController.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITRegistrationCardViewController.h"
#import "AKNumericFormatter.h"
#import "UITextField+AKNumericFormatter.h"

static NSString * formatterCodeMask = @"**********";

@interface NITRegistrationCardViewController()
<
UITextFieldDelegate
>

@property (nonatomic) IBOutlet UILabel * titleLbl;
@property (nonatomic) IBOutlet UILabel * subtitleLbl;
@property (nonatomic) IBOutlet UITextField * cardNumber;
@property (nonatomic) IBOutlet UIButton * nextBtn;
@property (nonatomic) IBOutlet NSLayoutConstraint * nextBtnBottom;

@property (nonatomic) BOOL nextButtonAvailable;

@end

@implementation NITRegistrationCardViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController hideTabBarAnimated:false];
    [self.view layoutIfNeeded];
}

#pragma mark - IBAction
- (IBAction)nextAction:(id)sender
{
    if(self.nextButtonAvailable)
    {
        [self.cardNumber endEditing:true];
        [self.presenter snapCardNumber];
    }
}

#pragma mark - Private
- (void)initUI
{
    //Default state
    self.cardNumber.numericFormatter = [AKNumericFormatter formatterWithMask:formatterCodeMask
                                                        placeholderCharacter:'*'
                                                                        mode:AKNumericFormatterMixed];
    self.cardNumber.text = [[NITDiscountCardModel new] cardNumber];
    [self.presenter checkCardNumber:self.cardNumber.text];
    self.nextBtnBottom.constant = - 50;
    
    //Keyboard
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification*)notification
{    
    [self.view layoutIfNeeded];
    self.nextBtnBottom.constant = [self keyboardHeight:notification] - (IS_IPHONE_5 ? 120 : 50);
    [UIView animateWithDuration:0.25 animations:^{
        if(IS_IPHONE_5)
        {
            CGRect frame = self.view.frame;
            frame.origin.y = -70;
            self.view.frame = frame;
        }
        
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    [self.view layoutIfNeeded];
    self.nextBtnBottom.constant = -50;
    [UIView animateWithDuration:0.25 animations:^{
        if(IS_IPHONE_5)
        {
            CGRect frame = self.view.frame;
            frame.origin.y = 0;
            self.view.frame = frame;
        }
        
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - NITRegistrationCardPresenterProtocol
- (void)updateNextBtn:(BOOL)update
{
    if(update)  [self.nextBtn setBackgroundColor:RGB(58, 160, 144)];
    else        [self.nextBtn setBackgroundColor:RGB(231, 242, 241)];

    self.nextButtonAvailable = update;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(str.length <= 10)
        [self.presenter checkCardNumber:str];
    
    return true;
}

@end
