//
//  NITCatalogFilterProtocols.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITFilterItemModel.h"
#import "NITCatalogFilterSectionModel.h"
#import "NITApplyingFilterModel.h"
#import "NITFilterItem.h"

@protocol NITCatalogFilterInteractorOutputProtocol;
@protocol NITCatalogFilterInteractorInputProtocol;
@protocol NITCatalogFilterViewProtocol;
@protocol NITCatalogFilterPresenterProtocol;
@protocol NITCatalogFilterLocalDataManagerInputProtocol;
@protocol NITCatalogFilterAPIDataManagerInputProtocol;

@class NITCatalogFilterWireFrame;

typedef CF_ENUM (NSUInteger, CatalogFilterPriceType) {
    CatalogFilterPriceTypeFrom  = 0,
    CatalogFilterPriceTypeTo    = 1
};

// Defines the public interface that something else can use to drive the user interface
@protocol NITCatalogFilterViewProtocol
@required
@property (nonatomic, strong) id <NITCatalogFilterPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadSectionByModels:(NSArray <NITCatalogFilterSectionModel *> *)models;
- (void)reloadtableWithFilterModel:(NITApplyingFilterModel *)filterModel;
- (void)updateClearButton:(BOOL)show;
@end

@protocol NITCatalogFilterWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITCatalogFilterModuleFrom:(id)fromViewController withtemplateID:(NSString *)templateID;
- (void)showCatalogOptionFromViewController:(id)fromViewController withModel:(NITFilterItemModel *)model;
- (void)applyFiltersFromViewController:(id)fromViewController;
- (void)presentAlert:(id)alert withView:(id)fromView;
@end

@protocol NITCatalogFilterPresenterProtocol
@required
@property (nonatomic, weak) id <NITCatalogFilterViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogFilterInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogFilterWireFrameProtocol> wireFrame;
@property (nonatomic) NSString * templateID;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)showFilterOptions:(NITFilterItemModel *)model;
- (void)initData;
- (void)updatePriceFromFilter:(NSString *)price withFilterModel:(NITApplyingFilterModel *)filterModel;
- (void)updatePriceToFilter:(NSString *)price withFilterModel:(NITApplyingFilterModel *)filterModel;
- (void)updateNovetly;
- (void)applyFilters;
- (void)clearFilters:(NITApplyingFilterModel *)filter;
@end

@protocol NITCatalogFilterInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateFilterModel:(NSArray <NITFilterItemModel *> *)models;
@end

@protocol NITCatalogFilterInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCatalogFilterInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCatalogFilterAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCatalogFilterLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getFilterByID:(NSString *)templateID;
@end


@protocol NITCatalogFilterDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCatalogFilterAPIDataManagerInputProtocol <NITCatalogFilterDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getFiltersWithHandler:(void(^)(NSArray<SLInfosystem *> *result))handler;
@end

@protocol NITCatalogFilterLocalDataManagerInputProtocol <NITCatalogFilterDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NSArray <NITFilterItem *> *)getFilters;
@end
