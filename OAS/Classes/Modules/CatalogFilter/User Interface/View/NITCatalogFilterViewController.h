//
//  NITCatalogFilterViewController.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCatalogFilterProtocols.h"
#import "NITBaseViewController.h"

@interface NITCatalogFilterViewController : NITBaseViewController <NITCatalogFilterViewProtocol>

@property (nonatomic, strong) id <NITCatalogFilterPresenterProtocol> presenter;

@end
