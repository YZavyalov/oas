//
//  NITCatalogFilterViewController.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogFilterViewController.h"
#import "NITCatalogFilterTableViewCell.h"
#import "NITCatalogFilterNovetlyTableViewCell.h"
#import "NITCatalogFilerPriceTableViewCell.h"
#import "NITCatalogFilterOptionTableViewCell.h"

@interface NITCatalogFilterViewController()
<UITableViewDelegate,
UITableViewDataSource,
NITCatalogFilterPriceTableViewCellDelegate,
NITCatalogFilterNovetyTableViewCellDelegate,
NITCatalogFilterOptionTableViewCellDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;

@property (nonatomic) IBOutlet UIButton * applyFilterBtn;
@property (nonatomic) IBOutlet NSLayoutConstraint * applyFilterBtnH;
@property (nonatomic) IBOutlet NSLayoutConstraint * applyFilterBottom;
@property (nonatomic) IBOutlet NSLayoutConstraint * tableViewH;

@property (nonatomic) NSArray <NITCatalogFilterSectionModel *> * sections;
@property (nonatomic) NITApplyingFilterModel * filterModel;

@end

@implementation NITCatalogFilterViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];

    [self initUI];
    [self.presenter initData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController hideTabBarAnimated:false];
}

#pragma mark - IBAction
- (IBAction)applyFilter:(id)sender
{
    [self.presenter applyFilters];
}

- (IBAction)clearFilters:(id)sender
{
    [self.presenter clearFilters:self.filterModel];
}

#pragma mark - Private
- (void)initUI
{
    //Default
    self.rightBarButtonItem = self.navigationItem.rightBarButtonItem;
    self.filterModel = [NITApplyingFilterModel new];
    self.applyFilterBottom.constant = -50;
    //Keyboard
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];

    //Table cell
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITCatalogFilterNovetlyTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITCatalogFilterNovetlyTableViewCell)];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITCatalogFilerPriceTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITCatalogFilerPriceTableViewCell)];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITCatalogFilterOptionTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITCatalogFilterOptionTableViewCell)];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    [self.view layoutIfNeeded];
    self.applyFilterBottom.constant = [self keyboardHeight:notification] - 50;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    [self.view layoutIfNeeded];
    self.applyFilterBottom.constant = -50;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - NITCatalogPresenterProtocol
- (void)reloadSectionByModels:(NSArray <NITCatalogFilterSectionModel *> *)models
{
    self.sections = models;
    [self.tableView reloadData];
}

- (void)reloadtableWithFilterModel:(NITApplyingFilterModel *)filterModel
{
    self.filterModel = filterModel;
    [self.tableView reloadData];
}

- (void)updateClearButton:(BOOL)show
{
    self.navigationItem.rightBarButtonItem = show ? self.rightBarButtonItem : nil;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NITCatalogFilterSectionModel * sectionModel = self.sections[section];
    return [sectionModel headerHeight];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NITCatalogFilterSectionModel * sectionModel = self.sections[section];
    return sectionModel.rowCount;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCatalogFilterSectionModel * sectionModel = self.sections[indexPath.section];
    return [sectionModel rowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCatalogFilterSectionModel * sectionModel = self.sections[indexPath.section];
    sectionModel.priceType = indexPath.row == 0 ? PriceTypeFrom : PriceTypeTo;
    sectionModel.rowIndex = indexPath.row;
    NITCatalogFilterTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:[sectionModel cellID] forIndexPath:indexPath];
    [cell setModel:sectionModel delegate:self];
    
    return cell;
}

#pragma mark - NITCatalogFilterNovetyCellDelegate
- (void)didSelectNovetly
{
    [self.presenter updateNovetly];
}

#pragma mark - NITCatalogFilterPriceCellDelegate
- (void)didSelectPriceFrom:(NSString *)price
{
    [self.presenter updatePriceFromFilter:price withFilterModel:self.filterModel];
}

- (void)didSelectPriceTo:(NSString *)price
{
    [self.presenter updatePriceToFilter:price withFilterModel:self.filterModel];
}

#pragma mark - NITCatalogFilterOptionCellDelegate
- (void)didSelectFilterOption:(NITFilterItemModel *)model
{
    [self.presenter showFilterOptions:model];
}

@end
