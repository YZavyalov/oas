//
//  NITCatalogFilterOptionTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 10.04.17.
//  Copyright © 2017 Napoleon. All rights reserved.
//

#import "NITCatalogFilterOptionTableViewCell.h"

@interface NITCatalogFilterOptionTableViewCell()

@property (nonatomic) IBOutlet UILabel * title;

@end

@implementation NITCatalogFilterOptionTableViewCell

#pragma mark - Life cycle
- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initData];
}

#pragma mark - Custom accessors
- (void)setModel:(NITCatalogFilterSectionModel *)model delegate:(id)delegate
{
    self.model      = model;
    self.delegate   = delegate;
    self.index      = model.rowIndex;
    
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)selectOptionFilter:(id)sender
{
    if([self.delegate respondsToSelector:@selector(didSelectFilterOption:)])
    {
        [self.delegate didSelectFilterOption:self.model.filterModel.filters[self.index]];
    }
}

#pragma mark - Private
- (void)initData
{
    self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_next"]];
}

- (void)updateData
{
    self.title.text = ((NITFilterItemModel *)self.model.filterModel.filters[self.index]).nameFromDocuments;
}

@end
