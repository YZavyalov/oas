//
//  NITCatalogFilterTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 26.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogFilterProtocols.h"
#import <UIKit/UIKit.h>

@interface NITCatalogFilterTableViewCell : UITableViewCell

- (void)setModel:(NITCatalogFilterSectionModel *)model delegate:(id)delegate;

@end
