//
//  NITCatalogFilterOptionTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 10.04.17.
//  Copyright © 2017 Napoleon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCatalogFilterTableViewCell.h"

@protocol  NITCatalogFilterOptionTableViewCellDelegate <NSObject>
@optional
- (void)didSelectFilterOption:(NITFilterItemModel *)model;
@end

@interface NITCatalogFilterOptionTableViewCell : NITCatalogFilterTableViewCell

@property (nonatomic) NITCatalogFilterSectionModel * model;
@property (nonatomic) NSUInteger index;
@property (nonatomic, weak) id <NITCatalogFilterOptionTableViewCellDelegate> delegate;

@end
