//
//  NITCatalogFilerPriceTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 10.04.17.
//  Copyright © 2017 Napoleon. All rights reserved.
//

#import "NITCatalogFilerPriceTableViewCell.h"

#define MAX_LENGTH 6

@interface NITCatalogFilerPriceTableViewCell()
<
UITextFieldDelegate
>

@property (nonatomic) IBOutlet UITextField * textField;
@property (nonatomic) IBOutlet UILabel * placeholder;
@property (nonatomic) NITApplyingFilterModel * filterModel;

@end

@implementation NITCatalogFilerPriceTableViewCell

#pragma mark - Custom accessors
- (void)setModel:(NITCatalogFilterSectionModel *)model delegate:(id)delegate
{
    self.model = model;
    self.delegate = delegate;
    
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)tapTextField:(id)sender
{
    [self.textField becomeFirstResponder];
}

#pragma mark - Private
- (void)updateData
{
    self.placeholder.text = self.model.placeholder;
    self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@""
                                                                           attributes:@{NSForegroundColorAttributeName: RGB(135, 142, 141)}];
    self.textField.delegate = self;
    self.priceType = self.model.priceType;
    self.textField.text = [self.model.filterModel.minPrice stringValue];
    switch (self.priceType)
    {
        case PriceTypeFrom:
        {
            self.filterModel = [NITApplyingFilterModel new];
            self.textField.text = !([self.filterModel.minPrice intValue] == 0) ? [self.filterModel.minPrice stringValue] : nil;
        } break;
            
        case PriceTypeTo:
        {
            self.filterModel = [NITApplyingFilterModel new];
            self.textField.text = !([self.filterModel.maxPrice intValue] == 0) ? [self.filterModel.maxPrice stringValue] : nil;
        } break;
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length >= MAX_LENGTH && range.length == 0)
    {
        return false;
    }
    else
    {
        NSString * str = [textField.text stringByReplacingCharactersInRange:range withString:string];
        switch (self.priceType)
        {
            case PriceTypeFrom:
            {
                if([self.delegate respondsToSelector:@selector(didSelectPriceFrom:)])
                {
                    [self.delegate didSelectPriceFrom:str.length > 0 ? str : nil];
                }
            } break;
                
            case PriceTypeTo:
            {
                if([self.delegate respondsToSelector:@selector(didSelectPriceTo:)])
                {
                    [self.delegate didSelectPriceTo:str.length > 0 ? str : nil];
                }
            } break;
        }
        
        return true;
    }
}

@end
