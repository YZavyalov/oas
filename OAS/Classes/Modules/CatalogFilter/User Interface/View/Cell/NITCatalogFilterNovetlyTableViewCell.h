//
//  NITCatalogFilterNovetlyTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 10.04.17.
//  Copyright © 2017 Napoleon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCatalogFilterTableViewCell.h"

@protocol  NITCatalogFilterNovetyTableViewCellDelegate <NSObject>
@optional
- (void)didSelectNovetly;
@end

@interface NITCatalogFilterNovetlyTableViewCell : NITCatalogFilterTableViewCell

@property (nonatomic, weak) id <NITCatalogFilterNovetyTableViewCellDelegate> delegate;

@end
