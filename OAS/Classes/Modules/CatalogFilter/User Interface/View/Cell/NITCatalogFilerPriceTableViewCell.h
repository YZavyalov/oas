//
//  NITCatalogFilerPriceTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 10.04.17.
//  Copyright © 2017 Napoleon. All rights reserved.
//

#import "NITCatalogFilterProtocols.h"
#import "NITCatalogFilterTableViewCell.h"
#import <UIKit/UIKit.h>

@protocol  NITCatalogFilterPriceTableViewCellDelegate <NSObject>
@optional
- (void)didSelectPriceFrom:(NSString *)price;
- (void)didSelectPriceTo:(NSString *)price;
@end

@interface NITCatalogFilerPriceTableViewCell : NITCatalogFilterTableViewCell

@property (nonatomic) NITCatalogFilterSectionModel * model;
@property (nonatomic) PriceType priceType;
@property (nonatomic, weak) id <NITCatalogFilterPriceTableViewCellDelegate> delegate;

@end
