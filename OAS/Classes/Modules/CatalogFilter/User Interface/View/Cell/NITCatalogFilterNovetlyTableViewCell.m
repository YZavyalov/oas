//
//  NITCatalogFilterNovetlyTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 10.04.17.
//  Copyright © 2017 Napoleon. All rights reserved.
//

#import "NITCatalogFilterNovetlyTableViewCell.h"

@interface NITCatalogFilterNovetlyTableViewCell()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UIImageView * checkmarkImg;

@end

@implementation NITCatalogFilterNovetlyTableViewCell

#pragma mark - Custom accessors
- (void)setModel:(NITCatalogFilterSectionModel *)model delegate:(id)delegate
{
    self.delegate = delegate;
    
    [self updateData];
}

- (IBAction)selectAction:(id)sender
{
    if([self.delegate respondsToSelector:@selector(didSelectNovetly)])
    {
        [self.delegate didSelectNovetly];
    }
}

#pragma mark - Private
- (void)updateData
{
    NITApplyingFilterModel * filterModel = [NITApplyingFilterModel new];
    self.checkmarkImg.hidden = !filterModel.novetly;
}

@end
