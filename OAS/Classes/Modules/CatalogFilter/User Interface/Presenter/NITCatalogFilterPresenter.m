//
//  NITCatalogFilterPresenter.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogFilterPresenter.h"
#import "NITCatalogFilterWireframe.h"

@interface NITCatalogFilterPresenter()

@property (nonatomic) NSArray <NITCatalogFilterSectionModel *> * sections;
@property (nonatomic) NSArray <NITFilterItemModel *> * models;

@end

@implementation NITCatalogFilterPresenter

#pragma mark - Customs accessors
- (NSArray<NITCatalogFilterSectionModel *> *)sections
{
    if (_sections.count != 3)
    {
        NITCatalogFilterSectionModel * novetly = [[NITCatalogFilterSectionModel alloc] initWithType:CatalogFilterSectionTypeNovetly];
        NITCatalogFilterSectionModel * price = [[NITCatalogFilterSectionModel alloc] initWithType:CatalogFilterSectionTypePrice];
        NITCatalogFilterSectionModel * option = [[NITCatalogFilterSectionModel alloc] initWithType:CatalogFilterSectionTypeOption];
        _sections = @[novetly, price, option];
    }
    
    return _sections;
}

#pragma mark - NITCatalogFilterPresenterProtocol
- (void)initData
{
    [self addObservers];
    [self updateClearButton];
    [self updateData];
}

- (void)updateNovetly
{
    NITApplyingFilterModel * filterModel = [NITApplyingFilterModel new];
    filterModel.novetly = !filterModel.novetly;
    [self.view reloadtableWithFilterModel:filterModel];
    [TRACKER trackEvent:TrackerEventSelectFilter value:@"Новинка"];
}

- (void)updatePriceFromFilter:(NSString *)price withFilterModel:(NITApplyingFilterModel *)filterModel
{
    filterModel.minPrice = @([price floatValue]);
    [TRACKER trackEvent:TrackerEventSelectFilter value:@"Цена от"];
}

- (void)updatePriceToFilter:(NSString *)price withFilterModel:(NITApplyingFilterModel *)filterModel
{
    filterModel.maxPrice = @([price floatValue]);
    [TRACKER trackEvent:TrackerEventSelectFilter value:@"Цена до"];
}

- (void)applyFilters
{
    NITApplyingFilterModel * filter = [NITApplyingFilterModel new];
    if(([filter.minPrice intValue] > [filter.maxPrice intValue]) && ([filter.maxPrice intValue] != 0))
    {
        [self alertErrorPrice];
    }
    else
    {
        [self.wireFrame applyFiltersFromViewController:self.view];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kChangeFilterNotification object:nil];
        });
    }
}

- (void)clearFilters:(NITApplyingFilterModel *)filter
{
    NITApplyingFilterModel * filterModel = [NITApplyingFilterModel new];
    [filterModel clearFilter];
    [[NSNotificationCenter defaultCenter] postNotificationName:kChangeFilterNotification object:nil];
    [self.view reloadtableWithFilterModel:filterModel];
}

- (void)showFilterOptions:(NITFilterItemModel *)model
{
    [self.wireFrame showCatalogOptionFromViewController:self.view withModel:model];
}

#pragma mark - NITCatalogInteractorOutputProtocol
- (void)updateFilterModel:(NSArray <NITFilterItemModel *> *)models
{
    self.models = models;
    NITCatalogFilterSectionModel * section = self.sections[CatalogFilterSectionTypeOption];
    section.filterModel = [NITApplyingFilterModel new];
    section.filterModel.filters = models;
    [self.view reloadSectionByModels:self.sections];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateClearButton) name:kUpdateFilterNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kCreateFilterNotification object:nil];
}

- (void)updateData
{
    [self.interactor getFilterByID:self.templateID];
}

- (void)updateClearButton
{
    [self.view updateClearButton:[[NITApplyingFilterModel new] isCreate]];
}

- (void)alertErrorPrice
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Неправильно выставлена цена", nil)
                                                                    message:nil
                                                             preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = RGB(58, 160, 144);
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"ОК", nil)
                                                      style:UIAlertActionStyleCancel
                                                    handler:^(UIAlertAction * action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                    }];
    
    [alert addAction:cancel];
    [self.wireFrame presentAlert:alert withView:self.view];
}

@end
