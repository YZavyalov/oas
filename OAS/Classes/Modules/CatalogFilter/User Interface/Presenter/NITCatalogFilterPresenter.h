//
//  NITCatalogFilterPresenter.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogFilterProtocols.h"

@class NITCatalogFilterWireFrame;

@interface NITCatalogFilterPresenter : NSObject <NITCatalogFilterPresenterProtocol, NITCatalogFilterInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCatalogFilterViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogFilterInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogFilterWireFrameProtocol> wireFrame;
@property (nonatomic) NSString * templateID;

@end
