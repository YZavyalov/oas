//
//  NITCatalogFilterWireFrame.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogFilterWireFrame.h"
#import "NITCatalogOptionWireFrame.h"

@implementation NITCatalogFilterWireFrame

#pragma mark - Private
+ (id)createNITCatalogFilterModule
{
    // Generating module components
    id <NITCatalogFilterPresenterProtocol, NITCatalogFilterInteractorOutputProtocol> presenter = [NITCatalogFilterPresenter new];
    id <NITCatalogFilterInteractorInputProtocol> interactor = [NITCatalogFilterInteractor new];
    id <NITCatalogFilterAPIDataManagerInputProtocol> APIDataManager = [NITCatalogFilterAPIDataManager new];
    id <NITCatalogFilterLocalDataManagerInputProtocol> localDataManager = [NITCatalogFilterLocalDataManager new];
    id <NITCatalogFilterWireFrameProtocol> wireFrame= [NITCatalogFilterWireFrame new];
    id <NITCatalogFilterViewProtocol> view = [(NITCatalogFilterWireFrame *)wireFrame createViewControllerWithKey:_s(NITCatalogFilterViewController) storyboardType:StoryboardTypeCatalog];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITCatalogFilterWireFrameProtocol
+ (void)presentNITCatalogFilterModuleFrom:(UIViewController*)fromViewController withtemplateID:(NSString *)templateID
{
    NITCatalogFilterViewController * view = [NITCatalogFilterWireFrame createNITCatalogFilterModule];
    view.presenter.templateID = templateID;
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)showCatalogOptionFromViewController:(UIViewController *)fromViewController withModel:(NITFilterItemModel *)model
{
    [NITCatalogOptionWireFrame presentNITCatalogOptionModuleFrom:fromViewController withModel:model];
}

- (void)applyFiltersFromViewController:(UIViewController *)fromViewController
{
    [fromViewController.navigationController popViewControllerAnimated:true];
}

- (void)presentAlert:(UIAlertController *)alert withView:(UIViewController *)fromView
{
    [fromView presentViewController:alert animated:YES completion:nil];
}

@end
