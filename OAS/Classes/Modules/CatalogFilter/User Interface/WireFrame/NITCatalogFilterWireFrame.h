//
//  NITCatalogFilterWireFrame.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogFilterProtocols.h"
#import "NITCatalogFilterViewController.h"
#import "NITCatalogFilterLocalDataManager.h"
#import "NITCatalogFilterAPIDataManager.h"
#import "NITCatalogFilterInteractor.h"
#import "NITCatalogFilterPresenter.h"
#import "NITCatalogFilterWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITCatalogFilterWireFrame : NITRootWireframe <NITCatalogFilterWireFrameProtocol>

@end
