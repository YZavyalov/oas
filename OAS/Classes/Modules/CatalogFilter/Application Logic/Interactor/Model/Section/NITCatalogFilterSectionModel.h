//
//  NITCatalogFilterSectionModel.h
//  OAS
//
//  Created by Yaroslav on 26.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFilterItemModel.h"
#import "NITApplyingFilterModel.h"

typedef CF_ENUM (NSUInteger, CatalogFilterSectionType) {
    CatalogFilterSectionTypeNovetly = 0,
    CatalogFilterSectionTypePrice   = 1,
    CatalogFilterSectionTypeOption  = 2
};

typedef CF_ENUM (NSUInteger, PriceType) {
    PriceTypeFrom = 0,
    PriceTypeTo   = 1
};

@interface NITCatalogFilterSectionModel : NSObject

@property (nonatomic) CatalogFilterSectionType type;
@property (nonatomic) PriceType priceType;
@property (nonatomic) NITApplyingFilterModel * filterModel;
@property (nonatomic) NSString * cellID;
@property (nonatomic) CGFloat headerHeight;
@property (nonatomic) CGFloat rowHeight;
@property (nonatomic) NSUInteger rowCount;
@property (nonatomic) NSUInteger rowIndex;
@property (nonatomic) NSString * placeholder;

- (instancetype)initWithType:(CatalogFilterSectionType)type;

@end
