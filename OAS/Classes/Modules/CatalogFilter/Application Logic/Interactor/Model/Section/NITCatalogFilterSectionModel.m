//
//  NITCatalogFilterSectionModel.m
//  OAS
//
//  Created by Yaroslav on 26.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogFilterSectionModel.h"
#import "NITCatalogFilerPriceTableViewCell.h"
#import "NITCatalogFilterNovetlyTableViewCell.h"
#import "NITCatalogFilterOptionTableViewCell.h"

@implementation NITCatalogFilterSectionModel

#pragma mark - Life cycle
- (instancetype)initWithType:(CatalogFilterSectionType)type
{
    if (self = [super init])
    {
        self.type = type;
    }
    
    return self;
}

#pragma mark - Custom accessors
- (CGFloat)headerHeight
{
    switch (self.type)
    {
        case CatalogFilterSectionTypeNovetly:   return 0;
        case CatalogFilterSectionTypePrice:     return 14;
        case CatalogFilterSectionTypeOption:    return 14;
    }
}

- (CGFloat)rowHeight
{
    switch (self.type)
    {
        case CatalogFilterSectionTypeNovetly:   return 50;
        case CatalogFilterSectionTypePrice:     return 50;
        case CatalogFilterSectionTypeOption:    return 50;
    }
}

- (NSUInteger)rowCount
{
    switch (self.type)
    {
        case CatalogFilterSectionTypeNovetly:   return 1;
        case CatalogFilterSectionTypePrice:     return 2;
        case CatalogFilterSectionTypeOption:    return [self.filterModel.filters count];
    }
}

- (NSString *)placeholder
{
    switch (self.priceType)
    {
        case PriceTypeFrom: return NSLocalizedString(@"Цена от", nil);
        case PriceTypeTo:   return NSLocalizedString(@"Цена до", nil);
    }
}

- (NSString *)cellID
{
    switch (self.type)
    {
        case CatalogFilterSectionTypeNovetly:   return _s(NITCatalogFilterNovetlyTableViewCell);
        case CatalogFilterSectionTypePrice:     return _s(NITCatalogFilerPriceTableViewCell);
        case CatalogFilterSectionTypeOption:    return _s(NITCatalogFilterOptionTableViewCell);
    }
}

@end
