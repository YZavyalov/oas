//
//  NITFilterOptionItemModel.m
//  OAS
//
//  Created by Yaroslav on 02.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterOptionItemModel.h"
#import "SLInfoItem.h"
#import "SLInfoItemWithRelations.h"
#import "NITFilterCityItem.h"
#import "NITFilterGenformItem.h"

@implementation NITFilterOptionItemModel

#pragma mark - Life cycle
- (instancetype)initWithModel:(SLInfoItem *)model
{
    if (self = [super init])
    {
        self.identifier = model._id;
        self.name       = model.name;
        self.selected   = false;
        self.weight     = @(0);
    }
    
    return self;
}

- (instancetype)initWithModelWithRelation:(SLInfoItemWithRelations *)model
{
    if (self = [super init])
    {
        self.identifier = model._id;
        self.name       = model.name;
        self.selected   = false;
        self.weight     = @(0);
    }
    
    return self;
}

- (instancetype)initWithModelCity:(NITFilterCityItem *)model
{
    if (self = [super init])
    {
        self.identifier = model.identifier;
        self.name       = model.name;
        self.selected   = false;
        self.weight = model.weight;
    }
    
    return self;
}

- (instancetype)initWithModelGenform:(NITFilterGenformItem *)model
{
    if (self = [super init])
    {
        self.identifier = model.identifier;
        self.name       = model.name;
        self.selected   = false;
        self.weight     = model.weight;
    }
    
    return self;
}

#pragma mark - Public
+ (NSArray <NITFilterOptionItemModel *> *)filterOptionsfromEntities:(NSArray<SLInfoItem *> *)entities
{
    return [entities bk_map:^id(SLInfoItem *obj) {
        return [[NITFilterOptionItemModel alloc] initWithModel:obj];
    }];
}

+ (NSArray <NITFilterOptionItemModel *> *)filterOptionsFromEntitiesWithrelation:(NSArray<SLInfoItemWithRelations *> *)entites
{
    return [entites bk_map:^id(SLInfoItemWithRelations *obj) {
        return [[NITFilterOptionItemModel alloc] initWithModelWithRelation:obj];
    }];
}

+ (NSArray <NITFilterOptionItemModel *> *)filterCityFromEntities:(NSArray<NITFilterCityItem *> *)entities
{
    return [entities bk_map:^id(NITFilterCityItem *obj) {
        return [[NITFilterOptionItemModel alloc] initWithModelCity:obj];
    }];
}

+ (NSArray <NITFilterOptionItemModel *> *)filterGenformFromEntities:(NSArray<NITFilterGenformItem *> *)entities
{
    return [entities bk_map:^id(NITFilterGenformItem *obj) {
        return [[NITFilterOptionItemModel alloc] initWithModelGenform:obj];
    }];
}

@end
