//
//  NITFilterItemModel.h
//  OAS
//
//  Created by Yaroslav on 02.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SLInfosystem.h"

@class SLInfosystem, NITFilterOptionItemModel, NITFilterItem;

@interface NITFilterItemModel : NITBaseModel

@property (nonatomic) NSString * nameFromDocuments;

@property (nonatomic) NSArray <NITFilterOptionItemModel *> * options;

- (instancetype)initWithModel:(SLInfosystem *)model;
- (instancetype)initWithLocalModel:(NITFilterItem *)model;
+ (NSArray <NITFilterItemModel *> *)filterModelFromEntities:(NSArray <SLInfosystem *> *)entities;
+ (NSArray <NITFilterItemModel *> *)filterModelFromLocalEntities:(NSArray <NITFilterItem *> *)entities;

@end
