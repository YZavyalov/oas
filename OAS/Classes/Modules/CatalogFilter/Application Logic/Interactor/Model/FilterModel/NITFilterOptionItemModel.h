//
//  NITFilterOptionItemModel.h
//  OAS
//
//  Created by Yaroslav on 02.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SLInfoItem, SLInfoItemWithRelations, NITFilterCityItem, NITFilterGenformItem;

@interface NITFilterOptionItemModel : NITBaseModel

@property (nonatomic) BOOL selected;
@property (nonatomic) NSNumber * weight;

- (instancetype)initWithModel:(SLInfoItem *)model;
- (instancetype)initWithModelWithRelation:(SLInfoItemWithRelations *)model;
- (instancetype)initWithModelCity:(NITFilterCityItem *)model;
- (instancetype)initWithModelGenform:(NITFilterGenformItem *)model;

+ (NSArray <NITFilterOptionItemModel *> *)filterOptionsfromEntities:(NSArray <SLInfoItem *> *)entities;
+ (NSArray <NITFilterOptionItemModel *> *)filterOptionsFromEntitiesWithrelation:(NSArray<SLInfoItemWithRelations *> *)entites;
+ (NSArray <NITFilterOptionItemModel *> *)filterCityFromEntities:(NSArray<NITFilterCityItem *> *)entities;
+ (NSArray <NITFilterOptionItemModel *> *)filterGenformFromEntities:(NSArray<NITFilterGenformItem *> *)entities;

@end
