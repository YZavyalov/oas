//
//  NITApplyingFilterModelConstants.h
//  OAS
//
//  Created by Yaroslav on 25.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#pragma mark - Notifications
static NSString * const kChangeFilterNotification = @"change_filter_notification";

