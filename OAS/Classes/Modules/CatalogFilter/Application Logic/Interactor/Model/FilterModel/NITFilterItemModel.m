//
//  NITFilterItemModel.m
//  OAS
//
//  Created by Yaroslav on 02.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterItemModel.h"
#import "NITMobileConstantsModel.h"
#import "NITFilterItem.h"

@implementation NITFilterItemModel

#pragma mark - Life cycle
- (instancetype)initWithModel:(SLInfosystem *)model
{
    if (self = [super init])
    {
        self.identifier = model._id;
        self.name       = model.name;
        if([model.name isEqualToString:@"country"]) self.nameFromDocuments = [[NITMobileConstantsModel new] county_filter_desc];
        if([model.name isEqualToString:@"firm"]) self.nameFromDocuments = [[NITMobileConstantsModel new] manufacturer_filter_desc];
        if([model.name isEqualToString:@"genform"]) self.nameFromDocuments = [[NITMobileConstantsModel new] form_filter_desc];
    }
    
    return self;
}

- (instancetype)initWithLocalModel:(NITFilterItem *)model
{
    if (self = [super init])
    {
        self.identifier         = model.identifier;
        self.name               = model.name;
        self.nameFromDocuments  = model.nameFromDocuments;
    }
    
    return self;
}

#pragma mark - Public
+ (NSArray <NITFilterItemModel *> *)filterModelFromEntities:(NSArray <SLInfosystem *> *)entities
{
    return [entities bk_map:^(SLInfosystem *obj) {
        return [[NITFilterItemModel alloc] initWithModel:obj];
    }];
}

+ (NSArray <NITFilterItemModel *> *)filterModelFromLocalEntities:(NSArray<NITFilterItem *> *)entities
{
    return [entities bk_map:^(NITFilterItem *obj) {
        return [[NITFilterItemModel alloc] initWithLocalModel:obj];
    }];
}

@end
