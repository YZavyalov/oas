//
//  NITApplyingFilterModel.h
//  OAS
//
//  Created by Yaroslav on 25.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITApplyingFilterModelConstants.h"
#import "NITFilterItemModel.h"

@class NITFilterItemModel, SLCatalogElasticQueryCondition;

@interface NITApplyingFilterModel : NITLocalSettings

@property (nonatomic) BOOL novetly;
@property (nonatomic) NSNumber * minPrice;
@property (nonatomic) NSNumber * maxPrice;
@property (nonatomic) NSArray <NITFilterItemModel *> * filters;
@property (nonatomic) NSMutableDictionary * filterWithOptions;

- (BOOL)isCreate;
- (void)clearFilter;
- (NSArray <NSDictionary *> *)jsonCountry;
- (NSArray <NSDictionary *> *)jsonFirm;
- (NSArray <NSDictionary *> *)jsonGenform;
- (NSDictionary *)jsonDictionary:(NSString *)searchString;
- (SLCatalogElasticQueryCondition *)filtersQuery;

@end
