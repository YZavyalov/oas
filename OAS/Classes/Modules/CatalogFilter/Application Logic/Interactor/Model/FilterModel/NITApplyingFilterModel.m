//
//  NITApplyingFilterModel.m
//  OAS
//
//  Created by Yaroslav on 25.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITApplyingFilterModel.h"
#import "SLCatalogElasticQueryCondition.h"

static NSString * const kFilterNovetly  = @"kFilterNovetly";
static NSString * const kFilterMinPrice = @"kFilterMinPrice";
static NSString * const kFilterMaxPrice = @"kFilterMaxPrice";
static NSString * const kFilterOptions  = @"kFilterOptions";

@implementation NITApplyingFilterModel

#pragma mark - Custom Accessors
- (BOOL)novetly
{
    return [self boolForKey:kFilterNovetly];
}

- (void)setNovetly:(BOOL)novetly
{
    [self setBool:novetly forKey:kFilterNovetly];
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateFilterNotification object:nil];
}

- (NSNumber *)minPrice
{
    return [self objectForKey:kFilterMinPrice];
}

- (void)setMinPrice:(NSNumber *)minPrice
{
    [self setObject:minPrice forKey:kFilterMinPrice];
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateFilterNotification object:nil];
}

- (NSNumber *)maxPrice
{
    return [self objectForKey:kFilterMaxPrice];
}

- (void)setMaxPrice:(NSNumber *)maxPrice
{
    [self setObject:maxPrice forKey:kFilterMaxPrice];
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateFilterNotification object:nil];
}

- (NSMutableDictionary *)filterWithOptions
{
    return [[self dictionaryForKey:kFilterOptions] mutableCopy];
}

- (void)setFilterWithOptions:(NSMutableDictionary *)filterWithOptions
{
    [self setDictionary:[filterWithOptions mutableCopy] forKey:kFilterOptions];
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateFilterNotification object:nil];
}

#pragma mark - Public
- (BOOL)isCreate
{
    BOOL isUpdate = false;
    
    if (self.novetly)
    {
        isUpdate = true;
    }
    
    if(self.minPrice)
    {
        isUpdate = true;
    }
    
    if(self.maxPrice)
    {
        isUpdate = true;
    }
    
    if([self.filterWithOptions allKeys].count > 0)
    {
        isUpdate = true;
    }
    
    return isUpdate;
}

- (void)clearFilter
{
    self.novetly = false;
    self.minPrice = nil;
    self.maxPrice = nil;
    self.filterWithOptions = [NSMutableDictionary new];
}

#pragma mark - Create JSON
- (NSArray <NSDictionary *> *)jsonCountry
{
    NSMutableArray <NSDictionary *> * countries = [NSMutableArray new];
    NSArray <NSString *> * filterCountries = [self.filterWithOptions objectForKey:@"country"];
    
    for (NSString * str in filterCountries)
    {
        [countries addObject:@{@"n":@"properties.key", @"v":@"is__country"}];
        [countries addObject:@{@"n":@"properties.value", @"v":str}];
    }
    
    return [countries mutableCopy];
}

- (NSArray <NSDictionary *> *)jsonFirm
{
    NSMutableArray <NSDictionary *> * firms = [NSMutableArray new];
    NSArray <NSString *> * filterFirm = [self.filterWithOptions objectForKey:@"firm"];
    
    for (NSString * str in filterFirm)
    {
        [firms addObject:@{@"n":@"properties.key", @"v":@"is__firm"}];
        [firms addObject:@{@"n":@"properties.value", @"v":str}];
    }
    
    return [firms mutableCopy];
}

- (NSArray <NSDictionary *> *)jsonGenform
{
    NSMutableArray <NSDictionary *> * genforms = [NSMutableArray new];
    NSArray <NSString *> * filterGenform = [self.filterWithOptions objectForKey:@"genform"];
    
    for (NSString * str in filterGenform)
    {
        [genforms addObject:@{@"n":@"properties.key", @"v":@"is__genform"}];
        [genforms addObject:@{@"n":@"properties.value", @"v":str}];
    }
    
    return [genforms mutableCopy];
}

- (SLCatalogElasticQueryCondition *)filtersQuery
{
    SLCatalogElasticQueryCondition * query = [SLCatalogElasticQueryCondition new];
    
    NSMutableArray * pr = [NSMutableArray new];
    
    // price range
    if (self.minPrice || self.maxPrice)
    {
        SLCatalogElasticQueryCondition * priceQuery = [SLCatalogElasticQueryCondition new];
        priceQuery.n = @"price";
        priceQuery.v = @[
                         self.minPrice ? : @(0),
                         self.maxPrice ? : @(MAXFLOAT)
                         ];
        
        [pr addObject:priceQuery];
    }
    
    // country filters
    NSArray * countryFilters = [self jsonCountry];
    if (countryFilters.count > 0)
    {
        SLCatalogElasticQueryCondition * countryQuery = [SLCatalogElasticQueryCondition new];
        countryQuery.op = @"opMust";
        countryQuery.pr = (id)countryFilters;
        
        [pr addObject:countryQuery];
    }
    
    // firm filters
    NSArray * firmFilters = [self jsonFirm];
    if (firmFilters.count > 0)
    {
        SLCatalogElasticQueryCondition * firmQuery = [SLCatalogElasticQueryCondition new];
        firmQuery.op = @"opMust";
        firmQuery.pr = (id)firmFilters;
        
        [pr addObject:firmQuery];
    }
    
    // genform filters
    NSArray * genformFilters = [self jsonGenform];
    if (genformFilters.count > 0)
    {
        SLCatalogElasticQueryCondition * genformQuery = [SLCatalogElasticQueryCondition new];
        genformQuery.op = @"opMust";
        genformQuery.pr = (id)genformFilters;
        
        [pr addObject:genformQuery];
    }
    
    // new filters
    if (self.novetly)
    {
        SLCatalogElasticQueryCondition * novetlyQuery = [SLCatalogElasticQueryCondition new];
        novetlyQuery.op = @"opMust";
        novetlyQuery.pr = (id)@[
                                @{@"n":@"properties.key", @"v":@"is_new"},
                                @{@"n":@"properties.value", @"v":@"true"}
                                ];
        
        [pr addObject:novetlyQuery];
    }
    
    if (pr.count > 0)
    {
        query.op = @"opMust";
        query.pr = (id)pr;
    }
    
    return query;
}

- (NSDictionary *)jsonDictionary:(NSString *)searchString
{
    NSDictionary * q;
 
    if(searchString.length > 2)
    {
        if(self.novetly)
        {
            q = @{@"n":@"name",
                  @"v":searchString,
                  @"op":@"&",
                  @"pr":@[
                          @{@"n":@"price", @"v":@[self.minPrice ? [self.minPrice stringValue] : @"0", self.maxPrice ? [self.maxPrice stringValue] : [NSString stringWithFormat:@"%f", MAXFLOAT]]
                            },
                          @{@"n":@"is_new", @"v":@"true"},
                          @{@"op":@"|",
                            @"pr":[self jsonCountry]
                            },
                          @{@"op":@"|",
                            @"pr":[self jsonFirm]
                            },
                          @{@"op":@"|",
                            @"pr":[self jsonGenform]
                            }
                          ]
                  };

        }
        else
        {
            q = @{
                  @"n":@"name",
                  @"v":searchString,
                  @"op":@"&",
                  @"pr":@[
                          @{@"n":@"price", @"v":@[self.minPrice ? [self.minPrice stringValue] : @"0", self.maxPrice ? [self.maxPrice stringValue] : [NSString stringWithFormat:@"%f", MAXFLOAT]]
                            },
                          @{@"op":@"|",
                            @"pr":[self jsonCountry]
                            },
                          @{@"op":@"|",
                            @"pr":[self jsonFirm]
                            },
                          @{@"op":@"|",
                            @"pr":[self jsonGenform]
                            }
                          ]
                  };
        }
    }
    else
    {
        if(self.novetly)
        {
            q = @{@"op":@"&",
                  @"pr":@[
                          @{@"n":@"price", @"v":@{@"min":self.minPrice ? [self.minPrice stringValue] : @"0", @"max":self.maxPrice ? [self.maxPrice stringValue] : [NSString stringWithFormat:@"%f", MAXFLOAT]}
                            },
                          @{@"n":@"is_new", @"v":@"true"},
                          @{@"op":@"|",
                            @"pr":[self jsonCountry]
                            },
                          @{@"op":@"|",
                            @"pr":[self jsonFirm]
                            },
                          @{@"op":@"|",
                            @"pr":[self jsonGenform]
                            }
                          ]
                  };
        }
        else
        {
            q = @{@"op":@"&",
                  @"pr":@[
                          @{@"n":@"price", @"v":@{@"min":self.minPrice ? [self.minPrice stringValue] : @"0", @"max":self.maxPrice ? [self.maxPrice stringValue] : [NSString stringWithFormat:@"%f", MAXFLOAT]}
                            },
                          @{@"op":@"|",
                            @"pr":[self jsonCountry]
                            },
                          @{@"op":@"|",
                            @"pr":[self jsonFirm]
                            },
                          @{@"op":@"|",
                            @"pr":[self jsonGenform]
                            }
                          ]
                  };
        }
    }
    
    return q;
}

@end
