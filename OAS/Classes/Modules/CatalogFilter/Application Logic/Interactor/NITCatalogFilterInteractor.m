//
//  NITCatalogFilterInteractor.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogFilterInteractor.h"

@implementation NITCatalogFilterInteractor

#pragma mark - NITCatalogFilterInteractorInputProtocol
- (void)getFilterByID:(NSString *)templateID
{
    [HELPER startLoading];
    NSArray <NITFilterItemModel *> * filters = [NITFilterItemModel filterModelFromLocalEntities:[self.localDataManager getFilters]];
    if(filters.count > 0)
    {
        [HELPER stopLoading];
        [self.presenter updateFilterModel:filters];
    }
}

@end
