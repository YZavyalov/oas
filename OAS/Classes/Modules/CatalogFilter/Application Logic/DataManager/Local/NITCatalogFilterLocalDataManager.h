//
//  NITCatalogFilterLocalDataManager.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogFilterProtocols.h"

@interface NITCatalogFilterLocalDataManager : NSObject <NITCatalogFilterLocalDataManagerInputProtocol>

@end
