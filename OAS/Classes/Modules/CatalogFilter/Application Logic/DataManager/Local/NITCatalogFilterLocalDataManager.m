//
//  NITCatalogFilterLocalDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogFilterLocalDataManager.h"

@implementation NITCatalogFilterLocalDataManager

#pragma mark - NITCatalogFilterDataManagerInputProtocol
- (NSArray <NITFilterItem *> *)getFilters
{
    return [NITFilterItem allFilterItem];
}

@end
