//
//  NITCatalogFilterAPIDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogFilterAPIDataManager.h"
#import "SLCatalogsApi.h"
#import "SLInfosystemsApi.h"

@implementation NITCatalogFilterAPIDataManager

#pragma mark - NITCatalogFilterAPIDataManagerInputProtocol
- (void)getFiltersWithHandler:(void(^)(NSArray<SLInfosystem *> *result))handler
{
    [HELPER startLoading];
    [[SLInfosystemsApi new] getStaticInfosystemsWithCompletionHandler:^(NSArray<SLInfosystem> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [ERROR_MESSAGE errorMessage:error_service_unavailable];
            [HELPER logError:error method:METHOD_NAME];
        }
        if (handler) handler (output);
    }];
}

@end
