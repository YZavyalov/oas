//
//  NITCatalogOptionViewController.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogOptionViewController.h"
#import "NITCatalogOptionTableViewCell.h"

@interface NITCatalogOptionViewController()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic) IBOutlet UIButton * doneBtn;
@property (nonatomic) IBOutlet UITableView * tableView;

@property (nonatomic) IBOutlet NSLayoutConstraint * doneBottom;

@property (nonatomic) NSArray <NITFilterOptionItemModel *> *models;

@end

@implementation NITCatalogOptionViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController hideTabBarAnimated:false];
}

#pragma mark - IBAction
- (IBAction)doneAction:(id)sender
{
    [self.presenter doneOptionWithModels:self.models];
}

- (IBAction)searchAction:(id)sender
{
    [self showSearchBar];
}

#pragma mark - Private
- (void)initUI
{
    self.searchBar.delegate = self;
    [self initSearchBar];
    [self hideSearchBar];
    self.doneBottom.constant = -50;
    
    //Table view
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITCatalogOptionTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITCatalogOptionTableViewCell)];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    [self.view layoutIfNeeded];
    self.doneBottom.constant = [self keyboardHeight:notification] - 50;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    [self.view layoutIfNeeded];
    self.doneBottom.constant = -50;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - NITCatalogOptionViewProtocol
- (void)realodDataByModels:(NSArray <NITFilterOptionItemModel *> *)models
{
    self.models = models;
    [self.tableView reloadData];
}

- (void)insertDataByModels:(NSArray<NITFilterOptionItemModel *> *)models
{
    if (models.count > 0)
    {
        NSMutableArray * allItems = [NSMutableArray arrayWithArray:self.models];
        [allItems addObjectsFromArray:models];
        
        NSArray * indexPaths = [models bk_map:^id(id obj) {
            return [NSIndexPath indexPathForRow:[allItems indexOfObject:obj] inSection:0];
        }];
        
        self.models = allItems;
        
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
        [self.tableView setContentOffset:CGPointMake(0, self.tableView.contentOffset.y + 50) animated:true];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCatalogOptionTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITCatalogOptionTableViewCell) forIndexPath:indexPath];
    cell.model = self.models[indexPath.row];
    
    return cell;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.isBottom)
    {
        if([self.presenter.model.name isEqualToString:@"firm"]) [self.presenter checkLoadNextDataFromOffset:self.models.count];
    }
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITFilterOptionItemModel * model = self.models[indexPath.row];
    model.selected = !model.selected;
    [self.tableView reloadData];
    
    if (model.selected)
    {
        [TRACKER trackEvent:TrackerEventSelectFilter value:model.name];
    }
}

#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{    
    [self.presenter searchByText:searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.presenter searchByText:nil];
    self.tableView.hidden = false;
    [self hideSearchBar];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar endEditing:true];
    [self enableSearchBarCanelButton];
    [self.presenter searchByText:searchBar.text];
}

@end
