//
//  NITCatalogOptionViewController.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCatalogOptionProtocols.h"
#import "NITBaseViewController.h"

@interface NITCatalogOptionViewController : NITBaseViewController <NITCatalogOptionViewProtocol>

@property (nonatomic, strong) id <NITCatalogOptionPresenterProtocol> presenter;

@end
