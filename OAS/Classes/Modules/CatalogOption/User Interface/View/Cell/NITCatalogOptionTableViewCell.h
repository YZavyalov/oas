//
//  NITCatalogOptionTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 10.04.17.
//  Copyright © 2017 Napoleon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITFilterOptionItemModel.h"

@interface NITCatalogOptionTableViewCell : UITableViewCell

@property (nonatomic) NITFilterOptionItemModel * model;

@end
