//
//  NITCatalogOptionTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 10.04.17.
//  Copyright © 2017 Napoleon. All rights reserved.
//

#import "NITCatalogOptionTableViewCell.h"
#import "NITApplyingFilterModel.h"

@interface NITCatalogOptionTableViewCell()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UIImageView * checkmarkImg;

@end

@implementation NITCatalogOptionTableViewCell

#pragma mark - Custom accessors
- (void)setModel:(NITFilterOptionItemModel *)model
{
    _model = model;
    
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{    
    self.title.text = self.model.name;
    [self.title setFont:self.model.selected ? [UIFont boldSystemFontOfSize:17] : [UIFont systemFontOfSize:17]];
    self.checkmarkImg.hidden = !self.model.selected;
}

@end
