//
//  NITCatalogOptionPresenter.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogOptionPresenter.h"
#import "NITCatalogOptionWireframe.h"

@interface NITCatalogOptionPresenter()
<
UISearchBarDelegate
>

@property (nonatomic) NITApplyingFilterModel * filterModel;
@property (nonatomic) NSArray <NITFilterOptionItemModel *> * itemModels;
@property (nonatomic) BOOL loading;
@property (nonatomic) NSString * searchText;
@end

@implementation NITCatalogOptionPresenter

#pragma mark - NITCatalogOptionPresenterProtocol
- (void)initData
{
    [self addObservers];
    [self updateData];
}

- (void)doneOptionWithModels:(NSArray <NITFilterOptionItemModel *> *)models
{
    self.filterModel = [NITApplyingFilterModel new];
    NSArray <NITFilterOptionItemModel *> * options = [models bk_select:^BOOL(NITFilterOptionItemModel *obj) {
                                                        return obj.selected;
                                                     }];
    NSMutableDictionary * dict = self.filterModel.filterWithOptions;
    [dict removeObjectForKey:self.model.name];
    NSMutableArray <NSString *> * optionID = [NSMutableArray new];
    for(NITFilterOptionItemModel * option in options)
    {
        [optionID addObject:option.identifier];
    }
    [dict setObject:optionID forKey:self.model.name];
    self.filterModel.filterWithOptions = dict;
    
    [self.wireFrame doneOptionFromViewController:self.view];
}

- (void)searchByText:(NSString *)text
{
    self.searchText = text;
    [self updateData];
}

#pragma mark - NITCatalogOptionInteractorOutputProtocol
- (void)reloadDataByModels:(NSArray <NITFilterOptionItemModel *> *)models
{
    self.itemModels = models;
    
    NITApplyingFilterModel * filter = [NITApplyingFilterModel new];
    NSArray <NSString *> * optionIDs = [filter.filterWithOptions objectForKey:self.model.name];
    for(NITFilterOptionItemModel * model in models)
    {
        for(NSString * optionID in optionIDs)
        {
            if([model.identifier isEqualToString:optionID]) model.selected = true;
        }
    }
    [self.view realodDataByModels:models];
}

- (void)addDataBymodels:(NSArray<NITFilterOptionItemModel *> *)models
{
    [self.view insertDataByModels:models];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.loading = false;
    });
}

#pragma mark - private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kUpdateInternetConnect object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kCreateFilterCityNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kCreateFilterGenformNotification object:nil];
}

- (void)updateData
{
    if([self.model.name isEqualToString:@"country"])
    {
        [self.interactor getLocalFilterCityWithSearchtext:self.searchText];
    }
    else if([self.model.name isEqualToString:@"genform"])
    {
        [self.interactor getLocalFilterGenFormWithSearchtext:self.searchText];
    }
    else
    {
        [self updateDataByOffset:nil];
    }
}

- (void)checkLoadNextDataFromOffset:(NSInteger)offset
{
    if (!self.loading && offset >= catalogLoadLimit)
    {
        self.loading = true;
        [self updateDataByOffset:@(offset)];
    }
}

- (void)updateDataByOffset:(NSNumber *)offset
{
    if (offset)
    {
        [self.interactor getNextFilterOptionBygroupID:self.model.identifier offset:offset searchString:self.searchText];
    }
    else
    {
        [self.interactor getFilterOptionByGroupID:self.model.identifier searchString:self.searchText];
    }
}

@end
