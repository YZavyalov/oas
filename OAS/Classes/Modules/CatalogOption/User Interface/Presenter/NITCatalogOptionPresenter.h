//
//  NITCatalogOptionPresenter.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogOptionProtocols.h"

@class NITCatalogOptionWireFrame;

@interface NITCatalogOptionPresenter : NSObject <NITCatalogOptionPresenterProtocol, NITCatalogOptionInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCatalogOptionViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogOptionInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogOptionWireFrameProtocol> wireFrame;

@property (nonatomic) NITFilterItemModel * model;

@end
