//
//  NITCatalogOptionWireFrame.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogOptionProtocols.h"
#import "NITCatalogOptionViewController.h"
#import "NITCatalogOptionLocalDataManager.h"
#import "NITCatalogOptionAPIDataManager.h"
#import "NITCatalogOptionInteractor.h"
#import "NITCatalogOptionPresenter.h"
#import "NITCatalogOptionWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITCatalogOptionWireFrame : NITRootWireframe <NITCatalogOptionWireFrameProtocol>

@end
