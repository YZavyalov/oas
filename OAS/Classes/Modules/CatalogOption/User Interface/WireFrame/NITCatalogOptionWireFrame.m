//
//  NITCatalogOptionWireFrame.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogOptionWireFrame.h"

@implementation NITCatalogOptionWireFrame

#pragma mark - Private
+ (id)createNITCatalogOptionModule
{
    // Generating module components
    id <NITCatalogOptionPresenterProtocol, NITCatalogOptionInteractorOutputProtocol> presenter = [NITCatalogOptionPresenter new];
    id <NITCatalogOptionInteractorInputProtocol> interactor = [NITCatalogOptionInteractor new];
    id <NITCatalogOptionAPIDataManagerInputProtocol> APIDataManager = [NITCatalogOptionAPIDataManager new];
    id <NITCatalogOptionLocalDataManagerInputProtocol> localDataManager = [NITCatalogOptionLocalDataManager new];
    id <NITCatalogOptionWireFrameProtocol> wireFrame= [NITCatalogOptionWireFrame new];
    id <NITCatalogOptionViewProtocol> view = [(NITCatalogOptionWireFrame *)wireFrame createViewControllerWithKey:_s(NITCatalogOptionViewController) storyboardType:StoryboardTypeCatalog];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITCatalogOptionWireFrameProtocol
+ (void)presentNITCatalogOptionModuleFrom:(UIViewController*)fromViewController withModel:(NITFilterItemModel *)model
{
    NITCatalogOptionViewController * view = [NITCatalogOptionWireFrame createNITCatalogOptionModule];
    view.title = model.nameFromDocuments;
    view.presenter.model = model;
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)doneOptionFromViewController:(UIViewController *)fromViewController
{
    [fromViewController.navigationController popViewControllerAnimated:true];
}

@end
