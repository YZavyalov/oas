//
//  NITCatalogOptionInteractor.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogOptionInteractor.h"

@implementation NITCatalogOptionInteractor

#pragma mark - NITCatalogOptionInteractorInputProtocol
- (void)getFilterOptionByGroupID:(NSString *)groupID searchString:(NSString *)searchString
{
    weaken(self);
    [self.APIDataManager getFilterDescriptionWithID:groupID offset:nil searchText:searchString withHandler:^(NSArray<SLInfoItemWithRelations *> * result) {
        [weakSelf.presenter reloadDataByModels:[NITFilterOptionItemModel filterOptionsFromEntitiesWithrelation:result]];
    }];
}

- (void)getNextFilterOptionBygroupID:(NSString *)groupID offset:(NSNumber *)offset searchString:(NSString *)searchString
{
    weaken(self);
    [self.APIDataManager getFilterDescriptionWithID:groupID offset:offset searchText:searchString withHandler:^(NSArray<SLInfoItemWithRelations *> * result) {
        [weakSelf.presenter addDataBymodels:[NITFilterOptionItemModel filterOptionsFromEntitiesWithrelation:result]];
    }];
}

- (void)getLocalFilterCityWithSearchtext:(NSString *)searchText
{
    [HELPER startLoading];
    NSArray <NITFilterCityItem *> * filterCity = [self.localDataManager getFilterCity];
    if(filterCity.count > 0)
    {        
        [HELPER stopLoading];
        [self sortingArray:[[NITFilterOptionItemModel filterCityFromEntities:filterCity] mutableCopy] searchText:searchText];
    }
}

- (void)getLocalFilterGenFormWithSearchtext:(NSString *)searchText
{
    [HELPER startLoading];
    NSArray <NITFilterGenformItem *> * filterGenform = [self.localDataManager getFilterGenform];
    if(filterGenform.count > 0)
    {
        [HELPER stopLoading];
        [self sortingArray:[[NITFilterOptionItemModel filterGenformFromEntities:filterGenform] mutableCopy] searchText:searchText];
    }
}

#pragma mark - Private
- (void)sortingArray:(NSMutableArray <NITFilterOptionItemModel *> *)filters searchText:(NSString *)searchText
{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"weight" ascending:false];
    [filters sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSArray <NITFilterOptionItemModel *> * searchFilters = filters;
    if(searchText.length > 0)
    searchFilters = [filters bk_select:^BOOL(NITFilterOptionItemModel *obj) {
        return ([obj.name rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound);
    }];

    [self.presenter reloadDataByModels:searchFilters];
}

@end
