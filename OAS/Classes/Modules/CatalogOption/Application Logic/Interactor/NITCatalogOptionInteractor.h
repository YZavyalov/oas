//
//  NITCatalogOptionInteractor.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogOptionProtocols.h"

@interface NITCatalogOptionInteractor : NSObject <NITCatalogOptionInteractorInputProtocol>

@property (nonatomic, weak) id <NITCatalogOptionInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCatalogOptionAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCatalogOptionLocalDataManagerInputProtocol> localDataManager;

@end
