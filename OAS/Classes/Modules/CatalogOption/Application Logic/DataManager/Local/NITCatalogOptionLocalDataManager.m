//
//  NITCatalogOptionLocalDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogOptionLocalDataManager.h"

@implementation NITCatalogOptionLocalDataManager

#pragma mark - NITCatalogOptionDataManagerInputProtocol
- (NSArray <NITFilterCityItem *> *)getFilterCity
{
    return [NITFilterCityItem allFilterCityItem];
}

- (NSArray <NITFilterGenformItem *> *)getFilterGenform
{
    return [NITFilterGenformItem allFilterGenformItem];
}

@end
