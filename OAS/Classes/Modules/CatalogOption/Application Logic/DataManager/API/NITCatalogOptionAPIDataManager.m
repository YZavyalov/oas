//
//  NITCatalogOptionAPIDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogOptionAPIDataManager.h"
#import "SLInfosystemsApi.h"

@implementation NITCatalogOptionAPIDataManager

#pragma mark - NITCatalogOptionDataManagerInputProtocol
- (void)getFilterDescriptionWithID:(NSString *)infoID offset:(NSNumber *)offset searchText:(NSString *)searchString withHandler:(void(^)(NSArray <SLInfoItemWithRelations *> *result))handler
{
    SLInfosystemQuery * query = [SLInfosystemQuery new];
    query.offset = offset;
    if(searchString.length > 2)
    {
        query.name = searchString;
        [HELPER startLoading];
    }
    else if(searchString.length == 0)
    {
        [HELPER startLoading];
    }

    [[SLInfosystemsApi new] postStaticInfosystemsByInfosystemidQuerywithrelationsWithInfosystemId:infoID body:query completionHandler:^(NSArray<SLInfoItemWithRelations> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [ERROR_MESSAGE errorMessage:error_service_unavailable];
            [HELPER logError:error method:METHOD_NAME];
        }
        if (handler) handler (output);
    }];
}

@end
