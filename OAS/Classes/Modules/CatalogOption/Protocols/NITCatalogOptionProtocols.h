//
//  NITCatalogOptionProtocols.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/10/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITFilterItemModel.h"
#import "NITFilterOptionItemModel.h"
#import "NITApplyingFilterModel.h"
#import "NITFilterCityItem.h"
#import "NITFilterGenformItem.h"

@protocol NITCatalogOptionInteractorOutputProtocol;
@protocol NITCatalogOptionInteractorInputProtocol;
@protocol NITCatalogOptionViewProtocol;
@protocol NITCatalogOptionPresenterProtocol;
@protocol NITCatalogOptionLocalDataManagerInputProtocol;
@protocol NITCatalogOptionAPIDataManagerInputProtocol;

@class NITCatalogOptionWireFrame;

static NSInteger const catalogLoadLimit = 20;

// Defines the public interface that something else can use to drive the user interface
@protocol NITCatalogOptionViewProtocol
@required
@property (nonatomic, strong) id <NITCatalogOptionPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)realodDataByModels:(NSArray <NITFilterOptionItemModel *> *)models;
- (void)insertDataByModels:(NSArray<NITFilterOptionItemModel *> *)models;
@end

@protocol NITCatalogOptionWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITCatalogOptionModuleFrom:(id)fromViewController withModel:(NITFilterItemModel *)model;
- (void)doneOptionFromViewController:(id)fromViewController;
@end

@protocol NITCatalogOptionPresenterProtocol
@required
@property (nonatomic, weak) id <NITCatalogOptionViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogOptionInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogOptionWireFrameProtocol> wireFrame;
@property (nonatomic) NITFilterItemModel * model;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)updateData;
- (void)doneOptionWithModels:(NSArray <NITFilterOptionItemModel *> *)models;
- (void)checkLoadNextDataFromOffset:(NSInteger)offset;
- (void)searchByText:(NSString *)text;
@end

@protocol NITCatalogOptionInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)reloadDataByModels:(NSArray <NITFilterOptionItemModel *> *)models;
- (void)addDataBymodels:(NSArray<NITFilterOptionItemModel *> *)models;
@end

@protocol NITCatalogOptionInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCatalogOptionInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCatalogOptionAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCatalogOptionLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getFilterOptionByGroupID:(NSString *)groupID searchString:(NSString *)searchString;
- (void)getNextFilterOptionBygroupID:(NSString *)groupID offset:(NSNumber *)offset searchString:(NSString *)searchString;
- (void)getLocalFilterCityWithSearchtext:(NSString *)searchText;
- (void)getLocalFilterGenFormWithSearchtext:(NSString *)searchText;
@end


@protocol NITCatalogOptionDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCatalogOptionAPIDataManagerInputProtocol <NITCatalogOptionDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getFilterDescriptionWithID:(NSString *)infoID offset:(NSNumber *)offset searchText:(NSString *)searchString withHandler:(void(^)(NSArray <SLInfoItemWithRelations *> *result))handler;
@end

@protocol NITCatalogOptionLocalDataManagerInputProtocol <NITCatalogOptionDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NSArray <NITFilterCityItem *> *)getFilterCity;
- (NSArray <NITFilterGenformItem *> *)getFilterGenform;
@end
