//
//  NITCatalogDirectoryProtocols.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/04/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITCatalogItemModel.h"
#import "NITGroupModel.h"
#import "NITApplyingFilterModel.h"
#import "NITShopItemModel.h"
#import "NITInfoItemModel.h"

@protocol NITCatalogDirectoryInteractorOutputProtocol;
@protocol NITCatalogDirectoryInteractorInputProtocol;
@protocol NITCatalogDirectoryViewProtocol;
@protocol NITCatalogDirectoryPresenterProtocol;
@protocol NITCatalogDirectoryLocalDataManagerInputProtocol;
@protocol NITCatalogDirectoryAPIDataManagerInputProtocol;

@class NITCatalogDirectoryWireFrame;

static NSInteger const catalogLoadLimit = 50;

typedef CF_ENUM (NSUInteger, CatalogDirectorySortType) {
    CatalogDirectorySortTypeAlphabet      = 0,
    CatalogDirectorySortTypeIncreasePrice = 1,
    CatalogDirectorySortTypeRedusePrice   = 2
};

typedef CF_ENUM (NSUInteger, CatalogViewType) {
    CatalogViewTypeDirectory    = 0,
    CatalogViewTypeContiguous   = 1,
    CatalogViewTypeBannerItems  = 2
};

// Defines the public interface that something else can use to drive the user interface
@protocol NITCatalogDirectoryViewProtocol
@required
@property (nonatomic, strong) id <NITCatalogDirectoryPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModels:(NSArray <NITCatalogItemModel *> *)models;
- (void)insertDataByModels:(NSArray <NITCatalogItemModel *> *)models;
- (void)reloadSearchResultByModels:(NSArray <NITCatalogItemModel *> *)models;
- (void)reloadSortingTitle:(NSString *)title;
@end

@protocol NITCatalogDirectoryWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITCatalogDirectoryModuleFrom:(id)fromView withModel:(NITBaseModel *)model withSearchText:(NSString *)text;
+ (void)presentNITCatalogContiguousModuleFrom:(id)fromView withModel:(NITBaseModel *)model;
+ (void)presentNITcatalogDirectoryBannerGroupFrom:(id)fromViewController withModel:(NITInfoItemModel *)model;
+ (void)presentNITcatalogDirectoryBannerItemsFrom:(id)fromViewController withModel:(NITInfoItemModel *)model;
- (void)showCatalogFitlersFrom:(id)fromView withTemplateID:(NSString *)templateID;
- (void)showCatalogProductDetailsFrom:(id)fromView withModel:(NITCatalogItemModel *)model withType:(CatalogViewType)viewType;
- (void)showSearchItemsWithViewController:(id)fromViewController withSearchText:(NSString *)text;
@end

@protocol NITCatalogDirectoryPresenterProtocol
@required
@property (nonatomic, weak) id <NITCatalogDirectoryViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogDirectoryInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogDirectoryWireFrameProtocol> wireFrame;
@property (nonatomic) NSString * parentID;
@property (nonatomic) NSString * defaultTemplateID;
@property (nonatomic) CatalogViewType viewType;
@property (nonatomic) NSString * searchText;
@property (nonatomic) NSArray <NSString *> * bannerItems;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)updateData;
- (void)checkLoadNextDataFromOffset:(NSInteger)offset;
- (void)contactAction;
- (void)searchByText:(NSString *)text;
- (void)addItemInBasket:(NITCatalogItemModel *)model;;
- (void)reduseItemInBasket:(NITCatalogItemModel *)model;;
- (void)showSortingActionSheet;
- (void)showCatalogFilter;
- (void)showProductDetailsByModel:(NITCatalogItemModel *)model;
- (void)showSearchItemsWithSearchText:(NSString *)text;
@end

@protocol NITCatalogDirectoryInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateCategoriesModels:(NSArray <NITCatalogItemModel *> *)models realOffset:(NSInteger)realOffset;
- (void)addCategoriesModels:(NSArray <NITCatalogItemModel *> *)models realOffset:(NSInteger)realOffset;
- (void)updateSearchResultModels:(NSArray <NITCatalogItemModel *> *)models;
@end

@protocol NITCatalogDirectoryInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCatalogDirectoryInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCatalogDirectoryAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCatalogDirectoryLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getCatalogItemsByGroupID:(NSString *)groupID sortKey:(NSString *)sortKey ascending:(BOOL)ascending searchString:(NSString *)text;
- (void)getNextCatalogItemsByGroupID:(NSString *)groupID offset:(NSNumber *)offset sortKey:(NSString *)sortKey ascending:(BOOL)ascending searchString:(NSString *)text;
- (void)getCatalogItemContiguousByID:(NSString *)parentID;
- (void)searchByText:(NSString *)text;
- (void)addItemToBasket:(NITCatalogItemModel *)model;
- (void)removeItemFromBasket:(NITCatalogItemModel *)model;
- (void)getBannerItems:(NSArray <NSString *> *)bannerItems;

@end


@protocol NITCatalogDirectoryDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCatalogDirectoryAPIDataManagerInputProtocol <NITCatalogDirectoryDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getCatalogItemsByGroupID:(NSString *)groupID offset:(NSNumber *)offset sortKey:(NSString *)sortKey ascending:(BOOL)ascending searchString:(NSString *)searchString withHandler:(void(^)(NSArray<SLCatalogItemWithRelations *> *result, NSInteger offsetCount))handler;
- (void)getCatalogItemContiguousByID:(NSString *)itemID withHandler:(void(^)(NSArray<SLCollectionRow *> *result))handler;
- (void)getItemDetailsByItemID:(NSString *)itemID withHandler:(void(^)(SLCatalogItemWithRelations *output))handler;
@end

@protocol NITCatalogDirectoryLocalDataManagerInputProtocol <NITCatalogDirectoryDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (void)addItemToBasket:(NITCatalogItemModel *)model;
- (void)removeItemFromBasket:(NITCatalogItemModel *)model;
@end
