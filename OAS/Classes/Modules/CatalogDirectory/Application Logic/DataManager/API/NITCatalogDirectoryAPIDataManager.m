//
//  NITCatalogDirectoryAPIDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/04/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogDirectoryAPIDataManager.h"
#import "NITPharmacyItem.h"
#import "SLCatalogsApi.h"
#import "SLCollectionsApi.h"

@implementation NITCatalogDirectoryAPIDataManager

#pragma mark - NITCatalogDirectoryAPIDataManagerInputProtocol
- (void)getCatalogItemsByGroupID:(NSString *)groupID offset:(NSNumber *)offset sortKey:(NSString *)sortKey ascending:(BOOL)ascending searchString:(NSString *)searchString withHandler:(void(^)(NSArray<SLCatalogItemWithRelations *> *result, NSInteger offsetCount))handler
{
    SLCatalogElasticQuery * query = [SLCatalogElasticQuery new];
    query.offset = offset;
    query.limit = @(catalogLoadLimit);
    query.order = ascending ? @"ASC" : @"DESC";
    query.orderBy = sortKey;
    
    /*if ([NITPharmacyItem allPharmacyForSearch].count > 0)
        query.shopIdsOr = [NITPharmacyItem allPharmacyForSearch];*/
    
    NITApplyingFilterModel * filterModel = [NITApplyingFilterModel new];
    query.query = [filterModel filtersQuery];
    
    if (searchString.length > 2)
    {
        // search
        query.query.n = @"name";
        query.query.v = searchString;
    }
    else
    {
        // catalog group
        SLCatalogElasticQueryCondition * groupQuery = [SLCatalogElasticQueryCondition new];
        groupQuery.n = @"groups";
        groupQuery.v = groupID;

        NSMutableArray * pr = [[NSMutableArray alloc] initWithArray:query.query.pr];
        [pr addObject:groupQuery];
        
        query.query.op = @"opMust";
        query.query.pr = (id)pr;
    }

    [HELPER startLoading];
    NSURLSessionTask * task = [[SLCatalogsApi new] postStaticCatalogElasticquerywithrelationsWithBody:query completionHandler:^(NSArray<SLCatalogItemWithRelations> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [ERROR_MESSAGE errorMessage:error_with_loading_product];
            [HELPER logError:error method:METHOD_NAME];
        }
        //if (handler) handler (output);
        
        //TODO: пока не внедрен в эластик поиск с аптеками, делать так ->
        SLCatalogItemsInShopsQuery * itemsShops = [SLCatalogItemsInShopsQuery new];
        itemsShops.shopIds = [NITPharmacyItem allPharmacyForSearch];
        itemsShops.itemIds = [output bk_map:^id(SLCatalogItemWithRelations *obj) {
            return obj._id;
        }];
        
        __block NSMutableArray <SLCatalogItemWithRelations *> *realItems = [NSMutableArray new];
        [[SLCatalogsApi new] postStaticCatalogItemsshopsWithBody:itemsShops completionHandler:^(NSArray<SLCatalogItemInShop> *result, NSError *resultError) {
            if(error) [HELPER logError:error method:METHOD_NAME];
            if(result)
            {
                for(SLCatalogItemWithRelations * outp in output)
                {
                    for(SLCatalogItemInShop * res in result)
                    {
                        if([outp._id isEqualToString:res.catalogItemId])
                        {
                            outp.price = res.price;
                            [realItems addObject:outp];
                        }
                    }
                }
                if(handler) handler (realItems, output.count);
            }
        }];
    }];
    
    [HELPER logString:[NSString stringWithFormat:@"%@\n%@", THIS_METHOD, [task.currentRequest description]]];
}

- (void)getCatalogItemContiguousByID:(NSString *)itemID withHandler:(void(^)(NSArray<SLCollectionRow *> *result))handler
{
    [HELPER startLoading];
    SLCollectionRow * body = [SLCollectionRow new];
    body.key = @"related_items"; body.value = itemID;
    [[SLCollectionsApi new] postStaticCollectionByCollectionkeyTupleSelectWithCollectionKey:@"related_products" body:body completionHandler:^(NSArray<NSArray<SLCollectionRow> *> *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler (output.firstObject);
    }];
}

- (void)getItemDetailsByItemID:(NSString *)itemID withHandler:(void(^)(SLCatalogItemWithRelations *output))handler
{
    [[SLCatalogsApi new] getStaticCatalogItemsByItemidWithItemId:itemID completionHandler:^(SLCatalogItemWithRelations *output, NSError *error) {
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (output) handler (output);
    }];
}

@end
