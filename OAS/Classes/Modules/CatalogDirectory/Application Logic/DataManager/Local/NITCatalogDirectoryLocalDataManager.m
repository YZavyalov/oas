//
//  NITCatalogDirectoryLocalDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/04/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogDirectoryLocalDataManager.h"
#import "NITBasketItem.h"

@implementation NITCatalogDirectoryLocalDataManager

#pragma mark - NITCatalogDirectoryLocalDataManagerInputProtocol
- (void)addItemToBasket:(NITCatalogItemModel *)model
{
    [NITBasketItem increaseBasketItemCountByModel:model];
}

- (void)removeItemFromBasket:(NITCatalogItemModel *)model
{
    [NITBasketItem dereaseBasketItemCountByModel:model];
}

@end
