//
//  NITCatalogDirectoryInteractor.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/04/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogDirectoryInteractor.h"

@implementation NITCatalogDirectoryInteractor

#pragma mark - NITCatalogDirectoryInteractorInputProtocol
- (void)getCatalogItemsByGroupID:(NSString *)groupID sortKey:(NSString *)sortKey ascending:(BOOL)ascending searchString:(NSString *)text
{
    weaken(self);
    [self.APIDataManager getCatalogItemsByGroupID:groupID offset:@(0) sortKey:sortKey ascending:ascending searchString:text withHandler:^(NSArray<SLCatalogItemWithRelations *> *result, NSInteger offsetCount) {
        NSArray * models = [NITCatalogItemModel catalogItemDetailModelsFromEntities:result withGroupID:groupID];
        [weakSelf.presenter updateCategoriesModels:models realOffset:offsetCount];
    }];
}

- (void)getNextCatalogItemsByGroupID:(NSString *)groupID offset:(NSNumber *)offset sortKey:(NSString *)sortKey ascending:(BOOL)ascending searchString:(NSString *)text
{
    weaken(self);
    [self.APIDataManager getCatalogItemsByGroupID:groupID offset:offset sortKey:sortKey ascending:ascending searchString:nil withHandler:^(NSArray<SLCatalogItemWithRelations *> *result, NSInteger offsetCount) {
        [weakSelf.presenter addCategoriesModels:[NITCatalogItemModel catalogItemDetailModelsFromEntities:result withGroupID:groupID] realOffset:offsetCount];
    }];
}

- (void)getCatalogItemContiguousByID:(NSString *)parentID
{
    weaken(self);
    [self.APIDataManager getCatalogItemContiguousByID:parentID withHandler:^(NSArray<SLCollectionRow *> *result) {        
        NSArray * models = [NITCatalogItemModel catalogItemModelsFromTupleEntities:result];
        NSMutableArray * modelsWithDetails = [NSMutableArray new];
        dispatch_group_t group = dispatch_group_create();
        
        [HELPER startLoading];
        [models bk_each:^(NITCatalogItemModel *obj) {
            dispatch_group_enter(group);
            [weakSelf.APIDataManager getItemDetailsByItemID:obj.identifier withHandler:^(SLCatalogItemWithRelations *output) {
                NITCatalogItemModel * model = [NITCatalogItemModel catalogItemDetailModelsFromEntities:@[output] withGroupID:nil].firstObject;
                [modelsWithDetails addObject:model];
                dispatch_group_leave(group);
            }];
        }];
        
        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            [HELPER stopLoading];
            [weakSelf.presenter updateCategoriesModels:modelsWithDetails realOffset:0];
        });
    }];
}

- (void)searchByText:(NSString *)searchText
{
    weaken(self);
    [self.APIDataManager getCatalogItemsByGroupID:nil offset:nil sortKey:nil ascending:true searchString:searchText withHandler:^(NSArray<SLCatalogItemWithRelations *> *result, NSInteger offsetCount) {
        [weakSelf.presenter updateSearchResultModels:[NITCatalogItemModel catalogItemDetailModelsFromEntities:result withGroupID:nil]];
    }];
}

- (void)getBannerItems:(NSArray <NSString *> *)bannerItems
{
    [HELPER startLoading];
    weaken(self);
    NSMutableArray <SLCatalogItemWithRelations *> * models = [NSMutableArray new];
    
    dispatch_group_t group = dispatch_group_create();
    [bannerItems bk_each:^(NSString *obj) {
        dispatch_group_enter(group);
        [weakSelf.APIDataManager getItemDetailsByItemID:obj withHandler:^(SLCatalogItemWithRelations *output) {
            [models addObject:output];
            dispatch_group_leave(group);
        }];
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [HELPER stopLoading];
        [self.presenter updateCategoriesModels:[NITCatalogItemModel catalogItemDetailModelsFromEntities:models withGroupID:nil] realOffset:0];
    });
}

- (void)addItemToBasket:(NITCatalogItemModel *)model
{
    [self.localDataManager addItemToBasket:model];
}

- (void)removeItemFromBasket:(NITCatalogItemModel *)model
{
    [self.localDataManager removeItemFromBasket:model];
}

@end
