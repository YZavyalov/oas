//
//  NITCatalogDirectoryInteractor.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/04/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogDirectoryProtocols.h"

@interface NITCatalogDirectoryInteractor : NSObject <NITCatalogDirectoryInteractorInputProtocol>

@property (nonatomic, weak) id <NITCatalogDirectoryInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCatalogDirectoryAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCatalogDirectoryLocalDataManagerInputProtocol> localDataManager;

@end
