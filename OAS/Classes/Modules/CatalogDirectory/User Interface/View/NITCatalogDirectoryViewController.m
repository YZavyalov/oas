//
//  NITCatalogDirectoryViewController.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/04/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogDirectoryViewController.h"
#import "NITCatalogDirectoryTableViewCell.h"
#import "NITCatalogTopSearchTableViewCell.h"

@interface NITCatalogDirectoryViewController()
<
UIScrollViewDelegate,
UITableViewDelegate,
UITableViewDataSource,
UISearchBarDelegate,
NITSearchViewDelegate,
NITEmptySearchViewDelegate,
NITCatalogDirectoryCellDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;

@property (nonatomic) IBOutlet UIView * topItemsView;
@property (nonatomic) IBOutlet UIButton * sortingBtn;
@property (nonatomic) IBOutlet UILabel * sortingLbl;
@property (nonatomic) IBOutlet UIButton * filterBtn;

@property (nonatomic) IBOutlet NSLayoutConstraint * sortingLblW;
@property (nonatomic) IBOutlet NSLayoutConstraint * topItemsViewH;
@property (nonatomic) IBOutlet UIView * emptyTableView;

@property (nonatomic) CGFloat topInset;
@property (nonatomic) NSArray <NITCatalogItemModel *> * items;

@end

@implementation NITCatalogDirectoryViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter initData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

#pragma mark - IBAction
- (IBAction)sortingAction:(id)sender
{
    [self.presenter showSortingActionSheet];
}

- (IBAction)filterAction:(id)sender
{
    [self searchBarCancelButtonClicked:self.searchBar];
    [self.presenter showCatalogFilter];
}

- (IBAction)searchAction:(id)sender
{
    [self showSearchBar];
}

- (IBAction)openFilters:(id)sender
{
    [self.presenter showCatalogFilter];
}

#pragma mark - Private
- (void)initUI
{
    self.searchBar.delegate = self;
    self.searchView.delegate = self;
    self.emptySearchView.delegate = self;
    
    [self.filterBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    
    // default state
    switch (self.presenter.viewType)
    {
        case CatalogViewTypeDirectory:
            self.filterBtn.hidden = false;
            break;
            
        case CatalogViewTypeContiguous:
            self.filterBtn.hidden = true;
            break;
            
        case CatalogViewTypeBannerItems:
        {
            self.topItemsViewH.constant = 0;
            self.filterBtn.hidden = true;
        } break;
    }
    self.sortingLblW.constant = [self.sortingLbl sizeOfMultiLineLabel].width;
    self.topInset = self.topItemsViewH.constant;
    
    // table view
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITCatalogDirectoryTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITCatalogDirectoryTableViewCell)];
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
    [self initSearchBar];
    [self hideSearchBar];
    
    [self.view layoutIfNeeded];
}

#pragma mark - NITCatalogDirectoryPresenterProtocol
- (void)reloadDataByModels:(NSArray<NITCatalogItemModel *> *)models
{
    if((self.presenter.searchText.length > 0) && ([[NITApplyingFilterModel new] isCreate] == false))
    {
        [self.emptySearchView setHidden:models.count > 0];
    }
    else
    {
        if(models.count > 0)
        {
            self.tableView.hidden       = false;
            self.emptyTableView.hidden  = true;
        }
        else
        {
            self.tableView.hidden       = true;
            self.emptyTableView.hidden  = false;
        }
    }
    
    self.items = models;
    [self.tableView reloadData];
}

- (void)insertDataByModels:(NSArray<NITCatalogItemModel *> *)models
{
    if (models.count > 0)
    {
        NSMutableArray * allItems = [NSMutableArray arrayWithArray:self.items];
        [allItems addObjectsFromArray:models];
        
        NSArray * indexPaths = [models bk_map:^id(id obj) {
            return [NSIndexPath indexPathForRow:[allItems indexOfObject:obj] inSection:0];
        }];
        
        self.items = allItems;
        
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
        [self.tableView setContentOffset:CGPointMake(0, self.tableView.contentOffset.y + 50) animated:true];
    }
}

- (void)reloadSearchResultByModels:(NSArray <NITCatalogItemModel *> *)models
{
    [self.emptySearchView setHidden:models.count > 0];
    [self.searchView reloadDataByModels:models];
}

- (void)reloadSortingTitle:(NSString *)title
{
    self.sortingLbl.text = title;
    self.sortingLblW.constant = [self.sortingLbl sizeOfMultiLineLabel].width;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCatalogDirectoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITCatalogDirectoryTableViewCell) forIndexPath:indexPath];
    cell.model = self.items[indexPath.row];
    cell.delegate = self;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self searchBarCancelButtonClicked:self.searchBar];
    [self.presenter showProductDetailsByModel:self.items[indexPath.row]];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.isBottom)
    {
        [self.presenter checkLoadNextDataFromOffset:self.items.count];
    }
}

#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.tableView.hidden = searchText.length > 2;
    self.searchView.hidden = searchText.length == 0;
    if(self.emptySearchView.hidden == false && searchText.length <= 3) self.emptySearchView.hidden = true;
    
    [self.presenter searchByText:searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.tableView.hidden = false;
    [self hideSearchBar];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar endEditing:true];
    [self enableSearchBarCanelButton];
    [self.presenter showSearchItemsWithSearchText:searchBar.text];
}

#pragma mark - NITCatalogDirectoryCellDelegate
- (void)didAddItem:(NITCatalogItemModel *)model
{
    [self.presenter addItemInBasket:model];
}

- (void)didReduseItem:(NITCatalogItemModel *)model
{
    [self.presenter reduseItemInBasket:model];
}

#pragma mark - NITSearchViewDelegate
- (void)didSelectSearchModel:(NITCatalogItemModel *)model
{
    [self.searchBar endEditing:true];
    [self.presenter showProductDetailsByModel:model];
}

#pragma mark - NITEmptySearchViewDelegate
- (void)didPressContact
{
    [self.presenter contactAction];
}

@end
