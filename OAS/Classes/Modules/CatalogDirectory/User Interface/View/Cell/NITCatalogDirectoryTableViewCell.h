//
//  NITCatalogDirectoryTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 04.04.17.
//  Copyright © 2017 Napoleon. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NITCatalogItemModel;

@protocol NITCatalogDirectoryCellDelegate <NSObject>
- (void)didAddItem:(NITCatalogItemModel *)model;
- (void)didReduseItem:(NITCatalogItemModel *)model;
@end

@interface NITCatalogDirectoryTableViewCell : UITableViewCell

@property (nonatomic) NITCatalogItemModel * model;
@property (nonatomic, weak) id <NITCatalogDirectoryCellDelegate> delegate;

@end
