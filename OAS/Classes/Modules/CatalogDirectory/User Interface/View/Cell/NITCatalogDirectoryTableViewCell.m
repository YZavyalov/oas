//
//  NITCatalogDirectoryTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 04.04.17.
//  Copyright © 2017 Napoleon. All rights reserved.
//

#import "NITCatalogDirectoryTableViewCell.h"
#import "NITCatalogItemModel.h"

@interface NITCatalogDirectoryTableViewCell()

@property (nonatomic) IBOutlet UIImageView * foto;
@property (nonatomic) IBOutlet UIImageView * isNewIcon;

@property (nonatomic) IBOutlet UIView * iphoneFiveCount;
@property (nonatomic) IBOutlet UIView * iphoneOtherCount;

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UILabel * detail;
@property (nonatomic) IBOutlet UILabel * recept;
@property (nonatomic) IBOutlet UILabel * underOrder;
@property (nonatomic) IBOutlet UILabel * price;

@property (nonatomic) IBOutlet UIButton * basketBtn;
@property (nonatomic) IBOutlet UIButton * plusBtn;
@property (nonatomic) IBOutlet UIButton * minusBtn;
@property (nonatomic) IBOutlet UILabel * count;
@property (nonatomic) IBOutlet UIButton * basketBtn5;
@property (nonatomic) IBOutlet UIButton * plusBtn5;
@property (nonatomic) IBOutlet UIButton * minusBtn5;
@property (nonatomic) IBOutlet UILabel * count5;

@property (nonatomic) IBOutlet NSLayoutConstraint * countTrealingToFive;
@property (nonatomic) IBOutlet NSLayoutConstraint * countTrealingToOther;
@property (nonatomic) IBOutlet NSLayoutConstraint * receptH;
@property (nonatomic) IBOutlet NSLayoutConstraint * receptW;
@property (nonatomic) IBOutlet NSLayoutConstraint * underOrderH;
@property (nonatomic) IBOutlet NSLayoutConstraint * underOrderW;
@property (nonatomic) IBOutlet NSLayoutConstraint * underOrderL;

@end

@implementation NITCatalogDirectoryTableViewCell

#pragma mark - Life cycle
- (void)awakeFromNib
{
    [super awakeFromNib];
    [self addObservers];
    
    if(!IS_IPHONE_5)
    {
        self.iphoneFiveCount.hidden = true;
        self.iphoneOtherCount.hidden = false;
        self.countTrealingToFive.priority = 500; self.countTrealingToOther.priority = 1000;
    }
    else
    {
        self.iphoneFiveCount.hidden = false;
        self.iphoneOtherCount.hidden = true;
        self.countTrealingToFive.priority = 1000; self.countTrealingToOther.priority = 500;
    }
    [self.basketBtn5 setHitTestEdgeInsets:UIEdgeInsetsMake(-20, -20, -20, -20)];
    [self.plusBtn5 setHitTestEdgeInsets:UIEdgeInsetsMake(-20, -20, -20, -20)];
    [self.minusBtn5 setHitTestEdgeInsets:UIEdgeInsetsMake(-20, -20, -20, -20)];
}

- (void)dealloc
{
    [self removeObservers];
}

#pragma mark - Custom accessors
- (void)setModel:(NITCatalogItemModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)basketAction:(id)sender
{
    [self addAction:sender];
}

- (IBAction)addAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didAddItem:)])
    {
        [self.delegate didAddItem:self.model];
    }
}

- (IBAction)reduseAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didReduseItem:)])
    {
        [self.delegate didReduseItem:self.model];
    }
}

#pragma mark - Private
- (void)updateData
{    
    [self updateBasketStateAnimated:false];
    [self updateCounter];
    
    [self.foto setImage:nil];
    if([NSURL URLWithString:self.model.image] && (self.model.image.length > 0))
        [self.foto setImageWithURL:[NSURL URLWithString:self.model.image]];
    else
        [self.foto setImage:[UIImage imageNamed:@"empty_foto"]];
    
    self.isNewIcon.hidden = !self.model.isNew;
    
    self.title.text = self.model.name;
    self.detail.text = self.model.descriptionText;
    self.price.text = [[self.model.price stringValue] stringByAppendingString:@" ₽"];
    
    self.receptH.constant = (self.model.hasRecipe || self.model.underOrder) ? 14 : 0;
    self.receptW.constant = self.model.hasRecipe ? 50 : 0;
    self.underOrderH.constant = (self.model.hasRecipe || self.model.underOrder) ? 14 : 0;
    self.underOrderW.constant = self.model.underOrder ? 70 : 0;
    self.underOrderL.constant = self.model.hasRecipe ? 8 : 0;
    
    [self layoutIfNeeded];
}

- (void)updateBasket:(NSNotification *)notification
{
    NSString * identifier = notification.object;
    
    if (identifier == nil || [identifier isEqualToString:self.model.identifier] )
    {
        [self updateBasketStateAnimated:false];
        [self updateCounter];
    }
}

- (void)updateBasketStateAnimated:(BOOL)animated
{
    BOOL inBasket = self.model.count > 0;
    
    [UIView animateWithDuration:animated ? .2 : 0 animations:^{
        if(!IS_IPHONE_5)
        {
            self.plusBtn.alpha     = inBasket ? 1 : 0;
            self.minusBtn.alpha    = inBasket ? 1 : 0;
            self.count.alpha       = inBasket ? 1 : 0;
            self.basketBtn.alpha   = inBasket ? 0 : 1;
        }
        else
        {
            self.plusBtn5.alpha     = inBasket ? 1 : 0;
            self.minusBtn5.alpha    = inBasket ? 1 : 0;
            self.count5.alpha       = inBasket ? 1 : 0;
            self.basketBtn5.alpha   = inBasket ? 0 : 1;
        }
    }];
}

- (void)updateCounter
{
    if(!IS_IPHONE_5)
    {
        self.count.text = [NSString stringWithFormat:@"%ld", (long)self.model.count];
    }
    else
    {
        self.count5.text = [NSString stringWithFormat:@"%ld", (long)self.model.count];
    }
    
    [self updateBasketStateAnimated:true];
}

- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBasket:) name:kSaveBasketItemNotification object:nil];
}

- (void)removeObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
