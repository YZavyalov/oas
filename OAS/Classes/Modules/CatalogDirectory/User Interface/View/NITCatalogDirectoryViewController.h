//
//  NITCatalogDirectoryViewController.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/04/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCatalogDirectoryProtocols.h"
#import "NITBaseViewController.h"

@interface NITCatalogDirectoryViewController : NITBaseViewController <NITCatalogDirectoryViewProtocol>

@property (nonatomic, strong) id <NITCatalogDirectoryPresenterProtocol> presenter;

@end
