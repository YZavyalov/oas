//
//  NITCatalogDirectoryPresenter.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/04/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogDirectoryPresenter.h"
#import "NITCatalogDirectoryWireframe.h"
#import "NITMobileConstantsModel.h"

@interface NITCatalogDirectoryPresenter()

@property (nonatomic) NSArray <NITCatalogItemModel *> * itemModels;
@property (nonatomic) CatalogDirectorySortType sortType;
@property (nonatomic) BOOL loading;

@property (nonatomic) NSInteger realOffset;

@end

@implementation NITCatalogDirectoryPresenter

#pragma mark - NITCatalogDirectoryPresenterProtocol
- (void)initData
{
    [self addObservers];
    self.sortType = CatalogDirectorySortTypeAlphabet;
    [self updateData];
}

- (void)updateData
{
    switch (self.viewType) {
        case CatalogViewTypeDirectory:
            [self updateDataByOffset:nil];
            break;
            
        case CatalogViewTypeContiguous:
            [self.view reloadSortingTitle:[self sortTitle]];
            if(self.itemModels.count > 0)
                [self sortingContiguousModels:self.itemModels];
            else
                [self.interactor getCatalogItemContiguousByID:self.parentID];
            break;
            
        case CatalogViewTypeBannerItems:
            [self.view reloadSortingTitle:[self sortTitle]];
            [self.interactor getBannerItems:self.bannerItems];
            break;
    }
}

- (void)checkLoadNextDataFromOffset:(NSInteger)offset
{
    if (self.viewType == CatalogViewTypeDirectory == !self.loading && self.realOffset >= catalogLoadLimit)
    {
        self.loading = true;
        [self updateDataByOffset:@(self.realOffset)];
    }
}

- (void)contactAction
{
    NSString * phone = [[NITMobileConstantsModel new] contact_phone];
    [HELPER callToPhoneNumber:phone];
}

- (void)searchByText:(NSString *)text
{
    if (text.length > 2)
    {
        [self.interactor searchByText:text];
    }
}

- (void)addItemInBasket:(NITCatalogItemModel *)model
{
    [self.interactor addItemToBasket:model];
}

- (void)reduseItemInBasket:(NITCatalogItemModel *)model
{
    [self.interactor removeItemFromBasket:model];
}

- (void)showSortingActionSheet
{
    weaken(self);
    
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{}];
    
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"По алфавиту", nil) actionBlock:^{
        self.sortType = CatalogDirectorySortTypeAlphabet;
        [weakSelf updateData];
    }];
    
    NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Сначала недорогие", nil) actionBlock:^{
        self.sortType = CatalogDirectorySortTypeIncreasePrice;
        [weakSelf updateData];
    }];
    
    NITActionButton * action3 = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Сначала дорогие", nil) actionBlock:^{
        self.sortType = CatalogDirectorySortTypeRedusePrice;
        [weakSelf updateData];
    }];
    
    [HELPER showActionSheetFromView:self.view withTitle:NSLocalizedString(@"Сортировать каталог", nil) actionButtons:@[action1, action2, action3] cancelButton:cancel];
}

- (void)showCatalogFilter
{
    [self.wireFrame showCatalogFitlersFrom:self.view withTemplateID:self.defaultTemplateID];
}

- (void)showProductDetailsByModel:(NITCatalogItemModel *)model
{
    [self.wireFrame showCatalogProductDetailsFrom:self.view withModel:model withType:self.viewType];
}

- (void)showSearchItemsWithSearchText:(NSString *)text
{
    [self.wireFrame showSearchItemsWithViewController:self.view withSearchText:text];
}

#pragma mark - NITCatalogDirectoryInteractorOutputProtocol
- (void)updateCategoriesModels:(NSArray<NITCatalogItemModel *> *)models realOffset:(NSInteger)realOffset
{
    self.realOffset += realOffset;
    
    self.itemModels = models;
    [self sortingContiguousModels:self.itemModels];
}

- (void)addCategoriesModels:(NSArray<NITCatalogItemModel *> *)models realOffset:(NSInteger)realOffset
{
    self.realOffset += realOffset;
    
    [self.view insertDataByModels:models];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.loading = false;
    });
}

- (void)updateSearchResultModels:(NSArray<NITCatalogItemModel *> *)models
{
    [self.view reloadSearchResultByModels:models];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kChangeFilterNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kUpdatePharmacyNotification object:nil];
}

- (void)updateDataByOffset:(NSNumber *)offset
{
    NSString * sortKey;
    BOOL ascending;
    
    switch (self.sortType) {
        case CatalogDirectorySortTypeAlphabet:
            sortKey = keyPath(NITCatalogItemModel.name);
            ascending = true;
            break;
            
        case CatalogDirectorySortTypeIncreasePrice:
            sortKey = keyPath(NITCatalogItemModel.price);
            ascending = true;
            break;
            
        case CatalogDirectorySortTypeRedusePrice:
            sortKey = keyPath(NITCatalogItemModel.price);
            ascending = false;
            break;
    }
    
    if (offset)
    {
        [self.interactor getNextCatalogItemsByGroupID:self.parentID offset:offset sortKey:sortKey ascending:ascending searchString:self.searchText];
    }
    else
    {
        [self.interactor getCatalogItemsByGroupID:self.parentID sortKey:sortKey ascending:ascending searchString:self.searchText];
    }
    
    [self.view reloadSortingTitle:[self sortTitle]];
}

- (NSString *)sortTitle
{
    switch (self.sortType) {
        case CatalogDirectorySortTypeAlphabet:
            return NSLocalizedString(@"По алфавиту", nil);
            
        case CatalogDirectorySortTypeIncreasePrice:
            return NSLocalizedString(@"Сначала недорогие", nil);
            
        case CatalogDirectorySortTypeRedusePrice:
            return NSLocalizedString(@"Сначала дорогие", nil);
    }
}

- (void)sortingContiguousModels:(NSArray <NITCatalogItemModel *> *)models
{
    //TODO: посмотреть здесь
    if(self.viewType == CatalogViewTypeContiguous)
    {
        switch (self.sortType)
        {
            case CatalogDirectorySortTypeAlphabet:
            {
                NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
                models = [models sortedArrayUsingDescriptors:@[sortDescriptor]];
                
            } break;
                
            case CatalogDirectorySortTypeIncreasePrice:
            {
                NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"price" ascending:YES];
                models = [models sortedArrayUsingDescriptors:@[sortDescriptor]];
            } break;
                
            case CatalogDirectorySortTypeRedusePrice:
            {
                NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"price" ascending:NO];
                models = [models sortedArrayUsingDescriptors:@[sortDescriptor]];
            } break;
        }
    }
    
    [self.view reloadDataByModels:models];
    //return models;
}

@end
