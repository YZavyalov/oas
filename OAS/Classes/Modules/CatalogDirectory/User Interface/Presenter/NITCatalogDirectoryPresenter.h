//
//  NITCatalogDirectoryPresenter.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/04/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogDirectoryProtocols.h"

@class NITCatalogDirectoryWireFrame;

@interface NITCatalogDirectoryPresenter : NSObject <NITCatalogDirectoryPresenterProtocol, NITCatalogDirectoryInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCatalogDirectoryViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogDirectoryInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogDirectoryWireFrameProtocol> wireFrame;
@property (nonatomic) NSString * parentID;
@property (nonatomic) NSString * defaultTemplateID;
@property (nonatomic) CatalogViewType viewType;
@property (nonatomic) NSString * searchText;
@property (nonatomic) NSArray <NSString *> * bannerItems;

@end
