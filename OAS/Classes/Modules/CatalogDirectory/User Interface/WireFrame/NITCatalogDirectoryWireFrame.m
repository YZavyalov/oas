//
//  NITCatalogDirectoryWireFrame.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/04/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogDirectoryWireFrame.h"
#import "NITCatalogFilterWireFrame.h"
#import "NITProductDetailsWireFrame.h"

@implementation NITCatalogDirectoryWireFrame

#pragma mark - Private
+ (id)createNITCatalogDirectoryModule
{
    // Generating module components
    id <NITCatalogDirectoryPresenterProtocol, NITCatalogDirectoryInteractorOutputProtocol> presenter = [NITCatalogDirectoryPresenter new];
    id <NITCatalogDirectoryInteractorInputProtocol> interactor = [NITCatalogDirectoryInteractor new];
    id <NITCatalogDirectoryAPIDataManagerInputProtocol> APIDataManager = [NITCatalogDirectoryAPIDataManager new];
    id <NITCatalogDirectoryLocalDataManagerInputProtocol> localDataManager = [NITCatalogDirectoryLocalDataManager new];
    id <NITCatalogDirectoryWireFrameProtocol> wireFrame= [NITCatalogDirectoryWireFrame new];
    id <NITCatalogDirectoryViewProtocol> view = [(NITCatalogDirectoryWireFrame *)wireFrame createViewControllerWithKey:_s(NITCatalogDirectoryViewController) storyboardType:StoryboardTypeCatalog];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITCatalogDirectoryWireFrameProtocol
+ (void)presentNITCatalogDirectoryModuleFrom:(UIViewController *)fromViewController withModel:(NITGroupModel *)model withSearchText:(NSString *)text
{
    NITCatalogDirectoryViewController * view = [NITCatalogDirectoryWireFrame createNITCatalogDirectoryModule];
    view.title = model.name.length > 0 ? model.name : text;
    view.presenter.parentID = model.identifier;
    view.presenter.defaultTemplateID = model.defaultTemplateID;
    view.presenter.viewType = CatalogViewTypeDirectory;
    view.presenter.searchText = text;
    [fromViewController.navigationController pushViewController:view animated:true];
}

+ (void)presentNITCatalogContiguousModuleFrom:(UIViewController *)fromViewController withModel:(NITBaseModel *)model
{
    NITCatalogDirectoryViewController * view = [NITCatalogDirectoryWireFrame createNITCatalogDirectoryModule];
    view.title = NSLocalizedString(@"Другие формы выпуска", nil);
    view.presenter.parentID = model.identifier;
    view.presenter.viewType = CatalogViewTypeContiguous;
    [fromViewController.navigationController pushViewController:view animated:true];
}

+ (void)presentNITcatalogDirectoryBannerGroupFrom:(UIViewController *)fromViewController withModel:(NITInfoItemModel *)model
{
    NITCatalogDirectoryViewController * view = [NITCatalogDirectoryWireFrame createNITCatalogDirectoryModule];
    view.title = model.name;
    view.presenter.parentID = model.events_value.firstObject;
    view.presenter.viewType = CatalogViewTypeDirectory;
    [fromViewController.navigationController pushViewController:view animated:true];
}

+ (void)presentNITcatalogDirectoryBannerItemsFrom:(UIViewController *)fromViewController withModel:(NITInfoItemModel *)model
{
    NITCatalogDirectoryViewController * view = [NITCatalogDirectoryWireFrame createNITCatalogDirectoryModule];
    view.title = model.name;
    view.presenter.bannerItems = model.events_value;
    view.presenter.viewType = CatalogViewTypeBannerItems;
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)showCatalogFitlersFrom:(UIViewController *)fromViewController withTemplateID:(NSString *)templateID
{
    [NITCatalogFilterWireFrame presentNITCatalogFilterModuleFrom:fromViewController withtemplateID:templateID];
}

- (void)showCatalogProductDetailsFrom:(UIViewController *)fromViewController withModel:(NITCatalogItemModel *)model withType:(CatalogViewType)viewType
{
    [NITProductDetailsWireFrame presentNITProductDetailsModuleFrom:fromViewController withModel:model withType:(int)viewType];
}

- (void)showSearchItemsWithViewController:(UIViewController *)fromViewController withSearchText:(NSString *)text
{
    [NITCatalogDirectoryWireFrame presentNITCatalogDirectoryModuleFrom:fromViewController withModel:nil withSearchText:text];
}

@end
