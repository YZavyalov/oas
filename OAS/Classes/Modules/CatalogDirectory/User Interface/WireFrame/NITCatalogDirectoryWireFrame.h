//
//  NITCatalogDirectoryWireFrame.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/04/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogDirectoryProtocols.h"
#import "NITCatalogDirectoryViewController.h"
#import "NITCatalogDirectoryLocalDataManager.h"
#import "NITCatalogDirectoryAPIDataManager.h"
#import "NITCatalogDirectoryInteractor.h"
#import "NITCatalogDirectoryPresenter.h"
#import "NITCatalogDirectoryWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITCatalogDirectoryWireFrame : NITRootWireframe <NITCatalogDirectoryWireFrameProtocol>

@end
