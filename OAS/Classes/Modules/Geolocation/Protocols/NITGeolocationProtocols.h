//
//  NITGeolocationProtocols.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SLAuthApi.h"

@protocol NITGeolocationInteractorOutputProtocol;
@protocol NITGeolocationInteractorInputProtocol;
@protocol NITGeolocationViewProtocol;
@protocol NITGeolocationPresenterProtocol;
@protocol NITGeolocationLocalDataManagerInputProtocol;
@protocol NITGeolocationAPIDataManagerInputProtocol;

@class NITGeolocationWireFrame;

@protocol NITGeolocationViewProtocol
@required
@property (nonatomic, strong) id <NITGeolocationPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

@protocol NITGeolocationWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITGeolocationModuleFrom:(id)fromView;
- (void)presentNITGeolocationModuleFromWindow:(UIWindow *)window;
- (void)showTabbar;
- (void)showGuideFrom:(id)fromView;
- (void)showCityFrom:(id)fromView withHandler:(void(^)())handler;
@end

@protocol NITGeolocationPresenterProtocol
@required
@property (nonatomic, weak) id <NITGeolocationViewProtocol> view;
@property (nonatomic, strong) id <NITGeolocationInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITGeolocationWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)closeAction;
- (void)specifyLocation;
- (void)acceptGeolocation;
@end

@protocol NITGeolocationInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITGeolocationInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITGeolocationInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITGeolocationAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITGeolocationLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end


@protocol NITGeolocationDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITGeolocationAPIDataManagerInputProtocol <NITGeolocationDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITGeolocationLocalDataManagerInputProtocol <NITGeolocationDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
