//
//  NITGeolocationWireFrame.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITGeolocationProtocols.h"
#import "NITGeolocationViewController.h"
#import "NITGeolocationLocalDataManager.h"
#import "NITGeolocationAPIDataManager.h"
#import "NITGeolocationInteractor.h"
#import "NITGeolocationPresenter.h"
#import "NITGeolocationWireframe.h"
#import <UIKit/UIKit.h>

@interface NITGeolocationWireFrame : NITRootWireframe <NITGeolocationWireFrameProtocol>

@end
