//
//  NITGeolocationWireFrame.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGeolocationWireFrame.h"
#import "NITTabBarWireFrame.h"
#import "NITGuideWireFrame.h"
#import "NITCitiesWireFrame.h"

@implementation NITGeolocationWireFrame

#pragma mark - Private
+ (id)createNITGeolocationModule
{
    // Generating module components
    id <NITGeolocationPresenterProtocol, NITGeolocationInteractorOutputProtocol> presenter = [NITGeolocationPresenter new];
    id <NITGeolocationInteractorInputProtocol> interactor = [NITGeolocationInteractor new];
    id <NITGeolocationAPIDataManagerInputProtocol> APIDataManager = [NITGeolocationAPIDataManager new];
    id <NITGeolocationLocalDataManagerInputProtocol> localDataManager = [NITGeolocationLocalDataManager new];
    id <NITGeolocationWireFrameProtocol> wireFrame= [NITGeolocationWireFrame new];
    id <NITGeolocationViewProtocol> view = [(NITGeolocationWireFrame *)wireFrame createViewControllerWithKey:_s(NITGeolocationViewController) storyboardType:StoryboardTypeMain];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITGeolocationWireFrameProtocol
+ (void)presentNITGeolocationModuleFrom:(UIViewController *)fromViewController
{
    NITGeolocationViewController * vc = [NITGeolocationWireFrame createNITGeolocationModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)presentNITGeolocationModuleFromWindow:(UIWindow *)window
{
    if (![HELPER geolocationSelected])
    {
        NITGeolocationViewController * vc = [NITGeolocationWireFrame createNITGeolocationModule];
        [self showRootViewController:vc inWindow:window];
    }
    else
    {
        NITGuideWireFrame * guideWireFrame = [NITGuideWireFrame new];
        [guideWireFrame presentNITGuideModuleFromWindow:window];
    }
}

- (void)showTabbar
{
    [NITTabBarWireFrame presentNITTabBarModuleFromWindow:WINDOW];
}

- (void)showGuideFrom:(UIViewController *)fromViewController
{
    [NITGuideWireFrame presentNITGuideModuleFrom:fromViewController];
}

- (void)showCityFrom:(UIViewController *)fromViewController withHandler:(void (^)())handler
{
    [NITCitiesWireFrame presentNITCitiesModuleFrom:fromViewController withType:CityViewTypeDefault withHandler:handler];
}

@end
