//
//  NITGeolocationPresenter.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITGeolocationProtocols.h"

@class NITGeolocationWireFrame;

@interface NITGeolocationPresenter : NITRootPresenter <NITGeolocationPresenterProtocol, NITGeolocationInteractorOutputProtocol>

@property (nonatomic, weak) id <NITGeolocationViewProtocol> view;
@property (nonatomic, strong) id <NITGeolocationInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITGeolocationWireFrameProtocol> wireFrame;

@end
