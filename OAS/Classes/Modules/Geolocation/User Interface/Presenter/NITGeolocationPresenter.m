//
//  NITGeolocationPresenter.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGeolocationPresenter.h"
#import "NITGeolocationWireframe.h"

@implementation NITGeolocationPresenter

#pragma mark - NITGeolocationPresenterProtocol
- (void)closeAction
{
    [HELPER setGeolocationSelected];
        
    if ([HELPER isFirstLanch])
    {
        [self.wireFrame showGuideFrom:self.view];
    }
    else
    {
        [self.wireFrame showTabbar];
    }
}

- (void)specifyLocation
{
    weaken(self);
    [self.wireFrame showCityFrom:self.view withHandler:^{
        [weakSelf closeAction];
    }];
}

- (void)acceptGeolocation
{
    weaken(self);
    [LOCATION_MANAGER updateLocationWithHandler:^{
        [self.wireFrame showCityFrom:self.view withHandler:^{
            [weakSelf closeAction];
        }];
    }];
}

@end
