//
//  NITGeolocationViewController.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGeolocationViewController.h"

@implementation NITGeolocationViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
}

#pragma mark - IBAction
- (IBAction)closeAction:(id)sender
{
    [self.presenter specifyLocation];
}

- (IBAction)acceptGeopositionAction:(id)sender
{
    [self.presenter acceptGeolocation];
}

- (IBAction)specifyLocationAction:(id)sender
{
    [self.presenter specifyLocation];
}

#pragma mark - Private
- (void)initUI
{
    [self.navigationController setNavigationBarHidden:true animated:false];
}

@end
