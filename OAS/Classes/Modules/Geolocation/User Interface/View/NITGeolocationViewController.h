//
//  NITGeolocationViewController.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITGeolocationProtocols.h"

@interface NITGeolocationViewController : NITBaseViewController <NITGeolocationViewProtocol>

@property (nonatomic, strong) id <NITGeolocationPresenterProtocol> presenter;

@end
