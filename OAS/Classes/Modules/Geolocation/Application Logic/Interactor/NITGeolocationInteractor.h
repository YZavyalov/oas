//
//  NITGeolocationInteractor.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITGeolocationProtocols.h"

@interface NITGeolocationInteractor : NSObject <NITGeolocationInteractorInputProtocol>

@property (nonatomic, weak) id <NITGeolocationInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITGeolocationAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITGeolocationLocalDataManagerInputProtocol> localDataManager;

@end
