//
//  NITGeolocationLocalDataManager.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITGeolocationProtocols.h"

@interface NITGeolocationLocalDataManager : NSObject <NITGeolocationLocalDataManagerInputProtocol>

@end
