//
//  NITProductDetailsProtocols.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/16/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITCatalogItemModel.h"
#import "NITProductDetailsSectionModel.h"
#import "NITProductOptionCellModel.h"
#import "SLShopsResponse.h"
#import "SLCatalogItemInShop.h"

@protocol NITProductDetailsInteractorOutputProtocol;
@protocol NITProductDetailsInteractorInputProtocol;
@protocol NITProductDetailsViewProtocol;
@protocol NITProductDetailsPresenterProtocol;
@protocol NITProductDetailsLocalDataManagerInputProtocol;
@protocol NITProductDetailsAPIDataManagerInputProtocol;

@class NITProductDetailsWireFrame;

typedef CF_ENUM (NSUInteger, ProductDetailsViewType) {
    ProductDetailsViewTypeDirectory    = 0,
    ProductDetailsViewTypeContiguous   = 1
};

// Defines the public interface that something else can use to drive the user interface
@protocol NITProductDetailsViewProtocol
@required
@property (nonatomic, strong) id <NITProductDetailsPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadSectionByModels:(NSArray <NITProductDetailsSectionModel *> *)models;
- (void)reloadFavoriteSate:(BOOL)active;
@end

@protocol NITProductDetailsWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITProductDetailsModuleFrom:(id)fromView withModel:(NITCatalogItemModel *)model withType:(ProductDetailsViewType)viewType;
- (void)showCatalogItemContiguousFromView:(id)fromView withModel:(NITCatalogItemModel *)model;
- (void)showPharmacyFromView:(id)fromView;
- (void)showBasketFromView:(id)fromView;
- (void)showImageWithView:(id)fromViewController withImage:(UIImage *)model;
@end

@protocol NITProductDetailsPresenterProtocol
@required
@property (nonatomic, weak) id <NITProductDetailsViewProtocol> view;
@property (nonatomic, strong) id <NITProductDetailsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITProductDetailsWireFrameProtocol> wireFrame;
@property (nonatomic) NITCatalogItemModel * catalogItem;
@property (nonatomic) ProductDetailsViewType viewType;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)updateData;
- (void)changeFavoriteState;
- (void)addToBasket;
- (void)removeFromBasket;
- (void)showCatalogItemContiguous;
- (void)showPharmacy;
- (void)showImage:(UIImage *)model;
@end

@protocol NITProductDetailsInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateCategoriesModels:(NSArray<NITCatalogItemModel *> *)models withProductModel:(NITCatalogItemModel *)model;
@end

@protocol NITProductDetailsInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITProductDetailsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITProductDetailsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITProductDetailsLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getCatalogItemDetailsByModel:(NITCatalogItemModel *)model;
- (void)changeFavoriteStateForModel:(NITCatalogItemModel *)model;
- (void)removeItemFromBasket:(NITCatalogItemModel *)model;
- (NSArray <NITCatalogItemModel *> *)getBasketItems;
- (void)addItemToBasket:(NITCatalogItemModel *)model;
@end


@protocol NITProductDetailsDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITProductDetailsAPIDataManagerInputProtocol <NITProductDetailsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getCatalogItemContiguousByID:(NSString *)itemID withHandler:(void(^)(NSArray<SLCollectionRow *> *result))handler;
@end

@protocol NITProductDetailsLocalDataManagerInputProtocol <NITProductDetailsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NSArray <NITBasketItem *> *)getBasketItems;
- (void)addFavoriteItem:(NITCatalogItemModel *)model;
- (void)removeFavoriteItem:(NITCatalogItemModel *)model;
@end
