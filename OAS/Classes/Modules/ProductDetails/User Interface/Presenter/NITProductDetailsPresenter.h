//
//  NITProductDetailsPresenter.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/16/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITProductDetailsProtocols.h"

@class NITProductDetailsWireFrame;

@interface NITProductDetailsPresenter : NSObject <NITProductDetailsPresenterProtocol, NITProductDetailsInteractorOutputProtocol>

@property (nonatomic, weak) id <NITProductDetailsViewProtocol> view;
@property (nonatomic, strong) id <NITProductDetailsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITProductDetailsWireFrameProtocol> wireFrame;
@property (nonatomic) NITCatalogItemModel * catalogItem;
@property (nonatomic) ProductDetailsViewType viewType;

@end
