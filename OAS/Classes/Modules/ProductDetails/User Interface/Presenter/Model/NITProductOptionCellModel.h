//
//  NITProductOptionCellModel.h
//  OAS
//
//  Created by Eugene Parafiynyk on 5/10/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef CF_ENUM (NSUInteger, ProductOptionType) {
    ProductOptionTypeAvailable  = 0,
    ProductOptionTypeForms      = 1
};

typedef CF_ENUM (NSUInteger, ProductOptionMode) {
    ProductOptionModeButton = 0,
    ProductOptionModeTitle  = 1
};

@interface NITProductOptionCellModel : NSObject

@property (nonatomic) NSString * title;
@property (nonatomic) ProductOptionType type;
@property (nonatomic) ProductOptionMode mode;
@property (nonatomic) UIFont * font;
@property (nonatomic) UIColor * color;

- (instancetype)initWithType:(ProductOptionType)type mode:(ProductOptionMode)mode;

@end
