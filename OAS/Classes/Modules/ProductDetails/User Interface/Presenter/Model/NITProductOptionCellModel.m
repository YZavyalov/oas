//
//  NITProductOptionCellModel.m
//  OAS
//
//  Created by Eugene Parafiynyk on 5/10/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductOptionCellModel.h"

@implementation NITProductOptionCellModel

#pragma mark - Life cycle
- (instancetype)initWithType:(ProductOptionType)type mode:(ProductOptionMode)mode
{
    if (self = [super init])
    {
        self.type = type;
        self.mode = mode;
    }
    
    return self;
}

#pragma mark - Custom accessors
- (NSString *)title
{
    switch (self.type) {
        case ProductOptionTypeForms:            return NSLocalizedString(@"Другие формы выпуска", nil);
        case ProductOptionTypeAvailable:
        {
            switch (self.mode)
            {
                case ProductOptionModeTitle:    return NSLocalizedString(@"Товар \"Под заказ\" будет доставлен через 2 дня", nil);
                case ProductOptionModeButton:   return NSLocalizedString(@"Проверить наличие", nil);
            }
        }
    }
}

- (UIFont *)font
{
    switch (self.mode) {
        case ProductOptionModeButton:   return [UIFont systemFontOfSize:17 weight:UIFontWeightRegular];;
        case ProductOptionModeTitle:    return [UIFont systemFontOfSize:15 weight:UIFontWeightRegular];
    }
}

- (UIColor *)color
{
    switch (self.mode) {
        case ProductOptionModeButton:   return [UIColor blackColor];
        case ProductOptionModeTitle:    return RGB(135, 142, 141);
    }
}

@end
