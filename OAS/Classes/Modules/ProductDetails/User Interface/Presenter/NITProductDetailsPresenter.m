//
//  NITProductDetailsPresenter.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/16/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITProductDetailsPresenter.h"
#import "NITProductDetailsWireframe.h"
#import "NITNotificationView.h"

@interface NITProductDetailsPresenter ()

@property (nonatomic) NSArray <NITCatalogItemModel *> * modelsContigious;
@property (nonatomic) NSArray <NITProductDetailsSectionModel *> * sections;
@property (nonatomic) BOOL minPriceOrderShowed;
@property (nonatomic) BOOL freeDeliveryShowed;
@property (nonatomic) BOOL maxCountShowed;

@end

@implementation NITProductDetailsPresenter

#pragma mark - Lifecycle
- (instancetype)init
{
    if (self = [super init])
    {
        [self addObservers];
    }
    
    return self;
}

#pragma mark - Customs accessors
- (NSArray<NITProductDetailsSectionModel *> *)sections
{
    if (/*_sections.count != 3 &&*/ self.catalogItem)
    {
        NITProductDetailsSectionModel * card = [[NITProductDetailsSectionModel alloc] initWithType:ProductDetailsSectionTypeCard];
        card.models = @[self.catalogItem];
        
        NITProductDetailsSectionModel * option = [[NITProductDetailsSectionModel alloc] initWithType:ProductDetailsSectionTypeOption];
        NITProductOptionCellModel * availableModel = [[NITProductOptionCellModel alloc] initWithType:ProductOptionTypeAvailable
                                                                                                mode:self.catalogItem.underOrder ? ProductOptionModeTitle : ProductOptionModeButton];
        NITProductOptionCellModel * formsModel = [[NITProductOptionCellModel alloc] initWithType:ProductOptionTypeForms mode:ProductOptionModeButton];
        if(self.viewType == ProductDetailsViewTypeDirectory)    option.models = self.modelsContigious.count > 0 ? @[availableModel, formsModel] : @[availableModel];
        else                                                    option.models = @[availableModel];
        
        NITProductDetailsSectionModel * description = [[NITProductDetailsSectionModel alloc] initWithType:ProductDetailsSectionTypeDescription];
        description.models = @[self.catalogItem];
        
        _sections = @[card, option, description];
    }
    
    return _sections;
}

#pragma mark - NITProductDetailsPresenterProtocol
- (void)updateData
{
    [self updateFavoriteView];
    [self.interactor getCatalogItemDetailsByModel:self.catalogItem];
}

- (void)showPharmacy
{
    [self.wireFrame showPharmacyFromView:self.view];
}

- (void)showCatalogItemContiguous
{
    [self.wireFrame showCatalogItemContiguousFromView:self.view withModel:self.catalogItem];
}

- (void)changeFavoriteState
{
    [self.interactor changeFavoriteStateForModel:self.catalogItem];
}

- (void)addToBasket
{
    [self.interactor addItemToBasket:self.catalogItem];
}

- (void)removeFromBasket
{
    [self.interactor removeItemFromBasket:self.catalogItem];
}

- (void)changeBasketState
{
    [self.interactor removeItemFromBasket:self.catalogItem];
}

- (void)showImage:(UIImage *)model
{
    [self.wireFrame showImageWithView:self.view withImage:model];
}

#pragma mark - NITProductDetailsInteractorOutputProtocol
- (void)updateCategoriesModels:(NSArray<NITCatalogItemModel *> *)models withProductModel:(NITCatalogItemModel *)model
{
    [[self.catalogItem allPropertyNames] bk_each:^(NSString *key) {
        if (![key isEqualToString:keyPath(model.count)])
        {
            [self.catalogItem setValue:[model valueForKey:key] forKey:key];
        }
    }];
    
    self.modelsContigious = models;
    [self.view reloadSectionByModels:self.sections];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kChangeBasketNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFavoriteView) name:kSaveFavoriteItemNotification object:nil];
}

- (void)updateFavoriteView
{
    [self.view reloadFavoriteSate:self.catalogItem.favorite];
}

@end
