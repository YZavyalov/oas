//
//  NITProductDetailsTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 16.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsBaseTableViewCell.h"

@class NITCatalogItemModel;

@protocol NITProductDetailsCellDelegate <NSObject>
- (void)didAddItem:(NITCatalogItemModel *)model;
- (void)didReduseItem:(NITCatalogItemModel *)model;
- (void)didPresentImage:(UIImage *)model;
@end

@interface NITProductDetailsTableViewCell : NITProductDetailsBaseTableViewCell

@property (nonatomic) NITCatalogItemModel * model;
@property (nonatomic, weak) id <NITProductDetailsCellDelegate> delegate;

@end
