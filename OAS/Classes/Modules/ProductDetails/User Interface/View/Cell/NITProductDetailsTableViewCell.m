//
//  NITProductDetailsTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 16.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsTableViewCell.h"
#import "NITCatalogItemModel.h"

@interface NITProductDetailsTableViewCell()

@property (nonatomic) IBOutlet UIImageView * foto;
@property (nonatomic) IBOutlet UIImageView * isNewIcon;

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UILabel * detail;
@property (nonatomic) IBOutlet UILabel * recept;
@property (nonatomic) IBOutlet UILabel * underOrder;
@property (nonatomic) IBOutlet UILabel * price;

@property (nonatomic) IBOutlet UIButton * basketBtn;
@property (nonatomic) IBOutlet UIButton * plusBtn;
@property (nonatomic) IBOutlet UIButton * minusBtn;
@property (nonatomic) IBOutlet UILabel * count;

@property (nonatomic) IBOutlet NSLayoutConstraint * fotoH;
@property (nonatomic) IBOutlet NSLayoutConstraint * receptW;
@property (nonatomic) IBOutlet NSLayoutConstraint * underOrderW;
@property (nonatomic) IBOutlet NSLayoutConstraint * underOrderL;

@property (nonatomic) BOOL isFullScreen;
@property (nonatomic) CGRect prevFrame;

@end

@implementation NITProductDetailsTableViewCell

#pragma mark - Life cycle
- (void)awakeFromNib
{
    [super awakeFromNib];
    [self addObservers];
}

- (void)dealloc
{
    [self removeObservers];
}

#pragma mark - Custom accessors
- (void)setModel:(id)model delegate:(id)delegate
{
    self.model = model;
    self.delegate = delegate;
    
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)basketAction:(id)sender
{
    [self addAction:sender];
}

- (IBAction)addAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didAddItem:)])
    {
        [self.delegate didAddItem:self.model];
    }
}

- (IBAction)reduseAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didReduseItem:)])
    {
        [self.delegate didReduseItem:self.model];
    }
}

- (IBAction)showPictureAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didPresentImage:)] && self.model.image)
    {
        [self.delegate didPresentImage:self.foto.image];
    }
}

#pragma mark - Private
- (void)updateData
{    
    [self updateFoto];
    [self updateCounter];
    [self updateBasketStateAnimated:false];
    
    self.isNewIcon.hidden = !self.model.isNew;
    
    self.title.text = self.model.name;
    self.detail.text = self.model.descriptionText;
    self.price.text = [[self.model.price stringValue] stringByAppendingString:@" ₽"];

    self.receptW.constant = self.model.hasRecipe ? 50 : 0;
    self.underOrderW.constant = self.model.underOrder ? 70 : 0;
    self.underOrderL.constant = self.model.hasRecipe ? 8 : 0;
    
    [self layoutIfNeeded];
}

- (void)updateFoto
{
    weaken(self);
    weakenObject(self.foto, weakFoto);
    [self.foto setImage:nil];
    if(self.model.image)
    {
        [self.foto setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.model.image] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30]
                     placeholderImage:nil
                              success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                  if (image && !weakFoto.image)
                                  {
                                      weakSelf.fotoH.constant = MIN(image.size.height, image.size.width) / MAX(image.size.height, image.size.width) * ViewWidth(weakSelf.foto);
                                      weakFoto.image = image;
                                  }
                              }
                              failure:nil];
    }
    else
    {
        UIImage * image = [UIImage imageNamed:@"empty_foto"];
        self.fotoH.constant = MIN(image.size.height, image.size.width) / MAX(image.size.height, image.size.width) * ViewWidth(self.foto);
        self.foto.image = image;
    }
}

- (void)updateBasket:(NSNotification *)notification
{
    NSString * identifier = notification.object;
    
    if (identifier == nil || [identifier isEqualToString:self.model.identifier] )
    {
        [self updateBasketStateAnimated:false];
        [self updateCounter];
    }
}

- (void)updateBasketStateAnimated:(BOOL)animated
{
    BOOL inBasket = self.model.count > 0;
    
    [UIView animateWithDuration:animated ? .2 : 0 animations:^{
        self.plusBtn.alpha     = inBasket ? 1 : 0;
        self.minusBtn.alpha    = inBasket ? 1 : 0;
        self.basketBtn.alpha   = inBasket ? 0 : 1;
        self.count.alpha       = inBasket ? 1 : 0;
    }];
}

- (void)updateCounter
{
    self.count.text = [NSString stringWithFormat:@"%ld", (long)self.model.count];
}

- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBasket:) name:kSaveBasketItemNotification object:nil];
}

- (void)removeObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
