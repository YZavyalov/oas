//
//  NITProductDescriptionTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 16.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDescriptionTableViewCell.h"

@interface NITProductDescriptionTableViewCell()

@property (nonatomic) IBOutlet UILabel * title;

@end

@implementation NITProductDescriptionTableViewCell

#pragma mark - custom accessors
- (void)setModel:(id)model delegate:(id)delegate
{
    self.model = model;
    
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.body;
}

@end
