//
//  NITProductDescriptionTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 16.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsBaseTableViewCell.h"

@interface NITProductDescriptionTableViewCell : NITProductDetailsBaseTableViewCell

@property (nonatomic) NITCatalogItemModel * model;

@end
