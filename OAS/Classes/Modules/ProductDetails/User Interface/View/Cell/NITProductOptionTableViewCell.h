//
//  NITProductAvailableTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 16.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsProtocols.h"
#import "NITProductDetailsBaseTableViewCell.h"
#import <UIKit/UIKit.h>

@protocol  NITProductOptionTableViewCellDelegate <NSObject>
@optional
- (void)didSelectAvailabel;
- (void)didSelectForms;
@end

@interface NITProductOptionTableViewCell : NITProductDetailsBaseTableViewCell

@property (nonatomic) NITProductOptionCellModel * model;
@property (nonatomic, weak) id <NITProductOptionTableViewCellDelegate> delegate;

@end
