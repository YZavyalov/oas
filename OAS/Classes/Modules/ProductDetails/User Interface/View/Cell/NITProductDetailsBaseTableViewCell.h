//
//  NITProductDetailsBaseTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 26.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsProtocols.h"
#import <UIKit/UIKit.h>

@interface NITProductDetailsBaseTableViewCell : UITableViewCell

- (void)setModel:(id)model delegate:(id)delegate;

@end
