//
//  NITProductAvailableTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 16.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductOptionTableViewCell.h"

@interface NITProductOptionTableViewCell()

@property (nonatomic) IBOutlet UILabel * title;

@end

@implementation NITProductOptionTableViewCell

#pragma mark - Custom accessors
- (void)setModel:(NITProductOptionCellModel *)model delegate:(id)delegate
{
    self.model = model;
    self.delegate = delegate;
    
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)selectAction:(id)sender
{
    if (self.model.mode == ProductOptionModeButton)
    {
        switch (self.model.type) {
            case ProductOptionTypeAvailable:
                if ([self.delegate respondsToSelector:@selector(didSelectAvailabel)])
                {
                    [self.delegate didSelectAvailabel];
                }
                break;
                
            case ProductOptionTypeForms:
                if ([self.delegate respondsToSelector:@selector(didSelectForms)])
                {
                    [self.delegate didSelectForms];
                }
                break;
        }
    }
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.title;
    self.title.font = self.model.font;
    self.title.textColor = self.model.color;
    
    switch (self.model.mode) {
        case ProductOptionModeButton:
            self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_next"]];
            break;
            
        case ProductOptionModeTitle:
            self.accessoryType = UITableViewCellAccessoryNone;
            self.accessoryView = nil;
            break;
    }
}

@end
