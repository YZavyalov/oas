//
//  NITProductDetailsViewController.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/16/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITProductDetailsViewController.h"
#import "NITProductDetailsBaseTableViewCell.h"
#import "NITProductDetailsTableViewCell.h"
#import "NITProductDescriptionTableViewCell.h"
#import "NITProductOptionTableViewCell.h"

@interface NITProductDetailsViewController()
<
UITableViewDelegate,
UITableViewDataSource,
NITProductDetailsCellDelegate,
NITProductOptionTableViewCellDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UIBarButtonItem * favoriteBtn;

@property (nonatomic) NSArray <NITProductDetailsSectionModel *> * sections;

@end

@implementation NITProductDetailsViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter updateData];
}

#pragma mark - IBAction
- (IBAction)favoriteAction:(id)sender
{
    [self.presenter changeFavoriteState];
}

#pragma mark - Private
- (void)initUI
{
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITProductDetailsTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITProductDetailsTableViewCell)];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITProductDescriptionTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITProductDescriptionTableViewCell)];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITProductOptionTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITProductOptionTableViewCell)];
}

#pragma mark - NITCatalogPresenterProtocol
- (void)reloadSectionByModels:(NSArray <NITProductDetailsSectionModel *> *)models
{
    self.sections = models;
    [self.tableView reloadData];
}

- (void)reloadFavoriteSate:(BOOL)active
{
    [self.favoriteBtn setTintColor:active ? RGB(56, 160, 144) : [UIColor blackColor]];
}

#pragma mark - UItableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NITProductDetailsSectionModel * sectionModel = self.sections[section];
    return sectionModel.models.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NITProductDetailsSectionModel * sectionModel = self.sections[section];
    return [sectionModel headerHeight:sectionModel.models.firstObject];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITProductDetailsSectionModel * sectionModel = self.sections[indexPath.section];
    return [sectionModel rowHeightByModel:sectionModel.models[indexPath.row]];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NITProductDetailsSectionModel * sectionModel = self.sections[section];
    return [sectionModel headerView];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITProductDetailsSectionModel * sectionModel = self.sections[indexPath.section];
    NITProductDetailsBaseTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:[sectionModel cellID] forIndexPath:indexPath];
    [cell setModel:sectionModel.models[indexPath.row] delegate:self];
    
    return cell;
}
    
#pragma mark - NITProductDetailsOptionCellDelegate
- (void)didSelectAvailabel
{
    [self.presenter showPharmacy];
}

- (void)didSelectForms
{
    [self.presenter showCatalogItemContiguous];
}

#pragma mark - NITProductDetailsCellDelegate
- (void)didAddItem:(NITCatalogItemModel *)model
{
    [self.presenter addToBasket];
}

- (void)didReduseItem:(NITCatalogItemModel *)model
{
    [self.presenter removeFromBasket];
}

- (void)didPresentImage:(UIImage *)model
{
    [self.presenter showImage:model];
}

@end
