//
//  NITProductDetailsViewController.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/16/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITProductDetailsProtocols.h"
#import "NITBaseViewController.h"

@interface NITProductDetailsViewController : NITBaseViewController <NITProductDetailsViewProtocol>

@property (nonatomic, strong) id <NITProductDetailsPresenterProtocol> presenter;

@end
