//
//  NITProductDetailsWireFrame.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/16/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITProductDetailsWireFrame.h"
#import "NITPharmacyWireFrame.h"
#import "NITCatalogDirectoryWireFrame.h"
#import "NITPresentPictureWireFrame.h"

@implementation NITProductDetailsWireFrame

#pragma mark - Privte
+ (id)createNITProductDetailsModule
{
    // Generating module components
    id <NITProductDetailsPresenterProtocol, NITProductDetailsInteractorOutputProtocol> presenter = [NITProductDetailsPresenter new];
    id <NITProductDetailsInteractorInputProtocol> interactor = [NITProductDetailsInteractor new];
    id <NITProductDetailsAPIDataManagerInputProtocol> APIDataManager = [NITProductDetailsAPIDataManager new];
    id <NITProductDetailsLocalDataManagerInputProtocol> localDataManager = [NITProductDetailsLocalDataManager new];
    id <NITProductDetailsWireFrameProtocol> wireFrame= [NITProductDetailsWireFrame new];
    id <NITProductDetailsViewProtocol> view = [(NITProductDetailsWireFrame *)wireFrame createViewControllerWithKey:_s(NITProductDetailsViewController) storyboardType:StoryboardTypeCatalog];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITProductDetailsWireFrameProtocol
+ (void)presentNITProductDetailsModuleFrom:(UIViewController*)fromViewController withModel:(NITCatalogItemModel *)model withType:(ProductDetailsViewType)viewType
{
    NITProductDetailsViewController * view = [NITProductDetailsWireFrame createNITProductDetailsModule];
    view.presenter.catalogItem = model;
    view.presenter.viewType = viewType;
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)showCatalogItemContiguousFromView:(UIViewController *)fromViewController withModel:(NITCatalogItemModel *)model
{
    [NITCatalogDirectoryWireFrame presentNITCatalogContiguousModuleFrom:fromViewController withModel:model];
}

- (void)showPharmacyFromView:(UIViewController *)fromViewController
{
    [NITPharmacyWireFrame presentNITPharmacyAvailableModuleFrom:fromViewController];
}

- (void)showBasketFromView:(UIViewController *)fromViewController
{
    [fromViewController.tabBarController setSelectedIndex:fromViewController.tabBarController.viewControllers.count - 1];
}

- (void)showImageWithView:(UIViewController *)fromViewController withImage:(UIImage *)model
{
    [NITPresentPictureWireFrame presentNITPresentPictureModuleFrom:fromViewController withImage:model];
}

@end
