//
//  NITProductDetailsWireFrame.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/16/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITProductDetailsProtocols.h"
#import "NITProductDetailsViewController.h"
#import "NITProductDetailsLocalDataManager.h"
#import "NITProductDetailsAPIDataManager.h"
#import "NITProductDetailsInteractor.h"
#import "NITProductDetailsPresenter.h"
#import "NITProductDetailsWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITProductDetailsWireFrame : NITRootWireframe <NITProductDetailsWireFrameProtocol>

@end
