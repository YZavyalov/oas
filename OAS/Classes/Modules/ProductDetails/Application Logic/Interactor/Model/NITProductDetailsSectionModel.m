//
//  NITProductDetailsSectionModel.m
//  OAS
//
//  Created by Yaroslav on 26.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITProductDetailsSectionModel.h"
#import "NITProductDetailsTableViewCell.h"
#import "NITProductOptionTableViewCell.h"
#import "NITProductDescriptionTableViewCell.h"

@implementation NITProductDetailsSectionModel

#pragma mark - Lifecycle
- (instancetype)initWithType:(ProductDetailsSectionType)type
{
    if (self = [super init])
    {
        self.type = type;
    }
    
    return self;
}

#pragma mark - Custom accessors
- (CGFloat)headerHeight:(id)model
{
    switch (self.type)
    {
        case ProductDetailsSectionTypeCard:         return 0;
        case ProductDetailsSectionTypeOption:       return 0;
        case ProductDetailsSectionTypeDescription:  return [(NITCatalogItemModel *)model body] ? 50 : 0;
    }
}

- (NSString *)cellID
{
    switch (self.type)
    {
        case ProductDetailsSectionTypeCard:         return _s(NITProductDetailsTableViewCell);
        case ProductDetailsSectionTypeOption:       return _s(NITProductOptionTableViewCell);
        case ProductDetailsSectionTypeDescription:  return _s(NITProductDescriptionTableViewCell);
    }
}

- (UIView *)headerView
{
    switch (self.type) {
        case ProductDetailsSectionTypeCard:
        case ProductDetailsSectionTypeOption:       return nil;
        case ProductDetailsSectionTypeDescription:  return [[[NSBundle mainBundle] loadNibNamed:@"NITDescriptionHeaderView" owner:self options:nil] firstObject];
    }
}

#pragma mark - Public
- (CGFloat)rowHeightByModel:(id)model
{
    switch (self.type)
    {
        case ProductDetailsSectionTypeCard: {
            
            NITCatalogItemModel * item = model;
            
            CGFloat k = 175.5 / 320.0;
            CGFloat titleH = [self calcHeightForText:item.name width:ViewWidth(WINDOW) * k font:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
            CGFloat detailH = [self calcHeightForText:item.descriptionText width:ViewWidth(WINDOW) * k font:[UIFont systemFontOfSize:14 weight:UIFontWeightRegular]];
            
            return MAX(ViewHeight(WINDOW) * 0.3, (titleH + detailH + 60 + ((item.hasRecipe || item.underOrder) ? 25 : 0)));
        };
        case ProductDetailsSectionTypeOption: {
            
            NITProductOptionCellModel * item = model;
            
            CGFloat k = 263.0 / 320.0;
            CGFloat height = [self calcHeightForText:item.title width:ViewWidth(WINDOW) * k font:item.font];
            
            return MAX(height, 44);
        }
        case ProductDetailsSectionTypeDescription: {
            
            CGFloat k = 296.0 / 320.0;
            CGFloat height = [self calcHeightForText:[(NITCatalogItemModel *)model body]  width:(ViewWidth(WINDOW) - 20) * k font:[UIFont systemFontOfSize:15 weight:UIFontWeightRegular]];
            
            return MAX(height + 10, 44);
        }
    }
}

#pragma mark - Private
- (CGFloat)calcHeightForText:(NSString *)text width:(CGFloat)width font:(UIFont *)font
{
    return [text boundingRectWithSize:(CGSize){width, CGFLOAT_MAX}
                              options:NSStringDrawingUsesLineFragmentOrigin
                           attributes:@{NSFontAttributeName : font}
                              context:nil].size.height;
}

@end
