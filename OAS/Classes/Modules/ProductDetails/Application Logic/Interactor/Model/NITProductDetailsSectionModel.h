//
//  NITProductDetailsSectionModel.h
//  OAS
//
//  Created by Yaroslav on 26.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef CF_ENUM (NSUInteger, ProductDetailsSectionType) {
    ProductDetailsSectionTypeCard           = 0,
    ProductDetailsSectionTypeOption         = 1,
    ProductDetailsSectionTypeDescription    = 2
};

@interface NITProductDetailsSectionModel : NSObject

@property (nonatomic) ProductDetailsSectionType type;
@property (nonatomic) NSArray * models;
@property (nonatomic) NSString * cellID;
@property (nonatomic) UIView * headerView;

- (instancetype)initWithType:(ProductDetailsSectionType)type;
- (CGFloat)rowHeightByModel:(id)model;
- (CGFloat)headerHeight:(id)model;

@end
