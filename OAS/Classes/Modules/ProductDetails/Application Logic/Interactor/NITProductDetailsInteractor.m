//
//  NITProductDetailsInteractor.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/16/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITProductDetailsInteractor.h"
#import "NITCatalogDirectoryLocalDataManager.h"

@implementation NITProductDetailsInteractor

#pragma mark - NITProductDetailsInteractorInputProtocol
- (void)getCatalogItemDetailsByModel:(NITCatalogItemModel *)model
{
    [self getCatalogItemContiguousWithModel:model];
}

- (void)getCatalogItemContiguousWithModel:(NITCatalogItemModel *)model
{
    weaken(self);
    [self.APIDataManager getCatalogItemContiguousByID:model.identifier withHandler:^(NSArray<SLCollectionRow *> *result) {
        [weakSelf.presenter updateCategoriesModels:[NITCatalogItemModel catalogItemModelsFromTupleEntities:result] withProductModel:model];
    }];
}

- (NSArray <NITCatalogItemModel *> *)getBasketItems
{
    return [self catalogItemsModelsFromBasket:[self.localDataManager getBasketItems]];
}

- (void)addItemToBasket:(NITCatalogItemModel *)model
{
    NITCatalogDirectoryLocalDataManager * dm = [NITCatalogDirectoryLocalDataManager new];
    [dm addItemToBasket:model];
}

- (void)removeItemFromBasket:(NITCatalogItemModel *)model
{
    NITCatalogDirectoryLocalDataManager * dm = [NITCatalogDirectoryLocalDataManager new];
    [dm removeItemFromBasket:model];
}

- (void)changeFavoriteStateForModel:(NITCatalogItemModel *)model
{
    if (model.favorite)
    {
        [self.localDataManager removeFavoriteItem:model];
    }
    else
    {
        [self.localDataManager addFavoriteItem:model];
    }
}

- (void)addFavoriteItem:(NITCatalogItemModel *)model
{
    [self.localDataManager addFavoriteItem:model];
}

#pragma mark - Private
- (NSArray <NITCatalogItemModel *> *)catalogItemsModelsFromBasket:(NSArray <NITBasketItem *> *)entities
{
    return [entities bk_map:^id(NITBasketItem * obj) {
        return [[NITCatalogItemModel alloc] initWithBasketModel:obj];
    }];
}

@end
