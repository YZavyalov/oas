//
//  NITProductDetailsInteractor.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/16/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITProductDetailsProtocols.h"

@interface NITProductDetailsInteractor : NSObject <NITProductDetailsInteractorInputProtocol>

@property (nonatomic, weak) id <NITProductDetailsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITProductDetailsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITProductDetailsLocalDataManagerInputProtocol> localDataManager;

@end
