//
//  NITProductDetailsAPIDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/16/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITProductDetailsAPIDataManager.h"
#import "SLCatalogsApi.h"
#import "SLCollectionsApi.h"
#import "SLShopsApi.h"

@implementation NITProductDetailsAPIDataManager

#pragma mark - NITProductDetailsAPIDataManagerInputProtocol
- (void)getCatalogItemContiguousByID:(NSString *)itemID withHandler:(void(^)(NSArray<SLCollectionRow *> *result))handler
{
    [HELPER startLoading];
    SLCollectionRow * body = [SLCollectionRow new];
    body.key = @"related_items"; body.value = itemID;    
    [[SLCollectionsApi new] postStaticCollectionByCollectionkeyTupleSelectWithCollectionKey:@"related_products" body:body completionHandler:^(NSArray<NSArray<SLCollectionRow> *> *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler (output.firstObject);
    }];
}

@end
