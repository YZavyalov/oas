//
//  NITProductDetailsLocalDataManager.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/16/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITProductDetailsProtocols.h"

@interface NITProductDetailsLocalDataManager : NSObject <NITProductDetailsLocalDataManagerInputProtocol>

@end
