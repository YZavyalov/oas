//
//  NITProductDetailsLocalDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/16/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITProductDetailsLocalDataManager.h"
#import "NITFavoriteItem.h"
#import "NITBasketItem.h"

@implementation NITProductDetailsLocalDataManager

#pragma mark - NITProductDetailsLocalDataManagerInputProtocol
- (NSArray <NITBasketItem *> *)getBasketItems
{
    return [NITBasketItem allBasketItems];
}

- (void)addFavoriteItem:(NITCatalogItemModel *)model
{
    [NITFavoriteItem createFavoriteItemByModel:model];
}

- (void)removeFavoriteItem:(NITCatalogItemModel *)model
{
    [NITFavoriteItem deleteFavoriteItemByID:model.identifier];
}

@end
