//
//  NITCatalogPresenter.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogProtocols.h"

@class NITCatalogWireFrame;

@interface NITCatalogPresenter : NITRootPresenter <NITCatalogPresenterProtocol, NITCatalogInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCatalogViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogWireFrameProtocol> wireFrame;

@end
