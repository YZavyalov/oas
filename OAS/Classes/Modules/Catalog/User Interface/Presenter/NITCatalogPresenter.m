//
//  NITCatalogPresenter.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogPresenter.h"
#import "NITCatalogWireframe.h"
#import "NITMobileConstantsModel.h"

@interface NITCatalogPresenter ()

@property (nonatomic) NSArray <NITCatalogSectionModel *> * sections;

@end

@implementation NITCatalogPresenter

#pragma mark - Customs accessors
- (NSArray<NITCatalogSectionModel *> *)sections
{
    if (_sections.count != 2)
    {
        NITCatalogSectionModel * stocks = [[NITCatalogSectionModel alloc] initWithType:CatalogSectionTypeStock];
        NITCatalogSectionModel * categories = [[NITCatalogSectionModel alloc] initWithType:CatalogSectionTypeCategory];
        _sections = @[stocks, categories];
    }
    
    return _sections;
}

#pragma mark - NITCatalogPresenterProtocol
- (void)updateData
{
    [self addObservers];
    [self initData];
}

- (void)initData
{
    [self.interactor getStocksAndCategories];
    //[self.interactor getStocks];
    //[self.interactor getCategories];
}

- (void)updateStocks
{
    [STOCKS updateStocks];
}

- (void)contactAction
{
    NSString * phone = [[NITMobileConstantsModel new] contact_phone];
    [HELPER callToPhoneNumber:phone];
}

- (void)searchByText:(NSString *)text
{
    if (text.length > 2)
    {
        [self.interactor searchByText:text];
    }
}

- (void)showCategoryByModel:(NITGroupModel *)model
{
    if([model.identifier isEqualToString:kProdCatalogGroup])
        [self.wireFrame showCatalogItemsFrom:self.view withModel:model];
    else
        [self.wireFrame showCategoryFrom:self.view withModel:model];
}

- (void)showStockByModel:(NITInfoItemModel *)model
{
    switch (model.type) {
        case InfoItemTypeCategory:
        {
            [self.wireFrame showBannersFrom:self.view withModel:model];
        } break;

        case InfoItemTypeCatalogItem:
        {
            if(model.events_value.count > 1)
                [self.wireFrame showBannersItemsFrom:self.view withModel:model];
            else if(model.events_value.count == 1)
                [self.interactor getBannerItem:model.events_value.firstObject];
        } break;
            
        case InfoItemTypeLink: {
            if(model.events_value.firstObject.length > 4)
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.events_value.firstObject]];
        } break;
            
        case InfoItemTypeNone: {
            //None
        } break;
    }
    
    [TRACKER trackEvent:TrackerEventOpenStock value:model.text];
}

- (void)showCatalogItemByModel:(NITCatalogItemModel *)model
{
    [self.wireFrame showCatalogItemDetailsFrom:self.view withModel:model];
}

- (void)showSearchItemsWithSearchText:(NSString *)text
{
    [self.wireFrame showSearchItemsWithViewController:self.view withSearchText:text];
}

- (void)loadItemDetails:(NITInfoItemModel *)model
{
    [self.interactor loadItemDetails:model];
}

#pragma mark - NITCatalogInteractorOutputProtocol
- (void)updateCategoriesModels:(NSArray<NITGroupModel *> *)models
{
    NITCatalogSectionModel * categories = self.sections[CatalogSectionTypeCategory];
    categories.models = models;
    [self.view reloadSectionByModels:self.sections];
}

- (void)updateStockModels:(NSArray<NITInfoItemModel *> *)models
{
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"sort" ascending:true selector:@selector(compare:)];
    models = [models sortedArrayUsingDescriptors:@[sort]];
   
    NSMutableArray <NITInfoItemModel *> * availableModels = [NSMutableArray new];
    NSDate * date = [NSDate date];
    [models bk_each:^(NITInfoItemModel *obj) {
        NSDate * startDate = [NSDate dateWithTimeIntervalSince1970:[obj.start intValue]];
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = 1;
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *endDate = [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate dateWithTimeIntervalSince1970:[obj.end intValue]] options:0];
        BOOL available = [self date:date isBetweenDate:startDate andDate:endDate];
        BOOL active = [obj.active intValue] == 0 ? false : true;
        if(available && active) [availableModels addObject:obj];
    }];
    models = [availableModels mutableCopy];

    NITCatalogSectionModel * stocks = self.sections[CatalogSectionTypeStock];
    if(models.count > 0)
    {
        NSMutableArray <NITInfoItemModel *> * loopModels = [[NSMutableArray alloc] init];
        [loopModels addObject:models.lastObject];
        for(NITInfoItemModel * model in models)
        {
            [loopModels addObject:model];
        }
        [loopModels addObject:models.firstObject];
        stocks.models = loopModels;
        
        [models bk_each:^(NITInfoItemModel *obj) {
            if(obj.image)
            {
                stocks.rowHeight = MIN(obj.image.size.height, obj.image.size.width) / MAX(obj.image.size.height, obj.image.size.width) * ViewWidth(WINDOW);
            }
        }];
    }
    else
    {
        stocks.rowHeight = 0;
    }
    [HELPER stopLoading];
    [self.view reloadSectionByModels:self.sections];
}

- (void)updateSearchResultModels:(NSArray *)models
{
    [self.view reloadSearchResultByModels:models];
}

- (void)showBannerItem:(NITCatalogItemModel *)model
{
    [self.wireFrame showBannersItemFrom:self.view withModel:model];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initData) name:kUpdateInternetConnect object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAlert) name:kShowSuccessOrderAlert object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showPickupAlert) name:kShowSuccessPickupOrderAlert object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBanners:) name:kUpdateStockNotification object:nil];
}

- (void)updateBanners:(NSNotification *)notification
{
    NSArray <NITInfoItemModel *> * models = notification.object;
    NITCatalogSectionModel * stocks = self.sections[CatalogSectionTypeStock];
    if(models.count > 0)
    {
        NSMutableArray <NITInfoItemModel *> * loopModels = [[NSMutableArray alloc] init];
        [loopModels addObject:models.lastObject];
        for(NITInfoItemModel * model in models)
        {
            [loopModels addObject:model];
        }
        [loopModels addObject:models.firstObject];
        stocks.models = loopModels;
        
        [models bk_each:^(NITInfoItemModel *obj) {
            if(obj.image)
            {
                stocks.rowHeight = MIN(obj.image.size.height, obj.image.size.width) / MAX(obj.image.size.height, obj.image.size.width) * ViewWidth(WINDOW);
            }
        }];
    }
    else
    {
        stocks.models = [NSArray new];
        stocks.rowHeight = 0;
    }
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.view reloadSectionByModels:self.sections];
    });
}

- (void)appWillResignActive:(NSNotification*)note
{
    [self updateStocks];
}

- (void)showAlert
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Ваш заказ оформлен", nil)
                                                                    message:NSLocalizedString(@"Мы свяжемся с вами и уточним детали", nil)
                                                             preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = RGB(58, 160, 144);
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ок", nil)
                                                      style:UIAlertActionStyleCancel
                                                    handler:^(UIAlertAction * action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                    }];
    
    [alert addAction:cancel];
    
    [self.wireFrame presentAlertFrom:self.view withAlert:alert];
}

- (void)showPickupAlert
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Ваш заказ оформлен", nil)
                                                                    message:NSLocalizedString(@"Вы получите информацию о статусе заказа по смс", nil)
                                                             preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = RGB(58, 160, 144);
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ок", nil)
                                                      style:UIAlertActionStyleCancel
                                                    handler:^(UIAlertAction * action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                    }];
    
    [alert addAction:cancel];
    
    [self.wireFrame presentAlertFrom:self.view withAlert:alert];
}

- (BOOL)date:(NSDate*)nowdate isBetweenDate:(NSDate*)raceStartDate andDate:(NSDate*)raceEndDate
{
    if ([nowdate compare:raceStartDate] == NSOrderedAscending)
        return NO;
    
    if ([nowdate compare:raceEndDate] == NSOrderedDescending)
        return NO;
    
    return YES;
}

@end
