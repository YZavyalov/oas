//
//  NITCatalogTableViewCell.h
//  OAS
//
//  Created by Eugene Parafiynyk on 4/21/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITCatalogTableViewCell : UITableViewCell

- (void)setModel:(id)model delegate:(id)delegate;

@end
