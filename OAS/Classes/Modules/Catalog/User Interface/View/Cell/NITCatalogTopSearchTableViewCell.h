//
//  NITCatalogTopSearchTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 16.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

@class NITCatalogItemModel;

@interface NITCatalogTopSearchTableViewCell : UITableViewCell

@property (nonatomic) NITCatalogItemModel * model;

@end
