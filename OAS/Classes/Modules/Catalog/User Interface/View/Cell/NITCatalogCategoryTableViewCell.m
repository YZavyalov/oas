//
//  NITCatalogCategoryTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 14.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogCategoryTableViewCell.h"
#import "NITGroupModel.h"

@interface NITCatalogCategoryTableViewCell()

@property (nonatomic) IBOutlet UILabel * title;

@end

@implementation NITCatalogCategoryTableViewCell

#pragma mark - Custom accessors
- (void)setModel:(id)model delegate:(id)delegate
{
    self.model = model;
}

- (void)setModel:(NITGroupModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.name;
}

@end
