//
//  NITCatalogCategoryTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 14.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCatalogTableViewCell.h"

@class NITGroupModel;

@interface NITCatalogCategoryTableViewCell : NITCatalogTableViewCell

@property (nonatomic) NITGroupModel * model;

@end
