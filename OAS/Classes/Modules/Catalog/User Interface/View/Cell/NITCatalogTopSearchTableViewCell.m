//
//  NITCatalogTopSearchTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 16.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogTopSearchTableViewCell.h"
#import "NITCatalogItemModel.h"

@interface NITCatalogTopSearchTableViewCell()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UILabel * price;
@property (nonatomic) IBOutlet NSLayoutConstraint * priceW;

@end

@implementation NITCatalogTopSearchTableViewCell

#pragma mark - Custom accessors
- (void)setModel:(NITCatalogItemModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    NSString *text = [NSString stringWithFormat:@"%@ %@", self.model.name, self.model.spec_prop];
    
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName: self.title.textColor,
                              NSFontAttributeName: self.title.font
                              };
    NSMutableAttributedString *attributedText =
    [[NSMutableAttributedString alloc] initWithString:text
                                           attributes:attribs];
    
    UIColor *blackColor = [UIColor blackColor];
    NSRange blackTextRange = [text rangeOfString:self.model.name];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:blackColor}
                            range:blackTextRange];
    
    UIColor *lightGrayColor = [UIColor lightGrayColor];
    NSRange lightGrayTextRange = [text rangeOfString:self.model.spec_prop];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:lightGrayColor}
                            range:lightGrayTextRange];
    
    self.title.attributedText = attributedText;
    
    //self.title.text = [NSString stringWithFormat:@"%@ %@", self.model.name, self.model.spec_prop];
    self.price.text = [[self.model.price stringValue] stringByAppendingString:@" ₽"];
    self.priceW.constant = [self.price sizeOfMultiLineLabel].width;
}

@end
