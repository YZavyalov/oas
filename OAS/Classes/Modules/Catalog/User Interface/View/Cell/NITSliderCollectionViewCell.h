//
//  NITSliderCollectionViewCell.h
//  OAS
//
//  Created by Yaroslav on 14.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NITInfoItemModel;

@protocol NITSliderCellDelegate <NSObject>
@optional
- (void)didSelectSliderModel:(NITInfoItemModel *)model;
- (void)didShowImageWithSize:(CGSize)size;
- (void)didAddItemToBasket:(NITInfoItemModel *)model;
@end

@interface NITSliderCollectionViewCell : UICollectionViewCell

@property (nonatomic) NITInfoItemModel * model;
@property (nonatomic, weak) id <NITSliderCellDelegate> delegate;

- (void)updateImage;

@end
