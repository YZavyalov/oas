
//  NITSliderCollectionViewCell.m
//  OAS
//
//  Created by Yaroslav on 14.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITSliderCollectionViewCell.h"
#import "NITInfoItemModel.h"
#import "NITAutoScrollCollectionView.h"

@interface NITSliderCollectionViewCell()

@property (nonatomic) IBOutlet UIImageView * imageView;
@property (nonatomic) IBOutlet UIImageView * basketView;
@property (nonatomic) IBOutlet UIButton * basketBtn;

@end

@implementation NITSliderCollectionViewCell

#pragma mark - Custom accessors
- (void)setModel:(NITInfoItemModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)basketAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didAddItemToBasket:)])
    {
        [self.delegate didAddItemToBasket:self.model];
    }
}

- (IBAction)sldierAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didSelectSliderModel:)])
    {
        [self.delegate didSelectSliderModel:self.model];
    }
}

#pragma mark - Public
- (void)updateImage
{
    //TODO: мигание картинки
    /*self.imageView.image = nil;
    __weak UIImageView * weakImageView = self.imageView;
    
    weaken(self);
    [self.imageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.model.imagePath]]
                          placeholderImage:nil
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                       if (image && !weakImageView.image)
                                       {
                                           weakImageView.image = image;
                                           if ([weakSelf.delegate respondsToSelector:@selector(didShowImageWithSize:)])
                                           {
                                               CGFloat height = MIN(image.size.height, image.size.width) / MAX(image.size.height, image.size.width) * ViewWidth(WINDOW);
                                               [weakSelf.delegate didShowImageWithSize:CGSizeMake(ViewWidth(WINDOW), height)];
                                           }
                                       }
                                   }
                                   failure:nil];*/
    
    if (self.model.image && self.imageView.image)
    {
        self.imageView.image = self.model.image;
        if ([self.delegate respondsToSelector:@selector(didShowImageWithSize:)])
        {
            CGFloat height = MIN(self.model.image.size.height, self.model.image.size.width) / MAX(self.model.image.size.height, self.model.image.size.width) * ViewWidth(WINDOW);
            [self.delegate didShowImageWithSize:CGSizeMake(ViewWidth(WINDOW), height)];
        }
    }

}

#pragma mark - Private
- (void)updateData
{
    self.imageView.image = nil;
    if(self.model.image)
    {
        [self.imageView setImage:self.model.image];
    }
    //[self.imageView setImageWithURL:[NSURL URLWithString:self.model.imagePath]];
    
    if(self.model.type == InfoItemTypeCatalogItem)
    {
        self.basketView.hidden = false;
        [self.basketBtn setEnabled:true];
    }
    else
    {
        self.basketView.hidden = true;
        [self.basketBtn setEnabled:false];
    }
}

@end
