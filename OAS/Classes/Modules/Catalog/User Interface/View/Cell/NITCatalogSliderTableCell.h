//
//  NITCatalogSliderTableCell.h
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/4/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITSliderCollectionViewCell.h"
#import "NITCatalogTableViewCell.h"

@class NITCatalogSectionModel, NITInfoItemModel;

@protocol NITCatalogSliderTableCellDelegate <NSObject>
@optional
- (void)didSelectStockModel:(NITInfoItemModel *)model;
- (void)didShowImageWithHeight:(CGFloat)height;
- (void)didAddInfoItemToBasket:(NITInfoItemModel *)model;
@end

@interface NITCatalogSliderTableCell : NITCatalogTableViewCell

@property (nonatomic) NITCatalogSectionModel * model;
@property (nonatomic, weak) id <NITCatalogSliderTableCellDelegate> delegate;

@end
