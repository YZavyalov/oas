//
//  NITCatalogSliderTableCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 11/4/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogSliderTableCell.h"
#import "NITSliderCollectionViewCell.h"
#import "NITAutoScrollCollectionView.h"
#import "NITCatalogSectionModel.h"
#import "NITInfoItemModel.h"

static NSInteger const sliderInterval = 5;

@interface NITCatalogSliderTableCell ()
<
UICollectionViewDelegate,
UICollectionViewDataSource,
UIScrollViewDelegate,
NITSliderCellDelegate
>

@property (nonatomic) IBOutlet NITAutoScrollCollectionView * collectionView;
@property (nonatomic) IBOutlet UIPageControl * pageControl;

@property (nonatomic) IBOutlet NSLayoutConstraint * collectionLeading;
@property (nonatomic) IBOutlet NSLayoutConstraint * collectionTrailing;

@end

@implementation NITCatalogSliderTableCell

#pragma mark - Life cycle
- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self addObservers];
    [self initUI];
}

- (void)dealloc
{
    [self removeObservers];
}

#pragma mark - Public
- (void)setModel:(id)model delegate:(id)delegate
{
    self.delegate = delegate;
    self.model = model;
}

#pragma mark - Custom accessors
- (void)setModel:(NITCatalogSectionModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)pageControlAction:(id)sender
{
    [self.collectionView setAutoScrollTimeInterval:0];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:[self.pageControl currentPage] inSection:0]
                                atScrollPosition:UICollectionViewScrollPositionNone
                                        animated:true];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enableScroll) name:kEnableScrollBanners object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disableScroll) name:kDisableScrollBanners object:nil];
}

- (void)removeObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)enableScroll
{
    [self.collectionView setScrollEnabled:true];
}

- (void)disableScroll
{
    [self.collectionView setScrollEnabled:false];
}

- (void)initUI
{
    [self.collectionView registerNib:[UINib nibWithNibName:_s(NITSliderCollectionViewCell) bundle:nil] forCellWithReuseIdentifier:_s(NITSliderCollectionViewCell)];
    UICollectionViewFlowLayout * flowLayout = (id)self.collectionView.collectionViewLayout;
    flowLayout.itemSize = CGSizeMake(ViewWidth(WINDOW), ViewHeight(self));
    
    self.collectionView.autoScrollTimeInterval = sliderInterval;
    self.collectionView.stopAutoScrollAfterSwipe = true;
    
    [self.pageControl setCurrentPageIndicatorTintColor:RGB(58, 160, 144)];
}

- (void)updateData
{
    UICollectionViewFlowLayout * flowLayout = (id)self.collectionView.collectionViewLayout;
    [[self.model models] bk_each:^(NITInfoItemModel *obj) {
        if(obj.image)
        {
            CGFloat height = MIN(obj.image.size.height, obj.image.size.width) / MAX(obj.image.size.height, obj.image.size.width) * ViewWidth(WINDOW);
            flowLayout.itemSize = CGSizeMake(ViewWidth(WINDOW), height);
        }
    }];
    
    self.collectionView.cellCount = [self.model models].count;
    self.pageControl.numberOfPages = [self.model models].count - 2;
    self.collectionView.currentIndex = [self.model models].count > 0 ? [NSIndexPath indexPathForRow:0 inSection:0] : nil;
    
    [self.collectionView reloadData];
    [self.collectionView performBatchUpdates:^{} completion:^(BOOL finished) {
        [self updateCurrentImage];
    }];
    
    [self.collectionView scrollRectToVisible:CGRectMake(ViewWidth(self.collectionView), 0, ViewWidth(self.collectionView), ViewHeight(self.collectionView)) animated:NO];
    
    [self updatePageControlDot];
    
    if([self.model models].count == 3)
    {
        self.pageControl.hidden = true;
        [self.collectionView setScrollEnabled:false];
        self.collectionView.autoScrollTimeInterval = 0;
    }
    else
    {
        self.pageControl.hidden = false;
        [self.collectionView setScrollEnabled:true];
    }
    self.pageControl.currentPage = 0;
}

- (void)updateCurrentImage
{
    NSArray <NITSliderCollectionViewCell *> * cells = [self.collectionView visibleCells];
    [cells bk_each:^(NITSliderCollectionViewCell *obj) {
        [obj updateImage];
    }];
}

- (void)updatePageControlDot
{
    for (int i = 0; i < self.pageControl.numberOfPages; i++)
    {
        UIView* dot = [_pageControl.subviews objectAtIndex:i];
        if (i == _pageControl.currentPage) {
            dot.backgroundColor = RGB(58, 160, 144);
            dot.layer.cornerRadius = dot.frame.size.height / 2;
            dot.layer.borderColor = [UIColor colorWithRed:58.0f / 255.0f green:160.0f/255.0f blue:144.0f/255.0f alpha:1.0f].CGColor;
            dot.layer.borderWidth = 0.5;
        } else {
            dot.backgroundColor = RGB(255, 255, 255);
            dot.layer.cornerRadius = dot.frame.size.height / 2;
            dot.layer.borderColor = [UIColor colorWithRed:188.0f / 255.0f green:196.0f/255.0f blue:195.0f/255.0f alpha:1.0f].CGColor;
            dot.layer.borderWidth = 0.5;
        }
    }
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.model models].count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NITSliderCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:_s(NITSliderCollectionViewCell) forIndexPath:indexPath];
    cell.model = [self.model models][indexPath.row];
    cell.delegate = self;
    
    return cell;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x == 0)
    {
        [scrollView scrollRectToVisible:CGRectMake((self.model.models.count - 2) * ViewWidth(WINDOW), 0 , ViewWidth(WINDOW), ViewHeight(self)) animated:NO];
        self.pageControl.currentPage = self.model.models.count - 1;
    }
    else if (scrollView.contentOffset.x == (self.model.models.count - 1) * ViewWidth(self.collectionView))
    {
        [scrollView scrollRectToVisible:CGRectMake(ViewWidth(WINDOW), 0, ViewWidth(WINDOW), ViewHeight(self)) animated:NO];
        self.pageControl.currentPage = 0;
    }
    else
    {
        self.pageControl.currentPage = (self.collectionView.contentOffset.x / ViewWidth(self.collectionView)) - 1;
    }
    
    self.collectionView.currentIndex = [NSIndexPath indexPathForRow:self.pageControl.currentPage inSection:0];
    
    [self updateCurrentImage];
    [self updatePageControlDot];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [self updateCurrentImage];
}

#pragma mark - NITCatalogSliderCellDelegate
- (void)didSelectSliderModel:(NITInfoItemModel *)model
{
    if ([self.delegate respondsToSelector:@selector(didSelectStockModel:)])
    {
        [self.delegate didSelectStockModel:model];
    }
}

- (void)didAddItemToBasket:(NITInfoItemModel *)model
{
    if ([self.delegate respondsToSelector:@selector(didAddInfoItemToBasket:)])
    {
        [self.delegate didAddInfoItemToBasket:model];
    }
}

- (void)didShowImageWithSize:(CGSize)size
{
    UICollectionViewFlowLayout * flowLayout = (id)self.collectionView.collectionViewLayout;
    flowLayout.itemSize = size;
    
    if ([self.delegate respondsToSelector:@selector(didShowImageWithHeight:)])
    {
        [self.delegate didShowImageWithHeight:size.height];
    }
}

@end
