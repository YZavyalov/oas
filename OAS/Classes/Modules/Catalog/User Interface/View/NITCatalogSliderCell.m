//
//  NITCatalogSliderCell.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/25/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogSliderCell.h"
#import "UIImageView+AFNetworking.h"

@interface NITCatalogSliderCell ()

@property (nonatomic) IBOutlet UIImageView * imageView;

@end

@implementation NITCatalogSliderCell

- (void)setModel:(NSString *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)selectAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didSelectSliderModel:)])
    {
        [self.delegate didSelectSliderModel:self.model];
    }
}

#pragma mark - Private
- (void)updateData
{
    self.imageView.image = nil;
}

@end
