//
//  NITCatalogSliderTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 13.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITCatalogSliderTableViewCell : UITableViewCell

@property (nonatomic) NSArray <NSString *> * sliderItems;
@property (nonatomic, weak) id <NITCatalogSliderCellDelegate> delegate;

@end
