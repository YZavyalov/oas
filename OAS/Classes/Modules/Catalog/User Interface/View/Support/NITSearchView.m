//
//  NITSearchView.m
//  OAS
//
//  Created by Eugene Parafiynyk on 4/22/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITSearchView.h"
#import "NITCatalogTopSearchTableViewCell.h"
#import "NITCatalogItemModel.h"

@interface NITSearchView ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) NSArray <NITCatalogItemModel *> * items;

@property (nonatomic) IBOutlet NSLayoutConstraint * tableViewBottom;

@end

@implementation NITSearchView

#pragma mark - Life cycle
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self initUI];
    }
    
    return self;
}

#pragma mark - Public
- (void)reloadDataByModels:(NSArray *)models
{
    self.items = models;
    [self.tableView reloadData];
}

#pragma mark - Private
- (void)initUI
{
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITCatalogTopSearchTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITCatalogTopSearchTableViewCell)];
    [self.tableView setTableFooterView:[UIView new]];
    
    //Keyboard
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    [self layoutIfNeeded];
    self.tableViewBottom.constant = [self keyboardHeight:notification] - 50;
    [UIView animateWithDuration:0.25 animations:^{
        [self layoutIfNeeded];
    }];
}


- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    [self layoutIfNeeded];
    self.tableViewBottom.constant = 0;
    [UIView animateWithDuration:0.25 animations:^{
        [self layoutIfNeeded];
    }];
}

- (CGFloat)keyboardHeight:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    return keyboardFrameBeginRect.size.height;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCatalogTopSearchTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITCatalogTopSearchTableViewCell) forIndexPath:indexPath];
    cell.model = self.items[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(didSelectSearchModel:)])
    {
        [self.delegate didSelectSearchModel:self.items[indexPath.row]];
    }
}


@end
