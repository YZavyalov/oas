//
//  NITEmptySearchView.m
//  OAS
//
//  Created by Eugene Parafiynyk on 4/21/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITEmptySearchView.h"

@implementation NITEmptySearchView

#pragma mark - IBAction
- (IBAction)contactAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didPressContact)])
    {
        [self.delegate didPressContact];
    }
}

@end
