//
//  NITSearchView.h
//  OAS
//
//  Created by Eugene Parafiynyk on 4/22/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XibView.h"

@class NITCatalogItemModel;

@protocol NITSearchViewDelegate <NSObject>

- (void)didSelectSearchModel:(NITCatalogItemModel *)model;

@end

@interface NITSearchView : XibView

@property (nonatomic, weak) id <NITSearchViewDelegate> delegate;

- (void)reloadDataByModels:(NSArray *)models;

@end
