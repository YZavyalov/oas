//
//  NITEmptySearchView.h
//  OAS
//
//  Created by Eugene Parafiynyk on 4/21/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XibView.h"

@protocol NITEmptySearchViewDelegate <NSObject>

- (void)didPressContact;

@end

@interface NITEmptySearchView : XibView

@property (nonatomic, weak) id <NITEmptySearchViewDelegate> delegate;

@end
