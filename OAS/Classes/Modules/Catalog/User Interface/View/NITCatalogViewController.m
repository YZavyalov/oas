//
//  NITCatalogViewController.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogViewController.h"
#import "NITCatalogTableViewCell.h"
#import "NITCatalogCategoryTableViewCell.h"
#import "NITCatalogSliderCell.h"
#import "NITCatalogSliderTableCell.h"
#import "NITCatalogTopSearchTableViewCell.h"
#import "NITEmptySearchView.h"
#import "NITSearchView.h"
#import "SLCatalogsApi.h"
#import "SLApiClient.h"
#import "SLInfosystemsApi.h"

@interface NITCatalogViewController()
<
UISearchBarDelegate,
UITableViewDelegate,
UITableViewDataSource,
UIScrollViewDelegate,
NITCatalogSliderTableCellDelegate,
NITSearchViewDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UIView * blurView;
@property (nonatomic) NSArray <NITCatalogSectionModel *> * sections;

@end

@implementation NITCatalogViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter updateData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.presenter updateStocks];
}

#pragma mark - Private
- (void)initUI
{
    self.title = NSLocalizedString(@"Каталог", nil);
    
    self.searchBar.delegate = self;
    self.searchView.delegate = self;

    // table view
    [self.tableView registerNibArray:@[_s(NITCatalogCategoryTableViewCell), _s(NITCatalogSliderTableCell)]];
    [self.tableView setTableFooterView:[UIView new]];
    
    [self.navigationItem setTitleView:self.searchBar];
    [self initSearchBar];
}

#pragma mark - UIButton
- (IBAction)hideBlurView:(id)sender
{
    self.blurView.hidden = true;
}

#pragma mark - NITCatalogPresenterProtocol
- (void)reloadSectionByModels:(NSArray <NITCatalogSectionModel *> *)models
{
    self.sections = models;
    [self.tableView reloadData];
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

- (void)reloadSearchResultByModels:(NSArray <NITCatalogItemModel *> *)models
{
    [self.searchView setHidden:models.count > 0 ? false : true];
    [self.blurView setHidden:false];
    [self.searchView reloadDataByModels:models];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NITCatalogSectionModel * sectionModel = self.sections[section];
    switch (sectionModel.type) {
        case CatalogSectionTypeStock:       return 1;
        case CatalogSectionTypeCategory:    return [sectionModel.models count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCatalogSectionModel * sectionModel = self.sections[indexPath.section];
    return [sectionModel rowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCatalogSectionModel * sectionModel = self.sections[indexPath.section];
    NITCatalogTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:[sectionModel cellID] forIndexPath:indexPath];
    
    switch (sectionModel.type) {
        case CatalogSectionTypeStock:
            [cell setModel:sectionModel delegate:self];
            break;
        case CatalogSectionTypeCategory:
            [cell setModel:sectionModel.models[indexPath.row] delegate:self];
            break;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self searchBarCancelButtonClicked:self.searchBar];
    NITCatalogSectionModel * sectionModel = self.sections[indexPath.section];
    [self.presenter showCategoryByModel:sectionModel.models[indexPath.row]];
}

#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchText.length == 0) self.blurView.hidden = true;
    else                       self.blurView.hidden = false;
    if(searchText.length < 3)   self.searchView.hidden = true;
    
    [self.presenter searchByText:searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.searchBar.text = @"";
    self.searchBar.showsCancelButton = false;
    [self.searchBar endEditing:true];
    
    self.searchView.hidden = true;
    self.blurView.hidden = true;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar endEditing:true];
    [self enableSearchBarCanelButton];
    [self.presenter showSearchItemsWithSearchText:searchBar.text];
}

#pragma mark - NITCatalogSliderTableCellDelegate
- (void)didSelectStockModel:(NITInfoItemModel *)model
{
    [self searchBarCancelButtonClicked:self.searchBar];
    [self.presenter showStockByModel:model];
}

- (void)didAddInfoItemToBasket:(NITInfoItemModel *)model
{
    [self.presenter loadItemDetails:model];
}

- (void)didShowImageWithHeight:(CGFloat)height
{    
    NITCatalogSectionModel * sectionModel = self.sections[CatalogSectionTypeStock];
    sectionModel.rowHeight = height;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

#pragma mark - ScrollDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView.contentOffset.y <= 0)
    {
        scrollView.contentOffset = CGPointZero;
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kDisableScrollBanners object:nil];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kEnableScrollBanners object:nil];
}

#pragma mark - NITSearchViewDelegate
- (void)didSelectSearchModel:(NITCatalogItemModel *)model
{
    [self.searchBar endEditing:true];
    [self.presenter showCatalogItemByModel:model];
}

#pragma mark - NITEmptySearchViewDelegate
- (void)didPressContact
{
    [self.presenter contactAction];
}

@end
