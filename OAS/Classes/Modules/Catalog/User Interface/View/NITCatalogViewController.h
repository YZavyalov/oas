//
//  NITCatalogViewController.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCatalogProtocols.h"

@interface NITCatalogViewController : NITBaseViewController <NITCatalogViewProtocol>

@property (nonatomic, strong) id <NITCatalogPresenterProtocol> presenter;

@end
