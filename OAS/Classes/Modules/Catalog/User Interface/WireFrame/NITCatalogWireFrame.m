//
//  NITCatalogWireFrame.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogWireFrame.h"
#import "NITCatalogCategoryWireFrame.h"
#import "NITCatalogDirectoryWireFrame.h"
#import "NITProductDetailsWireFrame.h"
#import "NITProductDetailsWireFrame.h"

@implementation NITCatalogWireFrame

#pragma mark - NITCatalogWireFrameProtocol
+ (id)createNITCatalogModule
{
    // Generating module components
    id <NITCatalogPresenterProtocol, NITCatalogInteractorOutputProtocol> presenter = [NITCatalogPresenter new];
    id <NITCatalogInteractorInputProtocol> interactor = [NITCatalogInteractor new];
    id <NITCatalogAPIDataManagerInputProtocol> APIDataManager = [NITCatalogAPIDataManager new];
    id <NITCatalogLocalDataManagerInputProtocol> localDataManager = [NITCatalogLocalDataManager new];
    id <NITCatalogWireFrameProtocol> wireFrame= [NITCatalogWireFrame new];
    id <NITCatalogViewProtocol> view = [(NITCatalogWireFrame *)wireFrame createViewControllerWithKey:_s(NITCatalogViewController) storyboardType:StoryboardTypeCatalog];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITCatalogModuleFrom:(UIViewController *)fromViewController
{
    NITCatalogViewController * vc = [NITCatalogWireFrame createNITCatalogModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)showCategoryFrom:(UIViewController *)fromViewController withModel:(id)model
{
    [NITCatalogCategoryWireFrame presentNITCatalogCategoryModuleFrom:fromViewController withModel:model];
}

- (void)showCatalogItemsFrom:(UIViewController *)fromViewController withModel:(id)model
{
    [NITCatalogDirectoryWireFrame presentNITCatalogDirectoryModuleFrom:fromViewController withModel:model withSearchText:nil];
}

- (void)showCatalogItemDetailsFrom:(UIViewController *)fromViewController withModel:(id)model
{
    [NITProductDetailsWireFrame presentNITProductDetailsModuleFrom:fromViewController withModel:model withType:0];
}

- (void)showSearchItemsWithViewController:(UIViewController *)fromViewController withSearchText:(NSString *)text
{
    [NITCatalogDirectoryWireFrame presentNITCatalogDirectoryModuleFrom:fromViewController withModel:nil withSearchText:text];
}

- (void)presentAlertFrom:(UIViewController *)fromViewController withAlert:(UIAlertController *)alert
{
    [fromViewController presentViewController:alert animated:YES completion:nil];
}

- (void)showBannersFrom:(UIViewController *)fromViewController withModel:(NITInfoItemModel *)model
{
    [NITCatalogDirectoryWireFrame presentNITcatalogDirectoryBannerGroupFrom:fromViewController withModel:model];
}

- (void)showBannersItemsFrom:(UIViewController *)fromViewController withModel:(NITInfoItemModel *)model
{
    [NITCatalogDirectoryWireFrame presentNITcatalogDirectoryBannerItemsFrom:fromViewController withModel:model];
}

- (void)showBannersItemFrom:(UIViewController *)fromViewController withModel:(NITCatalogItemModel *)model
{
    [NITProductDetailsWireFrame presentNITProductDetailsModuleFrom:fromViewController withModel:model withType:0];
}

@end
