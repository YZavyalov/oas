//
//  NITCatalogWireFrame.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogProtocols.h"
#import "NITCatalogViewController.h"
#import "NITCatalogLocalDataManager.h"
#import "NITCatalogAPIDataManager.h"
#import "NITCatalogInteractor.h"
#import "NITCatalogPresenter.h"
#import "NITCatalogWireframe.h"
#import <UIKit/UIKit.h>

@interface NITCatalogWireFrame : NITRootWireframe <NITCatalogWireFrameProtocol>

@end
