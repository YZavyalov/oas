//
//  NITBaseModel.h
//  OAS
//
//  Created by Eugene Parafiynyk on 4/21/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NITBaseModel : NSObject

@property (nonatomic) NSString * identifier;
@property (nonatomic) NSString * name;

@end
