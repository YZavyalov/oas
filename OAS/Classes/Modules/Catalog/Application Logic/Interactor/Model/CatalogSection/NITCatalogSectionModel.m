//
//  NITCatalogSectionModel.m
//  OAS
//
//  Created by Eugene Parafiynyk on 4/21/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogSectionModel.h"
#import "NITCatalogSliderTableCell.h"
#import "NITCatalogCategoryTableViewCell.h"

@implementation NITCatalogSectionModel

#pragma mark - Life cycle
- (instancetype)initWithType:(CatalogSectionType)type
{
    if (self = [super init])
    {
        self.type = type;
        self.rowHeight = 150;
    }
    
    return self;
}

#pragma mark - Custom accessors
- (CGFloat)rowHeight
{
    switch (self.type)
    {
        case CatalogSectionTypeStock:
            return [self.models count] > 0 ? _rowHeight : 0;
        case CatalogSectionTypeCategory:
            return 44;
    }
}

- (NSString *)cellID
{
    switch (self.type)
    {
        case CatalogSectionTypeStock:
            return _s(NITCatalogSliderTableCell);
        case CatalogSectionTypeCategory:
            return _s(NITCatalogCategoryTableViewCell);
    }
}

@end
