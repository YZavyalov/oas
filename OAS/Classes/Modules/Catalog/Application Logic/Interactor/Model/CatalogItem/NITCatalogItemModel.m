//
//  NITCatalogItemModel.m
//  OAS
//
//  Created by Eugene Parafiynyk on 4/21/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItemModel.h"
#import "SLCatalogsApi.h"
#import "SLCatalogItem.h"
#import "SLCatalogItemWithRelations.h"
#import "NITFavoriteItem.h"
#import "NITBasketItem.h"
#import "NITCatalogItem.h"
#import "SLCollectionRow.h"

@implementation NITCatalogItemModel

#pragma mark - Life cycle
- (instancetype)initWithModel:(SLCatalogItem *)model withparentID:(NSString *)parentID
{
    if (self = [super init])
    {
        self.catalogCategoryID  = parentID;
        self.identifier         = model._id;
        self.name               = model.name;
        self.body               = [model.body stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        self.creator            = model.creator;
        self.descriptionText    = model._description;
        self.image              = model.image;
        self.price              = @([NSString stringWithFormat:@"%.2f", model.price.floatValue].floatValue);
        self.createdAt          = [NSDate dateWithTimeIntervalSince1970:model.createdAt.doubleValue];
        self.updatedAt          = [NSDate dateWithTimeIntervalSince1970:model.updatedAt.doubleValue];
        self.count              = 0;
        self.isFull             = false;
    }
    
    return self;
}

- (instancetype)initWithDetailModel:(SLCatalogItemWithRelations *)model withGroupID:(NSString *)groupID
{
    if (self = [super init])
    {
        self.catalogCategoryID  = groupID;
        self.identifier         = model._id;
        self.name               = model.name;
        self.body               = [model.body stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        self.creator            = model.creator;
        self.descriptionText    = model._description;
        self.image              = model.image;
        self.price              = @([NSString stringWithFormat:@"%.2f", model.price.floatValue].floatValue);
        self.createdAt          = [NSDate dateWithTimeIntervalSince1970:model.createdAt.doubleValue];
        self.updatedAt          = [NSDate dateWithTimeIntervalSince1970:model.updatedAt.doubleValue];
        self.groups             = model.groups;
        self.count              = 0;
        self.isFull             = true;
        
        [model.properties bk_each:^(SLIPropertyValue * p) {
            
            if ([p.type isEqualToString:@"ipString"] && [self.allPropertyNames containsObject:p.key])
            {
                id value = p.value.firstObject ? : @"";
                [self setValue:value forKey:p.key];
            }
        }];
    }
    
    return self;
}

- (instancetype)initWithOrderModel:(SLOrderItem *)model
{
    if (self = [super init])
    {
        self.identifier         = model._id;
        self.catalogCategoryID  = model.catalogItemId;
        self.price              = model.price;
        self.orderCount         = [model.quantity integerValue];
    }
    
    return self;
}

- (instancetype)initWithTupleModel:(SLCollectionRow *)model
{
    if(self = [super init])
    {
        self.identifier = model.value;
    }
    
    return self;
}

- (instancetype)initWithFavoriteModel:(NITFavoriteItem *)model
{
    if (self = [super init])
    {
        //main
        self.catalogCategoryID  = model.parentID;
        self.identifier         = model.identifier;
        self.name               = model.name;
        self.body               = model.body;
        self.creator            = model.creator;
        self.descriptionText    = model.descriptionText;
        self.image              = model.image;
        self.price              = model.price;
        self.createdAt          = model.createdAt;
        self.updatedAt          = model.updatedAt;
        self.count              = 0;
        NSMutableArray * groupsModel = [NSMutableArray new];
        for(NITStringObject * group in model.groups)
        {
            [groupsModel addObject:group.value];
        }
        self.groups = [groupsModel mutableCopy];
        
        //properties
        self.prodform_id        = model.prodform_id;
        self.inn_id             = model.inn_id;
        self.gentrade_id        = model.gentrade_id;
        self.drugform_id        = model.drugform_id;
        self.Firm_ID            = model.Firm_ID;
        self.Country_ID         = model.Country_ID;
        self.wsize              = model.wsize;
        self.vmass              = model.vmass;
        self.lsize              = model.lsize;
        self.genprodform_name   = model.genprodform_name;
        self.cubeunit           = model.cubeunit;
        self.concunit           = model.concunit;
        self.amass              = model.amass;
        self.altlsize           = model.altlsize;
        self.recipe             = model.recipe;
        self.under_order        = model.under_order;
        self.is_new             = model.is_new;
    }
    
    return self;
}

- (instancetype)initWithBasketModel:(NITBasketItem *)model
{
    if(self = [super init])
    {
        self.catalogCategoryID  = model.parentID;
        self.identifier         = model.identifier;
        self.name               = model.name;
        self.body               = model.body;
        self.creator            = model.creator;
        self.descriptionText    = model.descriptionText;
        self.image              = model.image;
        self.price              = model.price;
        self.createdAt          = model.createdAt;
        self.updatedAt          = model.updatedAt;
        self.count              = 0;
        NSMutableArray * groupsModel = [NSMutableArray new];
        for(NITStringObject * group in model.groups)
        {
            [groupsModel addObject:group.value];
        }
        self.groups = [groupsModel mutableCopy];
        
        //properties
        self.prodform_id        = model.prodform_id;
        self.inn_id             = model.inn_id;
        self.gentrade_id        = model.gentrade_id;
        self.drugform_id        = model.drugform_id;
        self.Firm_ID            = model.Firm_ID;
        self.Country_ID         = model.Country_ID;
        self.wsize              = model.wsize;
        self.vmass              = model.vmass;
        self.lsize              = model.lsize;
        self.genprodform_name   = model.genprodform_name;
        self.cubeunit           = model.cubeunit;
        self.concunit           = model.concunit;
        self.amass              = model.amass;
        self.altlsize           = model.altlsize;
        self.recipe             = model.recipe;
        self.under_order        = model.under_order;
        self.is_new             = model.is_new;
    }
    
    return self;
}

- (instancetype)initWithOrderCatalog:(NITCatalogItem *)model
{
    if (self = [super init])
    {
        self.identifier = model.identifier;
        self.catalogCategoryID = model.catalogItemID;
        self.price = model.price;
        self.orderCount = [model.quantity integerValue];
    }
    
    return self;
}

#pragma mark - Custom accessors
- (BOOL)favorite
{
    return [NITFavoriteItem favoriteItemByID:self.identifier] ? true : false;
}

- (BOOL)hasRecipe
{
    return self.recipe.length > 0;
}

- (BOOL)underOrder
{
    return self.under_order.length > 0;
}

- (NSInteger)count
{
    return [[NITBasketItem basketItemByID:self.identifier].count integerValue];
}

- (BOOL)isNew
{
    return self.is_new.length > 0 ? true : false;
}

+ (NSArray <NITCatalogItemModel *> *)catalogItemModelsFromEntities:(NSArray <SLCatalogItem *> *)entities withParentID:(NSString *)parentID
{
    return [entities bk_map:^id(SLCatalogItem * obj) {
        return [[NITCatalogItemModel alloc] initWithModel:obj withparentID:parentID];
    }];
}

+ (NSArray <NITCatalogItemModel *> *)catalogItemDetailModelsFromEntities:(NSArray <SLCatalogItemWithRelations *> *)entities withGroupID:(NSString *)groupID
{
    return [entities bk_map:^id(SLCatalogItemWithRelations * obj) {
        return [[NITCatalogItemModel alloc] initWithDetailModel:obj withGroupID:groupID];
    }];
}

+ (NSArray <SLOrderItem> *)createSLOrderItemWithCatalogItem:(NSArray <NITCatalogItemModel *> *)models
{
    NSMutableArray * itemArray = [NSMutableArray new];
    for(NITCatalogItemModel * model in models)
    {
        SLOrderItem * slOrderItem = [SLOrderItem new];
        slOrderItem.catalogItemId = model.identifier;
        slOrderItem._id           = @"";
        slOrderItem.price         = model.price;
        slOrderItem.quantity      = @(model.count);
        [itemArray addObject:slOrderItem];
    }
    
    return [itemArray mutableCopy];
}

+ (NSArray <NITCatalogItemModel *> *)catalogOrderItemModelsFromEntities:(NSArray <SLOrderItem *> *)entities
{
    return [entities bk_map:^id(SLOrderItem *obj) {
        return [[NITCatalogItemModel alloc] initWithOrderModel:obj];
    }];
}

+ (NSArray <NITCatalogItemModel *> *)catalogItemModelsFromTupleEntities:(NSArray <SLCollectionRow *> *)entities
{
    return [entities bk_map:^id(SLCollectionRow *obj) {
        return [[NITCatalogItemModel alloc] initWithTupleModel:obj];
    }];
}

+ (NSArray <NITCatalogItemModel *> *)catalogItemsModelsFromBasketEntities:(NSArray <NITBasketItem *> *)entities
{
    return [entities bk_map:^id(NITBasketItem * obj) {
        return [[NITCatalogItemModel alloc] initWithBasketModel:obj];
    }];
}

@end
