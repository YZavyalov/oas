//
//  NITCatalogItemModel.h
//  OAS
//
//  Created by Eugene Parafiynyk on 4/21/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SLOrderItem.h"

@class SLCatalogItem, SLCatalogItemWithRelations, NITFavoriteItem, NITBasketItem, NITCatalogItem, SLCollectionRow;

@interface NITCatalogItemModel : NITBaseModel

// main
@property (nonatomic) NSString * catalogCategoryID;
@property (nonatomic) NSString * body;
@property (nonatomic) NSString * creator;
@property (nonatomic) NSString * descriptionText;
@property (nonatomic) NSString * image;
@property (nonatomic) NSNumber * price;
@property (nonatomic) NSDate * createdAt;
@property (nonatomic) NSDate * updatedAt;
@property (nonatomic) NSArray <NSString* > * groups;

// properties
@property (nonatomic) NSString * prodform_id;
@property (nonatomic) NSString * inn_id;
@property (nonatomic) NSString * gentrade_id;
@property (nonatomic) NSString * drugform_id;
@property (nonatomic) NSString * Firm_ID;
@property (nonatomic) NSString * Country_ID;
@property (nonatomic) NSString * wsize;
@property (nonatomic) NSString * vmass;
@property (nonatomic) NSString * lsize;
@property (nonatomic) NSString * genprodform_name;
@property (nonatomic) NSString * cubeunit;
@property (nonatomic) NSString * concunit;
@property (nonatomic) NSString * amass;
@property (nonatomic) NSString * altlsize;
@property (nonatomic) NSString * recipe;
@property (nonatomic) NSString * under_order;
@property (nonatomic) NSString * spec_prop;
@property (nonatomic) NSString * is_new;

// custom
@property (nonatomic) NSInteger orderCount;
@property (nonatomic) NSInteger count;
@property (nonatomic) BOOL isFull;
@property (nonatomic) BOOL isNew;
@property (nonatomic) BOOL favorite;
@property (nonatomic) BOOL underOrder;
@property (nonatomic) BOOL hasRecipe;

- (instancetype)initWithModel:(SLCatalogItem *)model withparentID:(NSString *)parentID;
- (instancetype)initWithDetailModel:(SLCatalogItemWithRelations *)model withGroupID:(NSString *)groupID;
- (instancetype)initWithOrderModel:(SLOrderItem *)model;
- (instancetype)initWithFavoriteModel:(NITFavoriteItem *)model;
- (instancetype)initWithBasketModel:(NITBasketItem *)model;
- (instancetype)initWithOrderCatalog:(NITCatalogItem *)model;
- (instancetype)initWithTupleModel:(SLCollectionRow *)model;

//- (void)loadDetailsWithHandler:(void(^)(NITCatalogItemModel * model))handler;
+ (NSArray <SLOrderItem> *)createSLOrderItemWithCatalogItem:(NSArray <NITCatalogItemModel *> *)models;
+ (NSArray <NITCatalogItemModel *> *)catalogItemModelsFromEntities:(NSArray <SLCatalogItem *> *)entities withParentID:(NSString *)parentID;
+ (NSArray <NITCatalogItemModel *> *)catalogItemDetailModelsFromEntities:(NSArray <SLCatalogItemWithRelations *> *)entities withGroupID:(NSString *)groupID;
+ (NSArray <NITCatalogItemModel *> *)catalogOrderItemModelsFromEntities:(NSArray <SLOrderItem *> *)entities;
+ (NSArray <NITCatalogItemModel *> *)catalogItemModelsFromTupleEntities:(NSArray <SLCollectionRow *> *)entities;
+ (NSArray <NITCatalogItemModel *> *)catalogItemsModelsFromBasketEntities:(NSArray <NITBasketItem *> *)entities;

@end
