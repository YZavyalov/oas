//
//  NITGroupModel.m
//  OAS
//
//  Created by Eugene Parafiynyk on 4/21/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGroupModel.h"
#import "SLGroup.h"
#import "NITCityItem.h"

@implementation NITGroupModel

#pragma mark - Life cycle
- (instancetype)initWithModel:(SLGroup *)model
{
    if (self = [super init])
    {
        self.identifier         = model._id;
        self.name               = model.name;
        self.parent             = model.parent;
        self.defaultTemplateID  = model.defaultTemplateId;
    }
    
    return self;
}

- (instancetype)initWithCityModel:(NITCityItem *)model
{
    if (self = [super init])
    {
        self.identifier         = model.identifier;
        self.name               = model.name;
        self.parent             = model.parent;
        self.defaultTemplateID  = model.defaultTemplateID;
    }
    
    return self;
}

#pragma mark - Public
+ (NSArray <NITGroupModel *> *)categoryModelsFromEntities:(NSArray <SLGroup *> *)entities
{
    return [entities bk_map:^id(SLGroup * obj) {
        return [[NITGroupModel alloc] initWithModel:obj];
    }];
}

+ (NSArray <NITGroupModel *> *)categoryModelsFromCityEntities:(NSArray <NITCityItem *> *)entities
{
    return [entities bk_map:^id(NITCityItem * obj) {
        return [[NITGroupModel alloc] initWithCityModel:obj];
    }];
}

@end
