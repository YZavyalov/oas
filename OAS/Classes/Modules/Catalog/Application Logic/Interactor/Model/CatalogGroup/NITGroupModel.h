//
//  NITGroupModel.h
//  OAS
//
//  Created by Eugene Parafiynyk on 4/21/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SLGroup, NITCityItem;

@interface NITGroupModel : NITBaseModel

@property (nonatomic) NSString * parent;
@property (nonatomic) NSString * defaultTemplateID;

- (instancetype)initWithModel:(SLGroup *)model;
- (instancetype)initWithCityModel:(NITCityItem *)model;
+ (NSArray <NITGroupModel *> *)categoryModelsFromEntities:(NSArray <SLGroup *> *)entities;
+ (NSArray <NITGroupModel *> *)categoryModelsFromCityEntities:(NSArray <NITCityItem *> *)entities;

@end
