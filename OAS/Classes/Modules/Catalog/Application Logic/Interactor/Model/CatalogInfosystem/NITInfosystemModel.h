//
//  NITInfosystemModel.h
//  OAS
//
//  Created by Eugene Parafiynyk on 4/21/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SLInfosystem;

@interface NITInfosystemModel : NITBaseModel

- (instancetype)initWithModel:(SLInfosystem *)model;
+ (NSArray <NITInfosystemModel *> *)infosystemsModelsFromEntities:(NSArray <SLInfosystem *> *)entities;

@end
