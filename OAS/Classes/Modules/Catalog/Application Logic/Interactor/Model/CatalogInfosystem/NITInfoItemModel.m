//
//  NITInfoItemModel.m
//  OAS
//
//  Created by Eugene Parafiynyk on 5/4/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITInfoItemModel.h"
#import "SLInfoItem.h"
#import "SLInfoItemWithRelations.h"

@implementation NITInfoItemModel

#pragma mark - Life cycle
- (instancetype)initWithModel:(SLInfoItem *)model
{
    if (self = [super init])
    {
        self.identifier = model._id;
        self.name = model.name;
        self.text = model.body;
        self.textPreview = model._description;
        self.imagePath = model.image;
    }
    
    return self;
}

- (instancetype)initWithModelWithProperties:(SLInfoItemWithRelations *)model
{
    if (self = [super init])
    {
        self.identifier = model._id;
        self.name = model.name;
        self.text = model.body;
        self.textPreview = model._description;
        self.imagePath = model.image;
        
        [model.properties bk_each:^(SLIPropertyValue *obj) {
            if([obj.key isEqualToString:@"active"])
            {
                self.active = obj.value.firstObject;
            }
            else if([obj.key isEqualToString:@"events_action"])
            {
                if([obj.value.firstObject isEqualToString:@"catalog_group"])
                {
                    self.type = InfoItemTypeCategory;
                }
                else if([obj.value.firstObject isEqualToString:@"catalog_items"])
                {
                    self.type = InfoItemTypeCatalogItem;
                }
                else if([obj.value.firstObject isEqualToString:@"ext_url"])
                {
                    self.type = InfoItemTypeLink;
                }
                else
                {
                    self.type = InfoItemTypeNone;
                }
            }
            else if([obj.key isEqualToString:@"events_value"])
            {
                self.events_value = obj.value;
            }
            else if([obj.key isEqualToString:@"end"])
            {
                self.end = obj.value.firstObject;
            }
            else if([obj.key isEqualToString:@"start"])
            {
                self.start = obj.value.firstObject;
            }
            else if([obj.key isEqualToString:@"sort"])
            {
                self.sort = obj.value.firstObject;
            }
        }];
    }
    
    return self;
}

- (void)setImage:(UIImage *)image
{
    _image = image;
}

#pragma mark - Public
+ (NSArray <NITInfoItemModel *> *)stockModelsFromEntities:(NSArray <SLInfoItem *> *)entities
{
    return [entities bk_map:^id(SLInfoItem * obj) {
        return [[NITInfoItemModel alloc] initWithModel:obj];
    }];
}

+ (NSArray <NITInfoItemModel *> *)stockModelsWithPropertiesfromEntities:(NSArray <SLInfoItemWithRelations *> *)entities
{
    return [entities bk_map:^id(SLInfoItemWithRelations *obj) {
        return [[NITInfoItemModel alloc] initWithModelWithProperties:obj];
    }];
}

@end
