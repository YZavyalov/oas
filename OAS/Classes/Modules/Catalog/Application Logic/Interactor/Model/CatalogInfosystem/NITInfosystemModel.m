//
//  NITInfosystemModel.m
//  OAS
//
//  Created by Eugene Parafiynyk on 4/21/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITInfosystemModel.h"
#import "SLInfosystem.h"

@implementation NITInfosystemModel

#pragma mark - Life cycle
- (instancetype)initWithModel:(SLInfosystem *)model
{
    if (self = [super init])
    {
        self.identifier = model._id;
        self.name = model.name;
    }
    
    return self;
}

#pragma mark - Public
+ (NSArray <NITInfosystemModel *> *)infosystemsModelsFromEntities:(NSArray <SLInfosystem *> *)entities
{
    return [entities bk_map:^id(SLInfosystem * obj) {
        return [[NITInfosystemModel alloc] initWithModel:obj];
    }];
}

@end
