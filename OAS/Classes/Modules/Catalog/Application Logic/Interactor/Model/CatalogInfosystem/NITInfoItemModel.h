//
//  NITInfoItemModel.h
//  OAS
//
//  Created by Eugene Parafiynyk on 5/4/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseModel.h"

@class SLInfoItem, SLInfoItemWithRelations;

typedef CF_ENUM (NSUInteger, InfoItemType) {
    InfoItemTypeCategory          = 0,
    InfoItemTypeCatalogItem       = 1,
    InfoItemTypeLink              = 2,
    InfoItemTypeNone              = 3
};

@interface NITInfoItemModel : NITBaseModel

@property (nonatomic) NSString * text;
@property (nonatomic) NSString * textPreview;
@property (nonatomic) NSString * imagePath;
@property (nonatomic) UIImage * image;
@property (nonatomic) InfoItemType type;
//properties
@property (nonatomic) NSString * active;
@property (nonatomic) NSArray <NSString *> * events_value;
@property (nonatomic) NSString * end;
@property (nonatomic) NSString * start;
@property (nonatomic) NSString * sort;

- (instancetype)initWithModel:(SLInfoItem *)model;
- (instancetype)initWithModelWithProperties:(SLInfoItemWithRelations *)model;
+ (NSArray <NITInfoItemModel *> *)stockModelsFromEntities:(NSArray <SLInfoItem *> *)entities;
+ (NSArray <NITInfoItemModel *> *)stockModelsWithPropertiesfromEntities:(NSArray <SLInfoItemWithRelations *> *)entities;

@end
