//
//  NITCatalogInteractor.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogProtocols.h"

@interface NITCatalogInteractor : NSObject <NITCatalogInteractorInputProtocol>

@property (nonatomic, weak) id <NITCatalogInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCatalogAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCatalogLocalDataManagerInputProtocol> localDataManager;

@end
