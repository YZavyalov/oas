//
//  NITCatalogInteractor.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogInteractor.h"

@interface NITCatalogInteractor ()

@property (nonatomic) NSTimer * timer;

@end

@implementation NITCatalogInteractor

#pragma mark - NITCatalogInteractorInputProtocol
- (void)getStocksAndCategories
{
    __block NSArray <NITInfoItemModel *> * infoItems;
    __block NSArray <NITGroupModel *> * items;
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    dispatch_group_enter(group);

    
    [self.APIDataManager getInfosystemItemsWithHandler:^(NSArray<SLInfoItemWithRelations *> *result) {
        if(result.count > 0)
        {
            infoItems = [NITInfoItemModel stockModelsWithPropertiesfromEntities:result];
            [infoItems bk_each:^(NITInfoItemModel *obj) {
                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:obj.imagePath]];
                obj.image = [UIImage imageWithData:data];
            }];
            dispatch_group_leave(group);
        }
    }];
    
    [self.APIDataManager getCatalogGroupsWithHandler:^(NSArray<SLGroup *> *result) {
        items = [NITGroupModel categoryModelsFromEntities:result];
        dispatch_group_leave(group);
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self.presenter updateStockModels:infoItems];
        [self.presenter updateCategoriesModels:items];
    });
}

- (void)getStocks
{
    weaken(self);
    [self.APIDataManager getInfosystemItemsWithHandler:^(NSArray<SLInfoItemWithRelations *> *result) {
        if(result.count > 0)
        {
            NSArray <NITInfoItemModel *> * infoItems = [NITInfoItemModel stockModelsWithPropertiesfromEntities:result];
            [infoItems bk_each:^(NITInfoItemModel *obj) {
                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:obj.imagePath]];
                obj.image = [UIImage imageWithData:data];
            }];
            [weakSelf.presenter updateStockModels:infoItems];
        }
    }];
}

- (void)getCategories
{
    weaken(self);
    [self.APIDataManager getCatalogGroupsWithHandler:^(NSArray<SLGroup *> *result) {
        [weakSelf.presenter updateCategoriesModels:[NITGroupModel categoryModelsFromEntities:result]];
    }];
}

- (void)searchByText:(NSString *)text
{
    weaken(self);
    [self.APIDataManager getCatalogItemsBySearchString:text withHandler:^(NSArray<SLCatalogItemWithRelations *> *result) {
        [weakSelf.presenter updateSearchResultModels:[NITCatalogItemModel catalogItemDetailModelsFromEntities:result withGroupID:nil]];
    }];
}

- (void)loadItemDetails:(NITInfoItemModel *)model
{
    weaken(self);
    [model.events_value bk_each:^(NSString *identifier) {
        [self.APIDataManager loadItemDetails:identifier withHandler:^(SLCatalogItemWithRelations *result) {
            if(result)
            {
                [weakSelf.localDataManager addItemToBasket:[NITCatalogItemModel catalogItemDetailModelsFromEntities:@[result] withGroupID:nil].firstObject];
            }
        }];
    }];
    
    
    /*NSString * identifer = model.events_value.firstObject;
    weaken(self);
    [self.APIDataManager loadItemDetails:identifer withHandler:^(SLCatalogItemWithRelations *result) {
        if(result)
        {
            [weakSelf.localDataManager addItemToBasket:[NITCatalogItemModel catalogItemDetailModelsFromEntities:@[result] withGroupID:nil].firstObject];
        }
    }];*/
}

- (void)getBannerItem:(NSString *)identifier
{
    weaken(self);
    [self.APIDataManager loadItemDetails:identifier withHandler:^(SLCatalogItemWithRelations *result) {
        [weakSelf.presenter showBannerItem:[[NITCatalogItemModel alloc] initWithDetailModel:result withGroupID:nil]];
    }];
}

@end
