//
//  NITCatalogLocalDataManager.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogProtocols.h"

@interface NITCatalogLocalDataManager : NSObject <NITCatalogLocalDataManagerInputProtocol>

@end
