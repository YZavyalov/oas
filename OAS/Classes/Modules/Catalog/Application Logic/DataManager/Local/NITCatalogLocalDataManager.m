//
//  NITCatalogLocalDataManager.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogLocalDataManager.h"

@implementation NITCatalogLocalDataManager

- (void)addItemToBasket:(NITCatalogItemModel *)model
{
    [NITBasketItem increaseBasketItemCountByModel:model];
}

@end
