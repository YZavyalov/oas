//
//  NITCatalogAPIDataManager.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogAPIDataManager.h"
#import "SLCatalogsApi.h"
#import "SLInfosystemsApi.h"
#import "NITPharmacyItem.h"
#import "NITApplyingFilterModel.h"

@implementation NITCatalogAPIDataManager

#pragma mark - NITCatalogAPIDataManagerInputProtocol
- (void)getCatalogGroupsWithHandler:(void(^)(NSArray<SLGroup *> *result))handler
{
    [HELPER startLoading];
    [[SLCatalogsApi new] getStaticCatalogGroupsWithCompletionHandler:^(NSArray<SLGroup> *output, NSError *error) {
        //[HELPER stopLoading];
        if (error)
        {
            [ERROR_MESSAGE errorMessage:error_service_unavailable];
            [HELPER logError:error method:METHOD_NAME];
        }
        if (handler) handler (output);
    }];
}

- (void)getInfosystemItemsWithHandler:(void(^)(NSArray<SLInfoItemWithRelations *> *result))handler
{
    SLInfosystemQuery * query = [SLInfosystemQuery new];
    
    [[SLInfosystemsApi new] postStaticInfosystemsByInfosystemidQuerywithrelationsWithInfosystemId:[API infosystemBannersID] body:query completionHandler:^(NSArray<SLInfoItemWithRelations> *output, NSError *error) {
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler (output);
    }];
}

- (void)loadItemDetails:(NSString *)itemID withHandler:(void(^)(SLCatalogItemWithRelations * result))handler
{
    [HELPER startLoading];
    [[SLCatalogsApi new] getStaticCatalogItemsByItemidWithItemId:itemID completionHandler:^(SLCatalogItemWithRelations *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [ERROR_MESSAGE errorMessage:error_service_unavailable];
            [HELPER logError:error method:METHOD_NAME];
        }
        if (handler) handler (output);
    }];
}

- (void)getCatalogItemsBySearchString:(NSString *)searchString withHandler:(void(^)(NSArray<SLCatalogItemWithRelations *> *result))handler
{    
    /*SLCatalogQuery * query = [SLCatalogQuery new];
    NSDictionary *q = @{@"op":@"&",
                        @"pr":@[@{@"n":@"name", @"v":searchString}]
                        };
    query.query = q;
    query.orderBy = @"name";
    query.order = @"ASC";
    
    if([NITPharmacyItem allPharmacyForSearch].count > 0)
        query.shopIdsOr = [NITPharmacyItem allPharmacyForSearch];*/
    
    SLCatalogElasticQuery * query = [SLCatalogElasticQuery new];
    
    if (searchString.length > 2)
    {
        SLCatalogElasticQueryCondition * subQuery = [SLCatalogElasticQueryCondition new];
        subQuery.n = @"name";
        subQuery.v = searchString;
        
        query.query = subQuery;
    }

    [HELPER startLoading];
    NSURLSessionTask * task = [[SLCatalogsApi new] postStaticCatalogElasticquerywithrelationsWithBody:query completionHandler:^(NSArray<SLCatalogItemWithRelations> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [ERROR_MESSAGE errorMessage:error_with_loading_product];
            [HELPER logError:error method:METHOD_NAME];
        }
        if (handler) handler (output);
    }];
    
    [HELPER logString:[NSString stringWithFormat:@"%@\n%@", THIS_METHOD, [task.currentRequest description]]];
}

@end
