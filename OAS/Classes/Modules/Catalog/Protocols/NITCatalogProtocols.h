//
//  NITCatalogProtocols.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITCatalogSectionModel.h"
#import "NITGroupModel.h"
#import "NITInfosystemModel.h"
#import "NITCatalogItemModel.h"
#import "NITInfoItemModel.h"
#import "NITBasketItem.h"

@protocol NITCatalogInteractorOutputProtocol;
@protocol NITCatalogInteractorInputProtocol;
@protocol NITCatalogViewProtocol;
@protocol NITCatalogPresenterProtocol;
@protocol NITCatalogLocalDataManagerInputProtocol;
@protocol NITCatalogAPIDataManagerInputProtocol;

@class NITCatalogWireFrame, SLGroup, SLInfosystem, SLInfoItem;

@protocol NITCatalogViewProtocol
@required
@property (nonatomic, strong) id <NITCatalogPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadSectionByModels:(NSArray <NITCatalogSectionModel *> *)models;
- (void)reloadSearchResultByModels:(NSArray <NITCatalogItemModel *> *)models;
@end

@protocol NITCatalogWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (id)createNITCatalogModule;
+ (void)presentNITCatalogModuleFrom:(id)fromView;
- (void)showCategoryFrom:(id)fromView withModel:(id)model;
- (void)showCatalogItemsFrom:(id)fromView withModel:(id)model;
- (void)showCatalogItemDetailsFrom:(id)fromView withModel:(id)model;
- (void)showSearchItemsWithViewController:(id)fromViewController withSearchText:(NSString *)text;
- (void)presentAlertFrom:(id)fromViewController withAlert:(id)alert;
- (void)showBannersFrom:(id)fromViewController withModel:(id)model;
- (void)showBannersItemsFrom:(id)fromViewController withModel:(NITInfoItemModel *)model;
- (void)showBannersItemFrom:(id)fromViewController withModel:(NITCatalogItemModel *)model;
@end

@protocol NITCatalogPresenterProtocol
@required
@property (nonatomic, weak) id <NITCatalogViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)updateData;
- (void)updateStocks;
- (void)contactAction;
- (void)searchByText:(NSString *)text;
- (void)showCategoryByModel:(NITGroupModel *)model;
- (void)showStockByModel:(NITInfoItemModel *)model;
- (void)showCatalogItemByModel:(NITCatalogItemModel *)model;
- (void)showSearchItemsWithSearchText:(NSString *)text;
- (void)loadItemDetails:(NITInfoItemModel *)model;
@end

@protocol NITCatalogInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateCategoriesModels:(NSArray <NITGroupModel *> *)models;
- (void)updateStockModels:(NSArray <NITInfoItemModel *> *)models;
- (void)updateSearchResultModels:(NSArray <NITCatalogItemModel *> *)models;
- (void)showBannerItem:(NITCatalogItemModel *)model;
@end

@protocol NITCatalogInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCatalogInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCatalogAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCatalogLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getStocksAndCategories;
- (void)getCategories;
- (void)getStocks;
- (void)searchByText:(NSString *)text;
- (void)loadItemDetails:(NITInfoItemModel *)model;
- (void)getBannerItem:(NSString *)identifier;
@end


@protocol NITCatalogDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCatalogAPIDataManagerInputProtocol <NITCatalogDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getCatalogGroupsWithHandler:(void(^)(NSArray<SLGroup *> *result))handler;
- (void)getInfosystemItemsWithHandler:(void(^)(NSArray<SLInfoItemWithRelations *> *result))handler;
- (void)getCatalogItemsBySearchString:(NSString *)searchString withHandler:(void(^)(NSArray<SLCatalogItemWithRelations *> *result))handler;
- (void)loadItemDetails:(NSString *)itemID withHandler:(void(^)(SLCatalogItemWithRelations * result))handler;
@end

@protocol NITCatalogLocalDataManagerInputProtocol <NITCatalogDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (void)addItemToBasket:(NITCatalogItemModel *)model;
@end
