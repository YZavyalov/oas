//
//  NITBasketProtocols.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITBasketItem.h"
#import "NITOrderItem.h"

@protocol NITBasketInteractorOutputProtocol;
@protocol NITBasketInteractorInputProtocol;
@protocol NITBasketViewProtocol;
@protocol NITBasketPresenterProtocol;
@protocol NITBasketLocalDataManagerInputProtocol;
@protocol NITBasketAPIDataManagerInputProtocol;

@class NITBasketWireFrame;

typedef CF_ENUM (NSUInteger, SelectDeliveryInfo) {
    SelectDeliveryInfoPickup      = 0,
    SelectDeliveryInfoDelivery    = 1
};

typedef CF_ENUM (NSUInteger, DeleteType) {
    DeleteTypeItem = 0,
    DeleteTypeAll  = 1
};

typedef CF_ENUM (NSUInteger, BasketSectionType) {
    BasketSectionTypeAvailable  = 0,
    BasketSectionTypeByOrder    = 1,
    BasketSectionTypeEmptyLast  = 2
};

@protocol NITBasketViewProtocol
@required
@property (nonatomic, strong) id <NITBasketPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)updatePrice:(NSString *)price;
- (void)updatePriceDescription:(NSString *)goods;
- (void)updateDataByModels:(NSArray <NITCatalogItemModel *> *)models;
- (void)updateUserCountOfOrders:(NSInteger)count;
- (void)updateEmptyView;
- (void)updateDeliveryButtonHidden;
@end

@protocol NITBasketWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (id)createNITBasketModule;
+ (void)presentNITBasketModuleFrom:(id)fromView;
- (void)showCurentOrdersfromViewController:(id)fromViewController;
- (void)showProductDetailsFromViewController:(id)fromViewController withModel:(NITCatalogItemModel *)model;
- (void)showCatalogFromViewController:(id)fromViewController;
- (void)showDeliveryInfoFromViewController:(id)fromViewController withType:(int)type;
- (void)showImageWithView:(id)fromViewController withImage:(UIImage *)model;
- (void)presentAlertFrom:(id)fromViewController withAlert:(id)alert;
@end

@protocol NITBasketPresenterProtocol
@required
@property (nonatomic, weak) id <NITBasketViewProtocol> view;
@property (nonatomic, strong) id <NITBasketInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITBasketWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)deleteBasketItem:(NITCatalogItemModel *)model;
- (void)reduseItemInBasket:(NITCatalogItemModel *)model;
- (void)addItemInBasket:(NITCatalogItemModel *)model;
- (void)clearBasket:(NSArray <NITCatalogItemModel *> *)items withdeleteType:(DeleteType)deleteType;
- (void)showCurentOrders;
- (void)showProdactDetailsWithModel:(NITCatalogItemModel *)model;
- (void)showCatalog;
- (void)showdeliveryInfoWithType:(int)type;
- (void)showImage:(UIImage *)model;
@end

@protocol NITBasketInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateDataByModels:(NSArray <NITCatalogItemModel *> *)models;
- (void)updateUserCountOfOrders:(NSInteger)count;
@end

@protocol NITBasketInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITBasketInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITBasketAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITBasketLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getBasketItems;
- (void)deleteAllBasketItems;
- (void)deleteBasketItem:(NITCatalogItemModel *)model;
- (void)addItemToBasket:(NITCatalogItemModel *)model;
- (void)removeItemFromBasket:(NITCatalogItemModel *)model;
- (void)getUserCountOfOrders;
- (void)updateItemsMaxCount;
@end


@protocol NITBasketDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITBasketAPIDataManagerInputProtocol <NITBasketDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITBasketLocalDataManagerInputProtocol <NITBasketDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NSArray <NITBasketItem *> *)getBasketItems;
- (void)deleteAllBasketItems;
- (void)deleteBasketItem:(NITCatalogItemModel *)model;
- (void)addItemToBasket:(NITCatalogItemModel *)model;
- (void)removeItemFromBasket:(NITCatalogItemModel *)model;
- (NSArray <NITOrderItem *> *)getOrders;
- (void)updateMaxCountByItemID:(NSString *)itemID;
@end
