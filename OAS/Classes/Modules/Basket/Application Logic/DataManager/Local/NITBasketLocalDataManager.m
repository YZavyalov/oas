//
//  NITBasketLocalDataManager.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketLocalDataManager.h"

@implementation NITBasketLocalDataManager

#pragma mark - NITBasketLocalDataManagerInputProtocol
- (NSArray <NITBasketItem *> *)getBasketItems
{
    return [NITBasketItem allBasketItems];
}

- (void)deleteAllBasketItems
{
    [NITBasketItem deleteAllBasketItems];
}

- (void)deleteBasketItem:(NITCatalogItemModel *)model
{
    [NITBasketItem deleteBasketItemByID:model.identifier];
}

- (void)addItemToBasket:(NITCatalogItemModel *)model
{
    [NITBasketItem increaseBasketItemCountByModel:model];
}

- (void)removeItemFromBasket:(NITCatalogItemModel *)model
{
    [NITBasketItem dereaseBasketItemCountByModel:model];
}

- (void)updateMaxCountByItemID:(NSString *)itemID
{
    [[NITBasketItem new] getMaxCountByItemID:itemID];
}

- (NSArray <NITOrderItem *> *)getOrders
{
    return [NITOrderItem allOrderItems];
}
@end
