//
//  NITOrderItemModel.h
//  OAS
//
//  Created by Yaroslav on 30.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SLOrderWithRelations, SLOrder, NITCatalogItemModel, NITOrderItem;

@interface NITOrderItemModel : NITBaseModel

//main
@property (nonatomic) NSNumber * createdAt;
@property (nonatomic) NSNumber * updatedAt;
@property (nonatomic) NSString * status;
@property (nonatomic) NSString * userID;

@property (nonatomic) NSArray <NITCatalogItemModel *> * items;

//properties
@property (nonatomic) NSString * buyer;
@property (nonatomic) NSString * phone;
@property (nonatomic) NSString * buyer_comment;
@property (nonatomic) NSString * shop_id;
@property (nonatomic) NSString * shop_address;
@property (nonatomic) NSString * pick_up_times;
@property (nonatomic) NSString * delivery_times;
@property (nonatomic) NSString * delivery_address;
@property (nonatomic) NSString * payment_method;
@property (nonatomic) NSString * disapproved_goods;
@property (nonatomic) NSString * order_info;
@property (nonatomic) NSString * attempts;
@property (nonatomic) NSString * under_order;
@property (nonatomic) NSString * pick_up;

@property (nonatomic) NSString * times;
@property (nonatomic) NSString * address;
@property (nonatomic) BOOL underOrder;

- (instancetype)initWithModel:(SLOrderWithRelations *)model;
- (instancetype)initWithParticalModel:(SLOrder *)model;
- (instancetype)initWithOrder:(NITOrderItem *)model;

+ (NITOrderItemModel *)orderItemModelFromEntities:(SLOrderWithRelations *)entities;
+ (NSArray <NITOrderItemModel *> *)orderItemModulsFromEntities:(NSArray <SLOrder *> *)entities;
+ (NSArray <NITOrderItemModel *> *)orderItemsModelsFromOrders:(NSArray <NITOrderItem *> *)entities;
- (void)loadDetailsWithHandler:(void(^)(NITOrderItemModel * model))handler;

@end
