//
//  NITOrderItemModel.m
//  OAS
//
//  Created by Yaroslav on 30.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderItemModel.h"
#import "NITCatalogItemModel.h"
#import "NITOrderItem.h"
#import "SLOrdersApi.h"

@implementation NITOrderItemModel

#pragma mark - Life cycle
- (instancetype)initWithModel:(SLOrderWithRelations *)model
{
    if (self = [super init])
    {
        self.identifier = model._id;
        self.status = model.status;
        self.createdAt = model.createdAt;
        self.updatedAt = model.updatedAt;
        self.userID = model.userId;
        self.items = [NITCatalogItemModel catalogOrderItemModelsFromEntities:model.items];
        [model.properties bk_each:^(SLIPropertyValue * p) {
            
            if ([p.type isEqualToString:@"ipString"] && [self.allPropertyNames containsObject:p.key])
            {
                id value = p.value.firstObject ? : @"";
                [self setValue:value forKey:p.key];
            }
        }];
    }
    
    return self;
}

- (instancetype)initWithParticalModel:(SLOrder *)model
{
    if (self = [super init])
    {
        self.identifier = model._id;
        self.createdAt = model.createdAt;
        self.updatedAt = model.updatedAt;
        self.status = model.status;
        self.userID = model.userId;
    }
    
    return self;
}

- (instancetype)initWithOrder:(NITOrderItem *)model
{
    if (self = [super init])
    {
        //main
        self.identifier     = model.identifier;
        self.status         = model.status;
        self.createdAt      = model.createdAt;
        self.updatedAt      = model.updatedAt;
        self.userID         = model.userID;
        //items
        NSMutableArray <NITCatalogItemModel *> * items = [NSMutableArray new];
        for(NITCatalogItem * item in model.items)
        {
            NITCatalogItemModel * catalog = [[NITCatalogItemModel alloc] initWithOrderCatalog:item];
            [items addObject:catalog];
        }
        self.items = [items mutableCopy];
        //properties
        self.buyer             = model.buyer;
        self.phone             = model.phone;
        self.buyer_comment     = model.buyer_comment;
        self.shop_id           = model.shop_id;
        self.shop_address      = model.shop_address;
        self.pick_up_times     = model.pick_up_times;
        self.delivery_times    = model.delivery_times;
        self.delivery_address  = model.delivery_address;
        self.payment_method    = model.payment_method;
        self.disapproved_goods = model.disapproved_goods;
        self.order_info        = model.order_info;
        self.attempts          = model.attempts;
        self.pick_up           = model.pick_up;
        self.under_order       = model.under_order;
    }
    
    return self;
}

#pragma mark - Custom accessors
- (NSString *)times
{
    return [self.pick_up isEqualToString:@"true"] ? self.pick_up_times : self.delivery_times;
}

- (NSString *)address
{
    return [self.pick_up isEqualToString:@"true"] ? self.shop_address : self.delivery_address;
}

- (BOOL)underOrder
{
    return [self.under_order isEqualToString:@"true"];
}

#pragma mark - Public
+ (NITOrderItemModel *)orderItemModelFromEntities:(SLOrderWithRelations *)entities
{
    return [[NITOrderItemModel alloc] initWithModel:entities];
}

+ (NSArray <NITOrderItemModel *> *)orderItemModulsFromEntities:(NSArray<SLOrder *> *)entities
{
    return [entities bk_map:^id(SLOrder *obj) {
        return [[NITOrderItemModel alloc] initWithParticalModel:obj];
    }];
}

+ (NSArray <NITOrderItemModel *> *)orderItemsModelsFromOrders:(NSArray <NITOrderItem *> *)entities
{
    return [entities bk_map:^id(NITOrderItem * obj) {
        return [[NITOrderItemModel alloc] initWithOrder:obj];
    }];
}

- (void)loadDetailsWithHandler:(void(^)(NITOrderItemModel * model))handler
{
    NSURLSessionTask * task = [[SLOrdersApi new] getDynamicOrderByOrderidWithOrderId:self.identifier completionHandler:^(SLOrderWithRelations *output, NSError *error) {
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (output) handler ([[NITOrderItemModel alloc] initWithModel:output]);
    }];

    [HELPER logString:[NSString stringWithFormat:@"%@\n%@", THIS_METHOD, [task.currentRequest description]]];
}

@end
