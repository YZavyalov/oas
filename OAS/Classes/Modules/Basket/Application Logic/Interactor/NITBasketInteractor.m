//
//  NITBasketInteractor.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketInteractor.h"

@implementation NITBasketInteractor

#pragma mark - NITBasketInteractorInputProtocol
//Basket
- (void)getBasketItems
{
    [self.presenter updateDataByModels:[self catalogItemsModelsFromBasket:[self.localDataManager getBasketItems]]];
}

- (void)deleteAllBasketItems
{
    [self.localDataManager deleteAllBasketItems];
    [self getBasketItems];
}

- (void)deleteBasketItem:(NITCatalogItemModel *)model
{
    [self.localDataManager deleteBasketItem:model];
}

- (void)addItemToBasket:(NITCatalogItemModel *)model
{
    [self.localDataManager addItemToBasket:model];
}

- (void)removeItemFromBasket:(NITCatalogItemModel *)model
{
    [self.localDataManager removeItemFromBasket:model];
}

- (void)updateItemsMaxCount
{
    NSArray <NITCatalogItemModel *> * basketItems = [self catalogItemsModelsFromBasket:[self.localDataManager getBasketItems]];
    for(NITCatalogItemModel * basketItem in basketItems)
    {
        [self.localDataManager updateMaxCountByItemID:basketItem.identifier];
    }
    [self.presenter updateDataByModels:basketItems];
}

//Order
- (void)getUserCountOfOrders
{
    [self.presenter updateUserCountOfOrders:[[self.localDataManager getOrders] count]];
}

#pragma mark - Private
- (NSArray <NITCatalogItemModel *> *)catalogItemsModelsFromBasket:(NSArray <NITBasketItem *> *)entities
{
    return [entities bk_map:^id(NITBasketItem * obj) {
        return [[NITCatalogItemModel alloc] initWithBasketModel:obj];
    }];
}

@end
