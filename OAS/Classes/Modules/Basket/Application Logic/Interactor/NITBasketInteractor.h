//
//  NITBasketInteractor.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITBasketProtocols.h"

@interface NITBasketInteractor : NSObject <NITBasketInteractorInputProtocol>

@property (nonatomic, weak) id <NITBasketInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITBasketAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITBasketLocalDataManagerInputProtocol> localDataManager;

@end
