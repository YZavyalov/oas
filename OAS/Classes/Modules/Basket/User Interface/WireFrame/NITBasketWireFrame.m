//
//  NITBasketWireFrame.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketWireFrame.h"
#import "NITCatalogWireFrame.h"
#import "NITCurrentOrdersWireFrame.h"
#import "NITCreateOrderWireFrame.h"
#import "NITProductDetailsWireFrame.h"
#import "NITPresentPictureWireFrame.h"

@implementation NITBasketWireFrame

#pragma mark - NITBasketWireFrameProtocol
+ (id)createNITBasketModule
{
    // Generating module components
    id <NITBasketPresenterProtocol, NITBasketInteractorOutputProtocol> presenter = [NITBasketPresenter new];
    id <NITBasketInteractorInputProtocol> interactor = [NITBasketInteractor new];
    id <NITBasketAPIDataManagerInputProtocol> APIDataManager = [NITBasketAPIDataManager new];
    id <NITBasketLocalDataManagerInputProtocol> localDataManager = [NITBasketLocalDataManager new];
    id <NITBasketWireFrameProtocol> wireFrame= [NITBasketWireFrame new];
    id <NITBasketViewProtocol> view = [(NITBasketWireFrame *)wireFrame createViewControllerWithKey:_s(NITBasketViewController) storyboardType:StoryboardTypeBasket];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITBasketModuleFrom:(UIViewController *)fromViewController
{
    NITBasketViewController * vc = [NITBasketWireFrame createNITBasketModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)showCurentOrdersfromViewController:(UIViewController *)fromViewController
{
    [NITCurrentOrdersWireFrame presentNITCurrentOrdersModuleFrom:fromViewController];
}

- (void)showDeliveryInfoFromViewController:(UIViewController *)fromViewController withType:(int)type
{
    [NITCreateOrderWireFrame presentNITDeliveryInfoModuleFrom:fromViewController withType:type];
}

- (void)showProductDetailsFromViewController:(UIViewController *)fromViewController withModel:(NITCatalogItemModel *)model
{
    [NITProductDetailsWireFrame presentNITProductDetailsModuleFrom:fromViewController withModel:model withType:0];
}

- (void)showCatalogFromViewController:(UIViewController *)fromViewController
{
    [fromViewController.tabBarController setSelectedIndex:0];
}

- (void)showImageWithView:(UIViewController *)fromViewController withImage:(UIImage *)model
{
    [NITPresentPictureWireFrame presentNITPresentPictureModuleFrom:fromViewController withImage:model];
}

- (void)presentAlertFrom:(UIViewController *)fromViewController withAlert:(UIAlertController *)alert
{
    [fromViewController presentViewController:alert animated:YES completion:nil];
}

@end
