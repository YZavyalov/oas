//
//  NITBasketWireFrame.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITBasketProtocols.h"
#import "NITBasketViewController.h"
#import "NITBasketLocalDataManager.h"
#import "NITBasketAPIDataManager.h"
#import "NITBasketInteractor.h"
#import "NITBasketPresenter.h"
#import "NITBasketWireframe.h"
#import <UIKit/UIKit.h>

@interface NITBasketWireFrame : NITRootWireframe <NITBasketWireFrameProtocol>

@end
