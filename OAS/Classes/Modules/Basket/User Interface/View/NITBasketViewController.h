//
//  NITBasketViewController.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITBasketProtocols.h"

@interface NITBasketViewController : NITBaseViewController <NITBasketViewProtocol>

@property (nonatomic, strong) id <NITBasketPresenterProtocol> presenter;

@end
