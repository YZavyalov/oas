//
//  NITBasketViewController.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketViewController.h"
#import "NITCatalogDirectoryTableViewCell.h"
#import "NITBaseHeaderTableViewCell.h"
#import "NITEmptyTableViewCell.h"
#import "NITMobileConstantsModel.h"
#import "NITGuideBasketView.h"
#import <math.h>

@interface NITBasketViewController()
<
UITableViewDelegate,
UITableViewDataSource,
NITCatalogDirectoryCellDelegate
>
//test
@property (nonatomic) IBOutlet UIView * totalPriveView;
@property (nonatomic) IBOutlet UILabel * priceDescriptionLbl;
@property (nonatomic) IBOutlet UILabel * priceLbl;
@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UIButton * pickupBtn;
@property (nonatomic) IBOutlet UIButton * deliveryBtn;
@property (nonatomic) IBOutlet UIButton * onlyPickupBtn;
@property (nonatomic) IBOutlet UIButton * disabledBtn;

@property (nonatomic) IBOutlet UIView * emptyView;
@property (nonatomic) IBOutlet UIButton * showCatalogBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint * pickupBtnW;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * deliveryBtnW;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * topItemsViewH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * priceDescriptionLblW;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * priceLblW;

@property (nonatomic) CGFloat topInset;

@property (nonatomic) BOOL isDeliveryAvailable;
@property (nonatomic) BOOL isLeftBarButtonItemAvailable;
@property (nonatomic) BOOL isRightBarButtonItemAvailable;
@property (nonatomic) BOOL orderAvailable;

@property (nonatomic) NSArray <NITCatalogItemModel *> * basketItems;
@property (nonatomic) NSArray <NITCatalogItemModel *> * basketItemsByOrder;

@property (nonatomic) BOOL isShowingGuideBasket;

@end

@implementation NITBasketViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter initData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.priceDescriptionLblW.constant = [self.priceDescriptionLbl sizeOfMultiLineLabel].width;
    self.priceLblW.constant = [self.priceLbl sizeOfMultiLineLabel].width;
    
    if([NITOrderItem allOrderItems].count > 0 && [NITBasketItem allBasketItems].count == 0)
        [self showGuideIfNeed];
}

#pragma mark - IBAction
- (IBAction)clearBasket:(id)sender
{
    [self.presenter clearBasket:[self.basketItems arrayByAddingObjectsFromArray:self.basketItemsByOrder] withdeleteType:DeleteTypeAll];
}

- (IBAction)ordersAction:(id)sender
{
    [self.presenter showCurentOrders];
}

- (IBAction)deliveryTypeAction:(UIButton *)sender
{
    if(self.orderAvailable)
    {
        if([sender tag] == 0)
        {
            [self.presenter showdeliveryInfoWithType:SelectDeliveryInfoPickup];
        }
        else if([sender tag] == 1)
        {
            [self.presenter showdeliveryInfoWithType:SelectDeliveryInfoDelivery];
        }
        else if([sender tag] == 2)
        {
            [self.presenter showdeliveryInfoWithType:SelectDeliveryInfoPickup];
        }
    }
    else
    {
        [NOTIFICATION showMinPriceBasket];
    }
}

- (IBAction)showCatalog:(id)sender
{
    [self.presenter showCatalog];
}

#pragma mark - Private
- (void)initUI
{
    //Nav bar
    self.leftBarButtonItem = self.navigationItem.leftBarButtonItem;
    self.isLeftBarButtonItemAvailable = true;
    self.isRightBarButtonItemAvailable = true;
    
    //Top view
    self.topInset = self.topItemsViewH.constant;
    
    //Table view
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITCatalogDirectoryTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITCatalogDirectoryTableViewCell)];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITBaseHeaderTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITBaseHeaderTableViewCell)];
    //To show last cell in tableView (buttons contain ALPHA = 0.75)
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITEmptyTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITEmptyTableViewCell)];
}

- (void)showGuideIfNeed
{
    if ([NITGuideBasketView notYetShown])
    {
        [NITGuideBasketView showWithView:self.view];
    }
}

#pragma mark - NITBasketPresenterProtocol
- (void)updatePriceDescription:(NSString *)goods
{
    self.priceDescriptionLbl.text = goods;
    self.priceDescriptionLblW.constant = [self.priceDescriptionLbl sizeOfMultiLineLabel].width;
}

- (void)updatePrice:(NSString *)price
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:[NSLocale currentLocale]];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    NSString * priceFormatter = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[price doubleValue]]];
    self.priceLbl.text = [priceFormatter stringByAppendingString:@" ₽"];
    
    int minPrice = [[[NITMobileConstantsModel new] min_sum_of_order] intValue];
    [self.pickupBtn setBackgroundColor: [price intValue] >= minPrice ? RGB(58, 160, 144) : RGB(225, 240, 238)];
    [self.deliveryBtn setBackgroundColor: [price intValue] >= minPrice ? RGB(58, 160, 144) : RGB(225, 240, 238)];
    [self.onlyPickupBtn setBackgroundColor: [price intValue] >= minPrice ? RGB(58, 160, 144) : RGB(225, 240, 238)];
    self.orderAvailable = [price intValue] >= minPrice ? true : false;
    
    self.isDeliveryAvailable = USER.cityDelivery;
    [UIView transitionWithView:self.view
                      duration:0.3f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        self.disabledBtn.alpha = self.orderAvailable ? 0 : 1;
                        self.pickupBtn.alpha = self.isDeliveryAvailable ? (self.orderAvailable ? 0.75 : 0) : 0;
                        self.deliveryBtn.alpha = self.isDeliveryAvailable ? (self.orderAvailable ? 0.75 : 0) : 0;
                        self.onlyPickupBtn.alpha = self.isDeliveryAvailable ? 0 : (self.orderAvailable ? (self.isDeliveryAvailable ? 0 : 0.75) : 0);
                    }
                    completion:nil];
}

- (void)updateDeliveryButtonHidden
{
    /*self.isDeliveryAvailable = USER.cityDelivery;
    
    if(self.isDeliveryAvailable)
    {
        self.pickupBtn.alpha        = 0.75;
        self.deliveryBtn.alpha      = 0.75;
        self.onlyPickupBtn.alpha    = 0;
    }
    else
    {
        self.pickupBtn.alpha        = 0;
        self.deliveryBtn.alpha      = 0;
        self.onlyPickupBtn.alpha    = 0.75;
    }*/
}

- (void)updateEmptyView
{
    self.emptyView.hidden                   = self.basketItems.count == 0 && self.basketItemsByOrder.count == 0 ? false : true;
    self.deliveryBtn.hidden                 = self.basketItems.count == 0 && self.basketItemsByOrder.count == 0 ? true : false;
    self.pickupBtn.hidden                   = self.basketItems.count == 0 && self.basketItemsByOrder.count == 0 ? true : false;
    self.tableView.hidden                   = self.basketItems.count == 0 && self.basketItemsByOrder.count == 0 ? true : false;
    self.totalPriveView.hidden              = self.basketItems.count == 0 && self.basketItemsByOrder.count == 0 ? true : false;
    self.navigationItem.leftBarButtonItem   = self.basketItems.count == 0 && self.basketItemsByOrder.count == 0 ? nil : self.leftBarButtonItem;
    self.disabledBtn.hidden                 = !self.emptyView.hidden;
}

- (void)updateDataByModels:(NSArray <NITCatalogItemModel *> *)models
{
    self.basketItems = [models bk_select:^BOOL(NITCatalogItemModel *obj) {
                        return !obj.underOrder;
                       }];
    self.basketItemsByOrder = [models bk_select:^BOOL(NITCatalogItemModel *obj) {
                                return obj.underOrder;
                              }];
    [self updateDeliveryButtonHidden];
    [self.tableView reloadData];
}

- (void)updateUserCountOfOrders:(NSInteger)count
{
    self.navigationItem.rightBarButtonItem.title = count > 0 ? [NSString stringWithFormat:@"%@%ld%@", NSLocalizedString(@"Заказы (", nil), (long)count, NSLocalizedString(@")", nil)] : self.navigationItem.rightBarButtonItem.title;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    BasketSectionType basketSectionType = section;
    switch (basketSectionType) {
        case BasketSectionTypeAvailable:
        {
            if(self.basketItemsByOrder.count > 0 && self.basketItems.count > 0) return 44;
            else                                                                return 0;
        }
        case BasketSectionTypeByOrder:
        {
            if(self.basketItemsByOrder.count > 0)   return 44;
            else                                    return 0;
        }
        case BasketSectionTypeEmptyLast:    return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    BasketSectionType basketSectionType = section;
    NITBaseHeaderTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITBaseHeaderTableViewCell)];
    switch (basketSectionType)
    {
        case BasketSectionTypeAvailable:    cell.titleText = NSLocalizedString(@"В НАЛИЧИЕ", nil);
        break;
        case BasketSectionTypeByOrder:      cell.titleText = NSLocalizedString(@"ПОД ЗАКАЗ (СРОК ДОСТАВКИ 3 ДНЯ)", nil);
        break;
        case BasketSectionTypeEmptyLast:    return nil;
        break;
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BasketSectionType basketSectionType = indexPath.section;
    switch (basketSectionType) {
        case BasketSectionTypeAvailable:    return 170;
        case BasketSectionTypeByOrder:      return 170;
        case BasketSectionTypeEmptyLast:    return 55;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    BasketSectionType basketSectionType = section;
    switch (basketSectionType) {
        case BasketSectionTypeAvailable:    return self.basketItems.count;
        case BasketSectionTypeByOrder:      return self.basketItemsByOrder.count;
        case BasketSectionTypeEmptyLast:    return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BasketSectionType basketSectionType = indexPath.section;
    switch (basketSectionType) {
        case BasketSectionTypeAvailable:
        {
            NITCatalogDirectoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITCatalogDirectoryTableViewCell) forIndexPath:indexPath];
            cell.model = self.basketItems[indexPath.row];
            cell.delegate = self;
            
            return cell;
        }
        case BasketSectionTypeByOrder:
        {
            NITCatalogDirectoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITCatalogDirectoryTableViewCell) forIndexPath:indexPath];
            cell.model = self.basketItemsByOrder[indexPath.row];
            cell.delegate = self;
            
            return cell;
        }
        case BasketSectionTypeEmptyLast:
        {
            //Create Emptty cell, cell.Height = BottomButtons.Height, to show all Last cell
            NITEmptyTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITEmptyTableViewCell) forIndexPath:indexPath];
            cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
            
            return cell;
        }
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NSLocalizedString(@"Удалить", nil);
}

#pragma mark - UITableViewDelegate
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    BasketSectionType basketSectionType = indexPath.section;
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        if(basketSectionType == BasketSectionTypeAvailable) [self.presenter deleteBasketItem:self.basketItems[indexPath.row]];
        if(basketSectionType == BasketSectionTypeByOrder)   [self.presenter deleteBasketItem:self.basketItemsByOrder[indexPath.row]];
    }
    [tableView reloadData];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BasketSectionType basketSectionType = indexPath.section;
    if(basketSectionType == BasketSectionTypeAvailable) [self.presenter showProdactDetailsWithModel:self.basketItems[indexPath.row]];
    if(basketSectionType == BasketSectionTypeByOrder)   [self.presenter showProdactDetailsWithModel:self.basketItemsByOrder[indexPath.row]];
}

#pragma mark - NITBasketCellDelegate
- (void)didAddItem:(NITCatalogItemModel *)model
{
    [self.presenter addItemInBasket:model];
}

- (void)didReduseItem:(NITCatalogItemModel *)model
{
    [self.presenter reduseItemInBasket:model];
}

- (void)didPresentImage:(UIImage *)model
{
    [self.presenter showImage:model];
}

@end
