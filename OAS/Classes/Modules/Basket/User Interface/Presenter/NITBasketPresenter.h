//
//  NITBasketPresenter.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITBasketProtocols.h"

@class NITBasketWireFrame;

@interface NITBasketPresenter : NITRootPresenter <NITBasketPresenterProtocol, NITBasketInteractorOutputProtocol>

@property (nonatomic, weak) id <NITBasketViewProtocol> view;
@property (nonatomic, strong) id <NITBasketInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITBasketWireFrameProtocol> wireFrame;

@end
