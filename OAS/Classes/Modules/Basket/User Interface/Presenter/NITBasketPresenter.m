//
//  NITBasketPresenter.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketPresenter.h"
#import "NITBasketWireframe.h"

@implementation NITBasketPresenter

#pragma mark - NITBasketPresenterProtocol
- (void)initData
{
    [self updateUserCountOfOrders];
    [self addObservers];
    [self updateData];
}

- (void)deleteBasketItem:(NITCatalogItemModel *)model
{
    [self.interactor deleteBasketItem:model];
}

- (void)addItemInBasket:(NITCatalogItemModel *)model
{
    [self.interactor addItemToBasket:model];
}

- (void)reduseItemInBasket:(NITCatalogItemModel *)model
{
    if(model.count > 1) [self.interactor removeItemFromBasket:model];
    else                [self showAlertControllerWithModels:@[model] withType:DeleteTypeItem];
}

- (void)clearBasket:(NSArray <NITCatalogItemModel *> *)items withdeleteType:(DeleteType)deleteType
{
    [self showAlertControllerWithModels:items withType:deleteType];
}

- (void)showCurentOrders
{
    [self.wireFrame showCurentOrdersfromViewController:self.view];
}

- (void)showdeliveryInfoWithType:(int)type
{
    [self.wireFrame showDeliveryInfoFromViewController:self.view withType:type];
    
    switch (type) {
        case 0: [TRACKER trackEvent:TrackerEventPlacePickupOrder];      break;
        case 1: [TRACKER trackEvent:TrackerEventPlaceDeliveryOrder];    break;
    }
}

- (void)showProdactDetailsWithModel:(NITCatalogItemModel *)model
{
    [self.wireFrame showProductDetailsFromViewController:self.view withModel:model];
}

- (void)showCatalog
{
    [self.wireFrame showCatalogFromViewController:self.view];
}

#pragma mark - NITBasketInteractorProtocol
- (void)updateDataByModels:(NSArray <NITCatalogItemModel *> *)models
{
    NSInteger count = 0;
    double price = 0;
    for(NITCatalogItemModel * model in models)
    {
        count += model.count;
        price += [model.price doubleValue] * model.count;
    }
    NSString * goods;
    if(fmodf(count, 10) == 1) goods = NSLocalizedString(@" товар ", nil);
    else if(fmodf(count, 10) > 1 && fmodf(count, 10) < 5) goods = NSLocalizedString(@" товара ", nil);
    else goods = NSLocalizedString(@" товаров ", nil);
    goods = [[[NSString stringWithFormat:@"%lu", (unsigned long)count] stringByAppendingString:goods] stringByAppendingString:NSLocalizedString(@"на сумму:", nil)];
    
    [self.view updateDataByModels:models];
    [self.view updatePrice:[NSString stringWithFormat:@"%.2f ₽", price]];
    [self.view updatePriceDescription:goods];
    [self.view updateEmptyView];
}

- (void)updateUserCountOfOrders:(NSInteger)count
{
    [self.view updateUserCountOfOrders:count];
}

- (void)showImage:(UIImage *)model
{
    [self.wireFrame showImageWithView:self.view withImage:model];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kSaveBasketItemNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showBasketChangeAlert) name:kChangeBasketNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateItemsMaxCount) name:kChangeCityNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateOrdersCount) name:kSaveOrderItemNotification object:nil];
}

- (void)updateData
{
    [self.interactor getBasketItems];
}

- (void)updateItemsMaxCount
{
    [self.interactor updateItemsMaxCount];
}

- (void)updateOrdersCount
{
    [self.interactor getUserCountOfOrders];
}

- (void)updateUserCountOfOrders
{
    [self.interactor getUserCountOfOrders];
}

- (void)showBasketChangeAlert
{
    //[ERROR_MESSAGE errorMessage:change_city_pushcart_clear];
}

- (void)showAlertControllerWithModels:(NSArray <NITCatalogItemModel *> *)items withType:(DeleteType)deleteType
{
    weaken(self);
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:deleteType == DeleteTypeAll ? NSLocalizedString(@"Очистить корзину?", nil) : NSLocalizedString(@"Удалить товар \n из корзины?", nil)
                                                                    message:nil
                                                             preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = RGB(58, 160, 144);
    
    UIAlertAction * done = [UIAlertAction actionWithTitle:deleteType == DeleteTypeAll ? NSLocalizedString(@"Очистить", nil) : NSLocalizedString(@"Удалить", nil)
                                                    style:UIAlertActionStyleCancel
                                                  handler:^(UIAlertAction * action) {
                                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                                      if(deleteType == DeleteTypeAll)   [weakSelf.interactor deleteAllBasketItems];
                                                      if(deleteType == DeleteTypeItem)  [weakSelf.interactor deleteBasketItem:[items firstObject]];
                                                  }];
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Отмена", nil)
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                    }];
    
    [alert addAction:cancel];
    [alert addAction:done];
    
    [self.wireFrame presentAlertFrom:self.view withAlert:alert];
}

@end
