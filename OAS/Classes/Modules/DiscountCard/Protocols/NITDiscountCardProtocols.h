//
//  NITDiscountCardProtocols.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITDiscountCardModel.h"
#import "SLCollection.h"

@protocol NITDiscountCardInteractorOutputProtocol;
@protocol NITDiscountCardInteractorInputProtocol;
@protocol NITDiscountCardViewProtocol;
@protocol NITDiscountCardPresenterProtocol;
@protocol NITDiscountCardLocalDataManagerInputProtocol;
@protocol NITDiscountCardAPIDataManagerInputProtocol;

@class NITDiscountCardWireFrame;

// Defines the public interface that something else can use to drive the user interface
@protocol NITDiscountCardViewProtocol
@required
@property (nonatomic, strong) id <NITDiscountCardPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)updateDiscountCardInfo;
@end

@protocol NITDiscountCardWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (id)createNITDiscountCardModule;
+ (void)presentNITDiscountCardModuleFrom:(id)fromViewController;
+ (void)presentNITDiscountCardWithoutAnimationModuleFrom:(id)fromViewController;
- (void)presentNITDiscountCardFrom:(id)fromViewController;
@end

@protocol NITDiscountCardPresenterProtocol
@required
@property (nonatomic, weak) id <NITDiscountCardViewProtocol> view;
@property (nonatomic, strong) id <NITDiscountCardInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITDiscountCardWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
-(void)initData;
@end

@protocol NITDiscountCardInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITDiscountCardInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITDiscountCardInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITDiscountCardAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITDiscountCardLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end


@protocol NITDiscountCardDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITDiscountCardAPIDataManagerInputProtocol <NITDiscountCardDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITDiscountCardLocalDataManagerInputProtocol <NITDiscountCardDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
