//
//  NITDiscountCardViewController.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITDiscountCardProtocols.h"
#import "NITBaseViewController.h"

@interface NITDiscountCardViewController : NITBaseViewController <NITDiscountCardViewProtocol>

@property (nonatomic, strong) id <NITDiscountCardPresenterProtocol> presenter;

@end
