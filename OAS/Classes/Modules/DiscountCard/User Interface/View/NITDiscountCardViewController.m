//
//  NITDiscountCardViewController.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITDiscountCardViewController.h"
#import "RSCodeGen.h"
#import "NITMobileConstantsModel.h"

@interface NITDiscountCardViewController()

@property (nonatomic) IBOutlet UIView * whiteView;
@property (nonatomic) IBOutlet UIView * backgroundView;
@property (nonatomic) IBOutlet UIImageView * barcodeImg;
@property (nonatomic) IBOutlet UILabel * barcodeLbl;
@property (nonatomic) IBOutlet UILabel * discountTitleLbl;
@property (nonatomic) IBOutlet UILabel * discountSubtitleLbl;
@property (nonatomic) IBOutlet UILabel * bonusTitleLbl;
@property (nonatomic) IBOutlet UILabel * bonusSubtitleLbl;

@property (nonatomic) IBOutlet NSLayoutConstraint * barcodeTop;
@property (nonatomic) IBOutlet NSLayoutConstraint * iconTop;
@property (nonatomic) IBOutlet NSLayoutConstraint * iconBottom;

@end

@implementation NITDiscountCardViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [HELPER stopLoading];
}

#pragma mark - IBAction
- (IBAction)familiarizeAction:(id)sender
{
    NSString * bonusInfoURL = [[NITMobileConstantsModel new] bonus_info];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:bonusInfoURL]];
}

#pragma mark - Private
- (void)initUI
{
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = true;
    [self.tabBarController hideTabBarAnimated:false];
    
    NSString * subtitleGray = NSLocalizedString(@"С правилами накопления и использования баллов «Областной Аптечный Склад» можно ознакомиться", nil);
    NSString * subtitleGreen = NSLocalizedString(@"здесь", nil);
    
    NSString *text = [NSString stringWithFormat:@"%@ %@", subtitleGray, subtitleGreen];
    
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName: self.bonusSubtitleLbl.textColor,
                              NSFontAttributeName: self.bonusSubtitleLbl.font
                              };
    NSMutableAttributedString *attributedText =
    [[NSMutableAttributedString alloc] initWithString:text
                                           attributes:attribs];
    
    UIColor *blackColor = RGB(135, 142, 141);
    NSRange blackTextRange = [text rangeOfString:subtitleGray];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:blackColor}
                            range:blackTextRange];
    
    UIColor *lightGrayColor = RGB(58, 160, 144);
    NSRange lightGrayTextRange = [text rangeOfString:subtitleGreen];
    [attributedText setAttributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                    NSForegroundColorAttributeName:lightGrayColor}
                            range:lightGrayTextRange];
    
    self.bonusSubtitleLbl.attributedText = attributedText;
    
    //constaints for IPhone5
    if(IS_IPHONE_5)
    {
        self.barcodeTop.constant = 15;
        self.iconTop.constant = 25;
        self.iconBottom.constant = 25;
    }
}

#pragma mark - NITDiscountCardPresenterProtocol
- (void)updateDiscountCardInfo
{
    self.whiteView.hidden = true;
    self.backgroundView.hidden = false;
    NITDiscountCardModel * model = [NITDiscountCardModel new];
    
    float bonus = [model.cardBonus doubleValue];
    NSMutableString * numberCode = [NSMutableString stringWithString:[NSString stringWithFormat:@"%C",[model.cardNumber characterAtIndex:0]]];
    for(int i = 1; i < [model.cardNumber length]; i++)
    {
        [numberCode appendString:[NSString stringWithFormat:@" %C",[model.cardNumber characterAtIndex:i]]];
    }
    self.barcodeLbl.text = numberCode;
    UIImage * code = [CodeGen genCodeWithContents:model.cardNumber machineReadableCodeObjectType:AVMetadataObjectTypeCode128Code];
    self.barcodeImg.image = code;
    
    self.bonusTitleLbl.text = [NSString stringWithFormat:@"НАКОПЛЕНО БОНУСОВ: %d", (int)bonus];
    
    float sum = [model.cardSum doubleValue];
    NITMobileConstantsModel * mobileConstants = [NITMobileConstantsModel new];
    NSArray * discountLevel = [mobileConstants discount_level];
    
    __block int maxPercent = 0;
    [discountLevel bk_each:^(NSDictionary *obj) {
        if([[obj objectForKey:@"percent"] intValue] > maxPercent) maxPercent = [[obj objectForKey:@"percent"] intValue];
    }];
    
    [discountLevel bk_each:^(NSDictionary *obj) {
        if((sum < [[obj objectForKey:@"max"] doubleValue]) && (sum > [[obj objectForKey:@"min"] doubleValue]))
        {
            self.discountTitleLbl.text = [NSString stringWithFormat:@"СКИДКА ПО КАРТЕ %d%@", [[obj objectForKey:@"percent"] intValue], @"%"];
            if([[obj objectForKey:@"percent"] intValue] != maxPercent)
            {
                int nextBonus = [[obj objectForKey:@"max"] intValue] - (int)sum;
                NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
                [numberFormatter setLocale:[NSLocale currentLocale]];
                [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
                [numberFormatter setMaximumFractionDigits:2];
                NSString * bonusFormatter = [numberFormatter stringFromNumber:[NSNumber numberWithInt:nextBonus]];

                self.discountSubtitleLbl.text = [NSString stringWithFormat:@"До скидки %d%@ осталось: %@ ₽", ([[obj objectForKey:@"percent"] intValue] + 1), @"%", bonusFormatter];
            }
            else
            {
                self.discountSubtitleLbl.hidden = true;
            }
        }
    }];
    
}

@end
