//
//  NITDiscountCardPresenter.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITDiscountCardProtocols.h"

@class NITDiscountCardWireFrame;

@interface NITDiscountCardPresenter : NSObject <NITDiscountCardPresenterProtocol, NITDiscountCardInteractorOutputProtocol>

@property (nonatomic, weak) id <NITDiscountCardViewProtocol> view;
@property (nonatomic, strong) id <NITDiscountCardInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITDiscountCardWireFrameProtocol> wireFrame;

@end
