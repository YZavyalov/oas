 //
//  NITDiscountCardPresenter.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITDiscountCardPresenter.h"
#import "NITDiscountCardWireframe.h"

@implementation NITDiscountCardPresenter

#pragma mark - NITDiscountCardPresenterProtocol
-(void)initData
{
    [self addObservers];
    [HELPER startLoading];
    if([NITDiscountCardModel new].cardIdentifier.length > 0) [self updateData];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kUpdateDiscountCardNotification object:nil];
}

- (void)updateData
{
    [HELPER stopLoading];
    [self.view updateDiscountCardInfo];
}

@end
