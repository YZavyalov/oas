//
//  NITDiscountCardWireFrame.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITDiscountCardProtocols.h"
#import "NITDiscountCardViewController.h"
#import "NITDiscountCardLocalDataManager.h"
#import "NITDiscountCardAPIDataManager.h"
#import "NITDiscountCardInteractor.h"
#import "NITDiscountCardPresenter.h"
#import "NITDiscountCardWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITDiscountCardWireFrame : NITRootWireframe <NITDiscountCardWireFrameProtocol>

@end
