//
//  NITDiscountCardWireFrame.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITDiscountCardWireFrame.h"

@implementation NITDiscountCardWireFrame

#pragma mark - NITDiscountCardWireFrameProtocol
+ (id)createNITDiscountCardModule;
{
    // Generating module components
    id <NITDiscountCardPresenterProtocol, NITDiscountCardInteractorOutputProtocol> presenter = [NITDiscountCardPresenter new];
    id <NITDiscountCardInteractorInputProtocol> interactor = [NITDiscountCardInteractor new];
    id <NITDiscountCardAPIDataManagerInputProtocol> APIDataManager = [NITDiscountCardAPIDataManager new];
    id <NITDiscountCardLocalDataManagerInputProtocol> localDataManager = [NITDiscountCardLocalDataManager new];
    id <NITDiscountCardWireFrameProtocol> wireFrame= [NITDiscountCardWireFrame new];
    id <NITDiscountCardViewProtocol> view = [(NITDiscountCardWireFrame *)wireFrame createViewControllerWithKey:_s(NITDiscountCardViewController) storyboardType:StoryboardTypeMap];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITDiscountCardModuleFrom:(UIViewController *)fromViewController
{
    NITDiscountCardViewController * vc = [NITDiscountCardWireFrame createNITDiscountCardModule];
    //[fromViewController.navigationController pushViewController:vc animated:true];
    [fromViewController.navigationController setViewControllers:[NSArray arrayWithObjects:vc, nil] animated:true];
    

}

+ (void)presentNITDiscountCardWithoutAnimationModuleFrom:(UIViewController *)fromViewController
{
    NITDiscountCardViewController * vc = [NITDiscountCardWireFrame createNITDiscountCardModule];
    [fromViewController.navigationController pushViewController:vc animated:false];
}

- (void)presentNITDiscountCardFrom:(UIViewController *)fromViewController
{
    
}


@end
