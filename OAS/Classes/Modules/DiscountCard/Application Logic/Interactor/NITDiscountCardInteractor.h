//
//  NITDiscountCardInteractor.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITDiscountCardProtocols.h"

@interface NITDiscountCardInteractor : NSObject <NITDiscountCardInteractorInputProtocol>

@property (nonatomic, weak) id <NITDiscountCardInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITDiscountCardAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITDiscountCardLocalDataManagerInputProtocol> localDataManager;

@end
