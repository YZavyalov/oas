//
//  NITDiscountCardModel.m
//  OAS
//
//  Created by Yaroslav on 09.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITDiscountCardModel.h"
#import "SLCollectionsApi.h"

static NSString * const kCardNumber         = @"kCardNumber";
static NSString * const kPhoneNumber        = @"kPhoneNumber";
static NSString * const kCardSum            = @"kCardSum";
static NSString * const kCardIdentifier     = @"kCardIdentifier";
static NSString * const kCardBonus          = @"kCardBonus";

@implementation NITDiscountCardModel

#pragma mark - Public
- (void)updateDiscountCard
{
    weaken(self);
    [self getDiscountCardWithHandler:^(NSArray<SLCollectionRow *> *result) {
        if(result)
        {
            for(SLCollectionRow * res in result)
            {
                if([res.key isEqualToString:@"sum"])     [weakSelf setCardSum:res.value];
                if([res.key isEqualToString:@"id"])      [weakSelf setCardIdentifier:res.value];
                if([res.key isEqualToString:@"number"])  [weakSelf setCardNumber:res.value];
            }
            [weakSelf sendNotification];

            [self getDiscountCardBonusesWithHandler:^(NSString *bonus) {
                if(bonus)
                {
                    [self setCardBonus:bonus];
                    [self sendNotification];
                }
            }];
        }
    }];
}

#pragma mark - Private
- (void)sendNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateDiscountCardNotification object:nil];\
}

- (void)getDiscountCardWithHandler:(void(^)(NSArray<SLCollectionRow *> *result))handler
{
    SLCollectionRow * body = [SLCollectionRow new];
    NITDiscountCardModel * model = [NITDiscountCardModel new];
    body.key = @"number"; body.value = model.cardNumber;
    [[SLCollectionsApi new] postStaticCollectionByCollectionkeyTupleSelectWithCollectionKey:@"cards" body:body completionHandler:^(NSArray<NSArray<SLCollectionRow> *> *output, NSError *error) {
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler (output.firstObject);
    }];
}

- (void)getDiscountCardBonusesWithHandler:(void(^)(NSString * bonus))handler
{
    NITDiscountCardModel * model = [NITDiscountCardModel new];
    NSString * stringURL = [[API bonusURL] stringByAppendingString:model.cardIdentifier];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:stringURL]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSString * str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        if(data)
        {
            NSRegularExpression * regexNumber = [NSRegularExpression regularExpressionWithPattern:@">(.*?)<" options:NSRegularExpressionCaseInsensitive error:nil];
            NSTextCheckingResult * resultNumber = [regexNumber firstMatchInString:str options:0 range:NSMakeRange(0, str.length)];
            NSRange rangeNumber = [resultNumber rangeAtIndex:1];
            if (handler) handler ([NSString stringWithFormat:@"%@", [str substringWithRange:rangeNumber]]);
        }
        else
        {
            [HELPER logError:error method:METHOD_NAME];
        }
    }] resume];
}

#pragma mark - Custom accessors
- (NSString *)cardNumber
{
    //return @"2210402753";
    return [self stringForKey:kCardNumber];
}

- (void)setCardNumber:(NSString *)cardNumber
{
    [self setString:cardNumber forKey:kCardNumber];
}

- (NSString *)phoneNumber
{
    return [self stringForKey:kPhoneNumber];
}

- (void)setPhoneNumber:(NSString *)phoneNumber
{
    [self setString:phoneNumber forKey:kPhoneNumber];
}

- (NSString *)cardSum
{
    return [self stringForKey:kCardSum];
}

- (void)setCardSum:(NSString *)cardSum
{
    [self setString:cardSum forKey:kCardSum];
}

- (NSString *)cardIdentifier
{
    return [self stringForKey:kCardIdentifier];
}

- (void)setCardIdentifier:(NSString *)cardIdentifier
{
    [self setString:cardIdentifier forKey:kCardIdentifier];
}

- (NSString *)cardBonus
{
    return [self stringForKey:kCardBonus];
}

- (void)setCardBonus:(NSString *)cardBonus
{
    [self setString:cardBonus forKey:kCardBonus];
}

@end
