//
//  NITDiscountCardModel.h
//  OAS
//
//  Created by Yaroslav on 09.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SLCollectionRow.h"

@interface NITDiscountCardModel : NITLocalSettings

@property (nonatomic) NSString * cardNumber;
@property (nonatomic) NSString * phoneNumber;
@property (nonatomic) NSString * cardSum;
@property (nonatomic) NSString * cardIdentifier;
@property (nonatomic) NSString * cardBonus;

- (void)updateDiscountCard;

@end
