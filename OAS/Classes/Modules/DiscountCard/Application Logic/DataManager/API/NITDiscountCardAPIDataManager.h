//
//  NITDiscountCardAPIDataManager.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITDiscountCardProtocols.h"

@interface NITDiscountCardAPIDataManager : NSObject <NITDiscountCardAPIDataManagerInputProtocol>

@end
