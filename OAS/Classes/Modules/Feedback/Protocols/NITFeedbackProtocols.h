//
//  NITFeedbackProtocols.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/18/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITFeedbackModel.h"

@protocol NITFeedbackInteractorOutputProtocol;
@protocol NITFeedbackInteractorInputProtocol;
@protocol NITFeedbackViewProtocol;
@protocol NITFeedbackPresenterProtocol;
@protocol NITFeedbackLocalDataManagerInputProtocol;
@protocol NITFeedbackAPIDataManagerInputProtocol;

@class NITFeedbackWireFrame;

typedef CF_ENUM (NSUInteger, FeedbackType) {
    FeedbackTypeName        = 0,
    FeedbackTypePhone       = 1,
    FeedbackTypeEmail       = 2,
    FeedbackTypeComment     = 3
};

// Defines the public interface that something else can use to drive the user interface
@protocol NITFeedbackViewProtocol
@required
@property (nonatomic, strong) id <NITFeedbackPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
-(void)updateSendButton:(NITFeedbackModel *)model;
@end

@protocol NITFeedbackWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITFeedbackModuleFrom:(id)fromViewController;
- (void)goBackFromViewController:(id)fromViewController;
@end

@protocol NITFeedbackPresenterProtocol
@required
@property (nonatomic, weak) id <NITFeedbackViewProtocol> view;
@property (nonatomic, strong) id <NITFeedbackInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITFeedbackWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (NITFeedbackModel *)updateModel;
- (void)sendFeedback;
- (void)updateFeedbackDataWithText:(NSString *)text withType:(FeedbackType)type;
- (void)updateAgreement:(BOOL)agreement;
@end

@protocol NITFeedbackInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)goBack;
@end

@protocol NITFeedbackInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITFeedbackInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITFeedbackAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITFeedbackLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)sendFeedbackWithModel:(NITFeedbackModel *)model;
@end


@protocol NITFeedbackDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITFeedbackAPIDataManagerInputProtocol <NITFeedbackDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)sendFeedbackWithModel:(NITFeedbackModel *)model withHnadler:(void(^)(NSError *error))handler;
@end

@protocol NITFeedbackLocalDataManagerInputProtocol <NITFeedbackDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
