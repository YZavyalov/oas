//
//  NITFeedbackWireFrame.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/18/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITFeedbackWireFrame.h"

@implementation NITFeedbackWireFrame

#pragma mark - Private
+ (id)createNITFeedbackModule
{
    // Generating module components
    id <NITFeedbackPresenterProtocol, NITFeedbackInteractorOutputProtocol> presenter = [NITFeedbackPresenter new];
    id <NITFeedbackInteractorInputProtocol> interactor = [NITFeedbackInteractor new];
    id <NITFeedbackAPIDataManagerInputProtocol> APIDataManager = [NITFeedbackAPIDataManager new];
    id <NITFeedbackLocalDataManagerInputProtocol> localDataManager = [NITFeedbackLocalDataManager new];
    id <NITFeedbackWireFrameProtocol> wireFrame= [NITFeedbackWireFrame new];
    id <NITFeedbackViewProtocol> view = [(NITFeedbackWireFrame *)wireFrame createViewControllerWithKey:_s(NITFeedbackViewController) storyboardType:StoryboardTypePharmacy];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITFeedbackWireFrameProtocol
+ (void)presentNITFeedbackModuleFrom:(UIViewController *)fromViewController
{
    NITFeedbackViewController * vc = [NITFeedbackWireFrame createNITFeedbackModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)goBackFromViewController:(UIViewController *)fromViewController
{
    [fromViewController.navigationController popViewControllerAnimated:true];
}

@end
