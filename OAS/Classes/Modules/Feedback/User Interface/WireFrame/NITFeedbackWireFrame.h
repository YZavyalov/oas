//
//  NITFeedbackWireFrame.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/18/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFeedbackProtocols.h"
#import "NITFeedbackViewController.h"
#import "NITFeedbackLocalDataManager.h"
#import "NITFeedbackAPIDataManager.h"
#import "NITFeedbackInteractor.h"
#import "NITFeedbackPresenter.h"
#import "NITFeedbackWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITFeedbackWireFrame : NITRootWireframe <NITFeedbackWireFrameProtocol>

@end
