//
//  NITFeedbackPresenter.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/18/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITFeedbackPresenter.h"
#import "NITFeedbackWireframe.h"

@interface NITFeedbackPresenter()

@property (nonatomic) NITFeedbackModel * model;

@end

@implementation NITFeedbackPresenter

#pragma mark - NITFeedbackPresenterProtocol
- (NITFeedbackModel *)updateModel
{
    self.model = [NITFeedbackModel new];
    
    return self.model;
}

- (void)updateFeedbackDataWithText:(NSString *)text withType:(FeedbackType)type
{
    switch (type)
    {
        case FeedbackTypeName:      self.model.name = text;
        break;
            
        case FeedbackTypePhone:     self.model.phone = text;
        break;
            
        case FeedbackTypeEmail:     self.model.email = text;
        break;
            
        case FeedbackTypeComment:   self.model.comment = text;
        break;
    }
    
    [self.view updateSendButton:self.model];
}

- (void)updateAgreement:(BOOL)agreement
{
    self.model.agreement = agreement;
    [self.view updateSendButton:self.model];
}

- (void)sendFeedback
{
    [self.interactor sendFeedbackWithModel:self.model];
}

#pragma mark - NITFeedbackInteractorOutputProtocol
- (void)goBack
{
    [self.wireFrame goBackFromViewController:self.view];
}

@end
