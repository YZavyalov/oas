//
//  NITFeedbackPresenter.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/18/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFeedbackProtocols.h"

@class NITFeedbackWireFrame;

@interface NITFeedbackPresenter : NSObject <NITFeedbackPresenterProtocol, NITFeedbackInteractorOutputProtocol>

@property (nonatomic, weak) id <NITFeedbackViewProtocol> view;
@property (nonatomic, strong) id <NITFeedbackInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITFeedbackWireFrameProtocol> wireFrame;

@end
