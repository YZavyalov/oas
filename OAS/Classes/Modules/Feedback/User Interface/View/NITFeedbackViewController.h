//
//  NITFeedbackViewController.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/18/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITFeedbackProtocols.h"
#import "NITBaseViewController.h"

@interface NITFeedbackViewController : NITBaseViewController <NITFeedbackViewProtocol>

@property (nonatomic, strong) id <NITFeedbackPresenterProtocol> presenter;

@end
