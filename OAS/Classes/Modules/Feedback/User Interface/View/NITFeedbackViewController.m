//
//  NITFeedbackViewController.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/18/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITFeedbackViewController.h"
#import "NITBaseHeaderTableViewCell.h"
#import "NITFeedbackCelTableViewCell.h"

@interface NITFeedbackViewController()
<
UITableViewDelegate,
UITableViewDataSource,
NITFeedbackCellDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UILabel * termsOfUSeLb;
@property (nonatomic) IBOutlet UIButton * termsOfUseBtn;
@property (nonatomic) IBOutlet UIButton * sendBtn;

@property (nonatomic) IBOutlet NSLayoutConstraint * tableViewH;
@property (nonatomic) IBOutlet NSLayoutConstraint * sendBtnBottom;

@property (nonatomic) NITFeedbackModel * model;

@end

@implementation NITFeedbackViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController hideTabBarAnimated:false];
}

#pragma mark - IBAction
- (IBAction)sendAction:(id)sender
{
    if(self.model.isCorrect)
    {
        [self.presenter sendFeedback];
    }
}

- (IBAction)termsOfUseAction:(id)sender
{
    [self.presenter updateAgreement:!self.model.agreement];
    [self.termsOfUseBtn setImage:[UIImage imageNamed:self.model.agreement ? @"checkbox_filled" : @"checkbox_empty"] forState:UIControlStateNormal];
}

#pragma mark - Private
- (void)initUI
{
    //default state
    self.model = [self.presenter updateModel];
    [self updateSendButton:self.model];
    self.model.agreement = true;
    [self.termsOfUseBtn setImage:[UIImage imageNamed:self.model.agreement ? @"checkbox_filled" : @"checkbox_empty"] forState:UIControlStateNormal];

    [self.termsOfUseBtn setImage:[UIImage imageNamed:self.model.agreement ? @"checkbox_filled" : @"checkbox_empty"] forState:UIControlStateNormal];
    self.sendBtnBottom.constant = -50;
    
    //Keyboard
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keybaordShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    //Table view
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITBaseHeaderTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITBaseHeaderTableViewCell)];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITFeedbackCelTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITFeedbackCelTableViewCell)];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self updateTableHight];
}

- (void)keybaordShow:(NSNotification *)notification
{
    [self.view layoutIfNeeded];
    self.sendBtnBottom.constant = [self keyboardHeight:notification] - 100;
    [UIView animateWithDuration:0.25 animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y = -50;
        self.view.frame = frame;
        
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardHide:(NSNotification *)notification
{
    [self.view layoutIfNeeded];
    self.sendBtnBottom.constant = -50;
    [UIView animateWithDuration:0.25 animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y = 0;
        self.view.frame = frame;
        
        [self.view layoutIfNeeded];
    }];
}

- (void)updateTableHight
{
    self.tableViewH.constant = 5 * 44;
}

#pragma mark - NITFeedbackPresenterProtocol
- (void)updateSendButton:(NITFeedbackModel *)model
{
    self.model = model;
    self.sendBtn.backgroundColor = self.model.isCorrect ? RGB(58, 160, 144) : RGB(225, 240, 238);
    self.sendBtn.tintColor = self.model.isCorrect ? RGB(254, 254, 254) : RGB(187, 219, 213);
}

#pragma mark - UItableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NITBaseHeaderTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITBaseHeaderTableViewCell)];
    cell.titleText = NSLocalizedString(@"КОНТАКТНАЯ ИНФОМРАЦИЯ", nil);
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITFeedbackCelTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITFeedbackCelTableViewCell) forIndexPath:indexPath];
    cell.feedbackType = indexPath.row;
    cell.model = self.model;
    cell.delegate = self;
    
    return cell;
}


#pragma mark - NITFeedbackCellDelegate
- (void)didEditingField:(NSString *)text withType:(FeedbackType)type
{
    [self.presenter updateFeedbackDataWithText:text withType:type];
}

@end
