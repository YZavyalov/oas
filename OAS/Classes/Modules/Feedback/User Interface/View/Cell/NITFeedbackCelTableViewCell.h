//
//  NITFeedbackCelTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 04.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITFeedbackProtocols.h"

@protocol  NITFeedbackCellDelegate <NSObject>
@optional
- (void)didEditingField:(NSString *)text withType:(FeedbackType)type;
@end

@interface NITFeedbackCelTableViewCell : UITableViewCell

@property (nonatomic) FeedbackType feedbackType;
@property (nonatomic) NITFeedbackModel * model;
@property (nonatomic, weak) id <NITFeedbackCellDelegate> delegate;

@end
