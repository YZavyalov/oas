//
//  NITFeedbackCelTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 04.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackCelTableViewCell.h"
#import "AKNumericFormatter.h"
#import "UITextField+AKNumericFormatter.h"

static NSString * formatterCodeMask = @"+7 (***) *** ** **";

@interface NITFeedbackCelTableViewCell()
<
UITextFieldDelegate
>

@property (nonatomic) IBOutlet UITextField * textField;

@end

@implementation NITFeedbackCelTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.textField.delegate = self;
}

#pragma mark - Custom accessors
- (void)setFeedbackType:(FeedbackType)feedbackType
{
    _feedbackType = feedbackType;
    
    [self updateData];
}

- (void)setModel:(NITFeedbackModel *)model
{
    _model = model;
    
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    [self.textField addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
    
    switch (self.feedbackType)
    {
        case FeedbackTypeName:
        {
            self.textField.placeholder = NSLocalizedString(@"Имя", nil);
            self.textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
            self.textField.text = self.model.name;
        }
        break;
            
        case FeedbackTypePhone:
        {
            self.textField.numericFormatter = [AKNumericFormatter formatterWithMask:formatterCodeMask
                                                                placeholderCharacter:'*'
                                                                                mode:AKNumericFormatterMixed];
            self.textField.placeholder = @"+7 (   )";
            [self.textField setKeyboardType:UIKeyboardTypePhonePad];
            if([self.model.phone length] > 4) self.textField.text = self.model.phone;
            else                              self.textField.text = @"+7";
        }
        break;
            
        case FeedbackTypeEmail:
        {
            self.textField.placeholder = NSLocalizedString(@"E-mail", nil);
            [self.textField setKeyboardType:UIKeyboardTypeEmailAddress];
            self.textField.text = self.model.email;
        }
        break;
            
        case FeedbackTypeComment:
        {
            self.textField.placeholder = NSLocalizedString(@"Комментарий", nil);
            self.textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
            self.textField.text = self.model.comment;
        }
        break;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([textField.text isEqualToString:@"+7"] && [string isEqualToString:@""] && (self.feedbackType == FeedbackTypePhone))
    {
        return false;
    }
    else
    {
        return true;
    }
}


#pragma mark - UITextFieldDelegate
- (void)textFieldDidChange
{
    if ([self.delegate respondsToSelector:@selector(didEditingField:withType:)])
    {
        [self.delegate didEditingField:self.textField.text withType:self.feedbackType];
    }
}

@end
