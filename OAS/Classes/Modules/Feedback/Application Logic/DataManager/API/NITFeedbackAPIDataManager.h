//
//  NITFeedbackAPIDataManager.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/18/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFeedbackProtocols.h"

@interface NITFeedbackAPIDataManager : NSObject <NITFeedbackAPIDataManagerInputProtocol>

@end
