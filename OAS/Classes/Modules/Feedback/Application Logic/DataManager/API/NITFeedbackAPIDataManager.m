//
//  NITFeedbackAPIDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/18/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITFeedbackAPIDataManager.h"
#import "SLSupportApi.h"

@implementation NITFeedbackAPIDataManager

- (void)sendFeedbackWithModel:(NITFeedbackModel *)model withHnadler:(void(^)(NSError *error))handler
{
    [HELPER startLoading];
    SLSupportFeedback * feedback = [SLSupportFeedback new];
    feedback.comment = model.comment;
    feedback.email = model.email;
    feedback.name = model.name;
    feedback.phone = model.phone;
    [[SLSupportApi new] postSupportFeedbackWithSupportFeedback:feedback completionHandler:^(NSError *error) {
        [HELPER stopLoading];
        if(error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
        if(handler) handler(error);
    }];
}

@end
