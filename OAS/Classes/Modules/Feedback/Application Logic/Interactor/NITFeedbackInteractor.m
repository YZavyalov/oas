//
//  NITFeedbackInteractor.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/18/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITFeedbackInteractor.h"

@implementation NITFeedbackInteractor

#pragma mark - NITFeedbackInteractorInputProtocol
- (void)sendFeedbackWithModel:(NITFeedbackModel *)model
{
    [self.APIDataManager sendFeedbackWithModel:model withHnadler:^(NSError *error) {
        if(error)
        {
            [ERROR_MESSAGE errorMessage:feedback_send_error];
        }
        else
        {
            [self.presenter goBack];
        }
    }];
    //[ERROR_MESSAGE errorMessage:feedback_send_error];
    //TODO: Need to send feedback
    //[self.presenter goBack];
}

@end
