//
//  NITFeedbackModel.h
//  OAS
//
//  Created by Yaroslav on 04.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NITFeedbackModel : NITLocalSettings

@property (nonatomic) NSString * name;
@property (nonatomic) NSString * phone;
@property (nonatomic) NSString * email;
@property (nonatomic) NSString * comment;
@property (nonatomic) BOOL agreement;

- (BOOL)isCorrect;

@end
