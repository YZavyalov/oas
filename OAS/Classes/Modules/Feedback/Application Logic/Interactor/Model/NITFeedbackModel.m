//
//  NITFeedbackModel.m
//  OAS
//
//  Created by Yaroslav on 04.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITFeedbackModel.h"

static NSString * const kFeedbackName       = @"kFeedbackName";
static NSString * const kFeedbackPhone      = @"kFeedbackPhone";
static NSString * const kFeedbackEmail      = @"kFeedbackEmail";
static NSString * const kFeedbackComment    = @"kFeedbackComment";
static NSString * const kFeedbackAgreement  = @"kFeedbackAgreement";

@implementation NITFeedbackModel

#pragma mark - Custom Accessors
- (NSString *)name
{
    return [self stringForKey:kFeedbackName];
}

- (void)setName:(NSString *)name
{
    [self setString:name forKey:kFeedbackName];
}

- (NSString *)phone
{
    return [self stringForKey:kFeedbackPhone];
}

- (void)setPhone:(NSString *)phone
{
    [self setString:phone forKey:kFeedbackPhone];
}

- (NSString *)email
{
    return [self stringForKey:kFeedbackEmail];
}

- (void)setEmail:(NSString *)email
{
    [self setString:email forKey:kFeedbackEmail];
}

- (NSString *)comment
{
    return [self stringForKey:kFeedbackComment];
}

- (void)setComment:(NSString *)comment
{
    [self setString:comment forKey:kFeedbackComment];
}

- (BOOL)agreement
{
    return [self boolForKey:kFeedbackAgreement];
}

- (void)setAgreement:(BOOL)agreement
{
    [self setBool:agreement forKey:kFeedbackAgreement];
}

#pragma mark - Public
- (BOOL)isCorrect
{
    BOOL correct = true;
    
    if ([self.name length] == 0)
    {
        correct = false;
    }
    
    if([self.comment length] == 0)
    {
        correct = false;
    }
    
    /*if (([self.phone length] == 0) && ([self.email length] == 0) && (![self isValidEmail]))
    {
        correct = false;
    }
    
    if (([self.email length] > 0) && (![self isValidEmail]) && ([self.phone length] == 0))
    {
        correct = false;
    }
    
    if(([self.phone length] > 0) && (self.phone.length != 18))
    {
        correct = false;
    }*/
    if(([self.phone length] < 18) && (![self isValidEmail]))
    {
        correct = false;
    }
    
    if(!self.agreement)
    {
        correct = false;
    }
    
    return correct;
}

#pragma mark - Private
- (BOOL)isValidEmail
{
    if (self.email.length == 0)
    {
        return false;
    }
    
    NSString * emailid = self.email;
    NSString * emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate * emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:emailid];
}


@end
