//
//  NITFeedbackInteractor.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/18/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFeedbackProtocols.h"

@interface NITFeedbackInteractor : NSObject <NITFeedbackInteractorInputProtocol>

@property (nonatomic, weak) id <NITFeedbackInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITFeedbackAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITFeedbackLocalDataManagerInputProtocol> localDataManager;

@end
