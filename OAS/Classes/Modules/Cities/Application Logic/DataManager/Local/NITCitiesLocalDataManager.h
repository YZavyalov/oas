//
//  NITCitiesLocalDataManager.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/20/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCitiesProtocols.h"

@interface NITCitiesLocalDataManager : NSObject <NITCitiesLocalDataManagerInputProtocol>

@end
