//
//  NITCitiesLocalDataManager.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/20/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCitiesLocalDataManager.h"
#import "NITCityItem.h"
#import "NITPharmacyItem.h"

@implementation NITCitiesLocalDataManager

#pragma mark - NITCitiesDataManagerInputProtocol
- (NSArray <NITCityItem *> *)getCitites
{
    return [NITCityItem allCityItems];
}

- (void)updateShops
{
    [[NITPharmacyItem new] updatePharmacyItems];
}

- (void)deleteAllBasket
{
    [NITBasketItem deleteAllBasketItems];
}

@end
