//
//  NITCitiesInteractor.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/20/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCitiesProtocols.h"

@interface NITCitiesInteractor : NSObject <NITCitiesInteractorInputProtocol>

@property (nonatomic, weak) id <NITCitiesInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCitiesAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCitiesLocalDataManagerInputProtocol> localDataManager;

@end
