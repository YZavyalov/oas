//
//  NITCitiesSectionModel.m
//  OAS
//
//  Created by Yaroslav on 02.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCitiesSectionModel.h"
#import "NITCityTableViewCell.h"

@implementation NITCitiesSectionModel

#pragma mark - Life cycle
- (instancetype)initWithModels:(NSArray <NITCityItemModel *> *)models withTitle:(NSString *)title
{
    if (self = [super init])
    {
        _models = models;
        _headerTitle = title;
    }
    
    return self;
}

#pragma mark - Custom accessors
- (NSString *)cellID
{
    return _s(NITCityTableViewCell);
}

- (float)headerHeight
{
    return 35;
}

@end
