//
//  NITCityItemModel.m
//  OAS
//
//  Created by Yaroslav on 04.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCityItemModel.h"
#import "NITCityItem.h"

@implementation NITCityItemModel

#pragma mark - Life cycle
- (instancetype)initWithModel:(NITCityItem *)model
{
    if (self = [super init])
    {
        self.identifier = model.identifier;
        self.name = model.name;
        self.parent = model.parent;
        self.lat = model.lat;
        self.lon = model.lon;
        self.delivery = model.delivery;
    }
    
    return self;
}

#pragma mark - Public
+ (NSArray <NITCityItemModel *> *)cityModelFromEntities:(NSArray <NITCityItem *> *)entities
{
    return [entities bk_map:^id(NITCityItem * obj) {
        return [[NITCityItemModel alloc] initWithModel:obj];
    }];
}

@end
