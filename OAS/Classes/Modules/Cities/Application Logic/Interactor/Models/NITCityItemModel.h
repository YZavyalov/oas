//
//  NITCityItemModel.h
//  OAS
//
//  Created by Yaroslav on 04.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NITCityItem;

@interface NITCityItemModel : NSObject

@property (nonatomic) NSString * identifier;
@property (nonatomic) NSString * name;
@property (nonatomic) NSString * parent;
@property (nonatomic) NSString * defaultTemplateID;
@property (nonatomic) NSString * lat;
@property (nonatomic) NSString * lon;
@property (nonatomic) NSString * delivery;

- (instancetype)initWithModel:(NITCityItem *)model;
+ (NSArray <NITCityItemModel *> *)cityModelFromEntities:(NSArray <NITCityItem *> *)entities;

@end
