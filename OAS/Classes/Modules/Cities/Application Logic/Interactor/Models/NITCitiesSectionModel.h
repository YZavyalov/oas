//
//  NITCitiesSectionModel.h
//  OAS
//
//  Created by Yaroslav on 02.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITGroupModel.h"
#import "NITCityItemModel.h"
#import "SLInfoItem.h"
#import "SLIPropertyValue.h"

@interface NITCitiesSectionModel : NSObject

@property (nonatomic) NSArray <NITCityItemModel *> * models;
@property (nonatomic) NSString * headerTitle;
@property (nonatomic) float headerHeight;
@property (nonatomic) NSString * cellID;

- (instancetype)initWithModels:(NSArray <NITCityItemModel *> *)models withTitle:(NSString *)title;

@end
