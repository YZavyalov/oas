//
//  NITCitiesInteractor.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/20/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCitiesInteractor.h"

@implementation NITCitiesInteractor

#pragma mark - NITCitiesInteractorInputProtocol
- (void)getCities
{
    [HELPER startLoading];
    NSArray <NITCityItemModel *> * cities = [NITCityItemModel cityModelFromEntities:[self.localDataManager getCitites]];
    if(cities.count > 0)
    {
        [HELPER stopLoading];
        [self.presenter updateCitiesModels:cities];
    }
    else
    {
        [ERROR_MESSAGE errorMessage:error_service_unavailable];
    }
}

- (void)getShopsByCity
{
    [self.localDataManager updateShops];
}

- (void)deleteAllBasket
{
    [self.localDataManager deleteAllBasket];
}

@end
