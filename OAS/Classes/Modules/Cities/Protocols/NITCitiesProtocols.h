//
//  NITCitiesProtocols.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/20/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITGroupModel.h"
#import "NITCitiesSectionModel.h"
#import "NITShopItemModel.h"
#import "NITCityItemModel.h"
#import "NITBasketItem.h"

@protocol NITCitiesInteractorOutputProtocol;
@protocol NITCitiesInteractorInputProtocol;
@protocol NITCitiesViewProtocol;
@protocol NITCitiesPresenterProtocol;
@protocol NITCitiesLocalDataManagerInputProtocol;
@protocol NITCitiesAPIDataManagerInputProtocol;

@class NITCitiesWireFrame;

typedef CF_ENUM (NSUInteger, CityViewType) {
    CityViewTypeDefault = 0,
    CityViewTypeSelect  = 1
};

typedef void (^SelectCityBlock) ();

// Defines the public interface that something else can use to drive the user interface
@protocol NITCitiesViewProtocol
@required
@property (nonatomic, strong) id <NITCitiesPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadSectionByModels:(NSArray <NITCitiesSectionModel *> *)sections;
@end

@protocol NITCitiesWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
@property (nonatomic) SelectCityBlock block;
+ (void)presentNITCitiesModuleFrom:(id)fromView withType:(CityViewType)viewType withHandler:(SelectCityBlock)handler;
- (void)closeView:(id)view withLoader:(BOOL)loader;
- (void)showAlertFromView:(id)fromViewController alert:(id)alert;
@end

@protocol NITCitiesPresenterProtocol
@required
@property (nonatomic, weak) id <NITCitiesViewProtocol> view;
@property (nonatomic, strong) id <NITCitiesInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCitiesWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)updateData;
- (void)cancel;
- (void)updateSearchWithString:(NSString *)searchText;
- (void)selectCity:(NITCityItemModel *)city;
@end

@protocol NITCitiesInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateCitiesModels:(NSArray<NITCityItemModel *> *)models;
@end

@protocol NITCitiesInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCitiesInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCitiesAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCitiesLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getCities;
- (void)getShopsByCity;
- (void)deleteAllBasket;
@end


@protocol NITCitiesDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCitiesAPIDataManagerInputProtocol <NITCitiesDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITCitiesLocalDataManagerInputProtocol <NITCitiesDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NSArray <NITCityItem *> *)getCitites;
- (void)updateShops;
- (void)deleteAllBasket;
@end
