//
//  NITCitiesViewController.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/20/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCitiesViewController.h"
#import "NITCityTableViewCell.h"

@interface NITCitiesViewController()
<
UISearchBarDelegate,
UITableViewDelegate,
UITableViewDataSource,
CLLocationManagerDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UIView * emptyView;

@property (nonatomic) IBOutlet NSLayoutConstraint * tableViewBottom;

@property (nonatomic) NSArray <NITCitiesSectionModel *> * sections;

@end

@implementation NITCitiesViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter updateData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.titleView = self.mainTitle;
    self.navigationItem.rightBarButtonItem = self.rightBarButtonItem;
    self.navigationItem.leftBarButtonItem = self.leftBarButtonItem;
    self.navigationItem.hidesBackButton = true;
}

#pragma mark - IBAction
- (IBAction)cancelAction:(id)sender
{
    [self.presenter cancel];
}

#pragma mark - Private
- (void)initUI
{
    //Default state
    [self.navigationController setNavigationBarHidden:false animated:false];
    switch (self.viewType)
    {
        case CityViewTypeDefault:
            self.navigationItem.leftBarButtonItem = nil;
            break;
            
        case CityViewTypeSelect:
            break;
    }
    
    //Keyboard
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    //Table view
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITCityTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITCityTableViewCell)];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self initSearchBar];
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    [self.view layoutIfNeeded];
    self.tableViewBottom.constant = [self keyboardHeight:notification];
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    [self.view layoutIfNeeded];
    self.tableViewBottom.constant = 0;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - NITCitiesPresenterProtocol
- (void)reloadSectionByModels:(NSArray <NITCitiesSectionModel *> *)sections
{
    if((!(self.searchBar.text.length > 0) && sections.count == 1 && sections.firstObject.models.count == 1 && [sections.firstObject.models.firstObject.identifier isEqualToString:USER.cityID]) || (sections.count == 0))
    {
        self.emptyView.hidden = false;
        self.tableView.hidden = true;
    }
    else
    {
        self.emptyView.hidden = true;
        self.tableView.hidden = false;
        self.sections = sections;
        [self.tableView reloadData];
    }
}

#pragma mark - UITableViewdataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.sections objectAtIndex:section].headerTitle;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.sections objectAtIndex:section].models count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCityTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITCityTableViewCell) forIndexPath:indexPath];
    cell.model = [[self.sections objectAtIndex:indexPath.section].models objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.presenter selectCity:[[self.sections objectAtIndex:indexPath.section].models objectAtIndex:indexPath.row]];
}

#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self.presenter updateSearchWithString:searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.searchBar.text = @"";
    self.searchBar.showsCancelButton = false;
    [self.searchBar endEditing:true];
    [self.presenter updateSearchWithString:@""];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar endEditing:true];
    [self enableSearchBarCanelButton];
    [self.presenter updateSearchWithString:searchBar.text];
}

@end
