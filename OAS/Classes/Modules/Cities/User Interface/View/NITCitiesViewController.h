//
//  NITCitiesViewController.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/20/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCitiesProtocols.h"
#import "NITBaseViewController.h"

@interface NITCitiesViewController : NITBaseViewController <NITCitiesViewProtocol>

@property (nonatomic, strong) id <NITCitiesPresenterProtocol> presenter;

@property (nonatomic) CityViewType viewType;

@end
