//
//  NITCityTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 20.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCityTableViewCell.h"

@interface NITCityTableViewCell()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UIImageView * checkmarkImg;

@end

@implementation NITCityTableViewCell

#pragma mark - Custom accessors
- (void)setModel:(NITCityItemModel *)model
{
    _model = model;
    
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.model.name;
    [self.title setFont:[self.model.identifier isEqualToString:USER.cityID] ? [UIFont boldSystemFontOfSize:17] : [UIFont systemFontOfSize:17]];
    self.checkmarkImg.hidden = ![self.model.identifier isEqualToString:USER.cityID];
}

@end
