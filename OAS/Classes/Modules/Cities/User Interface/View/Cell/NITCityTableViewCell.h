//
//  NITCityTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 20.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITGroupModel.h"
#import "NITCityItemModel.h"

@interface NITCityTableViewCell : UITableViewCell

@property (nonatomic) NITCityItemModel * model;

@end
