//
//  NITCitiesPresenter.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/20/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCitiesProtocols.h"

@class NITCitiesWireFrame;

@interface NITCitiesPresenter : NSObject <NITCitiesPresenterProtocol, NITCitiesInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCitiesViewProtocol> view;
@property (nonatomic, strong) id <NITCitiesInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCitiesWireFrameProtocol> wireFrame;

@end
