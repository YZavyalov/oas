//
//  NITCitiesPresenter.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/20/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCitiesPresenter.h"
#import "NITCitiesWireframe.h"
#import "NITCityItem.h"
#import "NITDeliveryInfoModel.h"

@interface NITCitiesPresenter()

@property (nonatomic) NSMutableArray <NITCitiesSectionModel *> * sections;
@property (nonatomic) NSArray <NITCityItemModel *> * models;

@end

@implementation NITCitiesPresenter

#pragma mark - NITCitiesPresenterProtocol
- (void)updateData
{
    [self addObservers];
    [self updateCity];
}

- (void)updateSearchWithString:(NSString *)searchText
{
    NSArray <NITCityItemModel *> * searchModel = [self.models bk_select:^BOOL(NITCityItemModel * obj) {
        return ([obj.name rangeOfString:searchText].location != NSNotFound);
    }];
    
    [self updateSections:searchText.length > 0 ? searchModel : self.models searchText:searchText];
}

- (void)selectCity:(NITCityItemModel *)city
{
    if([HELPER isFirstLanch])
    {
        USER.cityName = city.name;
        USER.cityID = city.identifier;
        
        NITCityItem * currentCity = [[NITCityItem allCityItems] bk_select:^BOOL(NITCityItem *obj) {
            return [obj.name isEqualToString:USER.cityName];
        }].firstObject;
        USER.cityDelivery = currentCity.delivery.length > 0 ? true : false;
        USER.cityLat = [currentCity.lat floatValue];
        USER.cityLon = [currentCity.lon floatValue];
        
        [[NITDeliveryInfoModel new] setShop_address:nil];
        [[NITDeliveryInfoModel new] setShop_id:nil];
        [[NITDeliveryInfoModel new] setIsHub:false];
        
        [self.interactor getShopsByCity];
        [[NSNotificationCenter defaultCenter] postNotificationName:kChangeCityNotification object:nil];
        [self closeView];
    }
    else
    {
        [self showAlert:city];
    }
}

- (void)cancel
{
    [self.wireFrame closeView:self.view withLoader:false];
}

#pragma mark - NITCitiesInteractorOutputProtocol
- (void)updateCitiesModels:(NSArray<NITCityItemModel *> *)models
{
    self.models = models;
    [self updateSections:self.models searchText:nil];
}

- (void)closeView
{
    [self.wireFrame closeView:self.view withLoader:true];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCity) name:kUpdateCityNotification object:nil];
}

- (void)updateCity
{
    [self.interactor getCities];
}

- (void)updateSections:(NSArray <NITCityItemModel *> *)models searchText:(NSString *)searchText
{
    self.sections = [NSMutableArray new];
    NSArray <NITCityItemModel *> * cities = models;
    
    if(!(searchText.length > 0))
    {
        if(([CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) && ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusNotDetermined) && [HELPER isFirstLanch])
        {
            __block float distance = MAXFLOAT;
            [models bk_each:^(NITCityItemModel *obj) {
                CLLocation * location = [[CLLocation alloc] initWithLatitude:[obj.lat floatValue] longitude:[obj.lon floatValue]];
                if([LOCATION_MANAGER.currentLocation distanceFromLocation:location] < distance)
                {
                    distance = [LOCATION_MANAGER.currentLocation distanceFromLocation:location];
                    USER.cityID = obj.identifier;
                    USER.cityName = obj.name;
                    USER.cityDelivery = obj.delivery.length > 0 ? true : false;
                    USER.cityLat = [obj.lat floatValue];
                    USER.cityLon = [obj.lon floatValue];
                }
            }];
        }
        
        if(USER.cityID.length > 0)
        {
            NITCityItemModel * city = [NITCityItemModel new];
            city.identifier = USER.cityID;
            city.name       = USER.cityName;
            NITCitiesSectionModel * section = [[NITCitiesSectionModel alloc] initWithModels:@[city] withTitle:@""];
            [self.sections addObject:section];
        }
    }
    
    NSMutableDictionary * itemsAlphabetically = [self alphabeticallyItems:cities];
    NSArray <NSString *> * citySectionTitle = [[itemsAlphabetically allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    for(NSString * title in citySectionTitle)
    {
        NSString * sectionTitle = [citySectionTitle objectAtIndex:[citySectionTitle indexOfObject:title]];
        NSArray <NITCityItemModel *> * sectionCities = [itemsAlphabetically objectForKey:sectionTitle];
        
        NITCitiesSectionModel * section = [[NITCitiesSectionModel alloc] initWithModels:sectionCities withTitle:[NSString stringWithFormat:@"%@", sectionTitle]];
        [self.sections addObject:section];
    }
    
    [self.sections bk_each:^(NITCitiesSectionModel *obj) {
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:true selector:@selector(compare:)];
        obj.models = [obj.models sortedArrayUsingDescriptors:@[sort]];
    }];

    [self.view reloadSectionByModels:[self.sections mutableCopy]];
}

- (NSMutableDictionary *)alphabeticallyItems:(NSArray <NITCityItemModel *> *)items
{
    NSMutableDictionary * dictAlphabet = [NSMutableDictionary dictionary];
    
    for (NITCityItemModel *item in items)
    {
        NSString *firstLetter = [item.name substringWithRange:NSMakeRange(0, 1)];
        NSMutableArray *arrayForLetter = [dictAlphabet objectForKey:firstLetter];
        if (arrayForLetter == nil)
        {
            arrayForLetter = [NSMutableArray array];
            [dictAlphabet setObject:arrayForLetter forKey:firstLetter];
        }
        [arrayForLetter addObject:item];
    }
    
    return dictAlphabet;
}

- (void)showAlert:(NITCityItemModel *)city
{
    weaken(self);
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"При изменении города корзина будет очищена. Изменить город?", nil)
                                                                    message:nil
                                                             preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = RGB(58, 160, 144);
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Отмена", nil)
                                                      style:UIAlertActionStyleCancel
                                                    handler:^(UIAlertAction * action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                    }];
    
    UIAlertAction * action1 = [UIAlertAction actionWithTitle:NSLocalizedString(@"Изменить", nil)
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action) {
                                                        USER.cityName = city.name;
                                                        USER.cityID = city.identifier;
                                                        
                                                        NITCityItem * currentCity = [[NITCityItem allCityItems] bk_select:^BOOL(NITCityItem *obj) {
                                                            return [obj.name isEqualToString:USER.cityName];
                                                        }].firstObject;
                                                        USER.cityDelivery = currentCity.delivery.length > 0 ? true : false;
                                                        USER.cityLat = [currentCity.lat floatValue];
                                                        USER.cityLon = [currentCity.lon floatValue];
                                                        
                                                        [[NITDeliveryInfoModel new] setShop_address:nil];
                                                        [[NITDeliveryInfoModel new] setShop_id:nil];
                                                        [[NITDeliveryInfoModel new] setIsHub:false];
                                                        
                                                        [weakSelf.interactor deleteAllBasket];
                                                        [weakSelf.interactor getShopsByCity];
                                                        [[NSNotificationCenter defaultCenter] postNotificationName:kChangeCityNotification object:nil];
                                                        [weakSelf closeView];
                                                    }];
    
    [alert addAction:cancel];
    [alert addAction:action1];
    
    [self.wireFrame showAlertFromView:self.view alert:alert];
}

@end
