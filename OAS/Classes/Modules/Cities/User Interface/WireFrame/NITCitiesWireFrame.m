//
//  NITCitiesWireFrame.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/20/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCitiesWireFrame.h"

@implementation NITCitiesWireFrame

#pragma mark - Private
+ (id)createNITCitiesModuleWithHandler:(SelectCityBlock)handler
{
    // Generating module components
    id <NITCitiesPresenterProtocol, NITCitiesInteractorOutputProtocol> presenter = [NITCitiesPresenter new];
    id <NITCitiesInteractorInputProtocol> interactor = [NITCitiesInteractor new];
    id <NITCitiesAPIDataManagerInputProtocol> APIDataManager = [NITCitiesAPIDataManager new];
    id <NITCitiesLocalDataManagerInputProtocol> localDataManager = [NITCitiesLocalDataManager new];
    id <NITCitiesWireFrameProtocol> wireFrame= [NITCitiesWireFrame new];
    id <NITCitiesViewProtocol> view = [(NITCitiesWireFrame *)wireFrame createViewControllerWithKey:_s(NITCitiesViewController) storyboardType:StoryboardTypeMain];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    wireFrame.block = handler;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITCitiesWireFrameProtocol
+ (void)presentNITCitiesModuleFrom:(UIViewController *)fromViewController withType:(CityViewType)viewType withHandler:(SelectCityBlock)handler
{
    NITCitiesViewController * vc = [NITCitiesWireFrame createNITCitiesModuleWithHandler:handler];
    vc.viewType = viewType;
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    [fromViewController presentViewController:navigationController animated:true completion:^{}];
}

- (void)closeView:(UIViewController *)viewController withLoader:(BOOL)loader
{
    [viewController dismissViewControllerAnimated:true completion:^{
        if (self.block) self.block();
        if(loader) [HELPER startLoading];
    }];
}

- (void)showAlertFromView:(UIViewController *)fromViewController alert:(UIAlertController *)alert
{
    [fromViewController presentViewController:alert animated:YES completion:nil];
}

@end
