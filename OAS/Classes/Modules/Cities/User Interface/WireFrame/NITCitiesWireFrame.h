//
//  NITCitiesWireFrame.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/20/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCitiesProtocols.h"
#import "NITCitiesViewController.h"
#import "NITCitiesLocalDataManager.h"
#import "NITCitiesAPIDataManager.h"
#import "NITCitiesInteractor.h"
#import "NITCitiesPresenter.h"
#import "NITCitiesWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITCitiesWireFrame : NITRootWireframe <NITCitiesWireFrameProtocol>

@property (nonatomic) SelectCityBlock block;

@end
