//
//  NITGuideViewModel.h
//  OAS
//
//  Created by Eugene Parafiynyk on 8/2/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NITGuideViewModel : NSObject

@property (nonatomic) NSString * title;
@property (nonatomic) NSString * text;
@property (nonatomic) NSString * bgImageName;
@property (nonatomic) NSString * gifName;
@property (nonatomic) CGFloat gifTime;

@end
