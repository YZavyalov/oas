//
//  NITGuideInteractor.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITGuideProtocols.h"

@interface NITGuideInteractor : NSObject <NITGuideInteractorInputProtocol>

@property (nonatomic, weak) id <NITGuideInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITGuideAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITGuideLocalDataManagerInputProtocol> localDataManager;

@end
