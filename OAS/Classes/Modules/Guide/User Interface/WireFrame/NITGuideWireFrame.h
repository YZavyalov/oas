//
//  NITGuideWireFrame.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITGuideProtocols.h"
#import "NITGuideViewController.h"
#import "NITGuideLocalDataManager.h"
#import "NITGuideAPIDataManager.h"
#import "NITGuideInteractor.h"
#import "NITGuidePresenter.h"
#import "NITGuideWireframe.h"
#import <UIKit/UIKit.h>

@interface NITGuideWireFrame : NITRootWireframe <NITGuideWireFrameProtocol>

@end
