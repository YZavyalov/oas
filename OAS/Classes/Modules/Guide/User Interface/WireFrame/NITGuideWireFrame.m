//
//  NITGuideWireFrame.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGuideWireFrame.h"
#import "NITTabBarWireFrame.h"

@implementation NITGuideWireFrame

#pragma mark - Private
+ (id)createNITGuideModule
{
    // Generating module components
    id <NITGuidePresenterProtocol, NITGuideInteractorOutputProtocol> presenter = [NITGuidePresenter new];
    id <NITGuideInteractorInputProtocol> interactor = [NITGuideInteractor new];
    id <NITGuideAPIDataManagerInputProtocol> APIDataManager = [NITGuideAPIDataManager new];
    id <NITGuideLocalDataManagerInputProtocol> localDataManager = [NITGuideLocalDataManager new];
    id <NITGuideWireFrameProtocol> wireFrame= [NITGuideWireFrame new];
    id <NITGuideViewProtocol> view = [(NITGuideWireFrame *)wireFrame createViewControllerWithKey:_s(NITGuideViewController) storyboardType:StoryboardTypeMain];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITGuideWireFrameProtocol
+ (void)presentNITGuideModuleFrom:(UIViewController *)fromViewController
{
    NITGuideViewController * vc = [NITGuideWireFrame createNITGuideModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)presentNITGuideModuleFromWindow:(UIWindow *)window
{
    if ([HELPER isFirstLanch])
    {
        NITGuideViewController * vc = [NITGuideWireFrame createNITGuideModule];
        [self showRootViewController:vc inWindow:window];
    }
    else
    {
        [NITTabBarWireFrame presentNITTabBarModuleFromWindow:window];
    }
}

- (void)showTabbar
{
    [NITTabBarWireFrame presentNITTabBarModuleFromWindow:WINDOW];
}

@end
