//
//  NITGuidePresenter.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGuidePresenter.h"
#import "NITGuideWireframe.h"

@implementation NITGuidePresenter

#pragma mark - NITGuidePresenterProtocol
- (void)initData
{
    NITGuideViewModel * model1 = [NITGuideViewModel new];
    model1.title = NSLocalizedString(@"Удобные заказы", nil);
    model1.text = NSLocalizedString(@"Выбирайте удобный способ\nполучения заказа — в ближайшей аптеке\nили с доставкой на дом", nil);
    model1.bgImageName = @"guide_image_1";
    model1.gifName = @"guide1.gif";
    model1.gifTime = 7.54;
    
    NITGuideViewModel * model2 = [NITGuideViewModel new];
    model2.title = NSLocalizedString(@"Избранное", nil);
    model2.text = NSLocalizedString(@"Добавляйте любимые товары\nв «Избранное»", nil);
    model2.bgImageName = @"guide_image_2";
    model2.gifName = @"guide2.gif";
    model2.gifTime = 3.96;
    
    NITGuideViewModel * model3 = [NITGuideViewModel new];
    model3.title = NSLocalizedString(@"Обратная связь", nil);
    model3.text = NSLocalizedString(@"Оставляйте отзывы\nоб обслуживании и предложения\nпо ассортименту", nil);
    model3.bgImageName = @"guide_image_3";
    model3.gifName = @"guide3.gif";
    model3.gifTime = 11.15;
    
    [self.view reloadDataByItems:@[model1,model2,model3]];
}

- (void)skipGuide
{
    [HELPER setFirstLanch];
    [self.wireFrame showTabbar];
}

@end
