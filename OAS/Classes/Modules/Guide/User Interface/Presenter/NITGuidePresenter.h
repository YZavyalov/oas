//
//  NITGuidePresenter.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITGuideProtocols.h"

@class NITGuideWireFrame;

@interface NITGuidePresenter : NITRootPresenter <NITGuidePresenterProtocol, NITGuideInteractorOutputProtocol>

@property (nonatomic, weak) id <NITGuideViewProtocol> view;
@property (nonatomic, strong) id <NITGuideInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITGuideWireFrameProtocol> wireFrame;

@end
