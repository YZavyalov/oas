//
//  NITGuideBasketView.h
//  OAS
//
//  Created by Yaroslav on 03.08.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITGuideBasketView : UIView

+ (instancetype)showWithView:(UIView *)selfView;
+ (BOOL)notYetShown;

@end
