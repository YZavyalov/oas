//
//  NITGuideBasketView.m
//  OAS
//
//  Created by Yaroslav on 03.08.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGuideBasketView.h"
#import "OverlayView.h"

@interface NITGuideBasketView ()

@property (nonatomic) IBOutlet UIButton * dismissBtn;
@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) OverlayView * overlay;
@property (nonatomic) UIView * selfView;

@end

@implementation NITGuideBasketView

+ (instancetype)showWithView:(UIView *)selfView
{
    NITGuideBasketView * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITGuideBasketView) owner:self options:nil] firstObject];
    view.backgroundColor = [UIColor clearColor];
    view.frame = WINDOW.frame;
    view.selfView = selfView;
    
    view.overlay = [OverlayView new];
    view.overlay.hideWhenTapped = false;
    view.overlay.animated = true;
    view.overlay.blurEfect = BlurEffectBasket;
    
    [view addBlurBeforeView:selfView];
    
    [view.overlay addHoleWithRect:CGRectZero form:OverlayHoleFormRectangle transparentEvent:true];
    [view.overlay show];
    [view.overlay addSubview:view];
    
    [UserDefaults setBool:true forKey:_s(NITGuideBasketView)];
    
    return view;
}

+ (BOOL)notYetShown
{
    return ![UserDefaults boolForKey:_s(NITGuideBasketView)];
}

#pragma mark - IBAction
- (IBAction)closeAction:(id)sender
{
    [self.overlay hide];
    [self removeBlur];
}

#pragma mark - Private
- (void)addBlurBeforeView:(UIView *)view
{
    UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView * blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = CGRectMake(0, 65, ViewWidth(WINDOW), ViewHeight(WINDOW) - 65);
    blurEffectView.tag = 2222;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [view addSubview:blurEffectView];
}

- (void)removeBlur
{
    [[WINDOW viewWithTag:2222] removeFromSuperview];
    [[WINDOW viewWithTag:3333] removeFromSuperview];
}

@end
