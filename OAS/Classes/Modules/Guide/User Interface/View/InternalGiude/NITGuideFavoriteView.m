//
//  NITGuideFavoriteView.m
//  OAS
//
//  Created by Yaroslav on 03.08.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGuideFavoriteView.h"
#import "OverlayView.h"

@interface NITGuideFavoriteView ()

@property (nonatomic) IBOutlet UIImageView * arrowImg;
@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UIButton * dismissBtn;
@property (nonatomic) OverlayView * overlay;
@property (nonatomic) UIView * selfView;
@property (nonatomic) UIBlurEffect * blurEffectView;

@end

@implementation NITGuideFavoriteView

+ (instancetype)showWidthOverlayRect:(CGRect)rect withView:(UIView *)selfView
{
    NITGuideFavoriteView * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITGuideFavoriteView) owner:self options:nil] firstObject];
    view.backgroundColor = [UIColor clearColor];
    view.frame = WINDOW.frame;
    view.selfView = selfView;
    
    view.overlay = [OverlayView new];
    view.overlay.hideWhenTapped = false;
    view.overlay.animated = true;
    view.overlay.blurEfect = BlurEffectFavorite;
    
    [view addBlurBeforeView:selfView];
    
    [view.overlay addHoleWithRect:rect form:OverlayHoleFormRectangle transparentEvent:true];
    [view.overlay show];
    [view.overlay addSubview:view];
    
    [UserDefaults setBool:true forKey:_s(NITGuideFavoriteView)];
    
    return view;
}

+ (BOOL)notYetShown
{
    return ![UserDefaults boolForKey:_s(NITGuideFavoriteView)];
}

#pragma mark - IBAction
- (IBAction)closeAction:(id)sender
{
    [self.overlay hide];
    [self removeBlur];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kEnableScrollFavoriteGuide object:nil];
}

#pragma mark - Private
- (void)addBlurBeforeView:(UIView *)view
{
    UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView * blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = CGRectMake(0, 230, ViewWidth(WINDOW), ViewHeight(WINDOW) - 230);
    blurEffectView.tag = 2222;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [view addSubview:blurEffectView];
}

- (void)removeBlur
{
    [[WINDOW viewWithTag:2222] removeFromSuperview];
    [[WINDOW viewWithTag:3333] removeFromSuperview];
}


@end
