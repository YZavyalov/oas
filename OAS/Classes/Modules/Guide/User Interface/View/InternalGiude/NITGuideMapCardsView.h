//
//  NITGuideMapCardsView.h
//  OAS
//
//  Created by Eugene Parafiynyk on 8/4/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITGuideMapCardsView : UIView

+ (instancetype)showWidthOverlayCollectionView:(UICollectionView *)collectionView;
+ (BOOL)notYetShown;

@end
