//
//  NITGuideFavoriteView.h
//  OAS
//
//  Created by Yaroslav on 03.08.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITGuideFavoriteView : UIView

+ (instancetype)showWidthOverlayRect:(CGRect)rect withView:(UIView *)selfView;
+ (BOOL)notYetShown;

@end
