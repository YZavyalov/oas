//
//  NITGuideMapCardsView.m
//  OAS
//
//  Created by Eugene Parafiynyk on 8/4/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGuideMapCardsView.h"
#import "OverlayView.h"
#import "CardLayout.h"

@interface NITGuideMapCardsView ()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) OverlayView * overlay;
@property (nonatomic) UICollectionView * collectionView;
@property (nonatomic) CGPoint scrollingPoint, endPoint;
@property (nonatomic) NSTimer * scrollingTimer;

@end

@implementation NITGuideMapCardsView

+ (instancetype)showWidthOverlayCollectionView:(UICollectionView *)collectionView
{
    NITGuideMapCardsView * view = [[[NSBundle mainBundle] loadNibNamed:_s(NITGuideMapCardsView) owner:self options:nil] firstObject];
    view.backgroundColor = [UIColor clearColor];
    view.frame = WINDOW.frame;
    view.collectionView = collectionView;
    
    view.overlay = [OverlayView new];
    view.overlay.hideWhenTapped = false;
    view.overlay.animated = true;
    
    [collectionView showCardsWithComplition:^{
        
        [view addBlurBeforeView:collectionView];
        [view.overlay addHoleWithView:collectionView padding:0.0f offset:CGSizeMake(0, 20) form:OverlayHoleFormRoundedRectangle transparentEvent:false];
        [view.overlay show];
        [view.overlay addSubview:view];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [view addPinToCollection];
            [view scrollSlowly];
        });
    }];
    
    [UserDefaults setBool:true forKey:_s(NITGuideMapCardsView)];
    
    return view;
}

+ (BOOL)notYetShown
{
    return ![UserDefaults boolForKey:_s(NITGuideMapCardsView)];
}

#pragma mark - IBAction
- (IBAction)closeAction:(id)sender
{
    [self.overlay hide];
    [self removeBlur];
}

#pragma mark - Private
- (void)animateCollectionCards
{
    [self.collectionView showCardsWithComplition:^{
        [self addPinToCollection];
        [self scrollSlowly];
    }];
}

- (void)addBlurBeforeView:(UIView *)view
{
    UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView * blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = WINDOW.frame;
    blurEffectView.tag = 2222;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [view.superview insertSubview:blurEffectView belowSubview:view];
}

- (void)removeBlur
{
    [[WINDOW viewWithTag:2222] removeFromSuperview];
    [[WINDOW viewWithTag:3333] removeFromSuperview];
}


- (void)addPinToCollection
{
    UIView * cell = [[self.collectionView visibleCells] firstObject];
    UIView * pin = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 37, 37)];
    pin.center = cell.center;
    pin.backgroundColor = RGB(67, 154, 140);
    pin.frame = RectSetY(pin.frame, 10);
    pin.alpha = 0.5;
    pin.layer.cornerRadius = 18.5;
    pin.layer.masksToBounds = true;
    pin.tag = 1111;
    pin.layer.borderColor = RGB(224, 255, 252).CGColor;
    pin.layer.borderWidth = 2;
    [cell addSubview:pin];
}

- (void)removePin
{
    UIView * pin = [self.collectionView viewWithTag:1111];
    pin.alpha = 0;
    [pin removeFromSuperview];
}

- (void)scrollSlowly
{
    self.endPoint = CGPointMake(0, 110);
    self.scrollingPoint = CGPointMake(0, 0);
    self.scrollingTimer = [NSTimer scheduledTimerWithTimeInterval:0.015 target:self selector:@selector(scrollSlowlyToPoint) userInfo:nil repeats:YES];
}

- (void)scrollSlowlyBack
{
    self.endPoint = CGPointMake(0, 0);
    self.scrollingPoint = CGPointMake(0, 110);
    self.scrollingTimer = [NSTimer scheduledTimerWithTimeInterval:0.015 target:self selector:@selector(scrollSlowlyToBackPoint) userInfo:nil repeats:YES];
}

- (void)scrollSlowlyToPoint
{
    self.collectionView.contentOffset = self.scrollingPoint;
    if (CGPointEqualToPoint(self.scrollingPoint, self.endPoint))
    {
        [self.scrollingTimer invalidate];
        UIView * pin = [self.collectionView viewWithTag:1111];
        pin.alpha = 0;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            pin.alpha = 0.5;
            [self scrollSlowlyBack];
        });
    }
    self.scrollingPoint = CGPointMake(self.scrollingPoint.x, self.scrollingPoint.y+1);
}

- (void)scrollSlowlyToBackPoint
{
    self.collectionView.contentOffset = self.scrollingPoint;
    if (CGPointEqualToPoint(self.scrollingPoint, self.endPoint))
    {
        [self.scrollingTimer invalidate];
        [self removePin];
        [self closeAction:nil];
    }
    self.scrollingPoint = CGPointMake(self.scrollingPoint.x, self.scrollingPoint.y-1);
}

@end
