//
//  NITGuideViewController.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITGuideProtocols.h"

@interface NITGuideViewController : NITBaseViewController <NITGuideViewProtocol>

@property (nonatomic, strong) id <NITGuidePresenterProtocol> presenter;

@end
