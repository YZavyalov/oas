//
//  NITGuideCollectionViewCell.m
//  OAS
//
//  Created by Eugene Parafiynyk on 8/2/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGuideCollectionViewCell.h"
#import "YFGIFImageView.h"
#import "UIImageView+PlayGIF.h"
#import "NITGuideViewModel.h"

@interface NITGuideCollectionViewCell ()
{
    NSTimer * timer;
}

@property (nonatomic) IBOutlet UIImageView * bgImage;
@property (nonatomic) IBOutlet NSLayoutConstraint * imageW;

@end

@implementation NITGuideCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

#pragma mark - Custom Accessors
- (void)setModel:(NITGuideViewModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Public
- (void)play
{
    YFGIFImageView * gif = [self viewWithTag:101];
    [gif startGIF];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        gif.hidden = false;
        self.bgImage.hidden = true;
        timer = [NSTimer scheduledTimerWithTimeInterval:self.model.gifTime target:self selector:@selector(pause) userInfo:nil repeats:false];
    });
}

- (void)stop
{
    self.bgImage.hidden = false;
    
    [timer invalidate];
    timer = nil;
    
    YFGIFImageView * gif = [self viewWithTag:101];
    [gif stopGIF];
    [gif removeFromSuperview];
}

#pragma mark - Private
- (void)updateData
{
    [self stop];
    
    self.imageW.constant = CGRectGetWidth(self.frame) * 377/414;
    [self layoutIfNeeded];
    
    self.bgImage.image = [UIImage imageNamed:self.model.bgImageName];
    self.bgImage.hidden = false;
    
    YFGIFImageView * gif = [[YFGIFImageView alloc] initWithFrame:self.bgImage.frame];
    gif.tag = 101;
    gif.contentMode = UIViewContentModeScaleAspectFit;
    gif.hidden = true;
    gif.center = self.bgImage.center;
    
    [self addSubview:gif];
    
    NSData * gifData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:self.model.gifName ofType:nil]];
    gif.gifData = gifData;
}

- (void)pause
{
    YFGIFImageView * gif = [self viewWithTag:101];
    [gif stopGIF];
}

@end
