//
//  NITGuideCollectionViewCell.h
//  OAS
//
//  Created by Eugene Parafiynyk on 8/2/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

@class NITGuideViewModel;

@interface NITGuideCollectionViewCell : UICollectionViewCell

@property (nonatomic) NITGuideViewModel * model;

- (void)play;
- (void)stop;

@end
