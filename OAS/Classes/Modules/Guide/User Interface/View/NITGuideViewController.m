//
//  NITGuideViewController.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITGuideViewController.h"
#import "NITGuideCollectionViewCell.h"

@interface NITGuideViewController ()
<
UICollectionViewDelegate,
UICollectionViewDelegate,
UICollectionViewDataSource
>

@property (nonatomic) IBOutlet UICollectionView * collectionView;
@property (nonatomic) IBOutlet UIPageControl * pageControl;
@property (nonatomic) IBOutlet UIButton * closeBtn;
@property (nonatomic) IBOutlet UILabel * titleText;
@property (nonatomic) IBOutlet UILabel * descriptionText;
@property (nonatomic) IBOutlet UIButton * nextBtn;

@property (nonatomic) NSArray <NITGuideViewModel *> * items;

@end

@implementation NITGuideViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter initData];
}

- (BOOL)prefersStatusBarHidden
{
    return true;
}

#pragma mark - IBAction
- (IBAction)skipAction:(id)sender
{
    [self.presenter skipGuide];
}

- (IBAction)nextAction:(id)sender
{
    if (self.pageControl.currentPage < self.pageControl.numberOfPages - 1)
    {
        self.pageControl.currentPage += 1;
        [self scrollPageToIndex:self.pageControl.currentPage animated:true];
    }
    else
    {
        [self skipAction:sender];
    }
}

- (IBAction)pageChangeAction:(id)sender
{
    [self scrollPageToIndex:self.pageControl.currentPage animated:true];
}

#pragma mark - Private
- (void)initUI
{
    [self.navigationItem setHidesBackButton:true];
    [self.collectionView registerNibArray:@[_s(NITGuideCollectionViewCell)]];
}

- (void)scrollPageToIndex:(NSInteger)index animated:(BOOL)animated
{
    if (index < self.items.count)
    {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]
                                    atScrollPosition:UICollectionViewScrollPositionNone
                                            animated:animated];
    }
}

- (void)presentPage
{
    NSInteger index = self.pageControl.currentPage;

    for (NSIndexPath * indexPath in self.collectionView.indexPathsForVisibleItems)
    {
        NITGuideCollectionViewCell * cell = (NITGuideCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        if (indexPath.row == index) [cell play];
        else                        [cell stop];
    }
    
    NITGuideViewModel * model = self.items[index];
    self.titleText.text = model.title;
    self.descriptionText.text = model.text;
    
    NSString * title = index == self.items.count - 1 ? NSLocalizedString(@"Хорошо", nil) : NSLocalizedString(@"Далее", nil);
    [self.nextBtn setTitle:title forState:UIControlStateNormal];
}

#pragma mark - NITGuidePresenterProtocol
- (void)reloadDataByItems:(NSArray <NITGuideViewModel *> *)items
{
    self.items = items;
    [self.pageControl setNumberOfPages:items.count];
    [self.collectionView reloadData];
    [self.collectionView performBatchUpdates:^{}
                                  completion:^(BOOL finished) {
                                      [self presentPage];
                                  }];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [collectionView dequeueReusableCellWithReuseIdentifier:_s(NITGuideCollectionViewCell) forIndexPath:indexPath];
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(NITGuideCollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell.model = self.items[indexPath.row];
    self.pageControl.currentPage = indexPath.row;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(collectionView.frame), CGRectGetHeight(collectionView.frame)); //377/414
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.pageControl.currentPage = self.collectionView.contentOffset.x / ViewWidth(self.collectionView);
    [self presentPage];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [self presentPage];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, 0)];
}

@end
