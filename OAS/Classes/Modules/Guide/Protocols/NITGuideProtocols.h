//
//  NITGuideProtocols.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITGuideViewModel.h"

@protocol NITGuideInteractorOutputProtocol;
@protocol NITGuideInteractorInputProtocol;
@protocol NITGuideViewProtocol;
@protocol NITGuidePresenterProtocol;
@protocol NITGuideLocalDataManagerInputProtocol;
@protocol NITGuideAPIDataManagerInputProtocol;

@class NITGuideWireFrame;

@protocol NITGuideViewProtocol
@required
@property (nonatomic, strong) id <NITGuidePresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByItems:(NSArray <NITGuideViewModel *> *)items;
@end

@protocol NITGuideWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITGuideModuleFrom:(id)fromView;
- (void)presentNITGuideModuleFromWindow:(UIWindow *)window;
- (void)showTabbar;
@end

@protocol NITGuidePresenterProtocol
@required
@property (nonatomic, weak) id <NITGuideViewProtocol> view;
@property (nonatomic, strong) id <NITGuideInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITGuideWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)skipGuide;
@end

@protocol NITGuideInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITGuideInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITGuideInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITGuideAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITGuideLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end


@protocol NITGuideDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITGuideAPIDataManagerInputProtocol <NITGuideDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITGuideLocalDataManagerInputProtocol <NITGuideDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
