//
//  NITTabBarLocalDataManager.m
//  OAS
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITTabBarLocalDataManager.h"

@implementation NITTabBarLocalDataManager

#pragma mark - NITTabBarLocalDataManagerInputProtocol
- (NSInteger)getBasketCounter
{
    NSInteger total = 0;
    NSArray <NITBasketItem *> * basketItems = [NITBasketItem allBasketItems];

    total = basketItems.count;
    
    return total;
}

@end
