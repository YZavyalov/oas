//
//  NITTabBarInteractor.m
//  OAS
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITTabBarInteractor.h"

@implementation NITTabBarInteractor

#pragma mark - NITtabBarInteractorInputProtocol
- (NSInteger)getBasketCounter
{
    return [self.localDataManager getBasketCounter];
}

@end
