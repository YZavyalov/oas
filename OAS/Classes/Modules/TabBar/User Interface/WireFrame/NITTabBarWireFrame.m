//
//  NITTabBarWireFrame.m
//  OAS
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITTabBarWireFrame.h"
#import "NITTabBarViewController.h"
#import "NITCatalogWireFrame.h"
#import "NITBasketWireFrame.h"
#import "NITMapWireFrame.h"
#import "NITPharmacyWireFrame.h"
#import "NITFavoriteWireFrame.h"
#import "NITDiscountCardWireFrame.h"

static NSString * const kCatalogNC  = @"NITCatalogNC";
static NSString * const kMapNC      = @"NITMapNC";
static NSString * const kFavoriteNC = @"NITFavoriteNC";
static NSString * const kPharmacyNC = @"NITPharmacyNC";
static NSString * const kBasketNC   = @"NITBasketNC";

@implementation NITTabBarWireFrame

#pragma mark - NITTabBarWireFrameProtocol
+ (id)createNITTabBarModule
{
    // Generating module components
    id <NITTabBarPresenterProtocol, NITTabBarInteractorOutputProtocol> presenter = [NITTabBarPresenter new];
    id <NITTabBarInteractorInputProtocol> interactor = [NITTabBarInteractor new];
    id <NITTabBarAPIDataManagerInputProtocol> APIDataManager = [NITTabBarAPIDataManager new];
    id <NITTabBarLocalDataManagerInputProtocol> localDataManager = [NITTabBarLocalDataManager new];
    id <NITTabBarWireFrameProtocol> wireFrame= [NITTabBarWireFrame new];
    id <NITTabBarViewProtocol> view = [(NITTabBarWireFrame *)wireFrame createViewControllerWithKey:_s(NITTabBarViewController) storyboardType:StoryboardTypeMain];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}


+ (void)presentNITTabBarModuleFromWindow:(UIWindow *)window
{
    NITTabBarViewController * view = [NITTabBarWireFrame createNITTabBarModule];
    [window setRootViewController:view];
}

- (void)setupViewControllersForTabbar:(UITabBarController *)tabbar
{
    // catalog
    UINavigationController * catalogNC = [self createViewControllerWithKey:kCatalogNC storyboardType:StoryboardTypeCatalog];
    NITCatalogViewController * catalogVC = [NITCatalogWireFrame createNITCatalogModule];
    NSParameterAssert(catalogNC);
    NSParameterAssert(catalogVC);
    catalogNC.viewControllers = @[catalogVC];
    
    // map
    UINavigationController * mapNC = [self createViewControllerWithKey:kMapNC storyboardType:StoryboardTypeMap];
    NITMapViewController * mapVC;
    if (![HELPER isDiscountCardBinded])
    {
        mapVC = [NITMapWireFrame createNITMapModule];
    }
    else
    {
        mapVC = [NITDiscountCardWireFrame createNITDiscountCardModule];
    }
    NSParameterAssert(mapNC);
    NSParameterAssert(mapVC);
    mapNC.viewControllers = @[mapVC];
    
    // favorite
    UINavigationController * favoriteNC = [self createViewControllerWithKey:kFavoriteNC storyboardType:StoryboardTypeFavorite];
    NITFavoriteViewController * favoriteVC = [NITFavoriteWireFrame createNITFavoriteModule];
    NSParameterAssert(favoriteNC);
    NSParameterAssert(favoriteVC);
    favoriteNC.viewControllers = @[favoriteVC];
    
    // pharmacy
    UINavigationController * pharmacyNC = [self createViewControllerWithKey:kPharmacyNC storyboardType:StoryboardTypePharmacy];
    NITFavoriteViewController * pharmacyVC = [NITPharmacyWireFrame createNITPharmacyModule];
    NSParameterAssert(pharmacyNC);
    NSParameterAssert(pharmacyVC);
    pharmacyNC.viewControllers = @[pharmacyVC];

    // basket
    UINavigationController * basketNC = [self createViewControllerWithKey:kBasketNC storyboardType:StoryboardTypeBasket];
    NITBasketViewController * basketVC = [NITBasketWireFrame createNITBasketModule];
    NSParameterAssert(basketNC);
    NSParameterAssert(basketVC);
    basketNC.viewControllers = @[basketVC];
    
    tabbar.viewControllers = @[catalogNC, mapNC, favoriteNC, pharmacyNC, basketNC];
}


@end
