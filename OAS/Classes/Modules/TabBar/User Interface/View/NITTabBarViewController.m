//
//  NITTabBarViewController.m
//  OAS
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITTabBarViewController.h"

@implementation NITTabBarViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.presenter setupViewControllersForTabbar:self];
    [self initButtons];
    [self.presenter updateData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.presenter rateAppIfNeed];
}

#pragma mark - Private
- (void) initButtons
{
    [(UITabBarItem *)self.tabBar.items[0] setImage:[UIImage imageNamed:@"catalog"]];
    [(UITabBarItem *)self.tabBar.items[1] setImage:[UIImage imageNamed:@"map"]];
    [(UITabBarItem *)self.tabBar.items[2] setImage:[UIImage imageNamed:@"save"]];
    [(UITabBarItem *)self.tabBar.items[3] setImage:[UIImage imageNamed:@"pharmacy"]];
    [(UITabBarItem *)self.tabBar.items[4] setImage:[UIImage imageNamed:@"cart"]];
    
    [(UITabBarItem *)self.tabBar.items[0] setSelectedImage:[UIImage imageNamed:@"catalog_active"]];
    [(UITabBarItem *)self.tabBar.items[1] setSelectedImage:[UIImage imageNamed:@"map_active"]];
    [(UITabBarItem *)self.tabBar.items[2] setSelectedImage:[UIImage imageNamed:@"save_active"]];
    [(UITabBarItem *)self.tabBar.items[3] setSelectedImage:[UIImage imageNamed:@"pharmacy_active"]];
    [(UITabBarItem *)self.tabBar.items[4] setSelectedImage:[UIImage imageNamed:@"cart_active"]];

    [(UITabBarItem *)self.tabBar.items[0] setTitle:NSLocalizedString(@"Каталог", @"")];
    [(UITabBarItem *)self.tabBar.items[1] setTitle:NSLocalizedString(@"Бонусы", @"")];
    [(UITabBarItem *)self.tabBar.items[2] setTitle:NSLocalizedString(@"Избранное", @"")];
    [(UITabBarItem *)self.tabBar.items[3] setTitle:NSLocalizedString(@"Аптеки", @"")];
    [(UITabBarItem *)self.tabBar.items[4] setTitle:NSLocalizedString(@"Корзина", @"")];
    
    if ([(UITabBarItem *)self.tabBar.items[4] respondsToSelector:@selector(setBadgeColor:)])
    {
        [(UITabBarItem *)self.tabBar.items[4] setBadgeColor:RGB(58, 160, 144)];
    }
}

#pragma mark - NITTabBarPresenterProtocol
- (void)reloadBascketCounter:(NSInteger)counter
{
    [(UITabBarItem *)self.tabBar.items[4] setBadgeValue:counter > 0 ? @(counter).stringValue : nil];
}

@end
