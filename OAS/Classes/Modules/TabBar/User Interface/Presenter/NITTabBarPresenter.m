//
//  NITTabBarPresenter.m
//  OAS
//
//  Created by Eugene Parafiynyk on 10/20/2016.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITTabBarPresenter.h"
#import "NITTabBarWireframe.h"

@implementation NITTabBarPresenter

#pragma mark - Life cycle
- (instancetype)init
{
    if (self = [super init])
    {
        [self addObservers];
    }
    
    return self;
}

#pragma mark - Data
- (void)updateData
{
    NSInteger counter = [self.interactor getBasketCounter];
    [self.view reloadBascketCounter:counter];
}

- (void)rateAppIfNeed
{
    if (USER.appRate == 0 && !USER.showRateAppDate)
    {
        USER.showRateAppDate = [NSDate date];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        if (USER.appRate == 0 && [USER.showRateAppDate timeIntervalSinceNow] > 60*60*24*2)
        {
            [self showRate];
        }
    });
}

- (void)showRate
{
  
}

#pragma mark - Navigation
- (void)setupViewControllersForTabbar:(UITabBarController *)tabbar
{
    [self.wireFrame setupViewControllersForTabbar:tabbar];
    HELPER.interfaceLoaded = true;
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kSaveBasketItemNotification object:nil];
}

@end
