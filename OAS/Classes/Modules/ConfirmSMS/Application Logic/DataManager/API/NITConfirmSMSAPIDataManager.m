//
//  NITConfirmSMSAPIDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITConfirmSMSAPIDataManager.h"
#import "SLAuthApi.h"

@implementation NITConfirmSMSAPIDataManager

#pragma mark - NITConfirmSMSDataManagerInputProtocol
- (void)sendSMSCodeWithCode:(NSString *)code withHandler:(void(^)(SLSignin *output))handler
{
    SLConfirmationQuery * query = [SLConfirmationQuery new];
    NITDiscountCardModel * model = [NITDiscountCardModel new];
    query.code = code;
    query.type = @"phone";
    query.value = [NSString stringWithPhone:model.phoneNumber];
    
    [[SLAuthApi new] postAuthSigninCodeConfirmationWithBody:query completionHandler:^(SLSignin *output, NSError *error) {
        if (error)
        {
            [ERROR_MESSAGE errorMessage:notification_error_with_sms];
            [HELPER logError:error method:METHOD_NAME];
        }
        if (output) handler (output);
    }];
}

- (void)resendSMSCodeWithHandler:(void(^)(SLErrorModel *output))handler
{
    [HELPER startLoading];
    SLCodeQuery * query = [SLCodeQuery new];
    NITDiscountCardModel * card = [NITDiscountCardModel new];
    query.phone = card.phoneNumber;
    [[SLAuthApi new] postAuthSigninCodeWithBody:query completionHandler:^(SLErrorModel *output, NSError *error) {
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (output) handler (output);
    }];
}

@end
