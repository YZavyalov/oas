//
//  NITConfirmSMSLocalDataManager.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITConfirmSMSProtocols.h"

@interface NITConfirmSMSLocalDataManager : NSObject <NITConfirmSMSLocalDataManagerInputProtocol>

@end
