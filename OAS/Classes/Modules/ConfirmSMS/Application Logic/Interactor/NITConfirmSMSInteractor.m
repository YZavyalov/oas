//
//  NITConfirmSMSInteractor.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITConfirmSMSInteractor.h"

@implementation NITConfirmSMSInteractor

#pragma mark - NITConfirmSMSInteractorInputProtocol
- (void)sendSMSCode:(NSString *)code
{
    weaken(self);
    [self.APIDataManager sendSMSCodeWithCode:code withHandler:^(SLSignin *output) {
        [weakSelf.presenter saveCidUidToken:output];
    }];
}

- (void)resendSMSCode
{
    weaken(self);
    [self.APIDataManager resendSMSCodeWithHandler:^(SLErrorModel *output) {
        [weakSelf.presenter successPosting:output ? @"Какая-то ошибка - повторная смс" : nil];
    }];
}

@end
