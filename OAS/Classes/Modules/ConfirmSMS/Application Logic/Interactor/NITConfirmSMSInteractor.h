//
//  NITConfirmSMSInteractor.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITConfirmSMSProtocols.h"

@interface NITConfirmSMSInteractor : NSObject <NITConfirmSMSInteractorInputProtocol>

@property (nonatomic, weak) id <NITConfirmSMSInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITConfirmSMSAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITConfirmSMSLocalDataManagerInputProtocol> localDataManager;

@end
