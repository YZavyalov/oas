//
//  NITConfirmSMSProtocols.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITDiscountCardModel.h"
#import "SLSignin.h"
#import "SLErrorModel.h"

@protocol NITConfirmSMSInteractorOutputProtocol;
@protocol NITConfirmSMSInteractorInputProtocol;
@protocol NITConfirmSMSViewProtocol;
@protocol NITConfirmSMSPresenterProtocol;
@protocol NITConfirmSMSLocalDataManagerInputProtocol;
@protocol NITConfirmSMSAPIDataManagerInputProtocol;

@class NITConfirmSMSWireFrame;

// Defines the public interface that something else can use to drive the user interface
@protocol NITConfirmSMSViewProtocol
@required
@property (nonatomic, strong) id <NITConfirmSMSPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)updateRegistrationtBtn:(BOOL)update;
@end

@protocol NITConfirmSMSWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITConfirmSMSModuleFrom:(id)fromViewController;
- (void)presentDiscountCardFromViewController:(id)fromViewController;
- (void)presentAlertFrom:(id)fromViewController withAlert:(id)alert;
@end

@protocol NITConfirmSMSPresenterProtocol
@required
@property (nonatomic, weak) id <NITConfirmSMSViewProtocol> view;
@property (nonatomic, strong) id <NITConfirmSMSInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITConfirmSMSWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)checkSMSNumber:(NSString *)SMSNumber;
- (void)confitmSMS:(NSString *)code;
- (void)resendSMS;
@end

@protocol NITConfirmSMSInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)saveCidUidToken:(SLSignin *)output;
- (void)successPosting:(NSString *)message;
@end

@protocol NITConfirmSMSInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITConfirmSMSInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITConfirmSMSAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITConfirmSMSLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)sendSMSCode:(NSString *)code;
- (void)resendSMSCode;
@end


@protocol NITConfirmSMSDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITConfirmSMSAPIDataManagerInputProtocol <NITConfirmSMSDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)sendSMSCodeWithCode:(NSString *)code withHandler:(void(^)(SLSignin *output))handler;
- (void)resendSMSCodeWithHandler:(void(^)(SLErrorModel *output))handler;
@end

@protocol NITConfirmSMSLocalDataManagerInputProtocol <NITConfirmSMSDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
