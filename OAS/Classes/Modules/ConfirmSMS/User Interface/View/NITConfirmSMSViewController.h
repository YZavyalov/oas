//
//  NITConfirmSMSViewController.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITConfirmSMSProtocols.h"
#import "NITBaseViewController.h"

@interface NITConfirmSMSViewController : NITBaseViewController <NITConfirmSMSViewProtocol>

@property (nonatomic, strong) id <NITConfirmSMSPresenterProtocol> presenter;

@end
