//
//  NITConfirmSMSViewController.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITConfirmSMSViewController.h"
#import "AKNumericFormatter.h"
#import "UITextField+AKNumericFormatter.h"

static NSString * formatterCodeMask = @"****";
static NSTimeInterval const delay = 120.f;

@interface NITConfirmSMSViewController()
<
UITextFieldDelegate
>

@property (nonatomic) IBOutlet UILabel * titleLbl;
@property (nonatomic) IBOutlet UILabel * subtitleLbl;
@property (nonatomic) IBOutlet UITextField * SMSField;
@property (nonatomic) IBOutlet UIButton * registrationBtn;
@property (nonatomic) IBOutlet UIButton * resendSMSBtn;
@property (nonatomic) IBOutlet NSLayoutConstraint * registrationBtnBottom;

@property (nonatomic) NSTimer *timer;
@property (nonatomic) int time;

@property (nonatomic) BOOL registrationBtnAvailable;

@end

@implementation NITConfirmSMSViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController hideTabBarAnimated:false];
}

#pragma mark - IBAction
- (IBAction)registrationAction:(id)sender
{
    if(self.registrationBtnAvailable)
        [self.presenter confitmSMS:self.SMSField.text];
}

- (IBAction)resendAction:(id)sender
{
    self.SMSField.text = @"";
    [self.registrationBtn setBackgroundColor:RGB(231, 242, 241)];
    self.registrationBtnAvailable = false;
    [self startTimer];
    [self.presenter resendSMS];
}

#pragma mark - Private
- (void)initUI
{
    //Default state
    [self startTimer];
    self.SMSField.numericFormatter = [AKNumericFormatter formatterWithMask:formatterCodeMask
                                                        placeholderCharacter:'*'
                                                                        mode:AKNumericFormatterMixed];
    self.SMSField.placeholder = @"0000";
    self.registrationBtnBottom.constant = -50;
    
    //Keyboard
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)startTimer
{
    self.subtitleLbl.hidden = false;
    self.resendSMSBtn.hidden = true;
    self.time = (int)delay;
    [self updateSubtitleText];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1
                                             target:self
                                           selector:@selector(timerTick:)
                                           userInfo:nil
                                            repeats:YES];
}

- (void)updateSubtitleText
{
    NSString * sec;
    if(fmodf(self.time, 10) == 0 || fmodf(self.time, 10) >= 5) sec = NSLocalizedString(@"секунд", nil);
    else if(fmodf(self.time, 10) == 1) sec = NSLocalizedString(@"секунда", nil);
    else if(fmodf(self.time, 10) > 1 && fmodf(self.time, 10) < 5) sec = NSLocalizedString(@"секунды", nil);
    self.subtitleLbl.text = [NSString stringWithFormat:@"Мы выслали вам СМС с кодом, \nосталось %d %@ до повтроного запроса", self.time, sec];
}

- (void)timerTick:(NSTimer*)timer
{
    self.time--;
    [self updateSubtitleText];
    if(self.time == 0)
    {
        [self stopTimer];
    }
}

- (void)stopTimer
{
    if (self.timer)
    {
        [self.timer invalidate];
        self.timer = nil;
    }
    self.subtitleLbl.hidden = true;
    self.resendSMSBtn.hidden = false;
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    [self.view layoutIfNeeded];
    self.registrationBtnBottom.constant = [self keyboardHeight:notification] - (IS_IPHONE_5 ? 120 : 50);
    [UIView animateWithDuration:0.25 animations:^{
        if(IS_IPHONE_5)
        {
            CGRect frame = self.view.frame;
            frame.origin.y = -70;
            self.view.frame = frame;
        }
        
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    [self.view layoutIfNeeded];
    self.registrationBtnBottom.constant = -50;
    [UIView animateWithDuration:0.25 animations:^{
        if(IS_IPHONE_5)
        {
            CGRect frame = self.view.frame;
            frame.origin.y = 0;
            self.view.frame = frame;
        }
        
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - NITConfirmSMSPresenterProtocol
- (void)updateRegistrationtBtn:(BOOL)update
{
    if(update)    [self.registrationBtn setBackgroundColor:RGB(58, 160, 144)];
    else          [self.registrationBtn setBackgroundColor:RGB(231, 242, 241)];
    
    self.registrationBtnAvailable = update;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self.presenter checkSMSNumber:str];

    return true;
}

@end
