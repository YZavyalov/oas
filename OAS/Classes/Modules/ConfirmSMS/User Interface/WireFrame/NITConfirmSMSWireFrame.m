//
//  NITConfirmSMSWireFrame.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITConfirmSMSWireFrame.h"
#import "NITDiscountCardWireFrame.h"

@implementation NITConfirmSMSWireFrame

#pragma mark - Private
+ (id)createNITConfirmSMSModule
{
    // Generating module components
    id <NITConfirmSMSPresenterProtocol, NITConfirmSMSInteractorOutputProtocol> presenter = [NITConfirmSMSPresenter new];
    id <NITConfirmSMSInteractorInputProtocol> interactor = [NITConfirmSMSInteractor new];
    id <NITConfirmSMSAPIDataManagerInputProtocol> APIDataManager = [NITConfirmSMSAPIDataManager new];
    id <NITConfirmSMSLocalDataManagerInputProtocol> localDataManager = [NITConfirmSMSLocalDataManager new];
    id <NITConfirmSMSWireFrameProtocol> wireFrame= [NITConfirmSMSWireFrame new];
    id <NITConfirmSMSViewProtocol> view = [(NITConfirmSMSWireFrame *)wireFrame createViewControllerWithKey:_s(NITConfirmSMSViewController) storyboardType:StoryboardTypeMap];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITConfirmSMSWireFrameProtocol
+ (void)presentNITConfirmSMSModuleFrom:(UIViewController *)fromViewController
{
    NITConfirmSMSViewController * vc = [NITConfirmSMSWireFrame createNITConfirmSMSModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)presentDiscountCardFromViewController:(UIViewController *)fromViewController
{
    [NITDiscountCardWireFrame presentNITDiscountCardModuleFrom:fromViewController];
}

- (void)presentAlertFrom:(UIViewController *)fromViewController withAlert:(UIAlertController *)alert
{
    [fromViewController presentViewController:alert animated:YES completion:nil];
}

@end
