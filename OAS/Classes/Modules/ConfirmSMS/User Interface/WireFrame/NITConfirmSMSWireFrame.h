//
//  NITConfirmSMSWireFrame.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITConfirmSMSProtocols.h"
#import "NITConfirmSMSViewController.h"
#import "NITConfirmSMSLocalDataManager.h"
#import "NITConfirmSMSAPIDataManager.h"
#import "NITConfirmSMSInteractor.h"
#import "NITConfirmSMSPresenter.h"
#import "NITConfirmSMSWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITConfirmSMSWireFrame : NITRootWireframe <NITConfirmSMSWireFrameProtocol>

@end
