//
//  NITConfirmSMSPresenter.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITConfirmSMSPresenter.h"
#import "NITConfirmSMSWireframe.h"
#import "NITDiscountCardModel.h"

@implementation NITConfirmSMSPresenter

#pragma mark - NITConfirmSMSPresenterProtocol
- (void)checkSMSNumber:(NSString *)SMSNumber
{
    if(SMSNumber.length > 3)   [self.view updateRegistrationtBtn:true];
    else                       [self.view updateRegistrationtBtn:false];
}

- (void)confitmSMS:(NSString *)code
{
    [self.interactor sendSMSCode:code];
}

- (void)resendSMS
{
    [self.interactor resendSMSCode];
}

#pragma mark - NITConfirmSMSInteractorOutputProtocol
- (void)saveCidUidToken:(SLSignin *)output
{
    [HELPER setDiscountCardBinded];
    USER.company = output.xCid;
    USER.identifier = output.xUid;
    USER.accessToken = output.xToken;
    [[NITDiscountCardModel new] updateDiscountCard];
    [self.wireFrame presentDiscountCardFromViewController:self.view];
}

- (void)successPosting:(NSString *)message
{
    if(message)
    {
        [self showAlertControllerWithMessage:message];
    }
}

#pragma mark - Private
- (void)showAlertControllerWithMessage:(NSString *)message
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Не удалось отправить номер телефона", nil)
                                                                    message:message
                                                             preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = RGB(58, 160, 144);
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ок", nil)
                                                      style:UIAlertActionStyleCancel
                                                    handler:^(UIAlertAction * action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                    }];
    
    [alert addAction:cancel];
    
    [self.wireFrame presentAlertFrom:self.view withAlert:alert];
}


@end
