//
//  NITConfirmSMSPresenter.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITConfirmSMSProtocols.h"

@class NITConfirmSMSWireFrame;

@interface NITConfirmSMSPresenter : NSObject <NITConfirmSMSPresenterProtocol, NITConfirmSMSInteractorOutputProtocol>

@property (nonatomic, weak) id <NITConfirmSMSViewProtocol> view;
@property (nonatomic, strong) id <NITConfirmSMSInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITConfirmSMSWireFrameProtocol> wireFrame;

@end
