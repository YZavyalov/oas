//
//  NITPresentPictureWireFrame.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/15/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITPresentPictureWireFrame.h"

@implementation NITPresentPictureWireFrame

#pragma mark - NITPharmacyWireFrameProtocol
+ (id)createNITPresentPictureModule
{
    // Generating module components
    id <NITPresentPicturePresenterProtocol, NITPresentPictureInteractorOutputProtocol> presenter = [NITPresentPicturePresenter new];
    id <NITPresentPictureInteractorInputProtocol> interactor = [NITPresentPictureInteractor new];
    id <NITPresentPictureAPIDataManagerInputProtocol> APIDataManager = [NITPresentPictureAPIDataManager new];
    id <NITPresentPictureLocalDataManagerInputProtocol> localDataManager = [NITPresentPictureLocalDataManager new];
    id <NITPresentPictureWireFrameProtocol> wireFrame= [NITPresentPictureWireFrame new];
    id <NITPresentPictureViewProtocol> view = [(NITPresentPictureWireFrame *)wireFrame createViewControllerWithKey:_s(NITPresentPictureViewController) storyboardType:StoryboardTypeCatalog];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITPresentPictureModuleFrom:(UIViewController *)fromViewController withImage:(UIImage *)model
{    
    NITPresentPictureViewController * vc = [NITPresentPictureWireFrame createNITPresentPictureModule];
    vc.presenter.image = model;
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    [fromViewController presentViewController:navigationController animated:true completion:^{}];
}

- (void)closeView:(UIViewController *)fromViewController
{
    [fromViewController dismissViewControllerAnimated:true completion:^{}];
}

@end
