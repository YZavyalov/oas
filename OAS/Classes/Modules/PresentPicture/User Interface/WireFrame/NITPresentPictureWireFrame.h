//
//  NITPresentPictureWireFrame.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/15/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPresentPictureProtocols.h"
#import "NITPresentPictureViewController.h"
#import "NITPresentPictureLocalDataManager.h"
#import "NITPresentPictureAPIDataManager.h"
#import "NITPresentPictureInteractor.h"
#import "NITPresentPicturePresenter.h"
#import "NITPresentPictureWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITPresentPictureWireFrame : NITRootWireframe <NITPresentPictureWireFrameProtocol>

@end
