//
//  NITPresentPictureViewController.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/15/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITPresentPictureViewController.h"

@interface NITPresentPictureViewController()

@property (nonatomic) IBOutlet UIImageView * picture;

@end

@implementation NITPresentPictureViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
}

#pragma mark - IBAction
- (IBAction)closeAction:(id)sender
{
    [self.presenter closeView];
}

#pragma mark - Private
- (void)initUI
{
    [self.navigationController setNavigationBarHidden:YES];

    self.picture.image = self.presenter.image;
}

@end
