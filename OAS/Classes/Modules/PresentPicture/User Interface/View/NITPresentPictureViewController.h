//
//  NITPresentPictureViewController.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/15/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITPresentPictureProtocols.h"

@interface NITPresentPictureViewController : NITBaseViewController <NITPresentPictureViewProtocol>

@property (nonatomic, strong) id <NITPresentPicturePresenterProtocol> presenter;

@end
