//
//  NITPresentPicturePresenter.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/15/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPresentPictureProtocols.h"

@class NITPresentPictureWireFrame;

@interface NITPresentPicturePresenter : NSObject <NITPresentPicturePresenterProtocol, NITPresentPictureInteractorOutputProtocol>

@property (nonatomic, weak) id <NITPresentPictureViewProtocol> view;
@property (nonatomic, strong) id <NITPresentPictureInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPresentPictureWireFrameProtocol> wireFrame;

@property (nonatomic) UIImage * image;

@end
