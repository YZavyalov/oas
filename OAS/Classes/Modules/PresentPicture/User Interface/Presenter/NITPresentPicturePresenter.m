//
//  NITPresentPicturePresenter.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/15/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITPresentPicturePresenter.h"
#import "NITPresentPictureWireframe.h"

@implementation NITPresentPicturePresenter

#pragma mark - NITPresentPicturePresenterProtocol
- (void)closeView
{
    [self.wireFrame closeView:self.view];
}

@end
