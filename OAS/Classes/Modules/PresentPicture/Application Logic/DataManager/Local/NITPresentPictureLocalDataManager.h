//
//  NITPresentPictureLocalDataManager.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/15/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITPresentPictureProtocols.h"

@interface NITPresentPictureLocalDataManager : NSObject <NITPresentPictureLocalDataManagerInputProtocol>

@end
