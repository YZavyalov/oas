//
//  NITPresentPictureProtocols.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 06/15/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol NITPresentPictureInteractorOutputProtocol;
@protocol NITPresentPictureInteractorInputProtocol;
@protocol NITPresentPictureViewProtocol;
@protocol NITPresentPicturePresenterProtocol;
@protocol NITPresentPictureLocalDataManagerInputProtocol;
@protocol NITPresentPictureAPIDataManagerInputProtocol;

@class NITPresentPictureWireFrame;

// Defines the public interface that something else can use to drive the user interface
@protocol NITPresentPictureViewProtocol
@required
@property (nonatomic, strong) id <NITPresentPicturePresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

@protocol NITPresentPictureWireFrameProtocol
@required
+ (id)createNITPresentPictureModule;
+ (void)presentNITPresentPictureModuleFrom:(id)fromViewController withImage:(UIImage *)model;
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
- (void)closeView:(id)fromViewController;
@end

@protocol NITPresentPicturePresenterProtocol
@required
@property (nonatomic, weak) id <NITPresentPictureViewProtocol> view;
@property (nonatomic, strong) id <NITPresentPictureInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITPresentPictureWireFrameProtocol> wireFrame;
@property (nonatomic) UIImage * image;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)closeView;
@end

@protocol NITPresentPictureInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITPresentPictureInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITPresentPictureInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITPresentPictureAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITPresentPictureLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end


@protocol NITPresentPictureDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITPresentPictureAPIDataManagerInputProtocol <NITPresentPictureDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITPresentPictureLocalDataManagerInputProtocol <NITPresentPictureDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
