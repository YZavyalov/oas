//
//  NITSnapPhoneProtocols.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITDiscountCardModel.h"
#import "SLErrorModel.h"

@protocol NITSnapPhoneInteractorOutputProtocol;
@protocol NITSnapPhoneInteractorInputProtocol;
@protocol NITSnapPhoneViewProtocol;
@protocol NITSnapPhonePresenterProtocol;
@protocol NITSnapPhoneLocalDataManagerInputProtocol;
@protocol NITSnapPhoneAPIDataManagerInputProtocol;

@class NITSnapPhoneWireFrame;

// Defines the public interface that something else can use to drive the user interface
@protocol NITSnapPhoneViewProtocol
@required
@property (nonatomic, strong) id <NITSnapPhonePresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)updateNextBtn:(BOOL)update;
@end

@protocol NITSnapPhoneWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITSnapPhoneModuleFrom:(id)fromViewController;
- (void)confitmSMSFromViewController:(id)fromViewController;
- (void)presentAlertFrom:(id)fromViewController withAlert:(id)alert;
@end

@protocol NITSnapPhonePresenterProtocol
@required
@property (nonatomic, weak) id <NITSnapPhoneViewProtocol> view;
@property (nonatomic, strong) id <NITSnapPhoneInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITSnapPhoneWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)checkPhoneNumber:(NSString *)phoneNumber;
- (void)confitmSMS;
@end

@protocol NITSnapPhoneInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)successPosting:(NSString *)message;
@end

@protocol NITSnapPhoneInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITSnapPhoneInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITSnapPhoneAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITSnapPhoneLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)postPhoneNumber;
@end


@protocol NITSnapPhoneDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITSnapPhoneAPIDataManagerInputProtocol <NITSnapPhoneDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)postPhoneNumberWithHandler:(void(^)(SLErrorModel *output))handler;
@end

@protocol NITSnapPhoneLocalDataManagerInputProtocol <NITSnapPhoneDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
