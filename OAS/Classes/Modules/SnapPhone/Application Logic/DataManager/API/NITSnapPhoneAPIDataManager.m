//
//  NITSnapPhoneAPIDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITSnapPhoneAPIDataManager.h"
#import "SLAuthApi.h"
#import "SLUsersApi.h"

@implementation NITSnapPhoneAPIDataManager

#pragma mark - NITSnapPhoneDataManagerInputProtocol
- (void)postPhoneNumberWithHandler:(void(^)(SLErrorModel *output))handler
{
    [HELPER startLoading];
    NITDiscountCardModel * card = [NITDiscountCardModel new];
    SLUserWithRelations * query = [SLUserWithRelations new];
    query.uid = USER.identifier;
    SLCommunication * communication = [SLCommunication new];
    communication.type = @"phone";
    communication.value = [NSString stringWithPhone:card.phoneNumber];
    
    query.communications = @[communication];
    
    [[SLUsersApi new] postDynamicUserByUseridWithUserId:USER.identifier body:query completionHandler:^(SLUserWithRelations *output, NSError *error) {
        if(error)
        {
            [HELPER stopLoading];
            [HELPER logError:error method:METHOD_NAME];
            [ERROR_MESSAGE errorMessage:notification_error_with_phone_registration];
        }
        if(output)
        {
            SLVerifyQuery * verify = [SLVerifyQuery new];
            verify.type = @"phone";
            verify.value = [NSString stringWithPhone:card.phoneNumber];
            
            [[SLAuthApi new] postAuthCheckCommunicationWithBody:verify completionHandler:^(SLErrorModel *output, NSError *error) {
                if(error)
                {
                    [HELPER logError:error method:METHOD_NAME];
                    [ERROR_MESSAGE errorMessage:notification_error_with_phone_registration];
                }
                if(output) handler (output);
            }];
        }
    }];
}

@end
