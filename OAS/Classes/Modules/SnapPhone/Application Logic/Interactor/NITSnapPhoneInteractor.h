//
//  NITSnapPhoneInteractor.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITSnapPhoneProtocols.h"

@interface NITSnapPhoneInteractor : NSObject <NITSnapPhoneInteractorInputProtocol>

@property (nonatomic, weak) id <NITSnapPhoneInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITSnapPhoneAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITSnapPhoneLocalDataManagerInputProtocol> localDataManager;

@end
