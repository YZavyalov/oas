//
//  NITSnapPhoneInteractor.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITSnapPhoneInteractor.h"

@implementation NITSnapPhoneInteractor

#pragma mark - NITSnapPhoneInteractorInputProtocol
- (void)postPhoneNumber
{
    weaken(self);
    [self.APIDataManager postPhoneNumberWithHandler:^(SLErrorModel *output) {
        [weakSelf.presenter successPosting:nil];
    }];
}

@end
