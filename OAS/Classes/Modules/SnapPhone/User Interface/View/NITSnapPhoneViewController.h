//
//  NITSnapPhoneViewController.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITSnapPhoneProtocols.h"
#import "NITBaseViewController.h"

@interface NITSnapPhoneViewController : NITBaseViewController <NITSnapPhoneViewProtocol>

@property (nonatomic, strong) id <NITSnapPhonePresenterProtocol> presenter;

@end
