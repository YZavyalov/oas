//
//  NITSnapPhonePresenter.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITSnapPhoneProtocols.h"

@class NITSnapPhoneWireFrame;

@interface NITSnapPhonePresenter : NSObject <NITSnapPhonePresenterProtocol, NITSnapPhoneInteractorOutputProtocol>

@property (nonatomic, weak) id <NITSnapPhoneViewProtocol> view;
@property (nonatomic, strong) id <NITSnapPhoneInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITSnapPhoneWireFrameProtocol> wireFrame;

@end
