//
//  NITSnapPhonePresenter.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITSnapPhonePresenter.h"
#import "NITSnapPhoneWireframe.h"

@implementation NITSnapPhonePresenter

#pragma mark - NITSnapPhonePresenterProtocol
- (void)checkPhoneNumber:(NSString *)phoneNumber
{
    [[NITDiscountCardModel new] setPhoneNumber:phoneNumber];
    if(phoneNumber.length > 17)
    {
        [self.view updateNextBtn:true];
    }
    else
    {
        [self.view updateNextBtn:false];
    }
}

- (void)confitmSMS
{
    [self.interactor postPhoneNumber];
}

#pragma mark - NITSnapPhoneInteractorOutputProtocol
- (void)successPosting:(NSString *)message
{
    if(message)
    {
        [self showAlertControllerWithMessage:message];
    }
    else
    {
        [self.wireFrame confitmSMSFromViewController:self.view];
    }
}

#pragma mark - Private
- (void)showAlertControllerWithMessage:(NSString *)message
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Не удалось отправить номер телефона", nil)
                                                                    message:message
                                                             preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = RGB(58, 160, 144);
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ок", nil)
                                                      style:UIAlertActionStyleCancel
                                                    handler:^(UIAlertAction * action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                    }];
    
    [alert addAction:cancel];
    
    [self.wireFrame presentAlertFrom:self.view withAlert:alert];
}

@end
