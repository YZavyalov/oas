//
//  NITSnapPhoneWireFrame.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITSnapPhoneProtocols.h"
#import "NITSnapPhoneViewController.h"
#import "NITSnapPhoneLocalDataManager.h"
#import "NITSnapPhoneAPIDataManager.h"
#import "NITSnapPhoneInteractor.h"
#import "NITSnapPhonePresenter.h"
#import "NITSnapPhoneWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITSnapPhoneWireFrame : NITRootWireframe <NITSnapPhoneWireFrameProtocol>

@end
