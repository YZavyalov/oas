//
//  NITSnapPhoneWireFrame.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/17/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITSnapPhoneWireFrame.h"
#import "NITConfirmSMSWireFrame.h"

@implementation NITSnapPhoneWireFrame

#pragma mark - Private
+ (id)createNITSnapPhoneModule
{
    // Generating module components
    id <NITSnapPhonePresenterProtocol, NITSnapPhoneInteractorOutputProtocol> presenter = [NITSnapPhonePresenter new];
    id <NITSnapPhoneInteractorInputProtocol> interactor = [NITSnapPhoneInteractor new];
    id <NITSnapPhoneAPIDataManagerInputProtocol> APIDataManager = [NITSnapPhoneAPIDataManager new];
    id <NITSnapPhoneLocalDataManagerInputProtocol> localDataManager = [NITSnapPhoneLocalDataManager new];
    id <NITSnapPhoneWireFrameProtocol> wireFrame= [NITSnapPhoneWireFrame new];
    id <NITSnapPhoneViewProtocol> view = [(NITSnapPhoneWireFrame *)wireFrame createViewControllerWithKey:_s(NITSnapPhoneViewController) storyboardType:StoryboardTypeMap];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITSnapPhoneWireFrameProtocol
+ (void)presentNITSnapPhoneModuleFrom:(UIViewController *)fromViewController
{
    NITSnapPhoneViewController * vc = [NITSnapPhoneWireFrame createNITSnapPhoneModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)confitmSMSFromViewController:(UIViewController *)fromViewController
{
    [NITConfirmSMSWireFrame presentNITConfirmSMSModuleFrom:fromViewController];
}

- (void)presentAlertFrom:(UIViewController *)fromViewController withAlert:(UIAlertController *)alert
{
    [fromViewController presentViewController:alert animated:YES completion:nil];
}

@end
