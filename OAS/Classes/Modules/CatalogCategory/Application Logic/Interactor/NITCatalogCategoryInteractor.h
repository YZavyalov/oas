//
//  NITCatalogCategoryInteractor.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/14/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogCategoryProtocols.h"

@interface NITCatalogCategoryInteractor : NSObject <NITCatalogCategoryInteractorInputProtocol>

@property (nonatomic, weak) id <NITCatalogCategoryInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCatalogCategoryAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCatalogCategoryLocalDataManagerInputProtocol> localDataManager;

@end
