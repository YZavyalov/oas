//
//  NITCatalogCategoryInteractor.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/14/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogCategoryInteractor.h"

@implementation NITCatalogCategoryInteractor

#pragma mark - NITCatalogCategoryInteractorInputProtocol
- (void)getCategoriesByGroupID:(NSString *)groupID
{
    weaken(self);
    [self.APIDataManager getCatalogGroupsByGroupID:groupID withHandler:^(NSArray<SLGroup *> *result) {
        [weakSelf.presenter updateCategoriesModels:[NITGroupModel categoryModelsFromEntities:result]];
    }];
}

- (void)searchByText:(NSString *)text
{
    weaken(self);
    [self.APIDataManager getCatalogItemsBySearchString:text withHandler:^(NSArray<SLCatalogItemWithRelations *> *result) {
        [weakSelf.presenter updateSearchResultModels:[NITCatalogItemModel catalogItemDetailModelsFromEntities:result withGroupID:nil]];
    }];
}

@end
