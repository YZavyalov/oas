//
//  NITCatalogCategoryAPIDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/14/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogCategoryAPIDataManager.h"
#import "SLCatalogsApi.h"
#import "NITPharmacyItem.h"
#import "NITApplyingFilterModel.h"

@implementation NITCatalogCategoryAPIDataManager

#pragma mark - NITCatalogCategoryAPIDataManagerInputProtocol
- (void)getCatalogGroupsByGroupID:(NSString *)groupID withHandler:(void(^)(NSArray<SLGroup *> *result))handler
{
    [HELPER startLoading];
    [[SLCatalogsApi new] getStaticCatalogGroupsByGroupidWithGroupId:groupID completionHandler:^(NSArray<SLGroup> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [ERROR_MESSAGE errorMessage:error_service_unavailable];
            [HELPER logError:error method:METHOD_NAME];
        }
        if (handler) handler (output);
    }];
}

- (void)getCatalogItemsBySearchString:(NSString *)searchString withHandler:(void(^)(NSArray<SLCatalogItemWithRelations *> *result))handler
{
    /*SLCatalogQuery * query = [SLCatalogQuery new];
    NSDictionary *q = @{@"op":@"&",
                        @"pr":@[@{@"n":@"name", @"v":searchString}]
                        };
    query.query = q;
    query.orderBy = @"name";
    query.order = @"ASC";
    if([NITPharmacyItem allPharmacyForSearch].count > 0)
        query.shopIdsOr = [NITPharmacyItem allPharmacyForSearch];
    
    [HELPER startLoading];    
    [[SLCatalogsApi new] postStaticCatalogQuerywithrelationsWithBody:query completionHandler:^(NSArray<SLCatalogItemWithRelations> *output, NSError *error) {
        [HELPER stopLoading];
        if (error){
            [ERROR_MESSAGE errorMessage:error_service_unavailable];
            [HELPER logError:error method:METHOD_NAME];
        }
        if (handler) handler (output);
    }];*/
    
    SLCatalogElasticQuery * query = [SLCatalogElasticQuery new];
    
    if (searchString.length > 2)
    {
        SLCatalogElasticQueryCondition * subQuery = [SLCatalogElasticQueryCondition new];
        subQuery.n = @"name";
        subQuery.v = searchString;
        
        query.query = subQuery;
    }
    
    [HELPER startLoading];
    NSURLSessionTask * task = [[SLCatalogsApi new] postStaticCatalogElasticquerywithrelationsWithBody:query completionHandler:^(NSArray<SLCatalogItemWithRelations> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [ERROR_MESSAGE errorMessage:error_with_loading_product];
            [HELPER logError:error method:METHOD_NAME];
        }
        if (handler) handler (output);
    }];
    
    [HELPER logString:[NSString stringWithFormat:@"%@\n%@", THIS_METHOD, [task.currentRequest description]]];
}


@end
