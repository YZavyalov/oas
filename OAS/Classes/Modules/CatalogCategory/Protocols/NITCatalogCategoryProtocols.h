//
//  NITCatalogCategoryProtocols.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/14/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITGroupModel.h"
#import "NITCatalogItemModel.h"

@protocol NITCatalogCategoryInteractorOutputProtocol;
@protocol NITCatalogCategoryInteractorInputProtocol;
@protocol NITCatalogCategoryViewProtocol;
@protocol NITCatalogCategoryPresenterProtocol;
@protocol NITCatalogCategoryLocalDataManagerInputProtocol;
@protocol NITCatalogCategoryAPIDataManagerInputProtocol;

@class NITCatalogCategoryWireFrame, SLGroup;

// Defines the public interface that something else can use to drive the user interface
@protocol NITCatalogCategoryViewProtocol
@required
@property (nonatomic, strong) id <NITCatalogCategoryPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModels:(NSArray <NITGroupModel *> *)models;
- (void)reloadSearchResultByModels:(NSArray <NITCatalogItemModel *> *)models;
@end

@protocol NITCatalogCategoryWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITCatalogCategoryModuleFrom:(id)fromView withModel:(id)model;
- (void)showCatalogDirectoryFrom:(id)fromView withModel:(id)model;
- (void)showCatalogItemDetailsFrom:(id)fromView withModel:(id)model;
- (void)showSearchItemsWithViewController:(id)fromViewController withSearchText:(NSString *)text;
@end

@protocol NITCatalogCategoryPresenterProtocol
@required
@property (nonatomic, weak) id <NITCatalogCategoryViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogCategoryInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogCategoryWireFrameProtocol> wireFrame;
@property (nonatomic) NSString * parentID;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)updateData;
- (void)contactAction;
- (void)searchByText:(NSString *)text;
- (void)showCatalogDirectoryByModel:(NITGroupModel *)model;
- (void)showCatalogItemByModel:(NITCatalogItemModel *)model;
- (void)showSearchItemsWithSearchText:(NSString *)text;
@end

@protocol NITCatalogCategoryInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateCategoriesModels:(NSArray <NITGroupModel *> *)models;
- (void)updateSearchResultModels:(NSArray <NITCatalogItemModel *> *)models;
@end

@protocol NITCatalogCategoryInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCatalogCategoryInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCatalogCategoryAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCatalogCategoryLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getCategoriesByGroupID:(NSString *)groupID;
- (void)searchByText:(NSString *)text;
@end


@protocol NITCatalogCategoryDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCatalogCategoryAPIDataManagerInputProtocol <NITCatalogCategoryDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getCatalogGroupsByGroupID:(NSString *)groupID withHandler:(void(^)(NSArray<SLGroup *> *result))handler;
- (void)getCatalogItemsBySearchString:(NSString *)searchString withHandler:(void(^)(NSArray<SLCatalogItemWithRelations *> *result))handler;
@end

@protocol NITCatalogCategoryLocalDataManagerInputProtocol <NITCatalogCategoryDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
