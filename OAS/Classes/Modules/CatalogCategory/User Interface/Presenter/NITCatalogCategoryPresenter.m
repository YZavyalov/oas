//
//  NITCatalogCategoryPresenter.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/14/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogCategoryPresenter.h"
#import "NITCatalogCategoryWireframe.h"
#import "NITMobileConstantsModel.h"
#import "NITApplyingFilterModel.h"

@implementation NITCatalogCategoryPresenter

#pragma mark - NITCatalogCategoryPresenterProtocol
- (void)updateData
{
    [self addObservers];
    [self initData];
}

- (void)initData
{
    [self.interactor getCategoriesByGroupID:self.parentID];
}

- (void)contactAction
{
    NSString * phone = [[NITMobileConstantsModel new] contact_phone];
    [HELPER callToPhoneNumber:phone];
}

- (void)searchByText:(NSString *)text
{
    if (text.length > 2)
    {
        [self.interactor searchByText:text];
    }
}

- (void)showCatalogDirectoryByModel:(NITGroupModel *)model
{
    NITApplyingFilterModel * filterModel = [NITApplyingFilterModel new];
    [filterModel clearFilter];
    [self.wireFrame showCatalogDirectoryFrom:self.view withModel:model];
}

- (void)showCatalogItemByModel:(NITCatalogItemModel *)model
{
    [self.wireFrame showCatalogItemDetailsFrom:self.view withModel:model];
}

- (void)showSearchItemsWithSearchText:(NSString *)text
{
    [self.wireFrame showSearchItemsWithViewController:self.view withSearchText:text];
}

#pragma mark - NITCatalogCategoryInteractorOutputProtocol
- (void)updateCategoriesModels:(NSArray<NITGroupModel *> *)models
{
    [self.view reloadDataByModels:models];
}

- (void)updateSearchResultModels:(NSArray<NITCatalogItemModel *> *)models
{
    [self.view reloadSearchResultByModels:models];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initData) name:kUpdateInternetConnect object:nil];
}

@end
