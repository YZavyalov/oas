//
//  NITCatalogCategoryPresenter.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/14/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogCategoryProtocols.h"

@class NITCatalogCategoryWireFrame;

@interface NITCatalogCategoryPresenter : NSObject <NITCatalogCategoryPresenterProtocol, NITCatalogCategoryInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCatalogCategoryViewProtocol> view;
@property (nonatomic, strong) id <NITCatalogCategoryInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCatalogCategoryWireFrameProtocol> wireFrame;
@property (nonatomic) NSString * parentID;

@end
