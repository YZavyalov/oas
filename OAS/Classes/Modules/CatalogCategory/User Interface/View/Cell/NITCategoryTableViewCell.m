//
//  NITCategoryTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 14.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCategoryTableViewCell.h"
#import "NITGroupModel.h"

@interface NITCategoryTableViewCell()

@property (nonatomic) IBOutlet UILabel * title;

@end

@implementation NITCategoryTableViewCell

#pragma mark - Life cycle
- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initData];
}

#pragma mark - Custom accessors
- (void)setModel:(NITGroupModel *)model
{
    _model = model;
    [self updateData];
}

#pragma mark - Private
- (void)initData
{
    self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_next"]];
}

- (void)updateData
{
    self.title.text = self.model.name;
}


@end
