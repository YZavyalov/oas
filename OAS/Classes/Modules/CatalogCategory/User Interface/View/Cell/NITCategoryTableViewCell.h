//
//  NITCategoryTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 14.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

@class NITGroupModel;

@interface NITCategoryTableViewCell : UITableViewCell

@property (nonatomic) NITGroupModel * model;

@end
