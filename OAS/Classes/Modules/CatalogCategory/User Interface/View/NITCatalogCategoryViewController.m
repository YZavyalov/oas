//
//  NITCatalogCategoryViewController.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/14/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogCategoryViewController.h"
#import "NITCategoryTableViewCell.h"
#import "NITCatalogTopSearchTableViewCell.h"

@interface NITCatalogCategoryViewController()
<
UISearchBarDelegate,
UITableViewDelegate,
UITableViewDataSource,
NITSearchViewDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UIView * blurView;
@property (nonatomic) NSArray <NITGroupModel *> * items;

@end

@implementation NITCatalogCategoryViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter updateData];
}

#pragma mark - IBAction
- (IBAction)searchAction:(id)sender
{
    [self showSearchBar];
}

#pragma mark - Private
- (void)initUI
{
    self.searchBar.delegate = self;
    self.searchView.delegate = self;
    
    // table view
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITCategoryTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITCategoryTableViewCell)];
    [self.tableView setTableFooterView:[UIView new]];

    [self initSearchBar];
}

#pragma mark - UIButton
- (IBAction)hideBlurView:(id)sender
{
    self.blurView.hidden = true;
}

#pragma mark - NITCatalogCategoryViewProtocol
- (void)reloadDataByModels:(NSArray<NITGroupModel *> *)models
{
    self.items = models;
    [self.tableView reloadData];
}

- (void)reloadSearchResultByModels:(NSArray <NITCatalogItemModel *> *)models
{
    [self.searchView setHidden:models.count > 0 ? false : true];
    [self.blurView setHidden:false];
    [self.searchView reloadDataByModels:models];
}

#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCategoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITCategoryTableViewCell) forIndexPath:indexPath];
    cell.model = self.items[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self searchBarCancelButtonClicked:self.searchBar];
    [self.presenter showCatalogDirectoryByModel:self.items[indexPath.row]];
}

#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchText.length == 0) self.blurView.hidden = true;
    else                       self.blurView.hidden = false;
    if(searchText.length < 3)   self.searchView.hidden = true;

    [self.presenter searchByText:searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.searchBar.text = @"";
    self.searchBar.showsCancelButton = false;
    [self.searchBar endEditing:true];
    
    self.searchView.hidden = true;
    self.blurView.hidden = true;
    [self hideSearchBar];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar endEditing:true];
    [self enableSearchBarCanelButton];
    [self.presenter showSearchItemsWithSearchText:searchBar.text];
}

#pragma mark - NITSearchViewDelegate
- (void)didSelectSearchModel:(NITCatalogItemModel *)model
{
    [self.searchBar endEditing:true];
    [self.presenter showCatalogItemByModel:model];
}

#pragma mark - NITEmptySearchViewDelegate
- (void)didPressContact
{
    [self.presenter contactAction];
}

@end
