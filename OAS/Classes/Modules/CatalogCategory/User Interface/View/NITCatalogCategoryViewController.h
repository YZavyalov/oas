//
//  NITCatalogCategoryViewController.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/14/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCatalogCategoryProtocols.h"
#import "NITBaseViewController.h"

@interface NITCatalogCategoryViewController : NITBaseViewController <NITCatalogCategoryViewProtocol>

@property (nonatomic, strong) id <NITCatalogCategoryPresenterProtocol> presenter;

@end
