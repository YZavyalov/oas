//
//  NITCatalogCategoryWireFrame.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/14/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCatalogCategoryProtocols.h"
#import "NITCatalogCategoryViewController.h"
#import "NITCatalogCategoryLocalDataManager.h"
#import "NITCatalogCategoryAPIDataManager.h"
#import "NITCatalogCategoryInteractor.h"
#import "NITCatalogCategoryPresenter.h"
#import "NITCatalogCategoryWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITCatalogCategoryWireFrame : NITRootWireframe <NITCatalogCategoryWireFrameProtocol>

@end
