//
//  NITCatalogCategoryWireFrame.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/14/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCatalogCategoryWireFrame.h"
#import "NITCatalogDirectoryWireFrame.h"
#import "NITProductDetailsWireFrame.h"

@implementation NITCatalogCategoryWireFrame

#pragma mark - Private
+ (id)createNITCatalogCategoryModule
{
    // Generating module components
    id <NITCatalogCategoryPresenterProtocol, NITCatalogCategoryInteractorOutputProtocol> presenter = [NITCatalogCategoryPresenter new];
    id <NITCatalogCategoryInteractorInputProtocol> interactor = [NITCatalogCategoryInteractor new];
    id <NITCatalogCategoryAPIDataManagerInputProtocol> APIDataManager = [NITCatalogCategoryAPIDataManager new];
    id <NITCatalogCategoryLocalDataManagerInputProtocol> localDataManager = [NITCatalogCategoryLocalDataManager new];
    id <NITCatalogCategoryWireFrameProtocol> wireFrame= [NITCatalogCategoryWireFrame new];
    id <NITCatalogCategoryViewProtocol> view = [(NITCatalogCategoryWireFrame *)wireFrame createViewControllerWithKey:_s(NITCatalogCategoryViewController) storyboardType:StoryboardTypeCatalog];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITCatalogCategoryWireFrameProtocol
+ (void)presentNITCatalogCategoryModuleFrom:(UIViewController *)fromViewController withModel:(NITBaseModel *)model
{
    NITCatalogCategoryViewController * view = [NITCatalogCategoryWireFrame createNITCatalogCategoryModule];
    view.title = model.name;
    view.presenter.parentID = model.identifier;
    [fromViewController.navigationController pushViewController:view animated:true];
}

- (void)showCatalogDirectoryFrom:(UIViewController *)fromViewController withModel:(NITGroupModel *)model
{
    [NITCatalogDirectoryWireFrame presentNITCatalogDirectoryModuleFrom:fromViewController withModel:model withSearchText:nil];
}

- (void)showCatalogItemDetailsFrom:(UIViewController *)fromViewController withModel:(NITCatalogItemModel *)model
{
    [NITProductDetailsWireFrame presentNITProductDetailsModuleFrom:fromViewController withModel:model withType:0];
}

- (void)showSearchItemsWithViewController:(UIViewController *)fromViewController withSearchText:(NSString *)text
{
    [NITCatalogDirectoryWireFrame presentNITCatalogDirectoryModuleFrom:fromViewController withModel:nil withSearchText:text];
}

@end
