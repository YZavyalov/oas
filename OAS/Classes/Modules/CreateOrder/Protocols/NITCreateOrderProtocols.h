//
//  NITCreateOrderProtocols.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITDeliveryInfoSectionModel.h"
#import "NITDeliveryInfoModel.h"
#import "SLOrder.h"
#import "NITBasketItem.h"
#import "NITShopItemModel.h"
#import "NITOrderItemModel.h"
#import "NITOrderItem.h"
#import "NITMobileConstantsModel.h"

@protocol NITCreateOrderInteractorOutputProtocol;
@protocol NITCreateOrderInteractorInputProtocol;
@protocol NITCreateOrderViewProtocol;
@protocol NITCreateOrderPresenterProtocol;
@protocol NITCreateOrderLocalDataManagerInputProtocol;
@protocol NITCreateOrderAPIDataManagerInputProtocol;

@class NITCreateOrderWireFrame;

typedef CF_ENUM (NSUInteger, DeliverySectionType) {
    DeliverySectionTypeTime         = 0,
    DeliverySectionTypeContactInfo  = 1,
    DeliverySectionTypePharmacy     = 2,
    DeliverySectionTypePayment      = 3
};

typedef CF_ENUM (NSUInteger, CreateOrderPropertyType) {
    CreateOrderPropertyTypename     = 0,
    CreateOrderPropertyTypePhone    = 1,
    CreateOrderPropertyTypeAddress  = 2,
    CreateOrderPropertyTypeComment  = 3
};

typedef CF_ENUM (NSUInteger, DeliveryPaymentType) {
    DeliveryPaymentTypeCache        = 0,
    DeliveryPaymentTypeCard         = 1
};

// Defines the public interface that something else can use to drive the user interface
@protocol NITCreateOrderViewProtocol
@required
@property (nonatomic, strong) id <NITCreateOrderPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadTitle:(NSString *)title;
- (void)reloadDataByModel:(NITDeliveryInfoModel *)model sections:(NSArray<NITDeliveryInfoSectionModel *> *)sections;
- (void)reloadDeliveryTime:(NSString *)deliveryTime;
@end

@protocol NITCreateOrderWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITDeliveryInfoModuleFrom:(id)fromView withType:(DeliveryType)type;
- (void)showCatalogFromViewController:(id)fromView;
- (void)showPharmacyFromViewController:(id)fromView withDelegate:(id)delegate;
- (void)presentCatalogFromViewController:(id)fromView;
- (void)popFromViewController:(id)fromView;
- (void)presentAlertFrom:(id)fromView withAlert:(id)alert;
@end

@protocol NITCreateOrderPresenterProtocol
@required
@property (nonatomic, weak) id <NITCreateOrderViewProtocol> view;
@property (nonatomic, strong) id <NITCreateOrderInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCreateOrderWireFrameProtocol> wireFrame;
@property (nonatomic) DeliveryType deliveryType;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (NITDeliveryInfoModel *)updateModel:(DeliveryType)deliveryType;
- (NSArray<NITDeliveryInfoSectionModel *> *)sections;
- (void)changeDeliveryTime;
- (void)changePharmacy;
- (void)updateOrderProperty:(NSString *)property withType:(CreateOrderPropertyType)type;
- (void)updateOrderShop:(NITShopItemModel *)model;
- (void)sendOrder:(NITDeliveryInfoModel *)model;
- (void)callAction;
@end

@protocol NITCreateOrderInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateOrderBasketItems:(NSArray <NITCatalogItemModel *> *)models;
- (void)orderCreatedSuccess;
- (void)orderCreateError;
@end

@protocol NITCreateOrderInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCreateOrderInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCreateOrderAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCreateOrderLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)sendOrder:(NITDeliveryInfoModel *)model;
- (void)getBasketItems;
- (void)clearBasket;
@end


@protocol NITCreateOrderDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCreateOrderAPIDataManagerInputProtocol <NITCreateOrderDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)sendOrder:(SLOrderWithRelations *)order withHandler:(void(^)(SLOrderWithRelations * order))handler;
@end

@protocol NITCreateOrderLocalDataManagerInputProtocol <NITCreateOrderDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NSArray <NITBasketItem *> *)getBasketItems;
- (void)clearBasket;
- (void)clearBasketItemsFromOrder:(NITOrderItemModel *)orderModel;
- (void)saveOrder:(NITOrderItemModel *)model;
@end
