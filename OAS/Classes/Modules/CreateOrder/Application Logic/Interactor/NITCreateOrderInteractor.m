//
//  NITDeliveryInteractor.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCreateOrderInteractor.h"
#import "SLOrderWithRelations.h"

@implementation NITCreateOrderInteractor

#pragma mark - NITCreateOrderInteractorInputProtocol
- (void)sendOrder:(NITDeliveryInfoModel *)model
{
    NSArray <NITCatalogItemModel *> * basketModels = [NITCatalogItemModel catalogItemsModelsFromBasketEntities:[self.localDataManager getBasketItems]];
    
    if (basketModels.count > 0)
    {
        NSArray <NITCatalogItemModel *> * basketModelNonUnderOrder = [basketModels bk_select:^BOOL(NITCatalogItemModel *obj) {
            return [obj underOrder] == false;
        }];
        NSArray <NITCatalogItemModel *> * basketModelUnderOrder = [basketModels bk_select:^BOOL(NITCatalogItemModel *obj) {
            return [obj underOrder] == true;
        }];
        
        weaken(self);
        __block NSInteger sendCounter = 0;
        dispatch_group_t group = dispatch_group_create();
        
        if (basketModelNonUnderOrder.count > 0)
        {
            sendCounter++;
            dispatch_group_enter(group);
            [self sendSeparatedOrder:model basketItems:basketModelNonUnderOrder withHandler:^(BOOL success, SLOrderWithRelations *output) {
                
                if (success)
                {
                    sendCounter--;
                    NITOrderItemModel * orderModel = [NITOrderItemModel orderItemModelFromEntities:output];
                    [weakSelf.localDataManager saveOrder:orderModel];
                    [weakSelf.localDataManager clearBasketItemsFromOrder:orderModel];
                }
                
                dispatch_group_leave(group);
            }];
        }
        
        if (basketModelUnderOrder.count > 0)
        {
            sendCounter++;
            dispatch_group_enter(group);
            [self sendSeparatedOrder:model basketItems:basketModelUnderOrder withHandler:^(BOOL success, SLOrderWithRelations *output) {
                
                if (success)
                {
                    sendCounter--;
                    NITOrderItemModel * orderModel = [NITOrderItemModel orderItemModelFromEntities:output];
                    [weakSelf.localDataManager saveOrder:orderModel];
                    [weakSelf.localDataManager clearBasketItemsFromOrder:orderModel];
                }
                
                dispatch_group_leave(group);
            }];
        }
        
        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            
            if (sendCounter > 0)
            {
                [weakSelf.presenter orderCreateError];
            }
            else
            {
                [weakSelf.presenter orderCreatedSuccess];
            }
        });
    }
    else
    {
        [self.presenter orderCreateError];
    }
}

- (void)getBasketItems
{
    [self.presenter updateOrderBasketItems:[NITCatalogItemModel catalogItemsModelsFromBasketEntities:[self.localDataManager getBasketItems]]];
}

- (void)clearBasket
{
    [self.localDataManager clearBasket];
}

#pragma mark - Private
- (void)sendSeparatedOrder:(NITDeliveryInfoModel *)orderModel basketItems:(NSArray <NITCatalogItemModel *> *)basketItems  withHandler:(void(^)(BOOL success, SLOrderWithRelations *output))handler
{
    BOOL isUnderOrder = [basketItems bk_select:^BOOL(NITCatalogItemModel *obj) {
        return [obj underOrder] == true;
    }].count > 0;
    
    orderModel.under_order = isUnderOrder ? @"true" : @"false";
    
    NSString * day = [NSString stringWithFormat:@"%d", (int)([NSDate date].day + 3)];
    if (orderModel.deliveryType == DeliveryTypeDelivery && isUnderOrder)
    {
        NSString * deliveryDate = [[NITDeliveryInfoModel new] delivery_times];
        deliveryDate = [NSString string:deliveryDate replacementWord:day inPosition:2];
        [[NITDeliveryInfoModel new] setDelivery_times:deliveryDate];
    }
    
    if (orderModel.deliveryType == DeliveryTypePickup && isUnderOrder)
    {
        NSString * pickupDate = [[NITDeliveryInfoModel new] pick_up_times];
        pickupDate = [NSString string:pickupDate replacementWord:day inPosition:2];
        [[NITDeliveryInfoModel new] setPick_up_times:pickupDate];
    }
    
    SLOrderWithRelations * body = [SLOrderWithRelations new];
    
    body.userId     = USER.identifier;
    body.status     = @"create";
    body.items      = [NITCatalogItemModel createSLOrderItemWithCatalogItem:basketItems];
    body.properties = [orderModel createOrderSLPropertyValues];
    
    [self.APIDataManager sendOrder:body withHandler:^(SLOrderWithRelations *result) {
        if (result)  handler(true, result);
        else         handler(false, nil);
    }];
}

@end
