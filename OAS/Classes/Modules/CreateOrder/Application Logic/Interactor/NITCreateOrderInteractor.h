//
//  NITDeliveryInteractor.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCreateOrderProtocols.h"

@interface NITCreateOrderInteractor : NSObject <NITCreateOrderInteractorInputProtocol>

@property (nonatomic, weak) id <NITCreateOrderInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCreateOrderAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCreateOrderLocalDataManagerInputProtocol> localDataManager;

@end
