//
//  NITDeliveryInfoSectionModel.h
//  OAS
//
//  Created by Yaroslav on 10.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef CF_ENUM(NSUInteger, CreateOrderSectionType) {
    CreateOrderSectionTypePrice        = 0,
    CreateOrderSectionTypeTime         = 1,
    CreateOrderSectionTypeChangeButton = 2,
    CreateOrderSectionTypeContact      = 3,
    CreateOrderSectionTypePaymant      = 4,
    CreateOrderSectionTypeAddress      = 5,
    CreateOrderSectionTypeAgreemant    = 6
};

@interface NITDeliveryInfoSectionModel : NSObject

@property (nonatomic) DeliveryType deliveryType;
@property (nonatomic) CreateOrderSectionType sectionType;
@property (nonatomic) NSString * cellID;
@property (nonatomic) NSString * headerText;
@property (nonatomic) CGFloat headerHeight;
@property (nonatomic) NSInteger numberOfRows;
@property (nonatomic) CGFloat rowHeight;
@property (nonatomic) CGFloat pickerheight;

- (instancetype)initWithType:(CreateOrderSectionType)sectionType andDeliveryType:(DeliveryType)deliveryType;

@end
