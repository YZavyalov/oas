//
//  NITDeliveryInfoModel.m
//  OAS
//
//  Created by Yaroslav on 10.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITDeliveryInfoModel.h"

static NSString * const kDeliveryTime       = @"kDeliveryTime";
static NSString * const kDeliveryName       = @"kDeliveryName";
static NSString * const kDeliveryPhone      = @"kDeliveryPhone";
static NSString * const kDeliveryComment    = @"kDeliveryComment";
static NSString * const kDeliveryAddress    = @"kDeliveryAddress";
static NSString * const kDeliveryCache      = @"kDeliveryCache";
static NSString * const kDeliveryAgreement  = @"kDeliveryAgreement";
static NSString * const kPickupShopAddress  = @"kPickupShopAddress";
static NSString * const kPickupShopID       = @"kPickupShopID";
static NSString * const kPickupTimes        = @"kPickupTimes";
static NSString * const kPickupShopIshub    = @"kPickupShopIshub";
static NSString * const kDeliveryTimes      = @"kDeliveryTimes";
static NSString * const kDeliveryUnderOrder = @"kDeliveryUnderOrder";

@implementation NITDeliveryInfoModel

#pragma mark - Custom Accessors
- (NSString *)deliveryTime
{
    return [self stringForKey:kDeliveryTime];
}

- (void)setDeliveryTime:(NSString *)deliveryTime
{
    [self setString:deliveryTime forKey:kDeliveryTime];
}

- (NSString *)delivery_times
{
    return [self stringForKey:kDeliveryTimes];
}

- (void)setDelivery_times:(NSString *)delivery_times
{
    [self setString:delivery_times forKey:kDeliveryTimes];
}

- (NSString *)buyer
{
    return [self stringForKey:kDeliveryName];
}

- (void)setBuyer:(NSString *)buyer
{
    [self setString:buyer forKey:kDeliveryName];
}

- (NSString *)phone
{
    return [self stringForKey:kDeliveryPhone];
}

- (void)setPhone:(NSString *)phone
{
    [self setString:phone forKey:kDeliveryPhone];
}

- (NSString *)buyer_comment
{
    return [self stringForKey:kDeliveryComment];
}

- (void)setBuyer_comment:(NSString *)buyer_comment
{
    [self setString:buyer_comment forKey:kDeliveryComment];
}

- (NSString *)delivery_address
{
    return [self stringForKey:kDeliveryAddress];
}

- (void)setDelivery_address:(NSString *)delivery_address
{
    [self setString:delivery_address forKey:kDeliveryAddress];
}

- (BOOL)cash
{
    return [self boolForKey:kDeliveryCache];
}

- (void)setCash:(BOOL)cash
{
    [self setBool:cash forKey:kDeliveryCache];
}

- (BOOL)deliveryAgreement
{
    return [self boolForKey:kDeliveryAgreement];
}

- (void)setDeliveryAgreement:(BOOL)deliveryAgreement
{
    [self setBool:deliveryAgreement forKey:kDeliveryAgreement];
}

- (NSString *)shop_address
{
    return [self stringForKey:kPickupShopAddress];
}

- (void)setShop_address:(NSString *)shop_address
{
    [self setString:shop_address forKey:kPickupShopAddress];
}

- (NSString *)shop_id
{
    return [self stringForKey:kPickupShopID];
}

- (void)setShop_id:(NSString *)shop_id
{
    [self setString:shop_id forKey:kPickupShopID];
}

- (NSString * )pick_up_times
{
    return [self stringForKey:kPickupTimes];
}

- (void)setPick_up_times:(NSString *)pick_up_times
{
    [self setString:pick_up_times forKey:kPickupTimes];
}

- (BOOL)isHub
{
    return [self boolForKey:kPickupShopIshub];
}

- (void)setIsHub:(BOOL)isHub
{
    [self setBool:isHub forKey:kPickupShopIshub];
}

- (NSString *)under_order
{
    return [self stringForKey:kDeliveryUnderOrder];
}

- (void)setUnder_order:(NSString *)under_order
{
    [self setString:under_order forKey:kDeliveryUnderOrder];
}

#pragma mark - Public
- (void)setDeliveryType:(DeliveryType)deliveryType
{
    _deliveryType = deliveryType;
}

- (BOOL)isCorrect
{
    BOOL correct = true;
    
    if ([self.buyer length] == 0)
    {
        correct = false;
    }
    
    if(self.phone.length < 18)
    {
        correct = false;
    }
    
    switch (self.deliveryType)
    {
        case DeliveryTypeDelivery:
        {
            if([self.deliveryTime length] == 0)
            {
                correct = false;
            }
            
            if([self.delivery_address length] == 0)
            {
                correct = false;
            }
        } break;
            
        case DeliveryTypePickup:
        {
            if([self.shop_address length] == 0 && [self.shop_id length] == 0)
            {
                correct = false;
            }
        } break;
    }
    
    if(!self.deliveryAgreement)
    {
        correct = false;
    }

    return correct;
}

- (NSArray <SLIPropertyValue> *)createOrderSLPropertyValues
{
    NSArray * orderKeys = @[@"buyer",@"phone",@"buyer_comment",@"delivery_address",@"delivery_times",@"shop_address",@"shop_id",@"pick_up_times",@"under_order",@"cash",@"pick_up",@"deliveryType"];
    NSMutableArray * propertyArray = [NSMutableArray new];
    
    for (NSString * key in self.allPropertyNames)
    {
        if (![orderKeys containsObject:key])
        {
            continue;
        }
        
        if ([key isEqualToString:keyPath(self.deliveryType)] ||
            [key isEqualToString:keyPath(self.pick_up)])
        {
            SLIPropertyValue * property = [SLIPropertyValue new];
            property.key = keyPath(self.pick_up);
            property.value = self.deliveryType == DeliveryTypePickup ? @[@"true"] : @[@"false"];
            [propertyArray addObject:property];
            continue;
        }
        else if ([key isEqualToString:keyPath(self.cash)])
        {
            if (self.deliveryType == DeliveryTypeDelivery)
            {
                SLIPropertyValue * property = [SLIPropertyValue new];
                property.key = @"payment_method";
                property.value = @[self.cash ? @"cash" : @"non-cash"];
                [propertyArray addObject:property];
            }
            continue;
        }
        else if ([key isEqualToString:keyPath(self.delivery_times)] ||
                 [key isEqualToString:keyPath(self.delivery_address)])
        {
            NSString * value = [self valueForKey:key];
            if (self.deliveryType == DeliveryTypeDelivery && value.length > 0)
            {
                SLIPropertyValue * property = [SLIPropertyValue new];
                property.key = key;
                property.value = @[value];
                [propertyArray addObject:property];
            }
            continue;
        }
        else if ([key isEqualToString:keyPath(self.pick_up_times)] ||
                 [key isEqualToString:keyPath(self.shop_address)]  ||
                 [key isEqualToString:keyPath(self.shop_id)])
        {
            NSString * value = [self valueForKey:key];
            if (self.deliveryType == DeliveryTypePickup && value.length > 0)
            {
                SLIPropertyValue * property = [SLIPropertyValue new];
                property.key = key;
                property.value = @[value];
                [propertyArray addObject:property];
            }
            continue;
        }
        
        // check string type
        if (![[self valueForKey:key] isKindOfClass:[NSString class]])
        {
            continue;
        }
        
        NSString * value = [self valueForKey:key];
        if (value.length > 0)
        {
            SLIPropertyValue * property = [SLIPropertyValue new];
            property.key = key;
            property.value = @[value];
            [propertyArray addObject:property];
        }
    }
    
    if (USER.cityName)
    {
        SLIPropertyValue * property = [SLIPropertyValue new];
        property.key = @"city";
        property.value = @[USER.cityName];
        [propertyArray addObject:property];
    }
    
    return [propertyArray mutableCopy];
}

@end
