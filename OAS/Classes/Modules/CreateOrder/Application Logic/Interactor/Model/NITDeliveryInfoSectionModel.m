//
//  NITDeliveryInfoSectionModel.m
//  OAS
//
//  Created by Yaroslav on 10.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITDeliveryInfoSectionModel.h"
#import "NITContactInfoTableViewCell.h"
#import "NITDeliveryTimeTableViewCell.h"
#import "NITChangeDateTimeCell.h"
#import "NITPharmacyAddressTableViewCell.h"
#import "NITPaymantCachTableViewCell.h"
#import "NITAgreemantTableViewCell.h"
#import "NITDeliveryPriceTableViewCell.h"

@implementation NITDeliveryInfoSectionModel

#pragma mark - Life cycle
- (instancetype)initWithType:(CreateOrderSectionType)sectionType andDeliveryType:(DeliveryType)deliveryType
{
    if(self = [super init])
    {
        self.deliveryType = deliveryType;
        self.sectionType = sectionType;
        self.pickerheight = 44;
    }
    
    return self;
}

#pragma mark - Custom accessors
- (NSString *)headerText
{
    switch (self.sectionType)
    {
        case CreateOrderSectionTypePrice:
            return nil;
            
        case CreateOrderSectionTypeTime:
            return NSLocalizedString(@"ВРЕМЯ ДОСТАВКИ", nil);
            
        case CreateOrderSectionTypeChangeButton:
            return nil;
            
        case CreateOrderSectionTypeContact:
            return NSLocalizedString(@"КОНТАКТНАЯ ИНФОРМАЦИЯ", nil);;
            
        case CreateOrderSectionTypeAddress:
            return NSLocalizedString(@"АПТЕКА", nil);;
            
        case CreateOrderSectionTypePaymant:
            return NSLocalizedString(@"СПОСОБ ОПЛАТЫ", nil);;
            
        case CreateOrderSectionTypeAgreemant:
            return nil;
    }
}

- (CGFloat)headerHeight
{
    switch (self.sectionType)
    {
        case CreateOrderSectionTypePrice:
            return 0;
            
        case CreateOrderSectionTypeTime:
        {
            if(self.deliveryType == DeliveryTypeDelivery)   return 44;
            else                                            return 0;
        }
            
        case  CreateOrderSectionTypeChangeButton:
        {
            return 0;
        }
            
        case CreateOrderSectionTypeContact:
        {
            return 44;
        }
            
        case CreateOrderSectionTypeAddress:
        {
            if(self.deliveryType == DeliveryTypeDelivery)   return 0;
            else                                            return 44;
        }
            
        case CreateOrderSectionTypePaymant:
        {
            if(self.deliveryType == DeliveryTypeDelivery)   return 44;
            else                                            return 0;
        }
            
        case CreateOrderSectionTypeAgreemant:
        {
            return 0;
        }
    }
}

- (NSInteger)numberOfRows
{
    switch (self.sectionType)
    {
        case CreateOrderSectionTypePrice:
            return 1;
            
        case CreateOrderSectionTypeTime:
            return 1;
            
        case CreateOrderSectionTypeChangeButton:
            return 1;
            
        case CreateOrderSectionTypeContact:
        {
            switch (self.deliveryType)
            {
                case DeliveryTypePickup:    return 3;
                case DeliveryTypeDelivery:  return 4;
            }
        }
            
        case CreateOrderSectionTypeAddress:
            return 1;
            
        case CreateOrderSectionTypePaymant:
            return 2;
            
        case CreateOrderSectionTypeAgreemant:
            return 1;
    }
}

- (CGFloat)rowHeight
{
    switch (self.sectionType)
    {
        case CreateOrderSectionTypePrice:
        {
            if(self.deliveryType == DeliveryTypeDelivery)   return 119;
            else                                            return 44;
        }
            
        case CreateOrderSectionTypeTime:
        {
            if(self.deliveryType == DeliveryTypeDelivery)   return 65;
            else                                            return 0;
        }
            
        case CreateOrderSectionTypeChangeButton:
        {
            if(self.deliveryType == DeliveryTypeDelivery)   return _pickerheight;
            else                                            return 0;
        }
            
        case CreateOrderSectionTypeContact:
        {
            return 44;
        }
            
        case CreateOrderSectionTypeAddress:
        {
            if(self.deliveryType == DeliveryTypeDelivery)   return 0;
            else                                            return 140;
        }
            
        case CreateOrderSectionTypePaymant:
        {
            if(self.deliveryType == DeliveryTypeDelivery)   return 44;
            else                                            return 0;
        }
            
        case CreateOrderSectionTypeAgreemant:
        {
            return 65;
        }
    }
}

- (NSString *)cellID
{
    switch (self.sectionType)
    {
        case CreateOrderSectionTypePrice:
            return _s(NITDeliveryPriceTableViewCell);
            
        case CreateOrderSectionTypeTime:
            return _s(NITDeliveryTimeTableViewCell);
            
        case CreateOrderSectionTypeChangeButton:
            return _s(NITChangeDateTimeCell);
            
        case CreateOrderSectionTypeContact:
            return _s(NITContactInfoTableViewCell);
            
        case CreateOrderSectionTypeAddress:
            return _s(NITPharmacyAddressTableViewCell);
            
        case CreateOrderSectionTypePaymant:
            return _s(NITPaymantCachTableViewCell);
            
        case CreateOrderSectionTypeAgreemant:
            return _s(NITAgreemantTableViewCell);
    }
}

@end
