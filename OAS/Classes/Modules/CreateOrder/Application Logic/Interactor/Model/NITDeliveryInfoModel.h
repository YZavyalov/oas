//
//  NITDeliveryInfoModel.h
//  OAS
//
//  Created by Yaroslav on 10.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITShopItemModel.h"

@interface NITDeliveryInfoModel : NITLocalSettings

@property (nonatomic) DeliveryType deliveryType;
@property (nonatomic) NSString * deliveryTime;
@property (nonatomic) NSString * buyer;
@property (nonatomic) NSString * phone;
@property (nonatomic) NSString * buyer_comment;
@property (nonatomic) NSString * delivery_address;
@property (nonatomic) NSString * delivery_times;
@property (nonatomic) NSString * shop_address;
@property (nonatomic) NSString * shop_id;
@property (nonatomic) NSString * pick_up_times;
@property (nonatomic) NSString * under_order;
@property (nonatomic) BOOL isHub;
@property (nonatomic) BOOL cash;
@property (nonatomic) BOOL deliveryAgreement;

- (BOOL)isCorrect;
- (NSArray <SLIPropertyValue> *)createOrderSLPropertyValues;

@end
