//
//  NITDeliveryAPIDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCreateOrderAPIDataManager.h"
#import "SLOrdersApi.h"

@implementation NITCreateOrderAPIDataManager

#pragma mark - NITCreateOrderAPIDataManagerInputProtocol
- (void)sendOrder:(SLOrderWithRelations *)order withHandler:(void(^)(SLOrderWithRelations * order))handler
{
    [HELPER startLoading];
    [[SLOrdersApi new] postDynamicOrderuserByUseridWithUserId:USER.identifier body:order completionHandler:^(SLOrderWithRelations *output, NSError *error) {
        [HELPER stopLoading];
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler (output);
    }];
}

@end
