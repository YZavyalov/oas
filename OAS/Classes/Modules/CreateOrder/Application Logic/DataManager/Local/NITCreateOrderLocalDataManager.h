//
//  NITDeliveryLocalDataManager.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCreateOrderProtocols.h"

@interface NITCreateOrderLocalDataManager : NSObject <NITCreateOrderLocalDataManagerInputProtocol>

@end
