//
//  NITDeliveryLocalDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCreateOrderLocalDataManager.h"

@implementation NITCreateOrderLocalDataManager

#pragma mark - NITCreateOrderLocalDataManagerInputProtocol
- (NSArray <NITBasketItem *> *)getBasketItems
{
    return [NITBasketItem allBasketItems];
}

- (void)clearBasket
{
    [NITBasketItem deleteAllBasketItems];
}

- (void)saveOrder:(NITOrderItemModel *)model
{
    [NITOrderItem createOrderItemByModel:model];
}

- (void)clearBasketItemsFromOrder:(NITOrderItemModel *)orderModel
{
    [orderModel.items bk_each:^(NITCatalogItemModel *obj) {
        [NITBasketItem deleteBasketItemByID:obj.identifier];
    }];
}

@end
