//
//  NITDeliveryViewController.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCreateOrderProtocols.h"
#import "NITBaseViewController.h"

@interface NITCreateOrderViewController : NITBaseViewController <NITCreateOrderViewProtocol>

@property (nonatomic, strong) id <NITCreateOrderPresenterProtocol> presenter;

@end
