//
//  NITDeliveryPriceTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 25.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCreateOrderPropertyTableViewCell.h"

@interface NITDeliveryPriceTableViewCell : NITCreateOrderPropertyTableViewCell

@end
