//
//  NITDeliveryPriceTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 25.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITDeliveryPriceTableViewCell.h"
#import "NITMobileConstantsModel.h"

@interface NITDeliveryPriceTableViewCell ()

@property (nonatomic) IBOutlet UILabel * priceDescriptionLbl;
@property (nonatomic) IBOutlet UILabel * priceLbl;
@property (nonatomic) IBOutlet UILabel * freeDeliveryLbl;
@property (nonatomic) IBOutlet UILabel * minSumOrderLbl;
@property (nonatomic) IBOutlet UIView * separateLine;

@property (nonatomic) IBOutlet NSLayoutConstraint * priceDescriptiionLblW;

@end

@implementation NITDeliveryPriceTableViewCell

#pragma mark - Custom accessors
- (void)setModel:(NITDeliveryInfoModel *)model delegate:(id)delegate propertyType:(CreateOrderPropertyType)contactType paymentType:(DeliveryPaymentType)paymantType
{
    self.model = model;
    
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.freeDeliveryLbl.hidden = self.model.deliveryType == DeliveryTypeDelivery ? false : true;
    self.minSumOrderLbl.hidden = self.model.deliveryType == DeliveryTypeDelivery ? false : true;
    self.separateLine.hidden = self.model.deliveryType == DeliveryTypeDelivery ? false : true;
    
    
    self.freeDeliveryLbl.text = [NSString stringWithFormat:@"Бесплатная доставка от %@ ₽", [[NITMobileConstantsModel new] free_delivery]];
    self.minSumOrderLbl.text = NSLocalizedString(@"Стоимость доставки: 100 ₽", nil);
    
    [self getBasketItems];
}

#pragma mark - NITBasketPresenterProtocol
- (void)getBasketItems
{
    [self updatePriceByModels:[self catalogItemsModelsFromBasket:[self basketItems]]];
}

- (NSArray <NITBasketItem *> *)basketItems
{
    return [NITBasketItem allBasketItems];
}

- (NSArray <NITCatalogItemModel *> *)catalogItemsModelsFromBasket:(NSArray <NITBasketItem *> *)entities
{
    return [entities bk_map:^id(NITBasketItem * obj) {
        return [[NITCatalogItemModel alloc] initWithBasketModel:obj];
    }];
}

- (void)updatePriceByModels:(NSArray <NITCatalogItemModel *> *)models
{
    NSInteger count = 0;
    double price = 0.0;
    for(NITCatalogItemModel * model in models)
    {
        count += model.count;
        price += [model.price doubleValue] * model.count;
    }
    NSString * goods;
    if(fmodf(count, 10) == 1) goods = NSLocalizedString(@" товар ", nil);
    else if(fmodf(count, 10) > 1 && fmodf(count, 10) < 5) goods = NSLocalizedString(@" товара ", nil);
    else goods = NSLocalizedString(@" товаров ", nil);
    goods = [[[NSString stringWithFormat:@"%lu", (unsigned long)count] stringByAppendingString:goods] stringByAppendingString:NSLocalizedString(@"на сумму:", nil)];
    
    [self updatePrice:[NSString stringWithFormat:@"%.2f ₽", price]];
    [self updatePriceDescription:goods];
}

- (void)updatePriceDescription:(NSString *)goods
{
    self.priceDescriptionLbl.text = goods;
    self.priceDescriptiionLblW.constant = [self.priceDescriptionLbl sizeOfMultiLineLabel].width;
}

- (void)updatePrice:(NSString *)price
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:[NSLocale currentLocale]];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    NSString * priceFormatter = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[price doubleValue]]];
    self.priceLbl.text = [priceFormatter stringByAppendingString:@" ₽"];
}

@end
