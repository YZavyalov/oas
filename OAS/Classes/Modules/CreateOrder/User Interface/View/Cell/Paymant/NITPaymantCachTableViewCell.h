//
//  NITPaymantCachTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 20.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCreateOrderProtocols.h"
#import <UIKit/UIKit.h>
#import "NITCreateOrderPropertyTableViewCell.h"

@protocol NITPaymantCacheCellDelegate <NSObject>
@optional
- (void)didChangePaymantCache:(BOOL)item;
- (void)didChangePaymantCard:(BOOL)item;
@end

@interface NITPaymantCachTableViewCell : NITCreateOrderPropertyTableViewCell

@property (nonatomic) DeliveryPaymentType paymantType;
@property (nonatomic, weak) id <NITPaymantCacheCellDelegate> delegate;

@end
