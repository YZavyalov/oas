//
//  NITPaymantCachTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 20.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPaymantCachTableViewCell.h"

@interface NITPaymantCachTableViewCell()

@property (nonatomic) IBOutlet UILabel * title;
@property (nonatomic) IBOutlet UIImageView * checkmarkImg;

@end

@implementation NITPaymantCachTableViewCell

#pragma mark - Custom accessors
- (void)setModel:(NITDeliveryInfoModel *)model delegate:(id)delegate propertyType:(CreateOrderPropertyType)contactType paymentType:(DeliveryPaymentType)paymantType
{
    self.model = model;
    self.delegate = delegate;
    self.paymantType = paymantType;
    
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)selectCell:(id)sender
{
    if(self.paymantType == DeliveryPaymentTypeCache)
    {
        if ([self.delegate respondsToSelector:@selector(didChangePaymantCache:)])
        {
            [self.delegate didChangePaymantCache:!self.checkmarkImg.hidden];
        }
    }
    else
    {
        if ([self.delegate respondsToSelector:@selector(didChangePaymantCard:)])
        {
            [self.delegate didChangePaymantCard:!self.checkmarkImg.hidden];
        }
    }

}

#pragma mark - Private
- (void)updateData
{
    if(self.paymantType == DeliveryPaymentTypeCache)
    {
        self.title.text = NSLocalizedString(@"Наличный расчет", nil);
        self.checkmarkImg.hidden = !self.model.cash;
    }
    else
    {
        self.title.text = NSLocalizedString(@"Расчет банковской картой", nil);
        self.checkmarkImg.hidden = self.model.cash;
    }
    
    self.contentView.hidden = self.model.deliveryType == DeliveryTypeDelivery ? false : true;
}

@end
