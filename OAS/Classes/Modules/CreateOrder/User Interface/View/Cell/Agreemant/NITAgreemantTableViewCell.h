//
//  NITAgreemantTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 14.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCreateOrderPropertyTableViewCell.h"
#import "NITCreateOrderProtocols.h"

@protocol NITAgreemantCellDelegate <NSObject>
@optional
- (void)didChangeAgreemantUserInfo;
@end

@interface NITAgreemantTableViewCell : NITCreateOrderPropertyTableViewCell

@property (nonatomic, weak) id <NITAgreemantCellDelegate> delegate;

@end
