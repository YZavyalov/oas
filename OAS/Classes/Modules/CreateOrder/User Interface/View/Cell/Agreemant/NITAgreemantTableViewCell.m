//
//  NITAgreemantTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 14.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITAgreemantTableViewCell.h"

@interface NITAgreemantTableViewCell ()

@property (nonatomic) IBOutlet UIImageView* agreeConditionsImg;

@end

@implementation NITAgreemantTableViewCell

#pragma mrak - Custom accessors
- (void)setModel:(NITDeliveryInfoModel *)model delegate:(id)delegate propertyType:(CreateOrderPropertyType)contactType paymentType:(DeliveryPaymentType)paymantType
{
    self.model = model;
    self.delegate = delegate;
    
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)agreemantAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didChangeAgreemantUserInfo)])
    {
        [self.delegate didChangeAgreemantUserInfo];
    }
}

#pragma mark - Private
- (void)updateData
{
    self.separatorInset = UIEdgeInsetsMake(0.f, self.bounds.size.width, 0.f, 0.f);

    self.agreeConditionsImg.image = [UIImage imageNamed:self.model.deliveryAgreement ? @"checkbox_filled" : @"checkbox_empty"];
}

@end
