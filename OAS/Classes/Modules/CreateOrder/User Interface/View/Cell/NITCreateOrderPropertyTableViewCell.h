//
//  NITCreateOrderPropertyTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 10.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCreateOrderProtocols.h"

@interface NITCreateOrderPropertyTableViewCell : UITableViewCell

@property (nonatomic) NITDeliveryInfoModel * model;

- (void)setModel:(NITDeliveryInfoModel *)model delegate:(id)delegate propertyType:(CreateOrderPropertyType)contactType paymentType:(DeliveryPaymentType)paymantType;

@end
