//
//  NITChangeDateTimeCell.h
//  OAS
//
//  Created by Yaroslav on 20.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCreateOrderPropertyTableViewCell.h"

@protocol NITChangeCellDelegate <NSObject>
@optional
- (void)didShowPicker;
- (void)didHidePicker;
@end

@interface NITChangeDateTimeCell : NITCreateOrderPropertyTableViewCell

@property (nonatomic, weak) id <NITChangeCellDelegate> delegate;

@end
