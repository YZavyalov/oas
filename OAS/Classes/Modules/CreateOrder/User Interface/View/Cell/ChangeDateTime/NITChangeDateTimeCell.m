//
//  NITChangeDateTimeCell.m
//  OAS
//
//  Created by Yaroslav on 20.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITChangeDateTimeCell.h"

@interface NITChangeDateTimeCell ()
<
UIPickerViewDelegate,
UIPickerViewDataSource
>

@property (nonatomic) IBOutlet UILabel * changeDateTimeLbl;
@property (nonatomic) IBOutlet UILabel * deliveryLbl;
@property (nonatomic) IBOutlet UILabel * saveLbl;
@property (nonatomic) IBOutlet UIButton * changeDateTimeBtn;
@property (nonatomic) IBOutlet UIButton * saveBtn;
@property (nonatomic) IBOutlet UIView * pickerView;
@property (nonatomic) IBOutlet UIPickerView * picker;

@property (nonatomic) BOOL updateDate;
@property (nonatomic) BOOL updateTime;
@property (nonatomic) NSMutableArray <NSNumber *> * activeRows;

@property (nonatomic) int currentHour;
@property (nonatomic) NSArray <NSString *> * delivery1;
@property (nonatomic) NSArray <NSString *> * delivery2;
@property (nonatomic) NSArray <NSString *> * delivery3;
@end

@implementation NITChangeDateTimeCell

#pragma mark - Life cycle
- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self initUI];
}

#pragma mark - Custom accessors
- (void)setModel:(NITDeliveryInfoModel *)model delegate:(id)delegate propertyType:(CreateOrderPropertyType)contactType paymentType:(DeliveryPaymentType)paymantType
{
    self.delegate = delegate;
}

#pragma mark - IBAction
- (IBAction)changeDateTimeAction:(id)sender
{
    [self updateUI:true];
    
    if ([self.delegate respondsToSelector:@selector(didShowPicker)])
    {
        [self.delegate didShowPicker];
    }
}

- (IBAction)saveAction:(id)sender
{
    [self updateUI:false];

    if([self.picker selectedRowInComponent:1] == 0)
    {
        [[NITDeliveryInfoModel new] setDeliveryTime:[[NITMobileConstantsModel new] delivery_hours_1]];
    }
    if([self.picker selectedRowInComponent:1] == 1)
    {
        [[NITDeliveryInfoModel new] setDeliveryTime:[[NITMobileConstantsModel new] delivery_hours_2]];
    }
    if([self.picker selectedRowInComponent:1] == 2)
    {
        [[NITDeliveryInfoModel new] setDeliveryTime:[[NITMobileConstantsModel new] delivery_hours_3]];
    }
    NSString * times = [self pickupTimeWithtext:[[NITDeliveryInfoModel new] deliveryTime] withDayOffset:[self.picker selectedRowInComponent:0]];
    [[NITDeliveryInfoModel new] setDelivery_times:times];
    
    if ([self.delegate respondsToSelector:@selector(didHidePicker)])
    {
        [self.delegate didHidePicker];
    }
}

#pragma mark - Private
- (void)initUI
{
    self.currentHour = (int)[NSDate date].hour;
    self.delivery1 = [[[NITMobileConstantsModel new] delivery_hours_1] componentsSeparatedByString:@" — "];
    self.delivery2 = [[[NITMobileConstantsModel new] delivery_hours_2] componentsSeparatedByString:@" — "];
    self.delivery3 = [[[NITMobileConstantsModel new] delivery_hours_3] componentsSeparatedByString:@" — "];
    
    self.picker.delegate = self;
    self.picker.dataSource = self;
    [self.picker reloadAllComponents];
    self.updateDate = true;
    [self updateDeliveryTimeWithRow:0 component:0];
}

- (void)updateUI:(BOOL)active
{
    self.changeDateTimeLbl.hidden   = active;
    self.deliveryLbl.hidden         = !active;
    self.saveLbl.hidden             = !active;
    self.pickerView.hidden          = !active;
    [self.changeDateTimeBtn setEnabled:!active];
    [self.saveBtn setEnabled:active];
}

- (void)updateDeliveryTimeWithRow:(NSInteger)row component:(NSInteger)component
{
    self.activeRows = [NSMutableArray new];
    int first = [self.delivery1.firstObject intValue];
    int second = [self.delivery2.firstObject intValue];
    int third = [self.delivery3.firstObject intValue];
    
    if(component == 0)
    {
        //dates
        if(row == 0)
        {
            if(self.currentHour < first)    [self.activeRows addObject:[NSNumber numberWithInteger:0]];
            if(self.currentHour < second)   [self.activeRows addObject:[NSNumber numberWithInteger:1]];
            if(self.currentHour < third)    [self.activeRows addObject:[NSNumber numberWithInteger:2]];
        }
        else
        {
            [self.activeRows addObject:[NSNumber numberWithInteger:0]];
            [self.activeRows addObject:[NSNumber numberWithInteger:1]];
            [self.activeRows addObject:[NSNumber numberWithInteger:2]];
        }
    }
    if(component == 1)
    {
        //times
        NSInteger dateRow = [self.picker selectedRowInComponent:0];
        if(dateRow == 0)
        {
            if(self.currentHour < first)    [self.activeRows addObject:[NSNumber numberWithInteger:0]];
            if(self.currentHour < second)   [self.activeRows addObject:[NSNumber numberWithInteger:1]];
            if(self.currentHour < third)    [self.activeRows addObject:[NSNumber numberWithInteger:2]];
        }
        else
        {
            [self.activeRows addObject:[NSNumber numberWithInteger:0]];
            [self.activeRows addObject:[NSNumber numberWithInteger:1]];
            [self.activeRows addObject:[NSNumber numberWithInteger:2]];
        }
    }
    
    if(self.updateDate)
    {
        self.updateDate = false;
        if(self.activeRows.count > 0)
        {
            [self.picker selectRow:row inComponent:0 animated:true];
            [self.picker selectRow:[self.activeRows.firstObject integerValue] inComponent:1 animated:true];
        }
        else
        {
            [self.picker selectRow:(row + 1) inComponent:0 animated:true];
            [self.picker selectRow:0 inComponent:1 animated:true];
        }
    }
    if(self.updateTime)
    {
        self.updateTime = false;
        
        __block BOOL needChange = true;
        [self.activeRows bk_each:^(NSNumber *obj) {
            if(row == [obj integerValue]) needChange = false;
        }];
        if(needChange)
        {
            [self.picker selectRow:[self.activeRows.firstObject integerValue] inComponent:1 animated:true];
        }
    }
}

#pragma mark - Delivery time
- (NSString *)pickupTimeWithtext:(NSString *)text withDayOffset:(NSInteger)dayOffset
{
    return [NSString stringWithFormat:@"Привезут %@ %@", [NSString stringdateFromNdays:(int)dayOffset], text];
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView
{
    return 2;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return ((ViewWidth(self.picker) - 40) / 2);
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    if(component == 0)  return 5;
    else                return 3;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* tView = (UILabel*)view;
    if (!tView)
    {
        tView = [[UILabel alloc] init];
        [tView setFont:[UIFont systemFontOfSize:IS_IPHONE_5 ? 19 : 23]];
        if(component == 0)
        {
            tView.textAlignment = NSTextAlignmentRight;
        }
        else
        {
            tView.textAlignment = NSTextAlignmentRight;
        }
    }
    if(component == 0)
    {
        if(row == 0) tView.text = [NSString stringWithFormat:@"%@\t", NSLocalizedString(@"Сегодня", nil)];
        else         tView.text = [NSString stringWithFormat:@"%@\t", [NSString stringdateWithDayOfWeekFromNdays:(int)row]];
    }
    else
    {
        if(row == 0)        tView.text = [[NITMobileConstantsModel new] delivery_hours_1];
        else if(row == 1)   tView.text = [[NITMobileConstantsModel new] delivery_hours_2];
        else                tView.text = [[NITMobileConstantsModel new] delivery_hours_3];
    }
    return tView;
}

#pragma mark - UIPickerViewDelegate
- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(component == 0)
    {
        if(row > 3) [self.picker selectRow:3 inComponent:0 animated:true];
        else
        {
            self.updateDate = true;
            [self updateDeliveryTimeWithRow:row component:component];
        }
    }
    if(component == 1)
    {
        self.updateTime = true;
        [self updateDeliveryTimeWithRow:row component:component];
    }
}

@end
