//
//  NITContactInfoTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 24.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCreateOrderProtocols.h"
#import <UIKit/UIKit.h>
#import "NITCreateOrderPropertyTableViewCell.h"

@protocol NITDeliveryContactCellDelegate <NSObject>
@optional
- (void)didChangeOrderProperty:(NSString *)property withType:(CreateOrderPropertyType)type;
@end


@interface NITContactInfoTableViewCell : NITCreateOrderPropertyTableViewCell

@property (nonatomic) CreateOrderPropertyType contactType;
@property (nonatomic, weak) id <NITDeliveryContactCellDelegate> delegate;

@end
