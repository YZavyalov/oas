//
//  NITContactInfoTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 24.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITContactInfoTableViewCell.h"
#import "AKNumericFormatter.h"
#import "UITextField+AKNumericFormatter.h"

static NSString * formatterCodeMask = @"+7 (***) *** ** **";

@interface NITContactInfoTableViewCell()

@property (nonatomic) IBOutlet UITextField * field;

@end

@implementation NITContactInfoTableViewCell

#pragma mark - Life cycle
- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mrak - Custom accessors
- (void)setModel:(NITDeliveryInfoModel *)model delegate:(id)delegate propertyType:(CreateOrderPropertyType)contactType paymentType:(DeliveryPaymentType)paymantType
{
    self.model = model;
    self.delegate = delegate;
    self.contactType = contactType;
    
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    switch (self.contactType)
    {
        case CreateOrderPropertyTypename:
        {
            self.field.placeholder = NSLocalizedString(@"Имя", nil);
            [self.field setKeyboardType:UIKeyboardTypeDefault];
            self.field.autocapitalizationType = UITextAutocapitalizationTypeWords;
            if([self.model.buyer length] > 0) self.field.text = self.model.buyer;
        }
        break;
            
        case CreateOrderPropertyTypePhone:
        {
            self.field.placeholder = NSLocalizedString(@"+7 (   )", nil);
            self.field.numericFormatter = [AKNumericFormatter formatterWithMask:formatterCodeMask
                                                           placeholderCharacter:'*'
                                                                           mode:AKNumericFormatterMixed];
            [self.field setKeyboardType:UIKeyboardTypePhonePad];
            if([self.model.phone length] > 4) self.field.text = self.model.phone;
            else                                      self.field.text = @"+7";
        }
        break;
            
        case CreateOrderPropertyTypeAddress:
        {
            switch (self.model.deliveryType)
            {
                case DeliveryTypeDelivery:
                {
                    self.field.placeholder = NSLocalizedString(@"Адрес доставки", nil);
                    [self.field setKeyboardType:UIKeyboardTypeDefault];
                    self.field.autocapitalizationType = UITextAutocapitalizationTypeSentences;
                    if([self.model.delivery_address length] > 0) self.field.text = self.model.delivery_address;
                    else                                         self.field.text = @"";
                }
                break;
                    
                case DeliveryTypePickup:
                {
                    self.contactType = CreateOrderPropertyTypeComment;
                    self.field.placeholder = NSLocalizedString(@"Комментарий (необязательно)", nil);
                    [self.field setKeyboardType:UIKeyboardTypeDefault];
                    self.field.autocapitalizationType = UITextAutocapitalizationTypeSentences;
                    if([self.model.buyer_comment length] > 0) self.field.text = self.model.buyer_comment;
                }
                break;
            }
        }
        break;
            
        case CreateOrderPropertyTypeComment:
        {
            self.field.placeholder = NSLocalizedString(@"Комментарий или промокод", nil);
            [self.field setKeyboardType:UIKeyboardTypeDefault];
            self.field.autocapitalizationType = UITextAutocapitalizationTypeSentences;
            if([self.model.buyer_comment length] > 0) self.field.text = self.model.buyer_comment;
            else                                        self.field.text = @"";
        }
        break;
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([textField.text isEqualToString:@"+7"] && [string isEqualToString:@""])
    {
        return false;
    }
    else
    {
        return true;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([self.delegate respondsToSelector:@selector(didChangeOrderProperty:withType:)])
    {
        [self.delegate didChangeOrderProperty:textField.text withType:self.contactType];
    }
}


@end
