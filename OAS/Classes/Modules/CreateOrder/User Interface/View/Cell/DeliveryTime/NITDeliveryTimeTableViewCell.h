//
//  NITDeliveryTimeTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 20.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCreateOrderProtocols.h"
#import <UIKit/UIKit.h>
#import "NITCreateOrderPropertyTableViewCell.h"

@protocol NITDeliveryTimeDelegate <NSObject>
@optional
- (void)didUpdateTime;
@end

@interface NITDeliveryTimeTableViewCell : NITCreateOrderPropertyTableViewCell

@property (nonatomic, weak) id <NITDeliveryTimeDelegate> delegate;

@end
