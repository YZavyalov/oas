//
//  NITDeliveryTimeTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 20.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITDeliveryTimeTableViewCell.h"
#import "NITMobileConstantsModel.h"

@interface NITDeliveryTimeTableViewCell()

@property (nonatomic) IBOutlet UILabel * deliveryTimeLbl;

@end

@implementation NITDeliveryTimeTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self addObservers];
}

- (void)dealloc
{
    [self removeObservers];
}

#pragma mark - Custom accessors
- (void)setModel:(NITDeliveryInfoModel *)model delegate:(id)delegate propertyType:(CreateOrderPropertyType)contactType paymentType:(DeliveryPaymentType)paymantType
{
    self.model = model;
    self.delegate = delegate;
    
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    int currentHour = (int)[NSDate date].hour;
    NSArray <NSString *> * delivery1 = [[[NITMobileConstantsModel new] delivery_hours_1] componentsSeparatedByString:@" — "];
    NSArray <NSString *> * delivery2 = [[[NITMobileConstantsModel new] delivery_hours_2] componentsSeparatedByString:@" — "];
    NSArray <NSString *> * delivery3 = [[[NITMobileConstantsModel new] delivery_hours_3] componentsSeparatedByString:@" — "];
    
    if(!(self.model.deliveryTime.length > 0))
    {
        if((currentHour < [delivery1.firstObject intValue]))
        {
            self.model.deliveryTime = [[NITMobileConstantsModel new] delivery_hours_1];
            self.model.pick_up_times = [self pickupTimeWithtext:self.model.deliveryTime];
            self.deliveryTimeLbl.text = [self timeTodayWithFromText:delivery1.firstObject withToText:delivery1.lastObject];
        }
        else if(currentHour < [delivery2.firstObject intValue])
        {
            self.model.deliveryTime = [[NITMobileConstantsModel new] delivery_hours_2];
            self.model.pick_up_times = [self pickupTimeWithtext:self.model.deliveryTime];
            self.deliveryTimeLbl.text = [self timeTodayWithFromText:delivery2.firstObject withToText:delivery2.lastObject];
        }
        else if(currentHour < [delivery3.firstObject intValue])
        {
            self.model.deliveryTime = [[NITMobileConstantsModel new] delivery_hours_3];
            self.model.pick_up_times = [self pickupTimeWithtext:self.model.deliveryTime];
            self.deliveryTimeLbl.text = [self timeTodayWithFromText:delivery3.firstObject withToText:delivery3.lastObject];
        }
        else
        {
            self.model.deliveryTime = [[NITMobileConstantsModel new] delivery_hours_1];
            self.model.pick_up_times = [self pickupTimeTomorrowWithtext:self.model.deliveryTime];
            self.deliveryTimeLbl.text = [self timeTomorrowWithFromText:delivery1.firstObject withToText:delivery1.lastObject];
        }
        self.model.delivery_times = self.model.pick_up_times;
    }
    else
    {
        //TODO: распарсить строку как в симпли
        NSString * time = [[[NITDeliveryInfoModel new] delivery_times] stringByReplacingOccurrencesOfString:@"Привезут " withString:@""];
        NSArray <NSString *> * dateArr = [time componentsSeparatedByString:@" "];
        NSArray <NSString *> * timeArr = [self.model.deliveryTime componentsSeparatedByString:@" — "];
        
        self.deliveryTimeLbl.text = [NSString stringWithFormat:@"%@ %@ с %@ до %@", dateArr[0], dateArr[1], timeArr[0], timeArr[1]];
    }
    
    self.contentView.hidden = self.model.deliveryType == DeliveryTypeDelivery ? false : true;
    
    if ([self.delegate respondsToSelector:@selector(didUpdateTime)])
    {
        [self.delegate didUpdateTime];
    }
}

#pragma mark - Private
- (NSString *)pickupTimeWithtext:(NSString *)text
{
    return [NSString stringWithFormat:@"Привезут %@ %@", [NSString stringDateFromToday], text];
}

- (NSString *)pickupTimeTomorrowWithtext:(NSString *)text
{
    return [NSString stringWithFormat:@"Привезут %@ %@", [NSString stringdateFromNextDay], text];
}

- (NSString *)timeTodayWithFromText:(NSString *)fromText withToText:(NSString *)toText
{
    return [NSString stringWithFormat:@"%@ с %@ до %@", [NSString stringDateFromToday], fromText, toText];
}

- (NSString *)timeTomorrowWithFromText:(NSString *)fromText withToText:(NSString *)toText
{
    return [NSString stringWithFormat:@"%@ с %@ до %@", [NSString stringdateFromNextDay], fromText, toText];
}

- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kUpdateDeliveryTimeNotification object:nil];
}

- (void)removeObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
