//
//  NITPharmacyAddressTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 23.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPharmacyAddressTableViewCell.h"
#import "NITMobileConstantsModel.h"
#import "SLShopsApi.h"

@interface NITPharmacyAddressTableViewCell()

@property (nonatomic) IBOutlet UILabel * addressLbl;
@property (nonatomic) IBOutlet UILabel * availableItem;
@property (nonatomic) IBOutlet UIView * separateLine;
@property (nonatomic) IBOutlet UILabel * extraInfo;

@end

@implementation NITPharmacyAddressTableViewCell

#pragma mark - life cycle
- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self addObservers];
}

#pragma mark - Custom accessors
- (void)setModel:(NITDeliveryInfoModel *)model delegate:(id)delegate propertyType:(CreateOrderPropertyType)contactType paymentType:(DeliveryPaymentType)paymantType
{
    self.model = model;
    self.delegate = delegate;
    
    [self updateData];
}

#pragma mark - IBAction
- (IBAction)selectAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didChangePharmacyAddress)])
    {
        [self.delegate didChangePharmacyAddress];
    }
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kChangeCityNotification object:nil];
}

- (void)updateData
{    
    self.addressLbl.text = [self.model.shop_address length] > 0 ? self.model.shop_address : NSLocalizedString(@"Выберите, где забрать заказ", nil);
    
    self.contentView.hidden = self.model.deliveryType == DeliveryTypeDelivery ? true : false;
    
    NSArray <NITCatalogItemModel *> * basketModelUnderOrder = [[self catalogItemsModelsFromBasket:[NITBasketItem allBasketItems]] bk_select:^BOOL(NITCatalogItemModel *obj) {
        return [obj underOrder] == true;
    }];
    
    if(basketModelUnderOrder.count > 0)
    {
        self.availableItem.text = NSLocalizedString(@"Товар под заказ вы можете забрать через 3 дня", nil);
    }
    else
    {
        if(self.model.shop_id.length > 0)
        {
            self.availableItem.hidden = false;
            BOOL success = self.model.isHub;
            NITMobileConstantsModel * time = [NITMobileConstantsModel new];
            if(success)
            {
                self.availableItem.text = time.pick_up_now;
            }
            else
            {
                NSInteger currentHour = [NSDate date].hour;
                NSInteger orderTime = [time.time_order_limit componentsSeparatedByString:@":"].firstObject.integerValue;
                if((int)currentHour < (int)orderTime)
                {
                    self.availableItem.text = time.pick_up_today;
                }
                else
                {
                    self.availableItem.text = time.pick_up_tomorrow;
                }
            }
        }
        else
        {
            self.availableItem.hidden = true;
        }
        
        self.separateLine.hidden = false;
        self.extraInfo.hidden = false;
    }
    
    if(self.model.shop_id.length > 0)
    {
        self.availableItem.hidden = false;
        BOOL success = self.model.isHub;
        NITMobileConstantsModel * time = [NITMobileConstantsModel new];
        if(success)
        {
            self.model.pick_up_times = [NSLocalizedString(@"Привезут ", nil) stringByAppendingString:[[[NSString stringDateFromToday] stringByAppendingString:NSLocalizedString(@" после ", nil)] stringByAppendingString:time.time_order_limit]];
        }
        else
        {
            NSInteger currentHour = [NSDate date].hour;
            NSInteger orderTime = [time.time_order_limit componentsSeparatedByString:@":"].firstObject.integerValue;
            if((int)currentHour < (int)orderTime)
            {
                self.model.pick_up_times = [NSLocalizedString(@"Привезут ", nil) stringByAppendingString:[[NSString stringDateFromToday] stringByAppendingString:NSLocalizedString(@" после 18:00", nil)]];
            }
            else
            {
                self.model.pick_up_times = [NSLocalizedString(@"Привезут ", nil) stringByAppendingString:[[NSString stringdateFromNextDay] stringByAppendingString:NSLocalizedString(@" после 18:00", nil)]];
            }
        }
    }
}

- (NSArray <NITCatalogItemModel *> *)catalogItemsModelsFromBasket:(NSArray <NITBasketItem *> *)entities
{
    return [entities bk_map:^id(NITBasketItem * obj) {
        return [[NITCatalogItemModel alloc] initWithBasketModel:obj];
    }];
}

@end
