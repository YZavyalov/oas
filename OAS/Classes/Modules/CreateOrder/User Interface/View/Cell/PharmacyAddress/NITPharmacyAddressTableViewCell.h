//
//  NITPharmacyAddressTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 23.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCreateOrderProtocols.h"
#import <UIKit/UIKit.h>
#import "NITCreateOrderPropertyTableViewCell.h"

@protocol NITPharmacyAddressCellDelegate <NSObject>
@optional
- (void)didChangePharmacyAddress;
@end


@interface NITPharmacyAddressTableViewCell : NITCreateOrderPropertyTableViewCell

@property (nonatomic, weak) id <NITPharmacyAddressCellDelegate> delegate;

@end
