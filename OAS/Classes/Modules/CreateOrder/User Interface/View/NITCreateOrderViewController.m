//
//  NITDeliveryViewController.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCreateOrderViewController.h"
#import "NITBaseHeaderTableViewCell.h"
#import "NITDeliveryTimeTableViewCell.h"
#import "NITContactInfoTableViewCell.h"
#import "NITPharmacyAddressTableViewCell.h"
#import "NITPaymantCachTableViewCell.h"
#import "NITCreateOrderPropertyTableViewCell.h"
#import "NITPharmacyProtocols.h"
#import "NITAgreemantTableViewCell.h"
#import "NITChangeDateTimeCell.h"
#import "NITDeliveryPriceTableViewCell.h"

@interface NITCreateOrderViewController()
<
NITPharmacyDelegate,
UITableViewDelegate,
UITableViewDataSource,
NITDeliveryContactCellDelegate,
NITPaymantCacheCellDelegate,
NITPharmacyAddressCellDelegate,
NITAgreemantCellDelegate,
NITChangeCellDelegate,
NITDeliveryTimeDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UIButton * checkoutBtn;
@property (nonatomic) IBOutlet NSLayoutConstraint * checkoutBtnBottom;

@property (nonatomic) NSArray <NITDeliveryInfoSectionModel *> * sections;
@property (nonatomic) NITDeliveryInfoModel * model;

@end

@implementation NITCreateOrderViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self.presenter initData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:true];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.tabBarController.tabBar setHidden:false];
}

#pragma mark - IBAction
- (IBAction)callAction:(id)sender
{
    [self.presenter callAction];
}

- (IBAction)checkoutAction:(id)sender
{
    [self.presenter sendOrder:self.model];
}

#pragma mark - Private
- (void)initUI
{
    [self setKeyboardActiv:true];
    
    // Table view
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView registerNibArray:@[_s(NITDeliveryPriceTableViewCell),
                                       _s(NITBaseHeaderTableViewCell),
                                       _s(NITDeliveryTimeTableViewCell),
                                       _s(NITChangeDateTimeCell),
                                       _s(NITContactInfoTableViewCell),
                                       _s(NITContactInfoTableViewCell),
                                       _s(NITPharmacyAddressTableViewCell),
                                       _s(NITPaymantCachTableViewCell),
                                       _s(NITAgreemantTableViewCell)]];
}

- (void)reloadCheckoutBtn
{
    [self.checkoutBtn setBackgroundColor: self.model.isCorrect ? RGB(58, 160, 144) : RGB(225, 240, 238)];
}

#pragma mark - NITCreateOrderPresenterProtocol
- (void)reloadTitle:(NSString *)title
{
    self.title = title;
}

- (void)reloadDataByModel:(NITDeliveryInfoModel *)model sections:(NSArray<NITDeliveryInfoSectionModel *> *)sections
{
    self.model = model;
    self.sections = sections;
    
    [self reloadCheckoutBtn];
    //[self.tableView reloadData];
}

- (void)reloadDeliveryTime:(NSString *)deliveryTime
{
    self.model.deliveryTime = deliveryTime;
    [self reloadCheckoutBtn];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NITDeliveryInfoSectionModel * sectionModel = self.sections[section];
    return sectionModel.headerHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NITDeliveryInfoSectionModel * sectionModel = self.sections[section];
    NITBaseHeaderTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITBaseHeaderTableViewCell)];
    cell.titleText = sectionModel.headerText;
    
    //return cell;
    
    //Скрываются хедеры при BeginUpdates, применяем костыль
    CGRect frame = cell.frame;
    frame.size.width = ViewWidth(WINDOW);
    cell.frame = frame;
    
    UIView * view = [[UIView alloc] init];
    [view addSubview:cell];
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NITDeliveryInfoSectionModel * sectionModel = self.sections[section];
    return sectionModel.numberOfRows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITDeliveryInfoSectionModel * sectionModel = self.sections[indexPath.section];
    return sectionModel.rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITDeliveryInfoSectionModel * sectionModel = self.sections[indexPath.section];
    NITCreateOrderPropertyTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:[sectionModel cellID] forIndexPath:indexPath];
    [cell setModel:self.model delegate:self propertyType:indexPath.row paymentType:indexPath.row];
    
    return cell;
}

#pragma mark - NITDeliveryTimeCellDelegate
- (void)didUpdateTime
{
    [self reloadCheckoutBtn];
}

#pragma mark - NITChangedelegate
- (void)didShowPicker
{
    [self.tableView beginUpdates];
    NITDeliveryInfoSectionModel * sectionModel = self.sections[CreateOrderSectionTypeChangeButton];
    sectionModel.pickerheight = 190;
    [self.tableView endUpdates];
}

- (void)didHidePicker
{
    NITDeliveryInfoSectionModel * sectionModel = self.sections[CreateOrderSectionTypeChangeButton];
    sectionModel.pickerheight = 44;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateDeliveryTimeNotification object:nil];
}

#pragma mark - NITDeliveryNameCellDelegate
- (void)didChangeOrderProperty:(NSString *)property withType:(CreateOrderPropertyType)type
{
    [self.presenter updateOrderProperty:property withType:type];
}

#pragma mark - NITPaymantCacheDelegate
- (void)didChangePaymantCache:(BOOL)item
{
    self.model.cash = true;
    [self.tableView reloadData];
}

- (void)didChangePaymantCard:(BOOL)item
{
    self.model.cash = false;
    [self.tableView reloadData];
}

#pragma mark - NITAgreemantDelegate
- (void)didChangeAgreemantUserInfo
{
    self.model.deliveryAgreement = !self.model.deliveryAgreement;
    [self reloadCheckoutBtn];
    [self.tableView reloadData];
}

#pragma mark - NITPharmacyAddressCellDelegate
- (void)didChangePharmacyAddress
{
    [self.presenter changePharmacy];
}

#pragma mark - NITPharmacyDelegate
- (void)didSelectAddress:(NITShopItemModel *)address
{
    [self.presenter updateOrderShop:address];
    [self.tableView reloadData];
}

@end
