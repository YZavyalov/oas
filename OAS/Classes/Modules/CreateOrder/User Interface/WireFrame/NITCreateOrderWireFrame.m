//
//  NITDeliveryWireFrame.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCreateOrderWireFrame.h"
#import "NITCatalogWireFrame.h"
#import "NITPharmacyWireFrame.h"

@implementation NITCreateOrderWireFrame

#pragma mark - Private
+ (id)createNITDeliveryModule
{
    // Generating module components
    id <NITCreateOrderPresenterProtocol, NITCreateOrderInteractorOutputProtocol> presenter = [NITCreateOrderPresenter new];
    id <NITCreateOrderInteractorInputProtocol> interactor = [NITCreateOrderInteractor new];
    id <NITCreateOrderAPIDataManagerInputProtocol> APIDataManager = [NITCreateOrderAPIDataManager new];
    id <NITCreateOrderLocalDataManagerInputProtocol> localDataManager = [NITCreateOrderLocalDataManager new];
    id <NITCreateOrderWireFrameProtocol> wireFrame= [NITCreateOrderWireFrame new];
    id <NITCreateOrderViewProtocol> view = [(NITCreateOrderWireFrame *)wireFrame createViewControllerWithKey:_s(NITCreateOrderViewController) storyboardType:StoryboardTypeBasket];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITCreateOrderWireFrameProtocol
+ (void)presentNITDeliveryInfoModuleFrom:(UIViewController *)fromViewController withType:(DeliveryType)type
{
    NITCreateOrderViewController * vc = [NITCreateOrderWireFrame createNITDeliveryModule];
    vc.presenter.deliveryType = type;
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)showCatalogFromViewController:(UIViewController *)fromViewController
{
    [NITCatalogWireFrame presentNITCatalogModuleFrom:fromViewController];
}

- (void)showPharmacyFromViewController:(UIViewController *)fromViewController withDelegate:(id)delegate
{
    [NITPharmacyWireFrame selectAddressPickupModuleFrom:fromViewController withDelegate:delegate];
}

- (void)presentCatalogFromViewController:(UIViewController *)fromViewController
{
    [fromViewController.tabBarController setSelectedIndex:0];
}

- (void)popFromViewController:(UIViewController *)fromViewController
{
    [fromViewController.navigationController popToRootViewControllerAnimated:false];
    [fromViewController dismissViewControllerAnimated:true completion:^{}];
}

- (void)presentAlertFrom:(UIViewController *)fromViewController withAlert:(UIAlertController *)alert
{
    [fromViewController presentViewController:alert animated:true completion:nil];
}

@end
