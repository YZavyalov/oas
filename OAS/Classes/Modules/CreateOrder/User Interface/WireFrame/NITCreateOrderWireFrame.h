//
//  NITDeliveryWireFrame.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCreateOrderProtocols.h"
#import "NITCreateOrderViewController.h"
#import "NITCreateOrderLocalDataManager.h"
#import "NITCreateOrderAPIDataManager.h"
#import "NITCreateOrderInteractor.h"
#import "NITCreateOrderPresenter.h"
#import "NITCreateOrderWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITCreateOrderWireFrame : NITRootWireframe <NITCreateOrderWireFrameProtocol>

@end
