//
//  NITDeliveryPresenter.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCreateOrderProtocols.h"

@class NITCreateOrderWireFrame;

@interface NITCreateOrderPresenter : NSObject <NITCreateOrderPresenterProtocol, NITCreateOrderInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCreateOrderViewProtocol> view;
@property (nonatomic, strong) id <NITCreateOrderInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCreateOrderWireFrameProtocol> wireFrame;
@property (nonatomic) DeliveryType deliveryType;

@end
