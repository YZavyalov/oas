//
//  NITDeliveryPresenter.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCreateOrderPresenter.h"
#import "NITCreateOrderWireframe.h"
#import "NITMobileConstantsModel.h"

@interface NITCreateOrderPresenter ()

@property (nonatomic) NSArray <NITDeliveryInfoSectionModel *> * sections;
@property (nonatomic) NITDeliveryInfoModel * model;

@end

@implementation NITCreateOrderPresenter

#pragma mark - Customs accessors
- (NSArray<NITDeliveryInfoSectionModel *> *)sections
{
    if (_sections.count != 5)
    {
        NITDeliveryInfoSectionModel * price = [[NITDeliveryInfoSectionModel alloc] initWithType:CreateOrderSectionTypePrice andDeliveryType:self.deliveryType];
        NITDeliveryInfoSectionModel * time = [[NITDeliveryInfoSectionModel alloc] initWithType:CreateOrderSectionTypeTime andDeliveryType:self.deliveryType];
        NITDeliveryInfoSectionModel * change = [[NITDeliveryInfoSectionModel alloc] initWithType:CreateOrderSectionTypeChangeButton andDeliveryType:self.deliveryType];
        NITDeliveryInfoSectionModel * contact = [[NITDeliveryInfoSectionModel alloc] initWithType:CreateOrderSectionTypeContact andDeliveryType:self.deliveryType];
        NITDeliveryInfoSectionModel * address = [[NITDeliveryInfoSectionModel alloc] initWithType:CreateOrderSectionTypeAddress andDeliveryType:self.deliveryType];
        NITDeliveryInfoSectionModel * paymant = [[NITDeliveryInfoSectionModel alloc] initWithType:CreateOrderSectionTypePaymant andDeliveryType:self.deliveryType];
        NITDeliveryInfoSectionModel * agreemant = [[NITDeliveryInfoSectionModel alloc] initWithType:CreateOrderSectionTypeAgreemant andDeliveryType:self.deliveryType];
        _sections = @[price, time, change, contact, address, paymant, agreemant];
    }
    
    return _sections;
}

#pragma mark - NITCreateOrderPresenterProtocol
- (void)initData
{
    NSString * title = self.deliveryType == DeliveryTypeDelivery ? NSLocalizedString(@"Доставка", nil) : NSLocalizedString(@"Самовывоз", nil);
    [self.view reloadTitle:title];
    
    [[NITDeliveryInfoModel new] setDeliveryTime:nil];
    [[NITDeliveryInfoModel new] setPick_up_times:nil];
    
    self.model = [NITDeliveryInfoModel new];
    self.model.deliveryType = self.deliveryType;
    self.model.deliveryAgreement = true;
    
    [self updateData];
}

- (void)updateOrderProperty:(NSString *)property withType:(CreateOrderPropertyType)type
{
    switch (type) {
        case CreateOrderPropertyTypename:       self.model.buyer = property;            break;
        case CreateOrderPropertyTypePhone:      self.model.phone = property;            break;
        case CreateOrderPropertyTypeAddress:    self.model.delivery_address = property; break;
        case CreateOrderPropertyTypeComment:    self.model.buyer_comment = property;    break;
    }
    
    [self.view reloadDataByModel:self.model sections:self.sections];
}

- (void)updateOrderShop:(NITShopItemModel *)model
{
    self.model.shop_address = model.address;
    self.model.shop_id      = model.identifier;
    self.model.isHub        = model.is_hub.length > 0 ? true : false;
    
    [self.view reloadDataByModel:self.model sections:self.sections];
}

- (void)changePharmacy
{
    [self.wireFrame showPharmacyFromViewController:self.view withDelegate:self.view];
}

- (void)changeDeliveryTime
{
    weaken(self);
    
    NITActionButton * cancel = [[NITActionButton alloc] initWithTitle:NSLocalizedString(@"Отмена", nil) actionBlock:^{
    }];
    
    NITActionButton * action1 = [[NITActionButton alloc] initWithTitle:[[NITMobileConstantsModel new] delivery_hours_1] actionBlock:^{
        [weakSelf.view reloadDeliveryTime:[[NITMobileConstantsModel new] delivery_hours_1]];
    }];
    
    NITActionButton * action2 = [[NITActionButton alloc] initWithTitle:[[NITMobileConstantsModel new] delivery_hours_2] actionBlock:^{
        [weakSelf.view reloadDeliveryTime:[[NITMobileConstantsModel new] delivery_hours_2]];
    }];
    
    NITActionButton * action3 = [[NITActionButton alloc] initWithTitle:[[NITMobileConstantsModel new] delivery_hours_3] actionBlock:^{
        [weakSelf.view reloadDeliveryTime:[[NITMobileConstantsModel new] delivery_hours_3]];
    }];
    
    [HELPER showActionSheetFromView:self.view withTitle:NSLocalizedString(@"Часы доставки", nil) actionButtons:@[action1, action2, action3] cancelButton:cancel];
}

- (void)callAction
{
    NSString * phone = [[NITMobileConstantsModel new] contact_phone];
    [HELPER callToPhoneNumber:phone];
}

- (void)sendOrder:(NITDeliveryInfoModel *)model
{
    if (model.isCorrect)
    {
        [self.interactor sendOrder:model];
    }
}

#pragma mark - NITCreateOrderInteractorOutputProtocol
- (void)updateOrderBasketItems:(NSArray<NITCatalogItemModel *> *)models
{
    [self.view reloadDataByModel:self.model sections:self.sections];
}

- (void)orderCreatedSuccess
{
    [self.interactor clearBasket];
    [self.wireFrame presentCatalogFromViewController:self.view];
    [self.wireFrame popFromViewController:self.view];
    
    NSString * notificationName = self.model.deliveryType == DeliveryTypeDelivery ? kShowSuccessOrderAlert : kShowSuccessPickupOrderAlert;
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil];
}

- (void)orderCreateError
{
    [ERROR_MESSAGE errorMessage:notification_error_with_post_order];
}

#pragma mark - Private
- (void)updateData
{
    [self.interactor getBasketItems];
}

- (void)showAlert
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Не удалось оформить заказ, неверные данные", nil)
                                                                    message:@""
                                                             preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = RGB(58, 160, 144);
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ок", nil)
                                                      style:UIAlertActionStyleCancel
                                                    handler:^(UIAlertAction * action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                    }];
    
    [alert addAction:cancel];
    
    [self.wireFrame presentAlertFrom:self.view withAlert:alert];
}

@end
