//
//  NITMapInteractor.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITMapProtocols.h"

@interface NITMapInteractor : NSObject <NITMapInteractorInputProtocol>

@property (nonatomic, weak) id <NITMapInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITMapAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITMapLocalDataManagerInputProtocol> localDataManager;

@end
