//
//  NITMapAPIDataManager.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITMapProtocols.h"

@interface NITMapAPIDataManager : NSObject <NITMapAPIDataManagerInputProtocol>

@end
