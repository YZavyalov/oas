//
//  NITMapViewController.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITMapViewController.h"
#import "NITCardDescriptionCollectionViewCell.h"

@interface NITMapViewController()
<
UICollectionViewDelegate,
UICollectionViewDataSource,
UIScrollViewDelegate
>

@property (nonatomic) IBOutlet UICollectionView * collectionView;
@property (nonatomic) IBOutlet UIPageControl * pageControl;
@property (nonatomic) IBOutlet UIButton * tieCardBtn;

@property (nonatomic) NSArray <NSDictionary *> * discountCardGuide;

@end

@implementation NITMapViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
}

#pragma mark - IBAction
- (IBAction)tieCardAction:(id)sender
{
    [self.presenter tieCard];
}

- (IBAction)pageControlAction:(id)sender
{
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:[self.pageControl currentPage] inSection:0]
                                atScrollPosition:UICollectionViewScrollPositionNone
                                        animated:true];
}

#pragma mark - Private
- (void)initUI
{
    self.discountCardGuide = [self.presenter getGuideInfo];
    
    //Collection view
    [self.collectionView registerNib:[UINib nibWithNibName:_s(NITCardDescriptionCollectionViewCell) bundle:nil] forCellWithReuseIdentifier:_s(NITCardDescriptionCollectionViewCell)];
    UICollectionViewFlowLayout * flowLayout = (id)self.collectionView.collectionViewLayout;
    flowLayout.itemSize = CGSizeMake(ViewWidth(self.view), ViewHeight(self.collectionView));
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.discountCardGuide count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(ViewWidth(self.view) - 10, ViewHeight(self.collectionView));
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NITCardDescriptionCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:_s(NITCardDescriptionCollectionViewCell) forIndexPath:indexPath];
    cell.model = self.discountCardGuide[indexPath.row];
    
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int index = (int)(scrollView.contentOffset.x / (ViewWidth(self.view) - 10));
    [self.pageControl setCurrentPage:index];
}

@end
