//
//  NITCardDescriptionCollectionViewCell.h
//  OAS
//
//  Created by Yaroslav on 17.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITCardDescriptionCollectionViewCell : UICollectionViewCell

@property (nonatomic) NSDictionary * model;

@end
