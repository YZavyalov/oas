//
//  NITCardDescriptionCollectionViewCell.m
//  OAS
//
//  Created by Yaroslav on 17.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCardDescriptionCollectionViewCell.h"

@interface NITCardDescriptionCollectionViewCell()

@property (nonatomic) IBOutlet UIImageView * image;
@property (nonatomic) IBOutlet UILabel * titleLbl;
@property (nonatomic) IBOutlet UILabel * subtitleLbl;

@end

@implementation NITCardDescriptionCollectionViewCell

#pragma mark - Custom accessors
- (void)setModel:(NSDictionary *)model
{
    _model = model;
    
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.image.image = [UIImage imageNamed:[self.model objectForKey:@"ImagePath"]];
    self.titleLbl.text = [self.model objectForKey:@"Title"];
    self.subtitleLbl.text = [self.model objectForKey:@"Subtitle"];
}

@end
