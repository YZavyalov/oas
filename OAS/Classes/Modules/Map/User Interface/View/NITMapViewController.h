//
//  NITMapViewController.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITMapProtocols.h"

@interface NITMapViewController : NITBaseViewController <NITMapViewProtocol>

@property (nonatomic, strong) id <NITMapPresenterProtocol> presenter;

@end
