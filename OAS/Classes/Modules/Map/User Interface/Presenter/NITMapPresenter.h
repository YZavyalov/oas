//
//  NITMapPresenter.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITMapProtocols.h"

@class NITMapWireFrame;

@interface NITMapPresenter : NITRootPresenter <NITMapPresenterProtocol, NITMapInteractorOutputProtocol>

@property (nonatomic, weak) id <NITMapViewProtocol> view;
@property (nonatomic, strong) id <NITMapInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITMapWireFrameProtocol> wireFrame;

@end
