//
//  NITMapPresenter.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITMapPresenter.h"
#import "NITMapWireframe.h"

@implementation NITMapPresenter

#pragma mark - NITMapPresenterProtocol
- (NSArray <NSDictionary *> *)getGuideInfo
{
    NSDictionary * guide1 = [[NSDictionary alloc] initWithObjectsAndKeys:@"illustration_gid_1", @"ImagePath",
                             NSLocalizedString(@"Всегда под рукой", nil), @"Title",
                             NSLocalizedString(@"Пользуйтесь картой прямо из приложения", nil), @"Subtitle", nil];
    
    NSDictionary * guide2 = [[NSDictionary alloc] initWithObjectsAndKeys:@"guide2_picture", @"ImagePath",
                             NSLocalizedString(@"Бонусная система", nil), @"Title",
                             NSLocalizedString(@"Отслеживайте свои бонусы в личном кабинете", nil), @"Subtitle", nil];
    
    NSDictionary * guide3 = [[NSDictionary alloc] initWithObjectsAndKeys:@"guide3_picture", @"ImagePath",
                             NSLocalizedString(@"Копите скидку", nil), @"Title",
                             NSLocalizedString(@"Накапливайте скидку для более выгодных покупок", nil), @"Subtitle", nil];
    
    NSArray <NSDictionary *> * discountCardGuide = [[NSArray alloc] initWithObjects:guide1, guide2, guide3, nil];
    
    return discountCardGuide;
}

- (void)tieCard
{
    [self.wireFrame showCardRegistrationFromView:self.view];
}

@end
