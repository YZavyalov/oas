//
//  NITMapWireFrame.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITMapWireFrame.h"
#import "NITRegistrationCardWireFrame.h"
#import "NITDiscountCardWireFrame.h"

@implementation NITMapWireFrame

#pragma mark - NITMapWireFrameProtocol
+ (id)createNITMapModule
{    
    // Generating module components
    id <NITMapPresenterProtocol, NITMapInteractorOutputProtocol> presenter = [NITMapPresenter new];
    id <NITMapInteractorInputProtocol> interactor = [NITMapInteractor new];
    id <NITMapAPIDataManagerInputProtocol> APIDataManager = [NITMapAPIDataManager new];
    id <NITMapLocalDataManagerInputProtocol> localDataManager = [NITMapLocalDataManager new];
    id <NITMapWireFrameProtocol> wireFrame= [NITMapWireFrame new];
    id <NITMapViewProtocol> view = [(NITMapWireFrame *)wireFrame createViewControllerWithKey:_s(NITMapViewController) storyboardType:StoryboardTypeMap];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITMapModuleFrom:(UIViewController *)fromViewController
{
    NITMapViewController * vc = [NITMapWireFrame createNITMapModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)showCardRegistrationFromView:(UIViewController *)fromViewController
{
    [NITRegistrationCardWireFrame presentNITRegistrationCardModuleFrom:fromViewController];
}

@end
