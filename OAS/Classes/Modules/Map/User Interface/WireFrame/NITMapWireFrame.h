//
//  NITMapWireFrame.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITMapProtocols.h"
#import "NITMapViewController.h"
#import "NITMapLocalDataManager.h"
#import "NITMapAPIDataManager.h"
#import "NITMapInteractor.h"
#import "NITMapPresenter.h"
#import "NITMapWireframe.h"
#import <UIKit/UIKit.h>

@interface NITMapWireFrame : NITRootWireframe <NITMapWireFrameProtocol>

@end
