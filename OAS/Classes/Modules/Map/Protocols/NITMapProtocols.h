//
//  NITMapProtocols.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SLCollection.h"

@protocol NITMapInteractorOutputProtocol;
@protocol NITMapInteractorInputProtocol;
@protocol NITMapViewProtocol;
@protocol NITMapPresenterProtocol;
@protocol NITMapLocalDataManagerInputProtocol;
@protocol NITMapAPIDataManagerInputProtocol;

@class NITMapWireFrame;

@protocol NITMapViewProtocol
@required
@property (nonatomic, strong) id <NITMapPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

@protocol NITMapWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (id)createNITMapModule;
+ (void)presentNITMapModuleFrom:(id)fromView;
- (void)showCardRegistrationFromView:(id)fromViewController;
@end

@protocol NITMapPresenterProtocol
@required
@property (nonatomic, weak) id <NITMapViewProtocol> view;
@property (nonatomic, strong) id <NITMapInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITMapWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)tieCard;
- (NSArray <NSDictionary *> *)getGuideInfo;
@end

@protocol NITMapInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol NITMapInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITMapInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITMapAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITMapLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end


@protocol NITMapDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITMapAPIDataManagerInputProtocol <NITMapDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITMapLocalDataManagerInputProtocol <NITMapDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
