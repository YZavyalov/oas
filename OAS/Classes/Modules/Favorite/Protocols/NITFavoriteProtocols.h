//
//  NITFavoriteProtocols.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITFavoriteItem.h"
#import "NITBasketItem.h"

@protocol NITFavoriteInteractorOutputProtocol;
@protocol NITFavoriteInteractorInputProtocol;
@protocol NITFavoriteViewProtocol;
@protocol NITFavoritePresenterProtocol;
@protocol NITFavoriteLocalDataManagerInputProtocol;
@protocol NITFavoriteAPIDataManagerInputProtocol;

@class NITFavoriteWireFrame;

@protocol NITFavoriteViewProtocol
@required
@property (nonatomic, strong) id <NITFavoritePresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)updateEmtySubtitle:(NSMutableAttributedString *)title;
- (void)reloadDataByModels:(NSArray <NITCatalogItemModel *> *)models;
- (void)updateEmptyViewWithHide:(BOOL)hide;
- (void)enableScroll;
@end

@protocol NITFavoriteWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (id)createNITFavoriteModule;
+ (void)presentNITFavoriteModuleFrom:(id)fromView;
- (void)showProductDetailsFromViewController:(id)fromViewController withModel:(NITCatalogItemModel *)model;
- (void)showCatalogFromViewController:(id)fromViewController;
- (void)showImageWithView:(id)fromViewController withImage:(UIImage *)model;
- (void)presentAlertFrom:(id)fromViewController withAlert:(id)alert;
@end

@protocol NITFavoritePresenterProtocol
@required
@property (nonatomic, weak) id <NITFavoriteViewProtocol> view;
@property (nonatomic, strong) id <NITFavoriteInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITFavoriteWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)initData;
- (void)addItemInBasket:(NITCatalogItemModel *)model;
- (void)reduseItemInBasket:(NITCatalogItemModel *)model;
- (void)deleteItem:(NITCatalogItemModel *)model;
- (void)clearFavorite:(NSArray <NITCatalogItemModel *> *)items;
- (void)showProductDetailsWithModel:(NITCatalogItemModel *)model;
- (void)presentCatalog;
- (void)showImage:(UIImage *)model;
@end

@protocol NITFavoriteInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateDataWithFavoriteModels:(NSArray <NITCatalogItemModel *> *)favoriteModels;
@end

@protocol NITFavoriteInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITFavoriteInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITFavoriteAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITFavoriteLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getFavoriteItems;
- (void)deleteItemByID:(NSString *)identifier;
- (void)deleteAllItems;
- (void)addItemInBasket:(NITCatalogItemModel *)model;
- (void)reduseItemInBasket:(NITCatalogItemModel *)model;
@end


@protocol NITFavoriteDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITFavoriteAPIDataManagerInputProtocol <NITFavoriteDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol NITFavoriteLocalDataManagerInputProtocol <NITFavoriteDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
- (NSArray <NITFavoriteItem *> *)getFavoriteItems;
- (void)deleteItemByID:(NSString *)identifier;
- (void)deleteAllItems;
- (void)addItemInBasket:(NITCatalogItemModel *)model;
- (void)reduseItemInBasket:(NITCatalogItemModel *)model;
@end
