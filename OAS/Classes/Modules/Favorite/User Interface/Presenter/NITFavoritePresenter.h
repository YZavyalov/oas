//
//  NITFavoritePresenter.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFavoriteProtocols.h"

@class NITFavoriteWireFrame;

@interface NITFavoritePresenter : NITRootPresenter <NITFavoritePresenterProtocol, NITFavoriteInteractorOutputProtocol>

@property (nonatomic, weak) id <NITFavoriteViewProtocol> view;
@property (nonatomic, strong) id <NITFavoriteInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITFavoriteWireFrameProtocol> wireFrame;

@end
