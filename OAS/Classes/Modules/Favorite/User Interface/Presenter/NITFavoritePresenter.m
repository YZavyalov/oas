//
//  NITFavoritePresenter.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoritePresenter.h"
#import "NITFavoriteWireframe.h"

@implementation NITFavoritePresenter

#pragma mark - NITFavoritePresenterProtocol
- (void)initData
{
    [self updateEmtySubtitle];
    [self addObservers];
    [self updateData];
}

- (void)clearFavorite:(NSArray <NITCatalogItemModel *> *)items
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Очистить избранное?", nil)
                                                                    message:nil
                                                             preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = RGB(58, 160, 144);
    
    UIAlertAction * done = [UIAlertAction actionWithTitle:NSLocalizedString(@"Очистить", nil)
                                                    style:UIAlertActionStyleCancel
                                                  handler:^(UIAlertAction * action) {
                                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                                      [self.interactor deleteAllItems];
                                                  }];
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Отмена", nil)
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                    }];
    [alert addAction:cancel];
    [alert addAction:done];
    
    
    [self.wireFrame presentAlertFrom:self.view withAlert:alert];
}

- (void)showProductDetailsWithModel:(NITCatalogItemModel *)model
{
    [self.wireFrame showProductDetailsFromViewController:self.view withModel:model];
}

- (void)presentCatalog
{
    [self.wireFrame showCatalogFromViewController:self.view];
}

- (void)showImage:(UIImage *)model
{
    [self.wireFrame showImageWithView:self.view withImage:model];
}

#pragma mark - NITFavoriteInteractorOutputProtocol
- (void)addItemInBasket:(NITCatalogItemModel *)model
{
    [self.interactor addItemInBasket:model];
}

- (void)reduseItemInBasket:(NITCatalogItemModel *)model
{
    [self.interactor reduseItemInBasket:model];
}

- (void)deleteItem:(NITCatalogItemModel *)model
{
    [self.interactor deleteItemByID:model.identifier];
}

#pragma mark - NITFavoriteInteractorOutputProtocol
- (void)updateDataWithFavoriteModels:(NSArray <NITCatalogItemModel *> *)favoriteModels
{
    if(favoriteModels.count > 0)
    {
        [self.view reloadDataByModels:favoriteModels];
        [self.view updateEmptyViewWithHide:false];
    }
    else
    {
        [self.view updateEmptyViewWithHide:true];
    }
}

#pragma mark - Private
- (void)updateEmtySubtitle
{
    NSTextAttachment * attachmentIcon = [[NSTextAttachment alloc] init];
    attachmentIcon.image = [UIImage imageNamed:@"save copy"];
    attachmentIcon.bounds = CGRectMake(0, -1, 15, 15);
    NSAttributedString * attachmentString = [NSAttributedString attributedStringWithAttachment:attachmentIcon];
    NSMutableAttributedString * subtitleText = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"Чтобы добавить товар, \n нажмите ", nil)];
    NSMutableAttributedString * subtitleTextNext = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@" в карточке товара", nil)];
    [subtitleText appendAttributedString:attachmentString];
    [subtitleText appendAttributedString:subtitleTextNext];
    [self.view updateEmtySubtitle:subtitleText];
}

- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kChangeBasketNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kSaveFavoriteItemNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kSaveBasketItemNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enableScroll) name:kEnableScrollFavoriteGuide object:nil];
}

- (void)updateData
{
    [self.interactor getFavoriteItems];
}

- (void)enableScroll
{
    [self.view enableScroll];
}

@end
