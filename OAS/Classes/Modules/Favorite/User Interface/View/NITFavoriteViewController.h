//
//  NITFavoriteViewController.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITFavoriteProtocols.h"

@interface NITFavoriteViewController : NITBaseViewController <NITFavoriteViewProtocol>

@property (nonatomic, strong) id <NITFavoritePresenterProtocol> presenter;

@end
