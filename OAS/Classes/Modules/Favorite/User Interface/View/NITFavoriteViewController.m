//
//  NITFavoriteViewController.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteViewController.h"
#import "NITCatalogDirectoryTableViewCell.h"
#import "NITGuideFavoriteView.h"

@interface NITFavoriteViewController()
<
UIScrollViewDelegate,
UITableViewDelegate,
UITableViewDataSource,
NITCatalogDirectoryCellDelegate
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UIView * emptyFavoriteView;
@property (nonatomic) IBOutlet UILabel * emptyFavoriteSubtitleLbl;
@property (nonatomic) IBOutlet UIButton * presentCatalogBtn;

@property (nonatomic) IBOutlet UIView * blurringNavBar;

@property (nonatomic) NSArray <NITCatalogItemModel *> * items;

@property (nonatomic) BOOL isShowingGuideFavorite;

@end

@implementation NITFavoriteViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter initData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if([NITFavoriteItem allFavoriteItems].count > 0)
        [self showGuideIfNeed];
}

#pragma mark - IBAction
- (IBAction)clearFavorites:(id)sender
{
    [self.presenter clearFavorite:self.items];
}

- (IBAction)presentCatalogAction:(id)sender
{
    [self.presenter presentCatalog];
}

#pragma mark - Private
- (void)initUI
{
    //Nav bar
    self.leftBarButtonItem = self.navigationItem.leftBarButtonItem;
    
    //Table view
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITCatalogDirectoryTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITCatalogDirectoryTableViewCell)];
}

- (void)showGuideIfNeed
{
    if ([NITGuideFavoriteView notYetShown])
    {
        NSIndexPath * cellIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        CGRect cellRect = [self.view convertRect:[self.tableView rectForRowAtIndexPath:cellIndexPath] toView:WINDOW];
        cellRect.origin.y = cellRect.origin.y + ViewHeight(self.navigationController.navigationBar) + 20;
        if(IS_IPHONE_6_PLUS)
        {
            cellRect.size.width += 50;
        }
        [NITGuideFavoriteView showWidthOverlayRect:cellRect withView:self.view];
        
        self.tableView.scrollEnabled = false;
        self.isShowingGuideFavorite = true;
    
    }
    else
    {
       self.isShowingGuideFavorite = false;
    }
}

- (void)enableScroll
{
    self.tableView.scrollEnabled = true;
}

- (void)updateEmptyViewWithHide:(BOOL)hide
{
    if(hide)
    {
        [self.tableView reloadData];
        self.navigationItem.leftBarButtonItem = nil;
        self.emptyFavoriteView.hidden = false;
    }
    else
    {
        [self.tableView reloadData];
        self.navigationItem.leftBarButtonItem = self.leftBarButtonItem;
        self.emptyFavoriteView.hidden = true;
    }
}

#pragma mark - NITFavoritePresenterProtocol
- (void)updateEmtySubtitle:(NSMutableAttributedString *)title
{
    self.emptyFavoriteSubtitleLbl.attributedText = title;
}

- (void)reloadDataByModels:(NSArray <NITCatalogItemModel *> *)models
{
    self.items = models;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITCatalogDirectoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITCatalogDirectoryTableViewCell) forIndexPath:indexPath];
    cell.model = self.items[indexPath.row];
    cell.delegate = self;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NSLocalizedString(@"Удалить", nil);
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) [self.presenter deleteItem:self.items[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.isShowingGuideFavorite == false)
    {
        [self.presenter showProductDetailsWithModel:self.items[indexPath.row]];
    }

}

#pragma mark - NITCatalogDirectoryCellDelegate
- (void)didAddItem:(NITCatalogItemModel *)model
{
    [self.presenter addItemInBasket:model];
}

- (void)didReduseItem:(NITCatalogItemModel *)model
{
    [self.presenter reduseItemInBasket:model];
}

- (void)didPresentImage:(UIImage *)model
{
    [self.presenter showImage:model];
}

@end
