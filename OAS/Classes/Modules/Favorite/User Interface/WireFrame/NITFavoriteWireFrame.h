//
//  NITFavoriteWireFrame.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFavoriteProtocols.h"
#import "NITFavoriteViewController.h"
#import "NITFavoriteLocalDataManager.h"
#import "NITFavoriteAPIDataManager.h"
#import "NITFavoriteInteractor.h"
#import "NITFavoritePresenter.h"
#import "NITFavoriteWireframe.h"
#import <UIKit/UIKit.h>

@interface NITFavoriteWireFrame : NITRootWireframe <NITFavoriteWireFrameProtocol>

@end
