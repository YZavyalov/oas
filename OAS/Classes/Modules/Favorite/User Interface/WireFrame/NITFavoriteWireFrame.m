//
//  NITFavoriteWireFrame.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteWireFrame.h"
#import "NITProductDetailsWireFrame.h"
#import "NITPresentPictureWireFrame.h"

@implementation NITFavoriteWireFrame

#pragma mark - NITfavoriteWireFrameProtocol
+ (id)createNITFavoriteModule
{
    // Generating module components
    id <NITFavoritePresenterProtocol, NITFavoriteInteractorOutputProtocol> presenter = [NITFavoritePresenter new];
    id <NITFavoriteInteractorInputProtocol> interactor = [NITFavoriteInteractor new];
    id <NITFavoriteAPIDataManagerInputProtocol> APIDataManager = [NITFavoriteAPIDataManager new];
    id <NITFavoriteLocalDataManagerInputProtocol> localDataManager = [NITFavoriteLocalDataManager new];
    id <NITFavoriteWireFrameProtocol> wireFrame= [NITFavoriteWireFrame new];
    id <NITFavoriteViewProtocol> view = [(NITFavoriteWireFrame *)wireFrame createViewControllerWithKey:_s(NITFavoriteViewController) storyboardType:StoryboardTypeFavorite];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

+ (void)presentNITFavoriteModuleFrom:(UIViewController *)fromViewController
{
    NITFavoriteViewController * vc = [NITFavoriteWireFrame createNITFavoriteModule];
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)showProductDetailsFromViewController:(UIViewController *)fromViewController withModel:(NITCatalogItemModel *)model
{
    [NITProductDetailsWireFrame presentNITProductDetailsModuleFrom:fromViewController withModel:model withType:0];
}

- (void)showCatalogFromViewController:(UIViewController *)fromViewController
{
    [fromViewController.navigationController.tabBarController setSelectedIndex:0];
}

- (void)showImageWithView:(UIViewController *)fromViewController withImage:(UIImage *)model
{
    [NITPresentPictureWireFrame presentNITPresentPictureModuleFrom:fromViewController withImage:model];
}

- (void)presentAlertFrom:(UIViewController *)fromViewController withAlert:(UIAlertController *)alert
{
    [fromViewController presentViewController:alert animated:YES completion:nil];
}

@end
