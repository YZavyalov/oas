//
//  NITFavoriteLocalDataManager.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteLocalDataManager.h"

@implementation NITFavoriteLocalDataManager

#pragma mark - NITFavoriteLocalDataManagerInputProtocol
//Favorite
- (NSArray <NITFavoriteItem *> *)getFavoriteItems
{
    return [NITFavoriteItem allFavoriteItems];
}

- (void)deleteItemByID:(NSString *)identifier
{
    [NITFavoriteItem deleteFavoriteItemByID:identifier];
}

- (void)deleteAllItems
{
    [NITFavoriteItem deleteAllFavoriteItems];
}

//Basket
- (void)addItemInBasket:(NITCatalogItemModel *)model
{
    [NITBasketItem increaseBasketItemCountByModel:model];
}

- (void)reduseItemInBasket:(NITCatalogItemModel *)model
{
    [NITBasketItem dereaseBasketItemCountByModel:model];
}

@end
