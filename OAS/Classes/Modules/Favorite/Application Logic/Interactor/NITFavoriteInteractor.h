//
//  NITFavoriteInteractor.h
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITFavoriteProtocols.h"

@interface NITFavoriteInteractor : NSObject <NITFavoriteInteractorInputProtocol>

@property (nonatomic, weak) id <NITFavoriteInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITFavoriteAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITFavoriteLocalDataManagerInputProtocol> localDataManager;

@end
