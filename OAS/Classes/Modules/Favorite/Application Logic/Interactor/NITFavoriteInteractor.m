//
//  NITFavoriteInteractor.m
//  OAS
//
//  Created by Eugene Parafiynyk on 04/13/2017.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteInteractor.h"

@implementation NITFavoriteInteractor

#pragma mark - NITFavoriteInteractorInputProtocol
- (void)getFavoriteItems
{
    [self.presenter updateDataWithFavoriteModels:[self catalogItemsModelsFromFavorite:[self.localDataManager getFavoriteItems]]];
}

- (void)deleteItemByID:(NSString *)identifier
{
    [self.localDataManager deleteItemByID:identifier];
    [self getFavoriteItems];
}

- (void)deleteAllItems
{
    [self.localDataManager deleteAllItems];
    [self getFavoriteItems];
}

- (void)addItemInBasket:(NITCatalogItemModel *)model
{
    [self.localDataManager addItemInBasket:model];
}

- (void)reduseItemInBasket:(NITCatalogItemModel *)model
{
    [self.localDataManager reduseItemInBasket:model];
}

#pragma mark - Private
- (NSArray <NITCatalogItemModel *> *)catalogItemsModelsFromFavorite:(NSArray <NITFavoriteItem *> *)entities
{
    return [entities bk_map:^id(NITFavoriteItem * obj) {
        return [[NITCatalogItemModel alloc] initWithFavoriteModel:obj];
    }];
}

@end
