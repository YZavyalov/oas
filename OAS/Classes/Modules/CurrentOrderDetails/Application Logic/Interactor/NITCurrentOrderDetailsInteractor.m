//
//  NITCurrentOrderDetailsInteractor.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCurrentOrderDetailsInteractor.h"

@implementation NITCurrentOrderDetailsInteractor

#pragma mark - NITCurrentOrderDetailsInteractorInputProtocol
- (void)getCatalogItemDetailsByID:(NSArray <NITCatalogItemModel *> *)models
{
    NSMutableArray <NITCatalogItemModel *> * fullModels = [NSMutableArray new];
    weaken(self);
    dispatch_group_t group = dispatch_group_create();
    
    [HELPER startLoading];
    [models bk_each:^(NITCatalogItemModel *obj) {
        dispatch_group_enter(group);
        [weakSelf.APIDataManager getCatalogItemDetailsByID:obj.catalogCategoryID ? obj.catalogCategoryID : obj.identifier withHandler:^(SLCatalogItemWithRelations *result) {
            NITCatalogItemModel * model =[[NITCatalogItemModel alloc] initWithDetailModel:result withGroupID:nil];
            model.orderCount = obj.orderCount;
            [fullModels addObject:model];
            dispatch_group_leave(group);

        }];
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [HELPER stopLoading];
        [weakSelf.presenter updateProductByModels:fullModels];
    });
}

@end
