//
//  NITCurrentOrderDetailsInteractor.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCurrentOrderDetailsProtocols.h"

@interface NITCurrentOrderDetailsInteractor : NSObject <NITCurrentOrderDetailsInteractorInputProtocol>

@property (nonatomic, weak) id <NITCurrentOrderDetailsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCurrentOrderDetailsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCurrentOrderDetailsLocalDataManagerInputProtocol> localDataManager;

@end
