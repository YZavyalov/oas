//
//  NITCurrentOrderDetailsAPIDataManager.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCurrentOrderDetailsAPIDataManager.h"
#import "SLCatalogsApi.h"

@implementation NITCurrentOrderDetailsAPIDataManager

#pragma mark - NITCurrentOrderDetailsDataManagerInputProtocol
- (void)getCatalogItemDetailsByID:(NSString *)itemID withHandler:(void(^)(SLCatalogItemWithRelations *result))handler
{
    [HELPER startLoading];
    [[SLCatalogsApi new] getStaticCatalogItemsByItemidWithItemId:itemID completionHandler:^(SLCatalogItemWithRelations *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [ERROR_MESSAGE errorMessage:error_with_loading_product];
            [HELPER logError:error method:METHOD_NAME];
        }
        if (handler) handler (output);
    }];
}

@end
