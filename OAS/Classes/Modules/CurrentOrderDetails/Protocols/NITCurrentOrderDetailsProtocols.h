//
//  NITCurrentOrderDetailsProtocols.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NITOrderItemModel.h"
#import "NITCatalogItemModel.h"

@protocol NITCurrentOrderDetailsInteractorOutputProtocol;
@protocol NITCurrentOrderDetailsInteractorInputProtocol;
@protocol NITCurrentOrderDetailsViewProtocol;
@protocol NITCurrentOrderDetailsPresenterProtocol;
@protocol NITCurrentOrderDetailsLocalDataManagerInputProtocol;
@protocol NITCurrentOrderDetailsAPIDataManagerInputProtocol;

@class NITCurrentOrderDetailsWireFrame;

// Defines the public interface that something else can use to drive the user interface
@protocol NITCurrentOrderDetailsViewProtocol
@required
@property (nonatomic, strong) id <NITCurrentOrderDetailsPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)reloadDataByModel:(NITOrderItemModel *)model;
- (void)updateOrderPrice:(NSString *)price;
- (void)updateOrderPriceDescription:(NSString *)goods;
@end

@protocol NITCurrentOrderDetailsWireFrameProtocol
@required
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
+ (void)presentNITCurrentOrderDetailsModuleFrom:(id)fromViewController withOrderNumber:(NSInteger)orderNumber withModel:(NITOrderItemModel *)model withType:(NSString *)type;
- (void)showCatalogProductDetailsFrom:(id)fromViewController withModel:(NITCatalogItemModel *)model;
@end

@protocol NITCurrentOrderDetailsPresenterProtocol
@required
@property (nonatomic, weak) id <NITCurrentOrderDetailsViewProtocol> view;
@property (nonatomic, strong) id <NITCurrentOrderDetailsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCurrentOrderDetailsWireFrameProtocol> wireFrame;
@property (nonatomic) NITOrderItemModel * model;
@property (nonatomic) NSString * viewType;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)updateData;
- (void)showProductDetailsByModel:(NITCatalogItemModel *)model;
@end

@protocol NITCurrentOrderDetailsInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)updateProductByModels:(NSArray <NITCatalogItemModel *> *)models;
@end

@protocol NITCurrentOrderDetailsInteractorInputProtocol
@required
@property (nonatomic, weak) id <NITCurrentOrderDetailsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <NITCurrentOrderDetailsAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <NITCurrentOrderDetailsLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)getCatalogItemDetailsByID:(NSArray <NITCatalogItemModel *> *)models;
@end


@protocol NITCurrentOrderDetailsDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol NITCurrentOrderDetailsAPIDataManagerInputProtocol <NITCurrentOrderDetailsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getCatalogItemDetailsByID:(NSString *)itemID withHandler:(void(^)(SLCatalogItemWithRelations *result))handler;
@end

@protocol NITCurrentOrderDetailsLocalDataManagerInputProtocol <NITCurrentOrderDetailsDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end
