//
//  NITCurrentOrderDetailsViewController.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCurrentOrderDetailsProtocols.h"
#import "NITBaseViewController.h"

@interface NITCurrentOrderDetailsViewController : NITBaseViewController <NITCurrentOrderDetailsViewProtocol>

@property (nonatomic, strong) id <NITCurrentOrderDetailsPresenterProtocol> presenter;

@end
