//
//  NITCurrentOrderDetailsViewController.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCurrentOrderDetailsViewController.h"
#import "NITCurrentOrderInfoTableViewCell.h"
#import "NITCurrentOrderDetailsInfoTableViewCell.h"

@interface NITCurrentOrderDetailsViewController()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic) IBOutlet UITableView * tableView;
@property (nonatomic) IBOutlet UILabel * priceDescriptionLbl;
@property (nonatomic) IBOutlet UILabel * priceLbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint * priceDescriptionLblW;

@property (nonatomic) NITOrderItemModel * model;

@end

@implementation NITCurrentOrderDetailsViewController

#pragma mark - ViewController Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
    [self.presenter updateData];
}

#pragma mark - Private
- (void)initUI
{    
    //Table view
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITCurrentOrderInfoTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITCurrentOrderInfoTableViewCell)];
    [self.tableView registerNib:[UINib nibWithNibName:_s(NITCurrentOrderDetailsInfoTableViewCell) bundle:nil] forCellReuseIdentifier:_s(NITCurrentOrderDetailsInfoTableViewCell)];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)updateOrderPrice:(NSString *)price
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:[NSLocale currentLocale]];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    NSString * priceFormatter = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[price doubleValue]]];
    self.priceLbl.text = [priceFormatter stringByAppendingString:@" ₽"];
}

- (void)updateOrderPriceDescription:(NSString *)goods
{
    self.priceDescriptionLbl.text = goods;
    self.priceDescriptionLblW.constant = [self.priceDescriptionLbl sizeOfMultiLineLabel].width;
}

#pragma mark - NITCurrentOrderDetailsViewProtocol
- (void)reloadDataByModel:(NITOrderItemModel *)model
{
    self.model = model;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self.presenter.viewType isEqualToString:@"Pickup"])
        return self.model.items.count + 1;
    else
        return self.model.items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < self.model.items.count) return 165;
    else                                       return 56;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < self.model.items.count)
    {
        NITCurrentOrderInfoTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITCurrentOrderInfoTableViewCell) forIndexPath:indexPath];
        cell.model = self.model.items[indexPath.row];
        
        return cell;
    }
    else
    {
        NITCurrentOrderDetailsInfoTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:_s(NITCurrentOrderDetailsInfoTableViewCell) forIndexPath:indexPath];
        
        return cell;
    }
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < self.model.items.count)
    {
        [self.presenter showProductDetailsByModel:self.model.items[indexPath.row]];
    }
}

@end
