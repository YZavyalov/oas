//
//  NITCurrentOrderInfoTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 20.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCurrentOrderInfoTableViewCell.h"

@interface NITCurrentOrderInfoTableViewCell()

@property (nonatomic) IBOutlet UIImageView * imageViewImg;
@property (nonatomic) IBOutlet UIImageView * isNewIcon;
@property (nonatomic) IBOutlet UILabel * titleLbl;
@property (nonatomic) IBOutlet UILabel * subtitleLbl;
@property (nonatomic) IBOutlet UILabel * priceLbl;
@property (nonatomic) IBOutlet UILabel * countLbl;
@property (nonatomic) IBOutlet UILabel * recept;
@property (nonatomic) IBOutlet UILabel * underOrder;

@property (nonatomic) IBOutlet NSLayoutConstraint * titleLblH;
@property (nonatomic) IBOutlet NSLayoutConstraint * priceW;
@property (nonatomic) IBOutlet NSLayoutConstraint * receptW;
@property (nonatomic) IBOutlet NSLayoutConstraint * underOrderW;
@property (nonatomic) IBOutlet NSLayoutConstraint * underOrderL;

@end

@implementation NITCurrentOrderInfoTableViewCell

#pragma mark - Custom accessors
- (void)setModel:(NITCatalogItemModel *)model
{
    _model = model;
    
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    [self.imageViewImg setImage:nil];
    if([NSURL URLWithString:self.model.image])
        [self.imageViewImg setImageWithURL:[NSURL URLWithString:self.model.image]];
    else
        [self.imageViewImg setImage:[UIImage imageNamed:@"empty_foto"]];
    self.titleLbl.text = self.model.name;
    self.titleLblH.constant = [self.titleLbl sizeOfMultiLineLabel].height;
    self.subtitleLbl.text = self.model.descriptionText;
    self.priceLbl.text = [NSString stringWithFormat:@"%.2f ₽", [self.model.price floatValue]];
    self.countLbl.text = [NSString stringWithFormat:@"%ld шт.", (long)self.model.orderCount];
    
    self.titleLblH.constant = [self.titleLbl sizeOfMultiLineLabel].height;
    self.priceW.constant = [self.priceLbl sizeOfMultiLineLabel].width;
    
    self.receptW.constant = self.model.hasRecipe ? 50 : 0;
    self.underOrderW.constant = self.model.underOrder ? 70 : 0;
    self.underOrderL.constant = self.model.hasRecipe ? 8 : 0;
    
    self.isNewIcon.hidden = !self.model.isNew;
}

@end
