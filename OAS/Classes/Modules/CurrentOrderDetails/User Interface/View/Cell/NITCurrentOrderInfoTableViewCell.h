//
//  NITCurrentOrderInfoTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 20.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NITCatalogItemModel.h"

@interface NITCurrentOrderInfoTableViewCell : UITableViewCell

@property (nonatomic) NITCatalogItemModel * model;

@end
