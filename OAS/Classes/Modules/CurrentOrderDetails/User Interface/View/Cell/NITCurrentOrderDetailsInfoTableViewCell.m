//
//  NITCurrentOrderDetailsInfoTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 20.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCurrentOrderDetailsInfoTableViewCell.h"

@implementation NITCurrentOrderDetailsInfoTableViewCell

#pragma mark - Life cycle
- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.preservesSuperviewLayoutMargins = false;
    self.separatorInset = UIEdgeInsetsMake(0.f, [UIScreen mainScreen].bounds.size.width, 0.f, 0.f);
    self.layoutMargins = UIEdgeInsetsZero;
}

@end
