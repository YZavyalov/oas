//
//  NITCurrentOrderDetailsPresenter.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCurrentOrderDetailsProtocols.h"

@class NITCurrentOrderDetailsWireFrame;

@interface NITCurrentOrderDetailsPresenter : NSObject <NITCurrentOrderDetailsPresenterProtocol, NITCurrentOrderDetailsInteractorOutputProtocol>

@property (nonatomic, weak) id <NITCurrentOrderDetailsViewProtocol> view;
@property (nonatomic, strong) id <NITCurrentOrderDetailsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <NITCurrentOrderDetailsWireFrameProtocol> wireFrame;

@property (nonatomic) NITOrderItemModel * model;
@property (nonatomic) NSString * viewType;

@end
