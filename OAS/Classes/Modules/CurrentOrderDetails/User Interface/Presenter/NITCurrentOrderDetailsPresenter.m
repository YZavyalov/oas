//
//  NITCurrentOrderDetailsPresenter.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCurrentOrderDetailsPresenter.h"
#import "NITCurrentOrderDetailsWireframe.h"

@implementation NITCurrentOrderDetailsPresenter

#pragma mark - NITCurrentOrderDetailsPresenterProtocol
- (void)updateData
{
    [self orderCount];
    [self orderPrice];
    [self.interactor getCatalogItemDetailsByID:self.model.items];
}

#pragma mark - NITCurrentOrderDetailsInteractorOutputProtocol
- (void)updateProductByModels:(NSArray <NITCatalogItemModel *> *)models
{
    self.model.items = [[NSArray alloc] initWithArray:models];
    [self.view reloadDataByModel:self.model];
}

- (void)showProductDetailsByModel:(NITCatalogItemModel *)model
{
    [self.wireFrame showCatalogProductDetailsFrom:self.view withModel:model];
}

#pragma mark - Private
- (void)orderCount
{
    NSInteger count = 0;
    for(NITCatalogItemModel * item in self.model.items)
    {
        count += item.orderCount;
    }
    
    NSString * goods;
    if(fmodf(count, 10) == 1) goods = NSLocalizedString(@" товар ", nil);
    else if(fmodf(count, 10) > 1 && fmodf(count, 10) < 5) goods = NSLocalizedString(@" товара ", nil);
    else goods = NSLocalizedString(@" товаров ", nil);
    goods = [[[NSString stringWithFormat:@"%lu", (unsigned long)count] stringByAppendingString:goods] stringByAppendingString:NSLocalizedString(@"на сумму:", nil)];
    
    [self.view updateOrderPriceDescription:goods];
}

- (void)orderPrice
{
    double price = 0.0;
    for(NITCatalogItemModel * item in self.model.items)
    {
        price += item.orderCount * [item.price doubleValue];
    }
    [self.view updateOrderPrice:[NSString stringWithFormat:@"%.2f ₽", price]];
}

@end
