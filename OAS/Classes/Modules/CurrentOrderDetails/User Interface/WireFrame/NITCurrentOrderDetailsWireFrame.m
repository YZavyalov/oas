//
//  NITCurrentOrderDetailsWireFrame.m
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import "NITCurrentOrderDetailsWireFrame.h"
#import "NITProductDetailsWireFrame.h"

@implementation NITCurrentOrderDetailsWireFrame

#pragma mark - Private
+ (id)createNITCurrentOrderDetailsModule
{
    // Generating module components
    id <NITCurrentOrderDetailsPresenterProtocol, NITCurrentOrderDetailsInteractorOutputProtocol> presenter = [NITCurrentOrderDetailsPresenter new];
    id <NITCurrentOrderDetailsInteractorInputProtocol> interactor = [NITCurrentOrderDetailsInteractor new];
    id <NITCurrentOrderDetailsAPIDataManagerInputProtocol> APIDataManager = [NITCurrentOrderDetailsAPIDataManager new];
    id <NITCurrentOrderDetailsLocalDataManagerInputProtocol> localDataManager = [NITCurrentOrderDetailsLocalDataManager new];
    id <NITCurrentOrderDetailsWireFrameProtocol> wireFrame= [NITCurrentOrderDetailsWireFrame new];
    id <NITCurrentOrderDetailsViewProtocol> view = [(NITCurrentOrderDetailsWireFrame *)wireFrame createViewControllerWithKey:_s(NITCurrentOrderDetailsViewController) storyboardType:StoryboardTypeBasket];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    return view;
}

#pragma mark - NITCurrentOrderDetailsWireFrameProtocol
+ (void)presentNITCurrentOrderDetailsModuleFrom:(UIViewController *)fromViewController withOrderNumber:(NSInteger)orderNumber withModel:(NITOrderItemModel *)model withType:(NSString *)type
{
    NITCurrentOrderDetailsViewController * vc = [NITCurrentOrderDetailsWireFrame createNITCurrentOrderDetailsModule];
    vc.title = [NSString stringWithFormat:@"Заказ №%ld", (orderNumber + 1)];
    vc.presenter.model = model;
    vc.presenter.viewType = type;
    [fromViewController.navigationController pushViewController:vc animated:true];
}

- (void)showCatalogProductDetailsFrom:(UIViewController *)fromViewController withModel:(NITCatalogItemModel *)model
{    
    [NITProductDetailsWireFrame presentNITProductDetailsModuleFrom:fromViewController withModel:model withType:ProductDetailsViewTypeDirectory];
}

@end
