//
//  NITCurrentOrderDetailsWireFrame.h
//  OAS
//
//  Created by Yaroslav Zavyalov on 04/20/2017.
//  Copyright © 2017 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITCurrentOrderDetailsProtocols.h"
#import "NITCurrentOrderDetailsViewController.h"
#import "NITCurrentOrderDetailsLocalDataManager.h"
#import "NITCurrentOrderDetailsAPIDataManager.h"
#import "NITCurrentOrderDetailsInteractor.h"
#import "NITCurrentOrderDetailsPresenter.h"
#import "NITCurrentOrderDetailsWireframe.h"
#import "NITRootWireframe.h"
#import <UIKit/UIKit.h>

@interface NITCurrentOrderDetailsWireFrame : NITRootWireframe <NITCurrentOrderDetailsWireFrameProtocol>

@end
