//
//  NITBaseHeaderTableViewCell.m
//  OAS
//
//  Created by Yaroslav on 04.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBaseHeaderTableViewCell.h"

@interface NITBaseHeaderTableViewCell()

@property (nonatomic) IBOutlet UILabel * title;

@end

@implementation NITBaseHeaderTableViewCell

#pragma mark - Custom accessors
- (void)setTitleText:(NSString *)titleText
{
    _titleText = titleText;
    
    [self updateData];
}

#pragma mark - Private
- (void)updateData
{
    self.title.text = self.titleText;
}

@end
