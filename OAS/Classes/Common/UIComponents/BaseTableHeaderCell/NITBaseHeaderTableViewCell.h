//
//  NITBaseHeaderTableViewCell.h
//  OAS
//
//  Created by Yaroslav on 04.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NITBaseHeaderTableViewCell : UITableViewCell

@property (nonatomic) NSString * titleText;

@end
