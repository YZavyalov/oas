//
//  UserData.m
//  OAS
//
//  Created by Eugene Parafiynyk on 10/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "UserData.h"

@implementation UserData

+ (UserData *)sharedInstance
{
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self new];
        [instance setup];
    });
    
    return instance;
}

- (void)setup
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout) name:kResetSessionNotification object:nil];
}

#pragma mark - Custom Accessors
- (BOOL)isKeychainCreated
{
    return [self boolForKey:kUserKeychainCreated];
}

- (void)setIsKeychainCreated:(BOOL)isKeychainCreated
{
    [self setBool:isKeychainCreated forKey:kUserKeychainCreated];
}

- (NSString *)identifier
{
    return [self stringForKey:kUserIdentifier].length > 0 ? [self stringForKey:kUserIdentifier] : @"";
}

- (void)setIdentifier:(NSString *)identifier
{
    [self setString:identifier forKey:kUserIdentifier];
}

- (NSString *)name
{
    return [self stringForKey:kUserName];
}

- (void)setName:(NSString *)name
{
    [self setString:name forKey:kUserName];
}

- (NSString *)login
{
    return [self stringForKey:kUserLogin];
}

- (void)setLogin:(NSString *)login
{
    [self setString:login forKey:kUserLogin];
}

- (NSString *)password
{
    return [self stringForKey:kUserPassword];
}

- (void)setPassword:(NSString *)password
{
    [self setString:password forKey:kUserPassword];
}

- (NSString *)cityName
{
    return [self stringForKey:kUserCityName];
}

- (void)setCityName:(NSString *)cityName
{
    [self setString:cityName forKey:kUserCityName];
}

- (NSString *)cityPhone
{
    return [self stringForKey:kUserCityPhone];
}

- (void)setCityPhone:(NSString *)cityPhone
{
    [self setString:cityPhone forKey:kUserCityPhone];
}

- (NSString *)cityID
{
    return [self stringForKey:kUserCityID] ? : @"";
}

- (void)setCityID:(NSString *)cityID
{
    [self setString:cityID forKey:kUserCityID];
}

- (BOOL)cityDelivery
{
    return [self boolForKey:kCityDelivery];
}

- (void)setCityDelivery:(BOOL)cityDelivery
{
    [self setBool:cityDelivery forKey:kCityDelivery];
}

- (float)cityLat
{
    return [self floatForKey:kCityLat];
}

- (void)setCityLat:(float)cityLat
{
    [self setFloat:cityLat forKey:kCityLat];
}

- (float)cityLon
{
    return [self floatForKey:kCityLon];
}

- (void)setCityLon:(float)cityLon
{
    [self setFloat:cityLon forKey:kCityLon];
}

- (NSString *)address
{
    return [self stringForKey:kUserAddress];
}

- (void)setAddress:(NSString *)address
{
    [self setString:address forKey:kUserAddress];
}

- (NSNumber *)bonus
{
    return @([self integerForKey:kUserBonus]);
}

- (void)setBonus:(NSNumber *)bonus
{
    [self setInteger:[bonus integerValue] forKey:kUserBonus];
}

- (NSNumber *)discount
{
    return @([self integerForKey:kUserDiscount]);
}

- (void)setDiscount:(NSNumber *)discount
{
    [self setInteger:[discount integerValue] forKey:kUserDiscount];
}

- (NSString *)discountCode
{
    return [self stringForKey:kUserDiscountCode];
}

- (void)setDiscountCode:(NSString *)discountCode
{
    [self setString:discountCode forKey:kUserDiscountCode];
}

- (NSNumber *)consent
{
    return @([self integerForKey:kUserDiscount]);
}

- (void)setConsent:(NSNumber *)consent
{
    [self setInteger:[consent integerValue] forKey:kUserConsent];
}

- (NSDate *)birthday
{
    return [self objectForKey:kUserBirthday];
}

- (void)setBirthday:(NSDate *)birthday
{
    [self setObject:birthday forKey:kUserBirthday];
}

- (Gender)gender
{
    return (Gender)[self integerForKey:kUserGender];
}

- (void)setGender:(Gender)gender
{
    NSAssert(gender == GenderUnknow || gender == GenderMale || gender == GenderFamale, @"Error range gender");
    [self setInteger:gender forKey:kUserGender];
}

- (NSString *)shopHubID
{
    return [self stringForKey:kShopHubID];
}

- (void)setShopHubID:(NSString *)shopHubID
{
    [self setString:shopHubID forKey:kShopHubID];
}

- (NSString *)phone
{
    return [self stringForKey:kUserPhone];
}

- (void)setPhone:(NSString *)phone
{
    [self setString:phone forKey:kUserPhone];
}

- (NSString *)email
{
    return [self stringForKey:kUserEmail];
}

- (void)setEmail:(NSString *)email
{
    [self setString:email forKey:kUserEmail];
}

- (NSString *)accessToken
{
    return [self stringForKey:kUserAccessToken].length > 0 ? [self stringForKey:kUserAccessToken] : @"";
}

- (void)setAccessToken:(NSString *)accessToken
{
    [self setString:accessToken forKey:kUserAccessToken];
}

- (void)setPushToken:(NSData *)pushToken
{
    [self setData:pushToken forKey:kUserPushToken];
}

- (NSData *)pushToken
{
    return [self dataForKey:kUserPushToken];
}

- (NSString *)pushTokenString
{
    NSData * tokenData = self.pushToken;
    
    if (tokenData)
    {
        const char *data = [tokenData bytes];
        NSMutableString *token = [NSMutableString string];
        
        for (int i = 0; i < [tokenData length]; i++)
        {
            [token appendFormat:@"%02.2hhX", data[i]];
        }
        
        return [token copy];
    }
    else
    {
        return nil;
    }
}

- (NSInteger)appRate
{
    return [self integerForKey:kAppRate];
}

- (void)setAppRate:(NSInteger)appRate
{
    [self setInteger:appRate forKey:kAppRate];
}

- (NSDate *)showRateAppDate
{
    return [self objectForKey:kShowRateAppDate];
}

- (void)setShowRateAppDate:(NSDate *)showRateAppDate
{
    [self setObject:showRateAppDate forKey:kShowRateAppDate];
}

- (NSString *)company
{
    //return [self stringForKey:kUserCompany].length > 0 ? [self stringForKey:kUserCompany] : @"f79538d8-fda4-451b-b5e6-4b558c22d102";
    return [self stringForKey:kUserCompany].length > 0 ? [self stringForKey:kUserCompany] : kProdCompanyID;
}

- (void)setCompany:(NSString *)company
{
    [self setString:company forKey:kUserCompany];
}

- (NSDictionary *)attributes
{
    NSMutableDictionary * result = [NSMutableDictionary new];
    
    return result;
}

- (void)setAttributes:(NSDictionary *)attributes
{
    
}

- (BOOL)isAutorize
{
    return self.identifier.length > 0 && self.accessToken.length > 0;
}

- (void)logout
{
    self.identifier = nil;
//    self.cityID     = nil;
//    self.cityName   = nil;
    self.address    = nil;
    self.email      = nil;
    self.phone      = nil;
    self.bonus      = nil;
    self.birthday   = nil;
    self.discount   = nil;
    self.consent    = nil;
    self.pushToken  = nil;
    self.attributes = nil;
    self.accessToken = nil;
    self.gender     = GenderUnknow;
}

@end
