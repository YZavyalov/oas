//
//  UserDataContstants.h
//  OAS
//
//  Created by Eugene Parafiynyk on 10/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

typedef NS_ENUM(NSInteger, Gender) {
    GenderUnknow    = -1,
    GenderMale      = 0,
    GenderFamale    = 1
};

typedef CF_ENUM (NSUInteger, DeliveryType) {
    DeliveryTypePickup      = 0,
    DeliveryTypeDelivery    = 1
};

static NSString * const kUserKeychainCreated    = @"kUserKeychainCreated";
static NSString * const kUserIdentifier         = @"kUserIdentifier";
static NSString * const kUserCityName           = @"kUserCityName";
static NSString * const kUserCityPhone          = @"kUserCityPhone";
static NSString * const kUserCityID             = @"kUserCityID";
static NSString * const kUserCity               = @"kUserCity";
static NSString * const kUserAddress            = @"kUserAddress";
static NSString * const kUserBonus              = @"kUserBonus";
static NSString * const kUserDiscount           = @"kUserDiscount";
static NSString * const kUserDiscountCode       = @"kUserDiscountCode";
static NSString * const kUserConsent            = @"kUserConsent";
static NSString * const kUserBirthday           = @"kUserBirthday";
static NSString * const kUserGender             = @"kUserGender";
static NSString * const kUserPhone              = @"kUserPhone";
static NSString * const kUserEmail              = @"kUserEmail";
static NSString * const kUserPushToken          = @"kUserPushToken";
static NSString * const kUserAccessToken        = @"kUserAccessToken";
static NSString * const kUserName               = @"kUserName";
static NSString * const kUserLogin              = @"kUserLogin";
static NSString * const kUserPassword           = @"kUserPassword";
static NSString * const kAppRate                = @"kAppRate";
static NSString * const kShowRateAppDate        = @"kShowRateAppDate";
static NSString * const kUserCompany            = @"kUserCompany";
static NSString * const kShopHubID              = @"kShopHubID";
static NSString * const kCityDelivery           = @"kCityDelivery";
static NSString * const kCityLat                = @"kCityLat";
static NSString * const kCityLon                = @"kCityLon";
