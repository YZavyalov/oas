//
//  UserData.h
//  OAS
//
//  Created by Eugene Parafiynyk on 10/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "UserDataContstants.h"

#define USER [UserData sharedInstance]

@interface UserData : NITLocalSettings

+ (UserData *) sharedInstance;

@property (nonatomic) BOOL isKeychainCreated;

@property (nonatomic) NSString * identifier;
@property (nonatomic) NSString * name;
@property (nonatomic) NSString * login;
@property (nonatomic) NSString * password;
@property (nonatomic) NSString * cityName;
@property (nonatomic) NSString * address;
@property (nonatomic) NSString * email;
@property (nonatomic) NSString * phone;
@property (nonatomic) NSString * cityPhone;
@property (nonatomic) NSString * accessToken;
@property (nonatomic) BOOL cityDelivery;
@property (nonatomic) float cityLat;
@property (nonatomic) float cityLon;

@property (nonatomic) NSString * company;
@property (nonatomic) NSString * cityID;
@property (nonatomic) NSNumber * bonus;
@property (nonatomic) NSNumber * discount;
@property (nonatomic) NSString * discountCode;
@property (nonatomic) NSNumber * consent;

@property (nonatomic) NSDate * birthday;

@property (nonatomic) NSInteger appRate;
@property (nonatomic) NSDate * showRateAppDate;

@property (nonatomic) NSData  * pushToken;

@property (nonatomic) NSDictionary * attributes;

@property (nonatomic) Gender gender;

@property (nonatomic) NSString * shopHubID;

@property (nonatomic, readonly) NSString * pushTokenString;
@property (nonatomic, readonly) BOOL isAutorize;

- (void)logout;

@end
