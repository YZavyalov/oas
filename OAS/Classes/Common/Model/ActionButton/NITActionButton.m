//
//  NITActionButton.m
//  Megaflowers
//
//  Created by Eugene Parafiynyk on 10/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITActionButton.h"

@implementation NITActionButton

- (id)initWithTitle:(NSString *)title actionBlock:(NITActionButtonBlock)actionBlock
{
    self = [super init];
    
    if (self)
    {
        self.title = title;
        self.actionBlock = actionBlock;
    }
    
    return self;
}


@end
