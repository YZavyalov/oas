//
//  UITabBarController+Extension.h
//  OAS
//
//  Created by Eugene Parafiynyk on 6/6/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBarController (Extension)

- (void)hideTabBarAnimated:(BOOL)animated;
- (void)showTabBarAnimated:(BOOL)animated;

@end
