//
//  NSArray+Extension.h
//  OAS
//
//  Created by Eugene Parafiynyk on 5/1/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Extension)

- (NSString *)JSONString;
- (NSString *)prettyPrintedJSONString;

@end
