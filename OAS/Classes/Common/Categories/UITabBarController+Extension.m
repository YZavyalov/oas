//
//  UITabBarController+Extension.m
//  OAS
//
//  Created by Eugene Parafiynyk on 6/6/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "UITabBarController+Extension.h"

@implementation UITabBarController (Extension)

- (void)hideTabBarAnimated:(BOOL)animated
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    if (animated)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
    }

    float fHeight = screenRect.size.height;
    
    if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft ||
        [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight)
    {
        fHeight = screenRect.size.width;
    }
    
    for (UIView * view in self.view.subviews)
    {
        if ([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, fHeight, view.frame.size.width, view.frame.size.height)];
        }
    }
    
    if (animated)
    {
        [UIView commitAnimations];
    }
}



- (void)showTabBarAnimated:(BOOL)animated
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    float fHeight = screenRect.size.height - self.tabBar.frame.size.height;
    
    if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft ||
        [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight)
    {
        fHeight = screenRect.size.width - self.tabBar.frame.size.height;
    }
    
    if (animated)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
    }
    
    for (UIView * view in self.view.subviews)
    {
        if ([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, fHeight, view.frame.size.width, view.frame.size.height)];
        }
    }
    
    if (animated)
    {
        [UIView commitAnimations];
    }
}

@end
