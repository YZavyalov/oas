//
//  NSDictionary+Extension.m
//  OAS
//
//  Created by Eugene Parafiynyk on 5/1/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NSDictionary+Extension.h"

static const int ddLogLevel = LOG_LEVEL_ERROR;

@implementation NSDictionary (Extension)

- (NSString *)JSONString
{
    NSError * serializationError = nil;
    NSData * data = [NSJSONSerialization dataWithJSONObject:self options:0 error:&serializationError];
    if (serializationError)
    {
        DDLogError(@"Failed serialising JSON: %@", serializationError.localizedDescription);
    }
    
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

- (NSString *)prettyPrintedJSONString
{
    NSError * serializationError = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                        options:NSJSONWritingPrettyPrinted
                                                          error:&serializationError];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

@end
