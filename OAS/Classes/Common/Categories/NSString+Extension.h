//
//  NSString+Extension.h
//  OAS
//
//  Created by Eugene Parafiynyk on 12/1/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extension)

- (id)objectFromJSONString;
- (NSString *)stringByStrippingHTML;
+ (NSString *)stringWithPhone:(NSString *)phone;
+ (NSString *)stringDateFromToday;
+ (NSString *)stringdateFromNextDay;
+ (NSString *)stringdateFromNdays:(int)offset;
+ (NSString *)stringdateWithDayOfWeekFromNdays:(int)offset;
+ (NSString *)string:(NSString *)string replacementWord:(NSString *)word inPosition:(NSInteger)position;

@end
