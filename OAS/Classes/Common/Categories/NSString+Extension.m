//
//  NSString+Extension.m
//  OAS
//
//  Created by Eugene Parafiynyk on 12/1/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NSString+Extension.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@implementation NSString (Extension)

- (id)objectFromJSONString
{
    NSError *serializationError = nil;
    id result = [NSJSONSerialization JSONObjectWithData:[self dataUsingEncoding:NSUTF8StringEncoding]
                                                options:0
                                                  error:&serializationError];
    if (serializationError)
    {
        DDLogError(@"Failed unserialising JSON: %@\nError: %@", self, serializationError.localizedDescription);
    }
    
    return result;
}


- (NSString *)stringByStrippingHTML
{
    NSRange r;
    NSString * s = [self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];

    for (NSString * tag in [self phpTags])
    {
        s = [s stringByReplacingOccurrencesOfString:tag withString:@" "];
    }
    
    return s;
}

+ (NSString *)stringWithPhone:(NSString *)phone
{
    phone = [phone stringByReplacingOccurrencesOfString:@"+" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@")" withString:@""];

    return phone;
}

+ (NSString *)stringDateFromToday
{
    NSString * deviceLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSLocale * locale = [[NSLocale alloc] initWithLocaleIdentifier:deviceLanguage];
    NSDateFormatter * format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd"];
    [format setLocale:locale];
    NSDate *now = [NSDate date];
    
    NSString * month;
    if(now.month == 1)          month = NSLocalizedString(@"января", nil);
    else if(now.month == 2)     month = NSLocalizedString(@"февраля", nil);
    else if(now.month == 3)     month = NSLocalizedString(@"марта", nil);
    else if(now.month == 4)     month = NSLocalizedString(@"апреля", nil);
    else if(now.month == 5)     month = NSLocalizedString(@"мая", nil);
    else if(now.month == 6)     month = NSLocalizedString(@"июня", nil);
    else if(now.month == 7)     month = NSLocalizedString(@"июля", nil);
    else if(now.month == 8)     month = NSLocalizedString(@"августа", nil);
    else if(now.month == 9)     month = NSLocalizedString(@"сентября", nil);
    else if(now.month == 10)    month = NSLocalizedString(@"октября", nil);
    else if(now.month == 11)    month = NSLocalizedString(@"ноября", nil);
    else if(now.month == 12)    month = NSLocalizedString(@"декабря", nil);

    NSString * dateString = [NSString stringWithFormat:@"%@ %@", [format stringFromDate:now], month];
    
    return dateString;
}

+ (NSString *)stringdateFromNextDay
{
    NSString * deviceLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSLocale * locale = [[NSLocale alloc] initWithLocaleIdentifier:deviceLanguage];
    NSDateFormatter * format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd"];
    [format setLocale:locale];
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = 1;
    NSCalendar * theCalendar = [NSCalendar currentCalendar];
    NSDate * nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
    
    NSString * month;
    if(nextDate.month == 1)          month = NSLocalizedString(@"января", nil);
    else if(nextDate.month == 2)     month = NSLocalizedString(@"февраля", nil);
    else if(nextDate.month == 3)     month = NSLocalizedString(@"марта", nil);
    else if(nextDate.month == 4)     month = NSLocalizedString(@"апреля", nil);
    else if(nextDate.month == 5)     month = NSLocalizedString(@"мая", nil);
    else if(nextDate.month == 6)     month = NSLocalizedString(@"июня", nil);
    else if(nextDate.month == 7)     month = NSLocalizedString(@"июля", nil);
    else if(nextDate.month == 8)     month = NSLocalizedString(@"августа", nil);
    else if(nextDate.month == 9)     month = NSLocalizedString(@"сентября", nil);
    else if(nextDate.month == 10)    month = NSLocalizedString(@"октября", nil);
    else if(nextDate.month == 11)    month = NSLocalizedString(@"ноября", nil);
    else if(nextDate.month == 12)    month = NSLocalizedString(@"декабря", nil);

    NSString * dateString = [NSString stringWithFormat:@"%@ %@", [format stringFromDate:nextDate], month];
    
    return dateString;
}

+ (NSString *)stringdateFromNdays:(int)offset
{
    NSString * deviceLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSLocale * locale = [[NSLocale alloc] initWithLocaleIdentifier:deviceLanguage];
    NSDateFormatter * format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd"];
    [format setLocale:locale];
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = offset;
    NSCalendar * theCalendar = [NSCalendar currentCalendar];
    NSDate * nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
    
    NSString * month;
    if(nextDate.month == 1)          month = NSLocalizedString(@"января", nil);
    else if(nextDate.month == 2)     month = NSLocalizedString(@"февраля", nil);
    else if(nextDate.month == 3)     month = NSLocalizedString(@"марта", nil);
    else if(nextDate.month == 4)     month = NSLocalizedString(@"апреля", nil);
    else if(nextDate.month == 5)     month = NSLocalizedString(@"мая", nil);
    else if(nextDate.month == 6)     month = NSLocalizedString(@"июня", nil);
    else if(nextDate.month == 7)     month = NSLocalizedString(@"июля", nil);
    else if(nextDate.month == 8)     month = NSLocalizedString(@"августа", nil);
    else if(nextDate.month == 9)     month = NSLocalizedString(@"сентября", nil);
    else if(nextDate.month == 10)    month = NSLocalizedString(@"октября", nil);
    else if(nextDate.month == 11)    month = NSLocalizedString(@"ноября", nil);
    else if(nextDate.month == 12)    month = NSLocalizedString(@"декабря", nil);
    
    NSString * dateString = [NSString stringWithFormat:@"%@ %@", [format stringFromDate:nextDate], month];
    
    return dateString;
}

+ (NSString *)stringdateWithDayOfWeekFromNdays:(int)offset
{
    NSString * deviceLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSLocale * locale = [[NSLocale alloc] initWithLocaleIdentifier:deviceLanguage];
    NSDateFormatter * format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd"];
    [format setLocale:locale];
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = offset;
    NSCalendar * theCalendar = [NSCalendar currentCalendar];
    NSDate * nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
    
    NSString * month;
    if(nextDate.month == 1)          month = NSLocalizedString(@"января", nil);
    else if(nextDate.month == 2)     month = NSLocalizedString(@"февраля", nil);
    else if(nextDate.month == 3)     month = NSLocalizedString(@"марта", nil);
    else if(nextDate.month == 4)     month = NSLocalizedString(@"апреля", nil);
    else if(nextDate.month == 5)     month = NSLocalizedString(@"мая", nil);
    else if(nextDate.month == 6)     month = NSLocalizedString(@"июня", nil);
    else if(nextDate.month == 7)     month = NSLocalizedString(@"июля", nil);
    else if(nextDate.month == 8)     month = NSLocalizedString(@"августа", nil);
    else if(nextDate.month == 9)     month = NSLocalizedString(@"сентября", nil);
    else if(nextDate.month == 10)    month = NSLocalizedString(@"октября", nil);
    else if(nextDate.month == 11)    month = NSLocalizedString(@"ноября", nil);
    else if(nextDate.month == 12)    month = NSLocalizedString(@"декабря", nil);
    
    NSString * weekDay;
    if(nextDate.weekday == 1)           weekDay = NSLocalizedString(@"вс", nil);
    else if (nextDate.weekday == 2)     weekDay = NSLocalizedString(@"пн", nil);
    else if (nextDate.weekday == 3)     weekDay = NSLocalizedString(@"вт", nil);
    else if (nextDate.weekday == 4)     weekDay = NSLocalizedString(@"ср", nil);
    else if (nextDate.weekday == 5)     weekDay = NSLocalizedString(@"чт", nil);
    else if (nextDate.weekday == 6)     weekDay = NSLocalizedString(@"пт", nil);
    else if (nextDate.weekday == 7)     weekDay = NSLocalizedString(@"сб", nil);
    
    NSString * dateString = [NSString stringWithFormat:@"%@ %@ %@",weekDay, [format stringFromDate:nextDate], month];
    
    return dateString;
}

+ (NSString *)string:(NSString *)string replacementWord:(NSString *)word inPosition:(NSInteger)position
{
    NSMutableArray <NSString *> * stringArray = [[string componentsSeparatedByString:@" "] mutableCopy];
    
    if(stringArray.count >= position)
    {
        stringArray[position - 1] = word;
    }
    else
    {
        return nil;
    }
    
    NSString * result = @"";
    for(NSString * str in stringArray)
    {
        result = [NSString stringWithFormat:@"%@ %@", result, str];
    }
    
    return result;
}

#pragma mark - Private
- (NSArray *)phpTags
{
    return @[
             // A.2.2. Special characters
             @"&quot;",
             @"&amp;",
             @"&apos;",
             @"&lt;",
             @"&gt;",
             
             // A.2.1. Latin-1 characters
             @"&nbsp;",
             @"&iexcl;",
             @"&cent;",
             @"&pound;",
             @"&curren;",
             @"&yen;",
             @"&brvbar;",
             @"&sect;",
             @"&uml;",
             @"&copy;",
             @"&ordf;",
             @"&laquo;",
             @"&not;",
             @"&shy;",
             @"&reg;",
             @"&macr;",
             @"&deg;",
             @"&plusmn;",
             @"&sup2;",
             @"&sup3;",
             @"&acute;",
             @"&micro;",
             @"&para;",
             @"&middot;",
             @"&cedil;",
             @"&sup1;",
             @"&ordm;",
             @"&raquo;",
             @"&frac14;",
             @"&frac12;",
             @"&frac34;",
             @"&iquest;",
             @"&Agrave;",
             @"&Aacute;",
             @"&Acirc;",
             @"&Atilde;",
             @"&Auml;",
             @"&Aring;",
             @"&AElig;",
             @"&Ccedil;",
             @"&Egrave;",
             @"&Eacute;",
             @"&Ecirc;",
             @"&Euml;",
             @"&Igrave;",
             @"&Iacute;",
             @"&Icirc;",
             @"&Iuml;",
             @"&ETH;",
             @"&Ntilde;",
             @"&Ograve;",
             @"&Oacute;",
             @"&Ocirc;",
             @"&Otilde;",
             @"&Ouml;",
             @"&times;",
             @"&Oslash;",
             @"&Ugrave;",
             @"&Uacute;",
             @"&Ucirc;",
             @"&Uuml;",
             @"&Yacute;",
             @"&THORN;",
             @"&szlig;",
             @"&agrave;",
             @"&aacute;",
             @"&acirc;",
             @"&atilde;",
             @"&auml;",
             @"&aring;",
             @"&aelig;",
             @"&ccedil;",
             @"&egrave;",
             @"&eacute;",
             @"&ecirc;",
             @"&euml;",
             @"&igrave;",
             @"&iacute;",
             @"&icirc;",
             @"&iuml;",
             @"&eth;",
             @"&ntilde;",
             @"&ograve;",
             @"&oacute;",
             @"&ocirc;",
             @"&otilde;",
             @"&ouml;",
             @"&divide;",
             @"&oslash;",
             @"&ugrave;",
             @"&uacute;",
             @"&ucirc;",
             @"&uuml;",
             @"&yacute;",
             @"&thorn;",
             @"&yuml;",
             
             // A.2.2. Special characters cont'd
             @"&OElig;",
             @"&oelig;",
             @"&Scaron;",
             @"&scaron;",
             @"&Yuml;",
             
             // A.2.3. Symbols
             @"&fnof;",
             
             // A.2.2. Special characters cont'd
             @"&circ;",
             @"&tilde;",
             
             // A.2.3. Symbols cont'd
             @"&Alpha;",
             @"&Beta;",
             @"&Gamma;",
             @"&Delta;",
             @"&Epsilon;",
             @"&Zeta;",
             @"&Eta;",
             @"&Theta;",
             @"&Iota;",
             @"&Kappa;",
             @"&Lambda;",
             @"&Mu;",
             @"&Nu;",
             @"&Xi;",
             @"&Omicron;",
             @"&Pi;",
             @"&Rho;",
             @"&Sigma;",
             @"&Tau;",
             @"&Upsilon;",
             @"&Phi;",
             @"&Chi;",
             @"&Psi;",
             @"&Omega;",
             @"&alpha;",
             @"&beta;",
             @"&gamma;",
             @"&delta;",
             @"&epsilon;",
             @"&zeta;",
             @"&eta;",
             @"&theta;",
             @"&iota;",
             @"&kappa;",
             @"&lambda;",
             @"&mu;",
             @"&nu;",
             @"&xi;",
             @"&omicron;",
             @"&pi;",
             @"&rho;",
             @"&sigmaf;",
             @"&sigma;",
             @"&tau;",
             @"&upsilon;",
             @"&phi;",
             @"&chi;",
             @"&psi;",
             @"&omega;",
             @"&thetasym;",
             @"&upsih;",
             @"&piv;",
             
             // A.2.2. Special characters cont'd
             @"&ensp;",
             @"&emsp;",
             @"&thinsp;",
             @"&zwnj;",
             @"&zwj;",
             @"&lrm;",
             @"&rlm;",
             @"&ndash;",
             @"&mdash;",
             @"&lsquo;",
             @"&rsquo;",
             @"&sbquo;",
             @"&ldquo;",
             @"&rdquo;",
             @"&bdquo;",
             @"&dagger;",
             @"&Dagger;",
             
             // A.2.3. Symbols cont'd
             @"&bull;",
             @"&hellip;",
             
             // A.2.2. Special characters cont'd
             @"&permil;",
             
             // A.2.3. Symbols cont'd
             @"&prime;",
             @"&Prime;",
             
             // A.2.2. Special characters cont'd
             @"&lsaquo;",
             @"&rsaquo;",
             
             // A.2.3. Symbols cont'd
             @"&oline;",
             @"&frasl;",
             
             // A.2.2. Special characters cont'd
             @"&euro;",
             
             // A.2.3. Symbols cont'd
             @"&image;",
             @"&weierp;",
             @"&real;",
             @"&trade;",
             @"&alefsym;",
             @"&larr;",
             @"&uarr;",
             @"&rarr;",
             @"&darr;",
             @"&darr;",
             @"&harr;",
             @"&crarr;",
             @"&lArr;",
             @"&uArr;",
             @"&rArr;",
             @"&dArr;",
             @"&hArr;",
             @"&forall;",
             @"&part;",
             @"&exist;",
             @"&empty;",
             @"&nabla;",
             @"&isin;",
             @"&notin;",
             @"&ni;",
             @"&prod;",
             @"&sum;",
             @"&minus;",
             @"&lowast;",
             @"&radic;",
             @"&prop;",
             @"&infin;",
             @"&ang;",
             @"&and;",
             @"&or;",
             @"&cap;",
             @"&cup;",
             @"&int;",
             @"&there4;",
             @"&sim;",
             @"&cong;",
             @"&asymp;",
             @"&ne;",
             @"&equiv;",
             @"&le;",
             @"&ge;",
             @"&sub;",
             @"&sup;",
             @"&nsub;",
             @"&sube;",
             @"&supe;",
             @"&oplus;",
             @"&otimes;",
             @"&perp;",
             @"&sdot;",
             @"&lceil;",
             @"&rceil;",
             @"&lfloor;",
             @"&rfloor;",
             @"&lang;",
             @"&rang;",
             @"&loz;",
             @"&spades;",
             @"&clubs;",
             @"&hearts;",
             @"&diams;",
             
             // C0 Controls and Basic Latin
             @"&quot;",
             @"&amp;",
             @"&apos;",
             
             // Latin Extended-A
             @"&OElig;",
             @"&oelig;",
             @"&Scaron;",
             @"&scaron;",
             @"&Yuml;",
             
             // Spacing Modifier Letters
             @"&circ;",
             @"&tilde;",
             
             // General Punctuation
             @"&ensp;",
             @"&emsp;",
             @"&thinsp;",
             @"&zwnj;",
             @"&zwj;",
             @"&lrm;",
             @"&rlm;",
             @"&ndash;",
             @"&mdash;",
             @"&lsquo;",
             @"&rsquo;",
             @"&sbquo;",
             @"&ldquo;",
             @"&rdquo;",
             @"&bdquo;",
             @"&dagger;",
             @"&Dagger;",
             @"&permil;",
             @"&lsaquo;",
             @"&rsaquo;",
             @"&euro;"
             ];
}


@end
