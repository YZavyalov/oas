//
//  JSONValueTransformer+Dictionary.h
//  OAS
//
//  Created by Eugene Parafiynyk on 5/16/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface JSONValueTransformer (Dictionary)

- (NSDictionary *)JSONObjectFromNSObject:(id)object;

@end
