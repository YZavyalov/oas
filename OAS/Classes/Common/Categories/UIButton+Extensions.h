//
//  UIButton+Extensions.h
//  OAS
//
//  Created by Yaroslav on 02.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Extensions)

@property(nonatomic, assign) UIEdgeInsets hitTestEdgeInsets;

@end
