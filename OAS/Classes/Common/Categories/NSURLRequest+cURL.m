//
//  NSURLRequest+cURL.m
//  NSURLReqeust-cURL
//
//  Created by Eugene Parafiynyk on 31/07/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NSURLRequest+cURL.h"

@implementation NSURLRequest (cURL)

- (NSString *)description
{
    __block NSMutableString * displayString = [NSMutableString stringWithFormat:@"curl -v -X %@", self.HTTPMethod];
    
    [displayString appendFormat:@"\n \'%@\'",  self.URL.absoluteString];
    
    [self.allHTTPHeaderFields enumerateKeysAndObjectsUsingBlock:^(id key, id val, BOOL *stop) {
        [displayString appendFormat:@"\n -H \'%@: %@\'", key, val];
    }];
    
    if ([self.HTTPMethod isEqualToString:@"POST"]   ||
        [self.HTTPMethod isEqualToString:@"PUT"]    ||
        [self.HTTPMethod isEqualToString:@"PATCH"])
    {
        [displayString appendFormat:@"\n -d \'%@\'", [[NSString alloc] initWithData:self.HTTPBody encoding:NSUTF8StringEncoding]];
    }
    
    return displayString;
}

- (NSString *)printedCurl
{
    __block NSMutableString * displayString = [NSMutableString stringWithFormat:@"curl -v -X %@", self.HTTPMethod];
    
    [displayString appendFormat:@"\n \'%@\'",  self.URL.absoluteString];
    
    [self.allHTTPHeaderFields enumerateKeysAndObjectsUsingBlock:^(id key, id val, BOOL *stop) {
        [displayString appendFormat:@"\n -H \'%@: %@\'", key, val];
    }];
    
    if ([self.HTTPMethod isEqualToString:@"POST"]   ||
        [self.HTTPMethod isEqualToString:@"PUT"]    ||
        [self.HTTPMethod isEqualToString:@"PATCH"])
    {
        id body = [[[NSString alloc] initWithData:self.HTTPBody encoding:NSUTF8StringEncoding] objectFromJSONString];
        
        if (body)
        {
            [displayString appendFormat:@"\n -d \'%@\'", body];
        }
    }
    
    return displayString;
}

@end
