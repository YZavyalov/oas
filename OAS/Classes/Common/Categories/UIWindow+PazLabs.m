//
//  UIWindow+PazLabs.m
//  OAS
//
//  Created by Eugene Parafiynyk on 10/31/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "UIWindow+PazLabs.h"

@implementation UIWindow (PazLabs)

- (UIViewController *)visibleController
{
    UIViewController * rootViewController = self.rootViewController;
    return [UIWindow getVisibleViewControllerFrom:rootViewController];
}

+ (UIViewController *) getVisibleViewControllerFrom:(UIViewController *)vc
{
    if ([vc isKindOfClass:[UINavigationController class]])
    {
        return [UIWindow getVisibleViewControllerFrom:[((UINavigationController *) vc) visibleViewController]];
    }
    else if ([vc isKindOfClass:[UITabBarController class]])
    {
        return [UIWindow getVisibleViewControllerFrom:[((UITabBarController *) vc) selectedViewController]];
    }
    else
    {
        if (vc.presentedViewController)
        {
            return [UIWindow getVisibleViewControllerFrom:vc.presentedViewController];
        }
        else
        {
            return vc;
        }
    }
}

@end
