//
//  UIImage+Extension.h
//  OAS
//
//  Created by Eugene Parafiynyk on 11/2/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extension)

- (UIImage *)imageFixOrientation;
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)screenshot;
+ (UIImage *)imageWithImage:(UIImage *)image withText:(NSString *)text;
- (UIImage *)bluredImage;
- (void)bluerdImageWithCompletion:(void(^)(UIImage *bluerdImage))completion;

@end
