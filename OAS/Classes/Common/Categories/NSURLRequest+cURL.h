//
//  NSURLRequest+cURL.m
//  NSURLReqeust-cURL
//
//  Created by Eugene Parafiynyk on 31/07/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLRequest (cURL)

- (NSString *)description;
- (NSString *)printedCurl;

@end
