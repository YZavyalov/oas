//
//  NSError+SWGResponseObject.m
//  OAS
//
//  Created by Eugene Parafiynyk on 1/12/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NSError+SWGResponseObject.h"

@implementation NSError (SWGResponseObject)

- (NSString *)swaggerDescription
{
    if (self.userInfo[@"SLResponseObject"])
    {
        if ([self.userInfo[@"SLResponseObject"] isKindOfClass:[NSDictionary class]])
        {
            return self.userInfo[@"SLResponseObject"][@"message"];
        }
        else if ([self.userInfo[@"SLResponseObject"] isKindOfClass:[NSData class]])
        {
            NSDictionary * userInfo = [[[NSString alloc] initWithData:self.userInfo[@"SLResponseObject"] encoding:NSUTF8StringEncoding] objectFromJSONString];
            if ([userInfo isKindOfClass:[NSDictionary class]])
            {
                if (userInfo[@"message"])
                {
                    return userInfo[@"message"];
                }
                else if (userInfo[@"msg"])
                {
                    return userInfo[@"msg"];
                }
            }
        }
    }
    
    return self.localizedDescription;
}

- (NSInteger)swaggerCode
{
    if (self.userInfo[@"SLResponseObject"])
    {
        if ([self.userInfo[@"SLResponseObject"] isKindOfClass:[NSDictionary class]])
        {
            return [self.userInfo[@"SLResponseObject"][@"status"] integerValue];
        }
        else if ([self.userInfo[@"SLResponseObject"] isKindOfClass:[NSData class]])
        {
            NSDictionary * userInfo = [[[NSString alloc] initWithData:self.userInfo[@"SLResponseObject"] encoding:0] objectFromJSONString];
            if ([userInfo isKindOfClass:[NSDictionary class]])
            {
                if (userInfo[@"code"])
                {
                    return [userInfo[@"code"] integerValue];
                }
            }
        }
    }
    
    return self.code;
}

@end
