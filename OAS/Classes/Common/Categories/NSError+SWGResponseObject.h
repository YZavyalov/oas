//
//  NSError+SWGResponseObject.h
//  OAS
//
//  Created by Eugene Parafiynyk on 1/12/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (SWGResponseObject)

- (NSString *)swaggerDescription;
- (NSInteger)swaggerCode;

@end
