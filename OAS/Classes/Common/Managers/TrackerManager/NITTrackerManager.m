//
//  NITTrackerManager.m
//  OAS
//
//  Created by Eugene Parafiynyk on 6/19/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITTrackerManager.h"
#import <YandexMobileMetrica/YandexMobileMetrica.h>
#import <Google/Analytics.h>

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@interface NITTrackerManager ()

@end

@implementation NITTrackerManager

+ (id)sharedInstance
{
    static dispatch_once_t once;
    static NITTrackerManager * shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id)init
{
    if (self = [super init])
    {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    
}

#pragma mark - Events
- (void)trackEvent:(TrackerEvent)eventType
{
    [self trackEvent:eventType value:nil];
}

- (void)trackEvent:(TrackerEvent)eventType value:(NSString *)value
{
    NITTrackerEvent * event = [NITTrackerEvent trackerEventByType:eventType label:value];
    
//    [self sendGoogleAnalyticsEvent:event];
//    [self sendYandexMetricaEvent:event];
    
    [HELPER logString:[NSString stringWithFormat:@"Send event: %@", event.action]];
}

- (void)trackShowScreenEvent:(TrackerShowScreenEvent)eventType
{
    NSString * eventName = [self showScreenEventByType:eventType];
    
//    [self sendGoogleAnalyticsShowScreenEvent:eventName];
//    [self sendYandexMetricaShowScreenEvent:eventName];
    
    [HELPER logString:[NSString stringWithFormat:@"Send show screen event: %@ ", eventName]];
}

#pragma mark - YandexMetrica
/**
 *  run in Appdelegate initialize
 */
- (void)initYandexMetrica
{
//    [YMMYandexMetrica activateWithApiKey:@"6736d5fa-34c5-4ca7-9a8b-b0be9d055961"];
//    [YMMYandexMetrica setReportCrashesEnabled:false];
//    [YMMYandexMetrica setTrackLocationEnabled:false];
}

- (void)sendYandexMetricaEvent:(NITTrackerEvent *)event
{
    [YMMYandexMetrica reportEvent:event.action
                       parameters:nil
                        onFailure:^(NSError *error) {
                            DDLogError(@"report event error: %@", [error localizedDescription]);
                        }];
}

- (void)sendYandexMetricaShowScreenEvent:(NSString *)eventName
{
    [YMMYandexMetrica reportEvent:eventName
                       parameters:nil
                        onFailure:^(NSError *error) {
                            DDLogError(@"report event error: %@", [error localizedDescription]);
                        }];
}

#pragma mark - Google Analytics
- (void)initGoogleAnalytics
{
//    NSError * configureError;
//    [[GGLContext sharedInstance] configureWithError:&configureError];
//    
//#if DEBUG
//    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
//#endif
//    
//    // Optional: configure GAI options.
//    GAI * gai = [GAI sharedInstance];
//    gai.trackUncaughtExceptions = false;  // report uncaught exceptions
//    gai.logger.logLevel = kGAILogLevelWarning;  // remove before app release
}

- (void)sendGoogleAnalyticsEvent:(NITTrackerEvent *)event
{
    // May return nil if a tracker has not already been initialized with a property
    // ID.
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    GAIDictionaryBuilder * builder = [GAIDictionaryBuilder createEventWithCategory:event.category
                                                                            action:event.action
                                                                             label:event.label
                                                                             value:event.value];
    [tracker send:[builder build]];
}

- (void)sendGoogleAnalyticsShowScreenEvent:(NSString *)eventName
{
    // May return nil if a tracker has not already been initialized with a property
    // ID.
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    GAIDictionaryBuilder * builder = [GAIDictionaryBuilder createEventWithCategory:@"show_screen"
                                                                            action:eventName
                                                                             label:nil
                                                                             value:nil];
    [tracker send:[builder build]];
}

#pragma mark - Private
- (NSString *)showScreenEventByType:(TrackerShowScreenEvent)eventType
{
    switch (eventType) {
        case TrackerShowScreenEventLocationRequest:         return @"showGeoRequest";
        case TrackerShowScreenEventGuide:                   return @"showGuide";
        case TrackerShowScreenEventSelectCity:              return @"showSelectCity";
        case TrackerShowScreenEventCatalog:                 return @"showCatalog";
        case TrackerShowScreenEventCatalogSection:          return @"showCategory";
        case TrackerShowScreenEventCatalogDirectory:        return @"showSection";
        case TrackerShowScreenEventSorting:                 return @"showSectionSort";
        case TrackerShowScreenEventFilters:                 return @"showFilter";
        case TrackerShowScreenEventFilterOption:            return @"showFilterOptions";
        case TrackerShowScreenEventProductDetails:          return @"showProduct";
        case TrackerShowScreenEventPharmaciesList:          return @"showAvailabilityList";
        case TrackerShowScreenEventPharmaciesListSort:      return @"showAvailabilityListSort";
        case TrackerShowScreenEventAvailabilityMap:         return @"showAvailabilityMap";
        case TrackerShowScreenEventOtherForms:              return @"showContiguous";
        case TrackerShowScreenEventQuickSearch:             return @"showQuickSearch";
        case TrackerShowScreenEventSearch:                  return @"showSearch";
        case TrackerShowScreenEventDiscountCardGuide:       return @"showDiscountGuide";
        case TrackerShowScreenEventRegisterCard:            return @"showDiscountCardNumber";
        case TrackerShowScreenEventAttachPhoneToCard:       return @"showDiscountPhoneNumber";
        case TrackerShowScreenEventConfirmnAttachCardBySMS: return @"showDiscountSms";
        case TrackerShowScreenEventDiscountCard:            return @"showDiscount";
        case TrackerShowScreenEventFavorite:                return @"showFavorites";
        case TrackerShowScreenEventPharmacies:              return @"showShopsList";
        case TrackerShowScreenEventPharmaciesOnMap:         return @"showShopsMap";
        case TrackerShowScreenEventPharmaciesSort:          return @"showShopsListSort";
        case TrackerShowScreenEventFeedback:                return @"showFeedback";
        case TrackerShowScreenEventBasket:                  return @"showPushcart";
        case TrackerShowScreenEventCurrentOrders:           return @"showOrderList";
        case TrackerShowScreenEventOrderDetails:            return @"showOrderDetails";
        case TrackerShowScreenEventDelivery:                return @"showDelivery";
        case TrackerShowScreenEventPickup:                  return @"showPickUp";
        case TrackerShowScreenEventSelectPharmacy:          return @"showSelectShopList";
        case TrackerShowScreenEventSelectPharmacySort:      return @"showSelectShopListSort";
        case TrackerShowScreenEventSelectPharmacyOnMap:     return @"showSelectShopMap";
    }
}

@end

@implementation NITTrackerEvent

+ (NITTrackerEvent *)trackerEventByType:(TrackerEvent)type label:(NSString *)label
{
    NITTrackerEvent * event = [NITTrackerEvent new];
    event.label = label;
    
    switch (type) {
        case TrackerEventAddToBasket: {
            event.category = @"catalog";
            event.action = @"ProductAction.ACTION_ADD";
            event.slAction = @"addedCatalogItemsToOrder";
            event.value = nil;
        } break;
            
        case TrackerEventOpenStock: {
            event.category = @"infoSystem";
            event.action = @"showInfoSystemItem";
            event.slAction = @"showInfosystemItem";
            event.value = @(1);
        } break;
            
        case TrackerEventPlacePickupOrder: {
            event.category = @"catalog";
            event.action = @"clickCheckoutPickUp";
            event.slAction = @"clickCheckoutPickUp";
            event.value = @(1);
        } break;
            
        case TrackerEventPlaceDeliveryOrder: {
            event.category = @"catalog";
            event.action = @"clickCheckoutDelivery";
            event.slAction = @"clickCheckoutDelivery";
            event.value = @(1);
        } break;
            
        case TrackerEventAddToFavorite: {
            event.category = @"catalog";
            event.action = @"addToFavorites";
            event.slAction = @"addedCatalogItemToFavorites";
            event.value = nil;
        } break;
            
        case TrackerEventSelectFilter: {
            event.category = @"catalog";
            event.action = @"clickFilter";
            event.slAction = @"clickFilter";
            event.value = @(1);
        } break;
    }
    
    return event;
}

@end
