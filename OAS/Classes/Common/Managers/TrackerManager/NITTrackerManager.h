//
//  NITTrackerManager.h
//  OAS
//
//  Created by Eugene Parafiynyk on 6/19/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#define TRACKER [NITTrackerManager sharedInstance]

typedef CF_ENUM (NSUInteger, TrackerEvent) {
    TrackerEventAddToBasket         = 0,    // Какие товары и как часто добавляют в корзину
    TrackerEventOpenStock           = 1,    // Количество кликов на акции
    TrackerEventPlacePickupOrder    = 2,    // Количество кликов на кнопку «Оформить заказ» в самовывозе
    TrackerEventPlaceDeliveryOrder  = 3,    // Количество кликов на кнопку «Оформить заказ» в доставке
    TrackerEventAddToFavorite       = 4,    // Товары, которые добавляют в избранное
    TrackerEventSelectFilter        = 5     // Клики на настройки фильтра (чтобы понять, какими фильтрами чаще всего пользуются)
};

typedef CF_ENUM (NSUInteger, TrackerShowScreenEvent) {
    TrackerShowScreenEventLocationRequest           = 0,    // 0.2  Запрос геолокации
    TrackerShowScreenEventGuide                     = 1,    // 0.3  Гид
    TrackerShowScreenEventSelectCity                = 2,    // 0.4-0.5  Выбор города
    TrackerShowScreenEventCatalog                   = 3,    // 1.1  Каталог
    TrackerShowScreenEventCatalogSection            = 4,    // 1.2  Выбор раздела
    TrackerShowScreenEventCatalogDirectory          = 5,    // 1.3  Внутри подраздела
    TrackerShowScreenEventSorting                   = 6,    // 1.4  Сортировка
    TrackerShowScreenEventFilters                   = 7,    // 1.5  Фильтр
    TrackerShowScreenEventFilterOption              = 8,    // 1.6  Выбор опций фильтра
    TrackerShowScreenEventProductDetails            = 9,    // 1.7, 1.11-1.13  Карточка товара (+рецепт, +под заказ, +без других форм выпуска)
    TrackerShowScreenEventPharmaciesList            = 10,   // 1.14 Наличие (список аптек)
    TrackerShowScreenEventPharmaciesListSort        = 11,   // 1.14  Наличие (список аптек)
    TrackerShowScreenEventAvailabilityMap           = 12,   // 1.16	Наличие (карта)
    TrackerShowScreenEventOtherForms                = 13,   // 1.17	Другие формы выпуска
    TrackerShowScreenEventQuickSearch               = 14,   // 1.18  Быстрый поиск (варианты товаров по мере ввода)
    TrackerShowScreenEventSearch                    = 15,   // 1.19-1.20  Поиск, Ничего не найдено
    TrackerShowScreenEventDiscountCardGuide         = 16,   // 2.1  Дисконтная карта (гид)
    TrackerShowScreenEventRegisterCard              = 17,   // 2.2  Регистрация карты (ввод номера карты)
    TrackerShowScreenEventAttachPhoneToCard         = 18,   // 2.3  Привязка номера к карте
    TrackerShowScreenEventConfirmnAttachCardBySMS   = 19,   // 2.4, 2.6  Подтверждение СМС
    TrackerShowScreenEventDiscountCard              = 20,   // 2.5  Дисконтная карта
    TrackerShowScreenEventFavorite                  = 21,   // 3.1, 3.4  Избранное, нет избранного
    TrackerShowScreenEventPharmacies                = 22,   // 4.1  Аптеки (список)
    TrackerShowScreenEventPharmaciesOnMap           = 23,   // 4.2, 4.3  Аптека на карте
    TrackerShowScreenEventPharmaciesSort            = 24,   // 4.5  Сортировка аптек
    TrackerShowScreenEventFeedback                  = 25,   // 4.6  Обратная связь
    TrackerShowScreenEventBasket                    = 26,   // 5.1, 5.2, 5.6-5.8, 5.11  Корзина
    TrackerShowScreenEventCurrentOrders             = 27,   // 5.9, 5.12, 5.13  Текущие заказы (+заказов нет)
    TrackerShowScreenEventOrderDetails              = 28,   // 5.10  Товары в заказе
    TrackerShowScreenEventDelivery                  = 29,   // 5.14, 5.16  Доставка
    TrackerShowScreenEventPickup                    = 30,   // 5.17, 5.19, 5.23  Самовывоз (+аптека выбрана)
    TrackerShowScreenEventSelectPharmacy            = 31,   // 5.18  Выбор аптеки (список)
    TrackerShowScreenEventSelectPharmacySort        = 32,   // 5.21  Сортировка аптек
    TrackerShowScreenEventSelectPharmacyOnMap       = 33    // 5.22  Выбор аптек (карта)
};

@interface NITTrackerManager : NSObject

+ (id)sharedInstance;

#pragma mark - Events
- (void)trackEvent:(TrackerEvent)eventType;
- (void)trackEvent:(TrackerEvent)eventType value:(NSString *)value;
- (void)trackShowScreenEvent:(TrackerShowScreenEvent)eventType;

#pragma mark - YandexMetrica
- (void)initYandexMetrica;

#pragma mark - Google Analytics
- (void)initGoogleAnalytics;

@end

@interface NITTrackerEvent : NSObject

@property (nonatomic) NSString * category;
@property (nonatomic) NSString * action;
@property (nonatomic) NSString * slAction;
@property (nonatomic) NSString * label;
@property (nonatomic) NSNumber * value;

+ (NITTrackerEvent *)trackerEventByType:(TrackerEvent)type label:(NSString *)label;

@end
