//
//  NITActivityIndicatorAnimationProtocol.h
//  
//
//  Created by Yaroslav on 05.06.17.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol NITActivityIndicatorAnimationProtocol <NSObject>

- (void)setupAnimationInLayer:(CALayer *)layer withSize:(CGSize)size tintColor:(UIColor *)tintColor;

@end
