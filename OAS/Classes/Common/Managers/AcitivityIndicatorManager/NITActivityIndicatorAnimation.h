//
//  NITActivityIndicatorAnimation.h
//  OAS
//
//  Created by Yaroslav on 05.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITActivityIndicatorAnimationProtocol.h"

@interface NITActivityIndicatorAnimation : NSObject <NITActivityIndicatorAnimationProtocol>

- (CABasicAnimation *)createBasicAnimationWithKeyPath:(NSString *)keyPath;
- (CAKeyframeAnimation *)createKeyframeAnimationWithKeyPath:(NSString *)keyPath;
- (CAAnimationGroup *)createAnimationGroup;

@end
