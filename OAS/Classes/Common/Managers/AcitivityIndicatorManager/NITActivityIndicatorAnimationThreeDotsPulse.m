//
//  NITActivityIndicatorAnimationThreeDotsPulse.m
//  OAS
//
//  Created by Yaroslav on 05.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITActivityIndicatorAnimationThreeDotsPulse.h"

@implementation NITActivityIndicatorAnimationThreeDotsPulse

- (void)setupAnimationInLayer:(CALayer *)layer withSize:(CGSize)size tintColor:(UIColor *)tintColor {
    CGFloat duration = 1.0f;
    NSArray *beginTimes = @[@0.0f, @0.4f, @0.5f, @0.6f, @1.0f];
    CGFloat circleSpacing = (size.width - 15.0f * 3) / 2;
    CGFloat circleSize = 15.0f;
    CGFloat x = (layer.bounds.size.width - size.width) / 2;
    CGFloat y = (layer.bounds.size.height - circleSize) / 2;
    
    CGFloat deltaX = (size.width - circleSpacing) / 2;
    CAMediaTimingFunction *timingFunciton = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    // Scale animation
    CAKeyframeAnimation *scaleAnimationCenter = [self createKeyframeAnimationWithKeyPath:@"transform.scale"];
    scaleAnimationCenter.duration = duration;
    scaleAnimationCenter.keyTimes = beginTimes;
    scaleAnimationCenter.values = @[@1.0f, @1.0f, @1.8f, @1.0f, @1.0f];
    
    // Aniamtion
    CAAnimationGroup *animationCenter = [self createAnimationGroup];;
    animationCenter.duration = duration;
    animationCenter.animations = @[scaleAnimationCenter];
    animationCenter.repeatCount = HUGE_VALF;
    animationCenter.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];

    CAKeyframeAnimation *animationLeft = [self createKeyframeAnimationWithKeyPath:@"transform.translation.x"];
    animationLeft.duration = duration;
    animationLeft.keyTimes = beginTimes;
    animationLeft.values = @[@0.0f, @(deltaX),@(deltaX), @(deltaX), @0.0f];
    animationLeft.timingFunctions = @[timingFunciton, timingFunciton, timingFunciton];
    animationLeft.repeatCount = HUGE_VALF;
    
    CAKeyframeAnimation *animationRight = [self createKeyframeAnimationWithKeyPath:@"transform.translation.x"];
    animationRight.duration = duration;
    animationRight.keyTimes = beginTimes;
    animationRight.values = @[@0.0f, @(-deltaX),@(-deltaX), @(-deltaX), @0.0f];
    animationRight.timingFunctions = @[timingFunciton, timingFunciton, timingFunciton];
    animationRight.repeatCount = HUGE_VALF;

    
    // Draw circles
    for (int i = 0; i < 3; i++) {
        CAShapeLayer *circle = [CAShapeLayer layer];
        UIBezierPath *circlePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, circleSize, circleSize) cornerRadius:circleSize / 2];
        circle.fillColor = tintColor.CGColor;
        circle.path = circlePath.CGPath;
        animationCenter.beginTime = [beginTimes[i] floatValue];
        
        if(i == 1)
        {
            [circle addAnimation:animationCenter forKey:@"animation"];
        }
        if(i == 0)
        {
            [circle addAnimation:animationLeft forKey:@"animation"];
        }
        if(i == 2)
        {
            [circle addAnimation:animationRight forKey:@"animation"];
        }
        
        circle.frame = CGRectMake(x + circleSize * i + circleSpacing * i, y, circleSize, circleSize);
        [layer addSublayer:circle];
    }
}

@end
