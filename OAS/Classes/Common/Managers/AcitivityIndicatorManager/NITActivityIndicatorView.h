//
//  NITActivityIndicatorView.h
//  OAS
//
//  Created by Yaroslav on 05.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, NITActivityIndicatorAnimationType) {
    NITActivityIndicatorAnimationTypeTrheeDotsPulse
};

@interface NITActivityIndicatorView : UIView

- (id)initWithType:(NITActivityIndicatorAnimationType)type;
- (id)initWithType:(NITActivityIndicatorAnimationType)type tintColor:(UIColor *)tintColor;
- (id)initWithType:(NITActivityIndicatorAnimationType)type tintColor:(UIColor *)tintColor size:(CGFloat)size;

@property (nonatomic) NITActivityIndicatorAnimationType type;
@property (nonatomic, strong) UIColor *tintColor;
@property (nonatomic) CGFloat size;

@property (nonatomic, readonly) BOOL animating;

- (void)startAnimating;
- (void)stopAnimating;

@end
