//
//  NITNotificationConstants.h
//  OAS
//
//  Created by Yaroslav on 14.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#pragma mark - Key
static NSString * const kShopIDs        = @"kShopIDs";
static NSString * const kItemCount      = @"kItemCount";
static NSString * const kMinPriceOrder  = @"kMinPriceOrder";
static NSString * const kFreeDelivery   = @"kFreeDelivery";
static NSString * const kMaxCountItem   = @"kMaxCountItem";
static NSString * const kMinPriceBasket = @"kMinPriceBasket";

