//
//  NITNotificationManager.m
//  OAS
//
//  Created by Yaroslav on 14.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITNotificationManager.h"
#import "SLShopsApi.h"
#import "SLCatalogsApi.h"
#import "SLShopsResponse.h"
#import "SLCatalogItemInShop.h"
#import "SLCatalogItemInShopQuery.h"
#import "NITBasketItem.h"
#import "NITBasketItem.h"
#import "NITNotificationView.h"
#import "NITMobileConstantsModel.h"

@interface NITNotificationManager ()

@property (nonatomic) NSTimer * timer;
@property (nonatomic) BOOL needShow;

@end

@implementation NITNotificationManager

#pragma mark - Life cycle
+ (NITNotificationManager *)sharedInstance
{
    static dispatch_once_t	once;
    static NITNotificationManager *shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    [self addObservers];
    [self clearNoptificationData];
}

#pragma mark - Custom accesstors
- (NSArray <NSString *> *)shopIDs
{
    return [self arrayForKey:kShopIDs];
}

- (void)setShopIDs:(NSArray<NSString *> *)shopIDs
{
    [self setArray:shopIDs forKey:kShopIDs];
}

- (NSMutableDictionary *)item_maxCountAndisShow
{
    return [[self dictionaryForKey:kItemCount] mutableCopy];
}

- (void)setItem_maxCountAndisShow:(NSMutableDictionary *)item_maxCountAndisShow
{
    [self setDictionary:[item_maxCountAndisShow mutableCopy] forKey:kItemCount];
}

- (BOOL)minPriceOrder
{
    return [self boolForKey:kMinPriceOrder];
}

- (void)setMinPriceOrder:(BOOL)minPriceOrder
{
    [self setBool:minPriceOrder forKey:kMinPriceOrder];
}

- (BOOL)freeDelivery
{
    return [self boolForKey:kFreeDelivery];
}

- (void)setFreeDelivery:(BOOL)freeDelivery
{
    [self setBool:freeDelivery forKey:kFreeDelivery];
}

- (BOOL)minPriceBasket
{
    return [self boolForKey:kMinPriceBasket];
}

- (void)setMinPriceBasket:(BOOL)minPriceBasket
{
    [self setBool:minPriceBasket forKey:kMinPriceBasket];
}

#pragma mark - Public
- (void)showNotificationByItem:(NITBasketItem *)item
{
    if(!self.needShow)
    {
        [self startTimer];
        
        float basketPrice = 0;
        NSArray <NITCatalogItemModel *> * basketItems = [self catalogItemsModelsFromBasket:[self getBasketItems]];
        for(NITCatalogItemModel * basketItem in basketItems)
        {
            basketPrice += [basketItem.price floatValue] * basketItem.count;
        }
        
        if(!self.minPriceOrder && (basketPrice < 500))
        {
            self.minPriceOrder = true;
            [NITNotificationView showWithText:[NSString stringWithFormat:@"Товар добавлен в корзину.\nМинимальная сумма онлайн-заказа %@ ₽.", [[NITMobileConstantsModel new] min_sum_of_order]] tapHandler:^{
            }];
        }
        else if(!self.freeDelivery && (basketPrice < 1500))
        {
            self.freeDelivery = true;
            [NITNotificationView showWithText:[NSString stringWithFormat:@"Вы добавили продукцию на сумму %.2f ₽.\nБесплатная доставка от %@ ₽.", basketPrice, [[NITMobileConstantsModel new] free_delivery]] tapHandler:^{
            }];
        }
        else if(item.count.integerValue == item.maxCount.integerValue && item)
        {
            NSMutableDictionary * dict = [NSMutableDictionary new];
            [dict setDictionary:self.item_maxCountAndisShow];
            //if(![dict objectForKey:item.identifier])
            //{
                [dict setValue:@"True" forKey:item.identifier];
                self.item_maxCountAndisShow = dict;
                [NITNotificationView showWithText:NSLocalizedString(@"Достигнуто максимальное количество товара.", nil) tapHandler:^{
                }];
            //}
        }
    }
}

- (void)showMinPriceBasket
{
    if(!self.needShow)
    {
        [self startTimer];
        
        float basketPrice = 0;
        NSArray <NITCatalogItemModel *> * basketItems = [self catalogItemsModelsFromBasket:[self getBasketItems]];
        for(NITCatalogItemModel * basketItem in basketItems)
        {
            basketPrice += [basketItem.price floatValue] * basketItem.count;
        }
        
        if((basketPrice < 500))
        {
            //self.minPriceBasket = true;
            [NITNotificationView showWithText:[NSString stringWithFormat:@"Минимальная сумма онлайн-заказа %@ ₽.", [[NITMobileConstantsModel new] min_sum_of_order]] tapHandler:^{
            }];
        }
    }
}

#pragma mark - Private
- (void)clearNoptificationData
{
    self.minPriceOrder = false;
    self.freeDelivery = false;
    self.minPriceBasket = false;
    self.item_maxCountAndisShow = nil;
}

- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearNotificationMaxCount) name:kChangeCityNotification object:nil];
}

- (void)clearNotificationMaxCount
{
    self.item_maxCountAndisShow = nil;
}

- (NSArray <NITBasketItem *> *)getBasketItems
{
    return [NITBasketItem allBasketItems];
}

- (NSArray <NITCatalogItemModel *> *)catalogItemsModelsFromBasket:(NSArray <NITBasketItem *> *)entities
{
    return [entities bk_map:^id(NITBasketItem * obj) {
        return [[NITCatalogItemModel alloc] initWithBasketModel:obj];
    }];
}

#pragma mark - Timer
- (void)startTimer
{
    self.needShow = true;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:3.0f
                                                  target:self
                                                selector:@selector(timerTick:)
                                                userInfo:nil
                                                 repeats:NO];
}

- (void)stopTimer
{
    if (self.timer)
    {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)timerTick:(NSTimer*)timer
{
    self.needShow = false;
}

@end
