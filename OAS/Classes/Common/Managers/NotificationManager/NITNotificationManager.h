//
//  NITNotificationManager.h
//  OAS
//
//  Created by Yaroslav on 14.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITNotificationConstants.h"

#define NOTIFICATION [NITNotificationManager sharedInstance]

@class NITBasketItem;

@interface NITNotificationManager : NITLocalSettings

@property (nonatomic) NSArray <NSString *> * shopIDs;
@property (nonatomic) NSMutableDictionary * item_maxCountAndisShow;
@property (nonatomic) BOOL minPriceOrder;
@property (nonatomic) BOOL freeDelivery;
@property (nonatomic) BOOL minPriceBasket;

+ (NITNotificationManager *)sharedInstance;

#pragma mark - Clear notificaiton data
- (void)clearNoptificationData;

#pragma mark - Show notification
- (void)showNotificationByItem:(NITBasketItem *)item;
- (void)showMinPriceBasket;

@end
