//
//  NITHelperManager.m
//  OAS
//
//  Created by Eugene Parafiynyk on 10/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITHelperManager.h"
#import "DGActivityIndicatorView.h"
#import "MSAlertController.h"
#import "SLInfoItem.h"
#import "SLGroup.h"
#import "SLCatalogItem.h"

#import "NITActivityIndicatorView.h"

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@interface NITHelperManager ()
{
    NITActivityIndicatorView * hud;
}

@end

@implementation NITHelperManager

+ (NITHelperManager *)sharedInstance
{
    static dispatch_once_t	once;
    static NITHelperManager *shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    
}

#pragma mark - Logs
- (void)logError:(NSError *)error method:(NSString *)method
{
    DDLogError(@"\n❗️Error --> %@ --> message: %@ (code: %@)\n", method, error.swaggerDescription, @(error.swaggerCode));
}

- (void)logString:(NSString *)string
{
    DDLogError(@"\n❇️ %@\n", string);
}

#pragma mark - App state
- (BOOL)isFirstLanch
{
    return ![UserDefaults boolForKey:kFirstLanchKey];
}

- (void)setFirstLanch
{
    [UserDefaults setBool:true forKey:kFirstLanchKey];
}

- (BOOL)geolocationSelected
{
    return [UserDefaults boolForKey:kGeolocationKey];
}

- (void)setGeolocationSelected
{
    [UserDefaults setBool:true forKey:kGeolocationKey];
}

- (BOOL)isDiscountCardBinded
{
    return [UserDefaults boolForKey:kDiscountCardKey];
}

- (void)setDiscountCardBinded
{
    [UserDefaults setBool:true forKey:kDiscountCardKey];
}

- (BOOL)isSendPushToken
{
    return [UserDefaults boolForKey:kCreatePushTokenNotification];
}

- (void)setSendPushToken
{
    [UserDefaults setBool:true forKey:kCreatePushTokenNotification];
}

#pragma mark - Language
- (NSString *)lang
{
    return [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
}

#pragma mark - 3D touch
- (BOOL)forceTouchAvailable
{
    if ([[WINDOW visibleController] respondsToSelector:@selector(traitCollection)] &&
        [[WINDOW visibleController].traitCollection respondsToSelector:@selector(forceTouchCapability)] &&
        ([WINDOW visibleController].traitCollection.forceTouchCapability == UIForceTouchCapabilityAvailable))
    {
        return true;
    }
    
        return false;
}


#pragma mark - UUID
- (NSString *)vendorUDID
{
    return [[UIDevice currentDevice] identifierForVendor].UUIDString;
}

- (NSString *)vendorFormatedUDID
{
    NSString * notFormatedUDID = [self vendorUDID];
    NSString * formatedUDID = [notFormatedUDID stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    return formatedUDID;
}

- (NSString *)generateUUID
{
    return [[[NSUUID UUID] UUIDString] stringByReplacingOccurrencesOfString:@"-" withString:@""];
}

#pragma mark - Loading
- (void)startLoading
{
    if (!hud && self.interfaceLoaded)
    {
        hud = [[NITActivityIndicatorView alloc] initWithType:NITActivityIndicatorAnimationTypeTrheeDotsPulse tintColor:RGB(58, 160, 144) size:75.0f];
        hud.center = WINDOW.center;
        [WINDOW addSubview:hud];
        [hud startAnimating];
    }

    ShowNetworkActivityIndicator();
}

- (void)stopLoading
{
    if (hud)
    {
        [hud stopAnimating];
        hud = nil;
    }
    
    HideNetworkActivityIndicator();
}

#pragma mark - Phone
- (void)callToPhoneNumber:(NSString *)phoneNumber
{
    NSURL * phoneUrl = [self phoneUrlFromText:phoneNumber];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

- (NSURL *)phoneUrlFromText:(NSString *)text
{
    NSString * phoneNumber = [[text componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""];
    NSURL * phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", phoneNumber]];
    
    return phoneUrl;
}

#pragma mark - Share application
- (void)shareApplication
{
    NSString * shareString = NSLocalizedString(@"Рекомендую приложение OAS\n", nil);
    NSURL * shareURL = [NSURL URLWithString:kShareAppURL];
    NSArray * shareArray = @[shareString, shareURL];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:shareArray
                                                                                         applicationActivities:nil];
    
    NSArray * activityCategoryAction = @[UIActivityTypePrint,
                                              UIActivityTypeCopyToPasteboard,
                                              UIActivityTypeAssignToContact,
                                              UIActivityTypeSaveToCameraRoll,
                                              UIActivityTypeAddToReadingList,
                                              UIActivityTypeAirDrop];
    
    NSArray * activitycategoryShare = @[UIActivityTypeMessage,
                                             UIActivityTypeMail,
                                             UIActivityTypePostToFacebook,
                                             UIActivityTypePostToTwitter,
                                             UIActivityTypePostToFlickr,
                                             UIActivityTypePostToVimeo,
                                             UIActivityTypePostToTencentWeibo,
                                             UIActivityTypePostToWeibo];
    
    NSArray * fullActivitiesArray = [[NSArray alloc] initWithObjects:activityCategoryAction, activitycategoryShare, nil];
    
    activityViewController.excludedActivityTypes = fullActivitiesArray;
    
    [WINDOW.visibleController presentViewController:activityViewController animated:YES completion:^{
        //With nil or adding something
    }];
}

#pragma mark - ActionSheet
- (void)showActionSheetFromView:(UIViewController *)view
                      withTitle:(NSString *)title
                  actionButtons:(NSArray <NITActionButton *> *)actionButtons
                   cancelButton:(NITActionButton *)cancelButton
{
    MSAlertController * actionSheetController = [MSAlertController alertControllerWithTitle:nil
                                                                                    message:title
                                                                             preferredStyle:MSAlertControllerStyleActionSheet];
    actionSheetController.messageColor = RGB(135.0f, 142.0f, 141.0f);
    actionSheetController.messageFont = [UIFont systemFontOfSize:13.0f];
    
    actionSheetController.enabledBlurEffect = true;
    
    if (cancelButton)
    {
        MSAlertAction * cancelAction = [MSAlertAction actionWithTitle:cancelButton.title
                                                                style:MSAlertActionStyleCancel
                                                              handler:^(MSAlertAction * action) {
                                                                  cancelButton.actionBlock();
                                                              }];
        cancelAction.titleColor = RGBA(50, 160, 144, 1);
        [actionSheetController addAction:cancelAction];
    }
    
    for (NITActionButton * actionButton in actionButtons)
    {
        MSAlertAction * action = [MSAlertAction actionWithTitle:actionButton.title
                                                          style:MSAlertActionStyleDefault
                                                        handler:^(MSAlertAction * action) {
                                                            actionButton.actionBlock();
                                                        }];
        
        action.titleColor = RGBA(0, 0, 0, 1);
        [actionSheetController addAction:action];
    }
    
    [view presentViewController:actionSheetController animated:true completion:nil];
}

#pragma mark - AlertSheet
- (void)showAlertSheetFromView:(UIViewController *)view
                     withTitle:(NSString *)title
                actionsButtons:(NSArray <NITActionButton *> *)actionButtons
                  cancelButton:(NITActionButton *)cancelButton
{
    MSAlertController * actionAlertController = [MSAlertController alertControllerWithTitle:nil
                                                                                    message:title
                                                                             preferredStyle:MSAlertControllerStyleAlert];
    
    actionAlertController.messageColor = RGB(135.0f, 142.0f, 141.0f);
    actionAlertController.messageFont = [UIFont systemFontOfSize:13.0f];
    
    actionAlertController.enabledBlurEffect = true;
    
    if (cancelButton)
    {
        MSAlertAction * cancelAction = [MSAlertAction actionWithTitle:cancelButton.title
                                                                style:MSAlertActionStyleCancel
                                                              handler:^(MSAlertAction * action) {
                                                                  cancelButton.actionBlock();
                                                              }];
        cancelAction.titleColor = RGBA(50, 160, 144, 1);
        [actionAlertController addAction:cancelAction];
    }
    
    for (NITActionButton * actionButton in actionButtons)
    {
        MSAlertAction * action = [MSAlertAction actionWithTitle:actionButton.title
                                                          style:MSAlertActionStyleDefault
                                                        handler:^(MSAlertAction * action) {
                                                            actionButton.actionBlock();
                                                        }];
        
        action.titleColor = RGBA(0, 0, 0, 1);
        [actionAlertController addAction:action];
    }
    
    [view presentViewController:actionAlertController animated:true completion:nil];
}

#warning test data
- (NSArray <SLInfoItem *> *)testInfoItems
{
    NSMutableArray * items = [NSMutableArray new];
    NSArray * images = @[@"https://www.zog.ua/images/News/3257189.jpg",
                         @"http://magiccoffee.in.ua/img/source/article-2016/1397570876_akcia.jpg",
                         @"http://st.storeland.net/9/1800/715/.jpg",
                         @"http://red-line.kiev.ua/images/banner.jpg",
                         @"http://static.svyaznoy.ru/upload/kartinki/2909_50sale_101_1075x400.jpg"];
    
    for (int c = 0; c < images.count; c++)
    {
        int i = c + 1;
        
        SLInfoItem * item = [SLInfoItem new];
        item.body = [NSString stringWithFormat:@"Полный текст %@", @(i)];
        item.createdAt = @([[NSDate date] timeIntervalSince1970]);
        item.creator = @(i).stringValue;
        item._description = [NSString stringWithFormat:@"Короткое описание %@", @(i)];
        item._id = @(i).stringValue;
        item.image = images[c];
        item.name = [NSString stringWithFormat:@"Название элемента %@", @(i)];
        item.updatedAt = @([[NSDate date] timeIntervalSince1970]);
        
        [items addObject:item];
    }
    
    return items;
}

- (NSArray <SLGroup *> *)testGroups
{
    NSMutableArray * items = [NSMutableArray new];
    
    for (int i = 1; i < 8; i++)
    {
        SLGroup * item = [SLGroup new];
        item._id = @(i).stringValue;
        item.name = [NSString stringWithFormat:@"Название группы %@", @(i)];
        item.parent = @(i).stringValue;
        item.defaultTemplateId = @(i).stringValue;

        [items addObject:item];
    }
    
    return items;
}

- (NSArray <SLCatalogItem *> *)testCatalogItems
{
    NSMutableArray * items = [NSMutableArray new];
    
    for (int i = 1; i < 10; i++)
    {
        SLCatalogItem * item = [SLCatalogItem new];
        item.body = [NSString stringWithFormat:@"Полный текст %@", @(i)];
        item.createdAt = @([[NSDate date] timeIntervalSince1970]);
        item.creator = @(i).stringValue;
        item._description = [NSString stringWithFormat:@"Короткое описание %@", @(i)];
        item._id = @(i).stringValue;
        item.image = @"http://apteka74.ru/uploads/ProductPhotoGalleryImage/18618/Image/0000.jpg";
        item.name = [NSString stringWithFormat:@"Название %@", @(i)];
        item.price = @(arc4random_uniform(1000));
        item.templateId = @(i).stringValue;
        item.updatedAt = @([[NSDate date] timeIntervalSince1970]);
        //TODO: ошика здесь -> item.groups = (id)[HELPER testGroups];
        
        [items addObject:item];
    }

    return items;
}

@end
