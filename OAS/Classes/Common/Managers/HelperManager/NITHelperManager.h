//
//  NITHelperManager.h
//  OAS
//
//  Created by Eugene Parafiynyk on 10/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITHelperConstants.h"

#define HELPER [NITHelperManager sharedInstance]

@class NITActionButton;
@class SLInfoItem, SLGroup, SLCatalogItem;

@interface NITHelperManager : NSObject

@property (nonatomic) BOOL interfaceLoaded;

+ (NITHelperManager *)sharedInstance;

#pragma mark - Logs
- (void)logError:(NSError *)error method:(NSString *)method;
- (void)logString:(NSString *)string;

#pragma mark - App state
- (BOOL)isFirstLanch;
- (void)setFirstLanch;
- (BOOL)geolocationSelected;
- (void)setGeolocationSelected;
- (BOOL)isDiscountCardBinded;
- (void)setDiscountCardBinded;
- (BOOL)isSendPushToken;
- (void)setSendPushToken;

#pragma mark - 3D touch
- (BOOL)forceTouchAvailable;

#pragma mark - Language
- (NSString *)lang;

#pragma mark - UUID
- (NSString *)vendorUDID;
- (NSString *)vendorFormatedUDID;
- (NSString *)generateUUID;

#pragma mark - Loading
- (void)startLoading;
- (void)stopLoading;

#pragma mark - Phone
- (void)callToPhoneNumber:(NSString *)phoneNumber;

#pragma mark - Share Application
- (void)shareApplication;

#pragma mark - ActionSheet
- (void)showActionSheetFromView:(id)view
                      withTitle:(NSString *)title
                  actionButtons:(NSArray <NITActionButton *> *)actionButtons
                   cancelButton:(NITActionButton *)cancelButton;

#pragma mark - AlertSheet
- (void)showAlertSheetFromView:(id)view
                     withTitle:(NSString *)title
                actionsButtons:(NSArray <NITActionButton *> *)actionButtons
                  cancelButton:(NITActionButton *)cancelButton;

#warning test data
- (NSArray <SLInfoItem *> *)testInfoItems;
- (NSArray <SLGroup *> *)testGroups;
- (NSArray <SLCatalogItem *> *)testCatalogItems;

@end
