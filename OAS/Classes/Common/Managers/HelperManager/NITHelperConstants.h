//
//  NITHelperConstants.h
//  OAS
//
//  Created by Eugene Parafiynyk on 10/20/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#pragma mark - API Keys
static NSString * const kGoogleAPIKey = @"AIzaSyDOj2skci6rMZB8qW1ati6kgdvKLyC6Lg8";

#pragma mark - Notifications
static NSString * const kUpdateInternetConnect          = @"kUpdateInternetConnect";
static NSString * const kChangeCityNotification         = @"kChangeCityNotification";
static NSString * const kShowSuccessOrderAlert          = @"success_order_notification";
static NSString * const kShowSuccessPickupOrderAlert    = @"success_pickup_order_notification";
static NSString * const kChangeBasketNotification       = @"kChangeBasketNotification";
static NSString * const kUpdateDiscountCardNotification = @"kUpdateDiscountCardNotification";
static NSString * const kUpdateCityNotification         = @"kUpdateCityNotification";
static NSString * const kUpdatePharmacyNotification     = @"kUpdatePharmacyNotification";
static NSString * const kUpdateFilterNotification       = @"kUpdateFilterNotification";
static NSString * const kCreateFilterNotification       = @"kCreateFilterNotification";
static NSString * const kCreateFilterCityNotification   = @"kCreateFilterCityNotification";
static NSString * const kCreateFilterGenformNotification = @"kCreateFilterGenformNotification";
static NSString * const kCreatePushTokenNotification    = @"kCreatePushTokenNotification";
static NSString * const kUpdateStockNotification        = @"kUpdateStockNotification";
static NSString * const kUpdateDeliveryTimeNotification = @"kUpdateDeliveryTimeNotification";

static NSString * const kEnableScrollBanners            = @"kEnableScrollBanners";
static NSString * const kDisableScrollBanners           = @"kdisableScrollBanners";
static NSString * const kEnableScrollFavoriteGuide      = @"kEnableScrollFavoriteGuide";

#pragma mark - Keys
static NSString * const kFirstLanchKey      = @"first_lanch_key";
static NSString * const kGeolocationKey     = @"geolocation_select_key";
static NSString * const kDiscountCardKey    = @"discount_card_key";
static NSString * const kAuthorizationKey   = @"authorization";

#pragma mark - Values
static NSString * const kShareAppURL    = @"https://oas.ru/";

#pragma mark - LettersNumbers
static NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

#pragma mark - Test
static NSString * kTestAccessToken = @"";

#pragma mark - Static IDs
static NSString * const kProdCompanyID          = @"889a4c32-3f85-4a31-be39-df5e0a55e13e";//@"f79538d8-fda4-451b-b5e6-4b558c22d102";
static NSString * const kProdCatalogGroup       = @"4b95b533-3371-4363-b7b3-8358c0620848";//@"d97c79b6-438a-45ae-bbea-24e9bbc4ca0d";
