//
//  NITStockManager.m
//  OAS
//
//  Created by Yaroslav on 24.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITStockManager.h"
#import "SLInfosystemsApi.h"
#import "NITInfoItemModel.h"

__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@implementation NITStockManager

#pragma mark - Life cycle
+ (NITStockManager *)sharedInstance
{
    static dispatch_once_t once;
    static NITStockManager *shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id)init
{
    self = [super init];
    
    if(self)
    {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    //Default setting
}

#pragma mark - Get stocks
- (void)updateStocks
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void){
        [self getInfosystemItemsWithHandler:^(NSArray<SLInfoItemWithRelations *> *result) {
            if(result.count > 0)
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void){
                    NSArray <NITInfoItemModel *> * infoItems = [NITInfoItemModel stockModelsWithPropertiesfromEntities:result];
                    [infoItems bk_each:^(NITInfoItemModel *obj) {
                        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:obj.imagePath]];
                        obj.image = [UIImage imageWithData:data];
                    }];
                    [self updateStockModels:infoItems];
                });
            }
        }];
    });
}

- (void)getInfosystemItemsWithHandler:(void(^)(NSArray<SLInfoItemWithRelations *> *result))handler
{
    SLInfosystemQuery * query = [SLInfosystemQuery new];
    
    [[SLInfosystemsApi new] postStaticInfosystemsByInfosystemidQuerywithrelationsWithInfosystemId:[API infosystemBannersID] body:query completionHandler:^(NSArray<SLInfoItemWithRelations> *output, NSError *error) {
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler (output);
    }];
}

- (void)updateStockModels:(NSArray<NITInfoItemModel *> *)models
{
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"sort" ascending:true selector:@selector(compare:)];
    models = [models sortedArrayUsingDescriptors:@[sort]];
    
    NSMutableArray <NITInfoItemModel *> * availableModels = [NSMutableArray new];
    NSDate * date = [NSDate date];
    [models bk_each:^(NITInfoItemModel *obj) {
        NSDate * startDate = [NSDate dateWithTimeIntervalSince1970:[obj.start intValue]];
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = 1;
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *endDate = [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate dateWithTimeIntervalSince1970:[obj.end intValue]] options:0];
        BOOL available = [self date:date isBetweenDate:startDate andDate:endDate];
        BOOL active = [obj.active intValue] == 0 ? false : true;
        if(available && active) [availableModels addObject:obj];
    }];
    models = [availableModels mutableCopy];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateStockNotification object:models];
}

- (BOOL)date:(NSDate*)nowdate isBetweenDate:(NSDate*)raceStartDate andDate:(NSDate*)raceEndDate
{
    if ([nowdate compare:raceStartDate] == NSOrderedAscending)
        return NO;
    
    if ([nowdate compare:raceEndDate] == NSOrderedDescending)
        return NO;
    
    return YES;
}

@end
