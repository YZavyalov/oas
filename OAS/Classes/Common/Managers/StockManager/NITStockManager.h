//
//  NITStockManager.h
//  OAS
//
//  Created by Yaroslav on 24.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

#define STOCKS [NITStockManager sharedInstance]

@interface NITStockManager : NSObject

+ (NITStockManager *)sharedInstance;

#pragma mark - Get stocks
- (void)updateStocks;

@end
