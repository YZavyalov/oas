//
//  NITErrorManager.m
//  OAS
//
//  Created by Yaroslav on 23.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITErrorManager.h"
#import "NITMobileConstantsModel.h"

@implementation NITErrorManager

#pragma mark - Life cycle
+ (NITErrorManager *)sharedInstance
{
    static dispatch_once_t	once;
    static NITErrorManager *shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    
}

#pragma mark - Error
- (void)errorMessage:(NITErrorType)type
{
    NSString * stringError;
    
    switch (type)
    {
        case feedback_send_error:                           stringError = NSLocalizedString(@"Что-то пошло не так. Попробуйте отправить ещё раз.", nil);                break;
        case notification_error_with_post_order:            stringError = NSLocalizedString(@"Что-то пошло не так. Попробуйте оформить заказ ещё раз.", nil);           break;
        case notification_card_not_find:                    stringError = NSLocalizedString(@"Не удалось найти вашу карту.", nil);                                      break;
        case notification_error_card:                       stringError = NSLocalizedString(@"Что-то пошло не так. Попробуйте ещё раз.", nil);                          break;
        case notification_error_with_phone_registration:    stringError = NSLocalizedString(@"Не удалось отправить смс на этот номер", nil);                            break;
        case notification_error_with_sms:                   stringError = NSLocalizedString(@"Неправильный код смс", nil);                                              break;
        case error_network:                                 stringError = NSLocalizedString(@"Проблемы с интернетом. Попробуйте ещё раз.", nil);                        break;
        case error_connectivity_unavailable:                stringError = NSLocalizedString(@"Нет подключения к интернету. Проверьте настройки сети.", nil);            break;
        case error_internal_server:                         stringError = NSLocalizedString(@"Мы проводим технические работы. Повторите попытку позже.", nil);          break;
        case error_service_unavailable:                     stringError = NSLocalizedString(@"Мы проводим технические работы. Повторите попытку позже.", nil);          break;
        case error_location_permission_denied:              stringError = NSLocalizedString(@"Чтобы увидеть ближайшие аптеки, разрешите доступ до геолокации", nil);    break;
        case error_with_loading_product:                    stringError = NSLocalizedString(@"Что-то пошло не так, попробуйте ещё раз.", nil);                          break;
        case error_with_loading_favorite:                   stringError = NSLocalizedString(@"Что-то пошло не так, попробуйте ещё раз.", nil);                          break;
        case pick_up_shop_not_change:                       stringError = NSLocalizedString(@"Выберите аптеку, в которую доставят заказ", nil);                         break;
        case change_city_pushcart_clear:                    stringError = NSLocalizedString(@"При изменении города корзина была изменена.", nil);                       break;
        case notification_pushcart_updated:                 stringError = NSLocalizedString(@"Корзина обновлена", nil);                                                 break;
        case error_not_found_maps_app:                      stringError = NSLocalizedString(@"На вашем смартфоне нет навигационных приложений. Маршрут построен в карте приложения.", nil);          break;
        case error_on_splash:                               stringError = NSLocalizedString(@"Мы проводим технические работы. Приложение скоро заработает.", nil);      break;
        case error_location_not_found:                      stringError = NSLocalizedString(@"Не удалось определить местоположение", nil);                              break;
        case error_connectivity_unavailable_on_splash:      stringError = NSLocalizedString(@"Нет подключения к интернету. Проверьте настройки сети.", nil);            break;
        case error_delivery_low_price:                      stringError = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Минимальная сумма онлайн-заказа ", nil), [[NITMobileConstantsModel new] min_sum_of_order]]; break;
    }
    
    [self showErrorWithMessage:stringError];
}

#pragma mark - Private
- (void)showErrorWithMessage:(NSString *)message
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:message
                                                                    message:nil
                                                             preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = RGB(58, 160, 144);
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"ОК", nil)
                                                      style:UIAlertActionStyleCancel
                                                    handler:^(UIAlertAction * action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                    }];
    
    [alert addAction:cancel];
    UIViewController * currentController = [UIApplication sharedApplication].keyWindow.rootViewController;
    [currentController presentViewController:alert animated:YES completion:nil];
}

@end
