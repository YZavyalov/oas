//
//  NITErrorManager.h
//  OAS
//
//  Created by Yaroslav on 23.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITErrorConstant.h"

#define ERROR_MESSAGE [NITErrorManager sharedInstance]

@interface NITErrorManager : NSObject

+ (NITErrorManager *)sharedInstance;

#pragma mark - Error
- (void)errorMessage:(NITErrorType)type;

@end
