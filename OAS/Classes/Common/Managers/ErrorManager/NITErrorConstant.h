//
//  NITErrorConstant.h
//  OAS
//
//  Created by Yaroslav on 23.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#pragma - mak - Error key
typedef NS_ENUM(NSUInteger, NITErrorType) {
    feedback_send_error                         = 0,
    notification_error_with_post_order          = 1,
    notification_card_not_find                  = 2,
    notification_error_card                     = 3,
    notification_error_with_phone_registration  = 4,
    notification_error_with_sms                 = 5,
    error_network                               = 6,
    error_connectivity_unavailable              = 7,
    error_internal_server                       = 8,
    error_service_unavailable                   = 9,
    error_location_permission_denied            = 10,
    error_with_loading_product                  = 11,
    error_with_loading_favorite                 = 12,
    pick_up_shop_not_change                     = 13,
    change_city_pushcart_clear                  = 14,
    notification_pushcart_updated               = 15,
    error_not_found_maps_app                    = 16,
    error_on_splash                             = 17,
    error_location_not_found                    = 18,
    error_connectivity_unavailable_on_splash    = 19,
    error_delivery_low_price                    = 20
};

