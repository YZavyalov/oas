//
//  NITPharmacyItem.m
//  OAS
//
//  Created by Yaroslav on 19.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITPharmacyItem.h"
#import "SLShopsApi.h"
#import "RLMResults+NSArray.h"
#import "NSArray+BlocksKit.h"

@implementation NITPharmacyItem

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             //main
             @"identifier"      : @"",
             @"name"            : @"",
             @"address"         : @"",
             @"groupID"         : @"",
             @"locationLat"     : @(0),
             @"locationLon"     : @(0),
             //properties
             @"closing_time"    : @"",
             @"opening_time"    : @"",
             @"is_hub"          : @"",
             @"remote_ia"       : @""
             //custom
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

@implementation NITPharmacyItem (Extension)

#pragma mark - Create
+ (NITPharmacyItem *)createPharmacyItemByModel:(SLShop *)model withProperties:(NSArray <SLIPropertyValue *> *)properties
{
    if (!model)
    {
        return nil;
    }
    
    NITPharmacyItem * pharmacyItem = [[NITPharmacyItem alloc] init];
    
    //main
    pharmacyItem.identifier  = model._id;
    pharmacyItem.name        = model.name;
    pharmacyItem.address     = model.address;
    pharmacyItem.locationLat = model.location.lat;
    pharmacyItem.locationLon = model.location.lon;
    pharmacyItem.groupID     = model.group;
    //properties
    [properties bk_each:^(SLIPropertyValue * p) {
        
        if ([p.type isEqualToString:@"ipString"] && [p.key isEqualToString:@"closing_time"])
        {
            pharmacyItem.closing_time = p.value.firstObject;
        }
        if ([p.type isEqualToString:@"ipString"] && [p.key isEqualToString:@"opening_time"])
        {
            pharmacyItem.opening_time = p.value.firstObject;
        }
        if ([p.type isEqualToString:@"ipString"] && [p.key isEqualToString:@"is_hub"])
        {
            pharmacyItem.is_hub = p.value.firstObject;
        }
        if ([p.type isEqualToString:@"ipString"] && [p.key isEqualToString:@"remote_ia"])
        {
            pharmacyItem.remote_ia = p.value.firstObject;
        }
    }];
    
    // save
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    [NITPharmacyItem createOrUpdateInRealm:realm withValue:pharmacyItem];
    [realm commitWriteTransaction];
    
    return pharmacyItem;
}

#pragma mark - Update
- (void)updatePharmacyItems
{
    [NITPharmacyItem deleteAllPharmacyItems];
    
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    
    [self getShopsWithHandler:^(SLShopsResponse *result) {
        [result.items bk_each:^(SLShop *obj) {
            dispatch_group_enter(group);
            [self loadDetailsWithModel:obj withHandler:^(BOOL success) {
                if (success) dispatch_group_leave(group);
            }];
        }];
        dispatch_group_leave(group);
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [HELPER stopLoading];
        [[NSNotificationCenter defaultCenter] postNotificationName:kUpdatePharmacyNotification object:nil];
    });
}

#pragma mark - Get
+ (NSArray <NITPharmacyItem *> *)allPharmacyItems
{
    return [[NITPharmacyItem allObjects] array];
}

+ (NITPharmacyItem *)pharmacyItemByID:(NSString *)identifier
{
    return [NITPharmacyItem objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", identifier]].firstObject;
}

+ (NSArray <NSString *> *)allPharmacyForSearch
{
    NSArray <NITPharmacyItem *> * shopHub = [[NITPharmacyItem allPharmacyItems] bk_select:^BOOL(NITPharmacyItem *obj) {
        return (obj.is_hub.length > 0);
    }];
    
    NSArray <NSString *> * shopIds;
    if(shopHub.count > 0)
    {
        shopIds = [shopHub bk_map:^(NITPharmacyItem *obj){
            return obj.identifier;
        }];
    }
    else
    {
        shopIds = [[[NITPharmacyItem allPharmacyItems] bk_select:^BOOL(NITPharmacyItem *obj) {
            return obj.remote_ia.length > 0;
        }]  bk_map:^(NITPharmacyItem *obj) {
            return obj.identifier;
        }];
        /*shopIds = [[NITPharmacyItem allPharmacyItems] bk_map:^(NITPharmacyItem *obj){
            return obj.identifier;
        }];*/
    }
    
    return shopIds;
}

#pragma mark - Delete
+ (void)deleteAllPharmacyItems
{
    RLMResults * results = [NITPharmacyItem allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
    }
}

#pragma mark - Private
- (void)getShopsWithHandler:(void(^)(SLShopsResponse *result))handler
{
    [[SLShopsApi new] getStaticShopsGroupsByGroupidWithGroupId:USER.cityID completionHandler:^(SLShopsResponse *output, NSError *error) {
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler (output);
    }];
}

- (void)loadDetailsWithModel:(SLShop *)model withHandler:(void(^)(BOOL success))handler
{
    [[SLShopsApi new] getStaticShopsItemsByItemidWithItemId:model._id completionHandler:^(SLShopWithRelations *output, NSError *error) {
        if (error)
        {
            [HELPER logError:error method:METHOD_NAME];
            handler (false);
        }
        else
        {
            [NITPharmacyItem createPharmacyItemByModel:model withProperties:output.properties];
            handler (true);
        }
    }];
}

@end
