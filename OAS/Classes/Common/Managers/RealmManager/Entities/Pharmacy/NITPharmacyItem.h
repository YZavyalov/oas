//
//  NITPharmacyItem.h
//  OAS
//
//  Created by Yaroslav on 19.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>

@interface NITPharmacyItem : RLMObject

//main
@property NSString * identifier;
@property NSString * name;
@property NSString * address;
@property NSString * groupID;
@property NSNumber <RLMFloat> * locationLat;
@property NSNumber <RLMFloat> * locationLon;
//properties
@property NSString * closing_time;
@property NSString * opening_time;
@property NSString * is_hub;
@property NSString * remote_ia;

@end

RLM_ARRAY_TYPE(NITPharmacyItem)

@class SLShop, SLIPropertyValue;

@interface NITPharmacyItem (Extension)

#pragma mark - Create
+ (NITPharmacyItem *)createPharmacyItemByModel:(SLShop *)model withProperties:(NSArray <SLIPropertyValue *> *)properties;

#pragma mark - Update
- (void)updatePharmacyItems;

#pragma mark - Get
+ (NSArray <NITPharmacyItem *> *)allPharmacyItems;
+ (NITPharmacyItem *)pharmacyItemByID:(NSString *)identifier;
+ (NSArray <NSString *> *)allPharmacyForSearch;

#pragma mark - Delete
+ (void)deleteAllPharmacyItems;

@end


