//
//  NITCatalogItem.h
//  OAS
//
//  Created by Yaroslav on 31.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>
#import "NITCatalogItemModel.h"

@interface NITCatalogItem : RLMObject

@property NSString * identifier;
@property NSString * catalogItemID;
@property NSNumber <RLMFloat> * price;
@property NSNumber <RLMInt> * quantity;

@end

RLM_ARRAY_TYPE(NITCatalogItem)

@class NITCatalogItemModel;

@interface NITCatalogItem (Extension)

#pragma mark - Create
+ (NITCatalogItem *)createCatalogItemByModel:(NITCatalogItemModel *)model;

#pragma mark - Get
+ (NSArray <NITCatalogItem *> *)allCatalogItems;
+ (NITCatalogItem *)catalogItemByID:(NSString *)identifier;

#pragma mark - Delete
+ (void)deleteAllCatalogItems;
+ (void)deleteCatalogItemByID:(NSString *)identifier;

@end
