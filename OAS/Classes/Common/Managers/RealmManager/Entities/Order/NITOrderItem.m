//
//  NITOrderItem.m
//  OAS
//
//  Created by Yaroslav on 30.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITOrderItem.h"
#import "RLMResults+NSArray.h"
#import "NSArray+BlocksKit.h"
#import "SLOrdersApi.h"
#import "NITCatalogItemModel.h"

@implementation NITOrderItem

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             //main
             @"identifier"          : @"",
             @"status"              : @"",
             @"createdAt"           : @(0),
             @"updatedAt"           : @(0),
             
             //items
             @"items"               : [[RLMArray alloc] initWithObjectClassName:NSStringFromClass([NITCatalogItem class])],
             
             //properties
             @"buyer"               : @"",
             @"phone"               : @"",
             
             @"buyer_comment"       : @"",
             @"shop_id"             : @"",
             @"shop_address"        : @"",
             @"pick_up_times"       : @"",
             @"delivery_times"      : @"",
             @"delivery_address"    : @"",
             @"payment_method"      : @"",
             @"disapproved_goods"   : @"",
             @"order_info"          : @"",
             @"attempts"            : @"",
             @"pick_up"             : @"",
             @"under_order"         : @""
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

@implementation NITOrderItem (Extension)

#pragma mark - Create
+ (NITOrderItem *)createOrderItemByModel:(NITOrderItemModel *)model;
{
    if(!model)
    {
        return nil;
    }
    
    NITOrderItem * orderItem = [[NITOrderItem alloc] init];
    
    //main
    orderItem.identifier    = model.identifier;
    orderItem.createdAt     = model.createdAt;
    orderItem.updatedAt     = model.updatedAt;
    orderItem.status        = model.status;
    orderItem.userID        = model.userID;
    
    //items
    for (NITCatalogItemModel * item in model.items)
    {
        [orderItem.items addObject:[NITCatalogItem createCatalogItemByModel:item]];
    }
    
    //proeprties
    orderItem.buyer             = model.buyer;
    orderItem.phone             = model.phone;
    orderItem.buyer_comment     = model.buyer_comment;
    orderItem.shop_id           = model.shop_id;
    orderItem.shop_address      = model.shop_address;
    orderItem.pick_up_times     = model.pick_up_times;
    orderItem.delivery_times    = model.delivery_times;
    orderItem.delivery_address  = model.delivery_address;
    orderItem.payment_method    = model.payment_method;
    orderItem.disapproved_goods = model.disapproved_goods;
    orderItem.order_info        = model.order_info;
    orderItem.attempts          = model.attempts;
    orderItem.pick_up           = model.pick_up;
    orderItem.under_order       = model.under_order;
    
    // save
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    [NITOrderItem createOrUpdateInRealm:realm withValue:orderItem];
    [realm commitWriteTransaction];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSaveOrderItemNotification object:nil];
    
    return orderItem;
}

#pragma mark - Update
- (void)updateOrderItems
{
    [self updateOrdersWithHandler:^(NSArray<SLOrder> *output) {
        NSArray <NITOrderItemModel *> * models = [NITOrderItemModel orderItemModulsFromEntities:output];
        [models bk_each:^(NITOrderItemModel *obj) {
            [obj loadDetailsWithHandler:^(NITOrderItemModel *model) {
                [[NITOrderItem new] updateOrder:model];
            }];
        }];
    }];
}

#pragma mark - Get
+ (NSArray <NITOrderItem *> *)allOrderItems
{
    return [[NITOrderItem allObjects] array];
}

+ (NITOrderItem *)orderItemByID:(NSString *)identifier
{
    return [NITOrderItem objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", identifier]].firstObject;
}

#pragma mark - Delete
+ (void)deleteAllOrderItems
{
    RLMResults * results = [NITOrderItem allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveOrderItemNotification object:nil];
    }
}

+ (void)deleteOrderItemByID:(NSString *)identifier
{
    NITOrderItem * order = [NITOrderItem orderItemByID:identifier];
    
    if (order)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObject:order];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveOrderItemNotification object:nil];
    }
}

#pragma mark - Private
- (void)updateOrdersWithHandler:(void(^)(NSArray<SLOrder> *output))handler
{
    NSURLSessionTask * task = [[SLOrdersApi new] getDynamicOrderuserByUseridWithUserId:USER.identifier completionHandler:^(NSArray<SLOrder> *output, NSError *error) {
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (output) handler (output);
    }];
    
    [HELPER logString:[NSString stringWithFormat:@"%@\n%@", THIS_METHOD, [task.currentRequest description]]];
}

- (void)updateOrder:(NITOrderItemModel *)model
{
    [NITOrderItem deleteOrderItemByID:model.identifier];
    [NITOrderItem createOrderItemByModel:model];
}

@end

