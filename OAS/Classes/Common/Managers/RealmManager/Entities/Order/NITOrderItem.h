//
//  NITOrderItem.h
//  OAS
//
//  Created by Yaroslav on 30.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>
#import "NITCatalogItem.h"
#import "NITOrderItemModel.h"

@interface NITOrderItem : RLMObject

//main
@property NSString * identifier;
@property NSString * status;
@property NSNumber <RLMFloat> * createdAt;
@property NSNumber <RLMFloat> *updatedAt;
@property NSString * userID;

//items
@property RLMArray <NITCatalogItem *> <NITCatalogItem> * items;

//properties
@property NSString * buyer;
@property NSString * phone;
@property NSString * buyer_comment;
@property NSString * shop_id;
@property NSString * shop_address;
@property NSString * pick_up_times;
@property NSString * delivery_times;
@property NSString * delivery_address;
@property NSString * payment_method;
@property NSString * disapproved_goods;
@property NSString * order_info;
@property NSString * attempts;
@property NSString * pick_up;
@property NSString * under_order;

@end

RLM_ARRAY_TYPE(NITOrderItem)

@class NITOrderItemModel;

@interface NITOrderItem (Extension)

#pragma mark - Create
+ (NITOrderItem *)createOrderItemByModel:(NITOrderItemModel *)model;

#pragma mark - Update
- (void)updateOrderItems;

#pragma mark - Get
+ (NSArray <NITOrderItem *> *)allOrderItems;
+ (NITOrderItem *)orderItemByID:(NSString *)identifier;

#pragma mark - Delete
+ (void)deleteAllOrderItems;
+ (void)deleteOrderItemByID:(NSString *)identifier;


@end



