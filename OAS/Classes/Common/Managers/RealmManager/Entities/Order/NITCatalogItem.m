//
//  NITCatalogItem.m
//  OAS
//
//  Created by Yaroslav on 31.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCatalogItem.h"
#import "RLMResults+NSArray.h"
#import "NSArray+BlocksKit.h"

@implementation NITCatalogItem

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier"      : @"",
             @"catalogItemID"   : @"",
             @"price"           : @(0),
             @"quantity"        : @(0),
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

@implementation NITCatalogItem (Extension)

#pragma mark - Create
+ (NITCatalogItem *)createCatalogItemByModel:(NITCatalogItemModel *)model;
{
    if(!model)
    {
        return nil;
    }
    
    NITCatalogItem * catalogItem = [[NITCatalogItem alloc] init];
    
    catalogItem.identifier      = model.identifier;
    catalogItem.catalogItemID   = model.catalogCategoryID;
    catalogItem.price           = model.price;
    catalogItem.quantity        = @(model.orderCount);
    
    // save
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    [NITCatalogItem createOrUpdateInRealm:realm withValue:catalogItem];
    [realm commitWriteTransaction];
    
    return catalogItem;
}

#pragma mark - Get
+ (NSArray <NITCatalogItem *> *)allCatalogItems
{
    return [[NITCatalogItem allObjects] array];
}

+ (NITCatalogItem *)catalogItemByID:(NSString *)identifier
{
    return [NITCatalogItem objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", identifier]].firstObject;
}

#pragma mark - Delete
+ (void)deleteAllCatalogItems
{
    RLMResults * results = [NITCatalogItem allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
    }
}

+ (void)deleteCatalogItemByID:(NSString *)identifier
{
    NITCatalogItem * catalog = [NITCatalogItem catalogItemByID:identifier];
    
    if (catalog)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObject:catalog];
        [realm commitWriteTransaction];
    }
}

@end
