//
//  NITFilterGenformLoader.m
//  OAS
//
//  Created by Yaroslav on 07.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterGenformLoader.h"
#import "SLInfosystemsApi.h"
#import "NITFilterGenformItem.h"

@interface NITFilterGenformLoader ()

@property (nonatomic) NSString * genformID;
@property (nonatomic) NSArray <SLInfoItemWithRelations *> * models;

@end

@implementation NITFilterGenformLoader

#pragma mark - Public
- (void)updateFilterGenformItemsWithID:(NSString *)identifier
{
    NSDate * currentDay = [NSDate date];
    NSDate * createDay = [[NITFilterGenformItem allFilterGenformItem].firstObject timeCreate];
    
    if(!([NITFilterGenformItem allFilterGenformItem].count > 0) || (currentDay.day != createDay.day))
    {
        [NITFilterGenformItem deleteAllFilterGenformItems];
    
        self.genformID = identifier;
        [self updateDataByOffset:nil];
    }
}

#pragma mark - Private
- (void)getFilterDescriptionWithID:(NSString *)infoID offset:(NSNumber *)offset withHandler:(void(^)(NSArray <SLInfoItemWithRelations *> *result))handler
{
    SLInfosystemQuery * query = [SLInfosystemQuery new];
    query.offset = offset;
    
    [[SLInfosystemsApi new] postStaticInfosystemsByInfosystemidQuerywithrelationsWithInfosystemId:infoID body:query completionHandler:^(NSArray<SLInfoItemWithRelations> *output, NSError *error) {
        if (error)
        {
            [ERROR_MESSAGE errorMessage:error_service_unavailable];
            [HELPER logError:error method:METHOD_NAME];
        }
        if (handler) handler (output);
    }];
}

- (void)getFilterOptionByGroupID:(NSString *)groupID
{
    [self getFilterDescriptionWithID:groupID offset:nil withHandler:^(NSArray<SLInfoItemWithRelations *> * result) {
        [self insertDataByModels:result];
    }];
}

- (void)getNextFilterOptionBygroupID:(NSString *)groupID offset:(NSNumber *)offset
{
    [self getFilterDescriptionWithID:groupID offset:offset withHandler:^(NSArray<SLInfoItemWithRelations *> * result) {
        [self insertDataByModels:result];
    }];
}

- (void)insertDataByModels:(NSArray<SLInfoItemWithRelations *> *)models
{
    if (models.count > 0)
    {
        NSMutableArray * allItems = [NSMutableArray arrayWithArray:self.models];
        [allItems addObjectsFromArray:models];
        
        self.models = allItems;
        
        [self checkLoadNextDataFromOffset:self.models.count];
    }
    else
    {
        [self filterCityFromEntitiesWithrelation:self.models];
        [[NSNotificationCenter defaultCenter] postNotificationName:kCreateFilterGenformNotification object:nil];
        NSLog(@"create");
    }
}

- (void)checkLoadNextDataFromOffset:(NSInteger)offset
{
    [self updateDataByOffset:@(offset)];
}

- (void)updateDataByOffset:(NSNumber *)offset
{
    if (offset)
    {
        [self getNextFilterOptionBygroupID:self.genformID offset:offset];
    }
    else
    {
        [self getFilterOptionByGroupID:self.genformID];
    }
}

- (void)filterCityFromEntitiesWithrelation:(NSArray<SLInfoItemWithRelations *> *)entites
{
    [entites bk_map:^id(SLInfoItemWithRelations *obj) {
        return [NITFilterGenformItem createFilterGenformItemByModel:obj];
    }];
}

@end
