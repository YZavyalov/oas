//
//  NITFilterGenformItem.h
//  OAS
//
//  Created by Yaroslav on 07.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>

@interface NITFilterGenformItem : RLMObject

@property NSString * identifier;
@property NSString * name;
@property NSNumber <RLMInt> * weight;
@property NSDate * timeCreate;

@end

RLM_ARRAY_TYPE(NITFilterGenformItem)

@class SLInfoItemWithRelations;

@interface NITFilterGenformItem (Extension)

#pragma mark - Create
+ (NITFilterGenformItem *)createFilterGenformItemByModel:(SLInfoItemWithRelations *)model;

#pragma mark - Get
+ (NSArray <NITFilterGenformItem *> *)allFilterGenformItem;
+ (NITFilterGenformItem *)filterGenformItemByID:(NSString *)identifier;

#pragma mark - Delete
+ (void)deleteAllFilterGenformItems;
+ (void)deleteFilterGenformItemByID:(NSString *)identifier;

@end
