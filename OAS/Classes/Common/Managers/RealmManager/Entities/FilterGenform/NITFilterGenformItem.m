//
//  NITFilterGenformItem.m
//  OAS
//
//  Created by Yaroslav on 07.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterGenformItem.h"
#import "SLInfosystemsApi.h"
#import "RLMResults+NSArray.h"
#import "NSArray+BlocksKit.h"

@implementation NITFilterGenformItem

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier"          : @"",
             @"name"                : @"",
             @"weight"              : @(0),
             @"timeCreate"          : [NSDate date]
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

@implementation NITFilterGenformItem (Extension)

#pragma mark - Create
+ (NITFilterGenformItem *)createFilterGenformItemByModel:(SLInfoItemWithRelations *)model
{
    if(!model)
    {
        return nil;
    }
    
    NITFilterGenformItem * filterGenformItem = [[NITFilterGenformItem alloc] init];
    
    filterGenformItem.identifier   = model._id;
    filterGenformItem.name         = model.name;
    filterGenformItem.timeCreate   = [NSDate date];

    [model.properties bk_each:^(SLIPropertyValue *obj) {
        if([obj.key isEqualToString:@"weight"]) filterGenformItem.weight = @([obj.value.firstObject integerValue]);
    }];
    
    // save
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    [NITFilterGenformItem createOrUpdateInRealm:realm withValue:filterGenformItem];
    [realm commitWriteTransaction];
    
    return filterGenformItem;
}

#pragma mark - Get
+ (NSArray <NITFilterGenformItem *> *)allFilterGenformItem
{
    return [[NITFilterGenformItem allObjects] array];
}

+ (NITFilterGenformItem *)filterGenformItemByID:(NSString *)identifier
{
    return [NITFilterGenformItem objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", identifier]].firstObject;
}

#pragma mark - Delete
+ (void)deleteAllFilterGenformItems
{
    RLMResults * results = [NITFilterGenformItem allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
    }
}

+ (void)deleteFilterGenformItemByID:(NSString *)identifier
{
    NITFilterGenformItem * filter = [NITFilterGenformItem filterGenformItemByID:identifier];
    
    if(filter)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObject:filter];
        [realm commitWriteTransaction];
    }
}

@end

