//
//  NITFavoriteItem.m
//  OAS
//
//  Created by Yaroslav on 27.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITFavoriteItem.h"
#import "RLMResults+NSArray.h"
#import "NSArray+BlocksKit.h"
#import "NITCatalogItemModel.h"
#import "SLCatalogsApi.h"

@implementation NITFavoriteItem

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             //main
             @"parentID"        : @"",
             @"identifier"      : @"",
             @"name"            : @"",
             @"body"            : @"",
             @"creator"         : @"",
             @"descriptionText" : @"",
             @"image"           : @"",
             @"price"           : @(0),
             @"createdAt"       : [NSDate date],
             @"updatedAt"       : [NSDate date],
             @"groups"          : [[RLMArray alloc] initWithObjectClassName:NSStringFromClass([NITStringObject class])],
             
             //properties
             @"prodform_id"     : @"",
             @"inn_id"          : @"",
             @"gentrade_id"     : @"",
             @"drugform_id"     : @"",
             @"Firm_ID"         : @"",
             @"Country_ID"      : @"",
             @"wsize"           : @"",
             @"vmass"           : @"",
             @"lsize"           : @"",
             @"genprodform_name": @"",
             @"cubeunit"        : @"",
             @"concunit"        : @"",
             @"amass"           : @"",
             @"altlsize"        : @"",
             @"recipe"          : @"",
             @"under_order"     : @"",
             @"is_new"          : @"",
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

@implementation NITFavoriteItem (Extension)

#pragma mark - Create
+ (NITFavoriteItem *)createFavoriteItemByModel:(NITCatalogItemModel *)model
{
    if(!model)
    {
        return nil;
    }
    
    NITFavoriteItem * favoriteItem = [[NITFavoriteItem alloc] init];
    
    //main
    favoriteItem.parentID           = model.catalogCategoryID;
    favoriteItem.identifier         = model.identifier;
    favoriteItem.name               = model.name;
    favoriteItem.body               = model.body;
    favoriteItem.creator            = model.creator;
    favoriteItem.descriptionText    = model.descriptionText;
    favoriteItem.image              = model.image;
    favoriteItem.price              = model.price;
    favoriteItem.createdAt          = model.createdAt;
    favoriteItem.updatedAt          = model.updatedAt;
    for(NSString * group in model.groups)
    {
        NITStringObject * stringObject = [[NITStringObject alloc] init];
        stringObject.value = group;
        [favoriteItem.groups addObject:stringObject];
    }
    
    //properties
    favoriteItem.prodform_id        = model.prodform_id;
    favoriteItem.inn_id             = model.inn_id;
    favoriteItem.gentrade_id        = model.gentrade_id;
    favoriteItem.drugform_id        = model.drugform_id;
    favoriteItem.Firm_ID            = model.Firm_ID;
    favoriteItem.Country_ID         = model.Country_ID;
    favoriteItem.wsize              = model.wsize;
    favoriteItem.vmass              = model.vmass;
    favoriteItem.lsize              = model.lsize;
    favoriteItem.genprodform_name   = model.genprodform_name;
    favoriteItem.cubeunit           = model.cubeunit;
    favoriteItem.concunit           = model.concunit;
    favoriteItem.amass              = model.amass;
    favoriteItem.altlsize           = model.altlsize;
    favoriteItem.recipe             = model.recipe;
    favoriteItem.under_order        = model.under_order;
    favoriteItem.is_new             = model.is_new;
    
    // save
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    [NITFavoriteItem createOrUpdateInRealm:realm withValue:favoriteItem];
    [realm commitWriteTransaction];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSaveFavoriteItemNotification object:nil];
    [TRACKER trackEvent:TrackerEventAddToFavorite];
    
    return favoriteItem;
}

#pragma mark - Update
- (void)updateFavoriteItems
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSArray <NITFavoriteItem *> * favoriteItems = [NITFavoriteItem allFavoriteItems];
        [favoriteItems bk_each:^(NITFavoriteItem * obj) {
            [self getCatalogItemDetailsByID:obj.identifier withHandler:^(SLCatalogItemWithRelations *result) {
                if (result)
                {
                    NITCatalogItemModel * item = [[NITCatalogItemModel alloc] initWithDetailModel:result withGroupID:obj.parentID];
                    [NITFavoriteItem updateFavoriteByModel:item];
                }
            }];
        }];
    });
}

#pragma mark - Get
+ (NSArray <NITFavoriteItem *> *)allFavoriteItems
{
    return [[NITFavoriteItem allObjects] array];
}

+ (NITFavoriteItem *)favoriteItemByID:(NSString *)identifier
{
    return [NITFavoriteItem objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", identifier]].firstObject;
}

#pragma mark - Delete
+ (void)deleteAllFavoriteItems
{
    RLMResults * results = [NITFavoriteItem allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveFavoriteItemNotification object:nil];
    }
}

+ (void)deleteFavoriteItemByID:(NSString *)identifier
{
    NITFavoriteItem * favorite = [NITFavoriteItem favoriteItemByID:identifier];
    
    if (favorite)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObject:favorite];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveFavoriteItemNotification object:nil];
    }
}

#pragma mark - Private
+ (NITFavoriteItem *)updateFavoriteByModel:(NITCatalogItemModel *)model
{
    NITFavoriteItem * favoriteItem = [NITFavoriteItem favoriteItemByID:model.identifier];
    
    // save
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    //main
    favoriteItem.name               = model.name;
    favoriteItem.body               = model.body;
    favoriteItem.creator            = model.creator;
    favoriteItem.descriptionText    = model.descriptionText;
    favoriteItem.image              = model.image;
    favoriteItem.price              = model.price;
    favoriteItem.createdAt          = model.createdAt;
    favoriteItem.updatedAt          = model.updatedAt;
    //properties
    favoriteItem.prodform_id        = model.prodform_id;
    favoriteItem.inn_id             = model.inn_id;
    favoriteItem.gentrade_id        = model.gentrade_id;
    favoriteItem.drugform_id        = model.drugform_id;
    favoriteItem.Firm_ID            = model.Firm_ID;
    favoriteItem.Country_ID         = model.Country_ID;
    favoriteItem.wsize              = model.wsize;
    favoriteItem.vmass              = model.vmass;
    favoriteItem.lsize              = model.lsize;
    favoriteItem.genprodform_name   = model.genprodform_name;
    favoriteItem.cubeunit           = model.cubeunit;
    favoriteItem.concunit           = model.concunit;
    favoriteItem.amass              = model.amass;
    favoriteItem.altlsize           = model.altlsize;
    favoriteItem.recipe             = model.recipe;
    favoriteItem.under_order        = model.under_order;
    favoriteItem.is_new             = model.is_new;
    //
    [NITFavoriteItem createOrUpdateInRealm:realm withValue:favoriteItem];
    [realm commitWriteTransaction];
    
    return favoriteItem;
}

- (void)getCatalogItemDetailsByID:(NSString *)itemID withHandler:(void(^)(SLCatalogItemWithRelations *result))handler
{
    [[SLCatalogsApi new] getStaticCatalogItemsByItemidWithItemId:itemID completionHandler:^(SLCatalogItemWithRelations *output, NSError *error) {
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler (output);
    }];
}

@end
