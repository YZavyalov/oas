//
//  NITFavoriteItem.h
//  OAS
//
//  Created by Yaroslav on 27.04.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>
#import "NITStringObject.h"

@interface NITFavoriteItem : RLMObject

//main
@property NSString * parentID;
@property NSString * identifier;
@property NSString * name;
@property NSString * body;
@property NSString * creator;
@property NSString * descriptionText;
@property NSString * image;
@property NSNumber <RLMFloat> * price;
@property NSDate * createdAt;
@property NSDate * updatedAt;
@property RLMArray <NITStringObject *> <NITStringObject> * groups;

// properties
@property NSString * prodform_id;
@property NSString * inn_id;
@property NSString * gentrade_id;
@property NSString * drugform_id;
@property NSString * Firm_ID;
@property NSString * Country_ID;
@property NSString * wsize;
@property NSString * vmass;
@property NSString * lsize;
@property NSString * genprodform_name;
@property NSString * cubeunit;
@property NSString * concunit;
@property NSString * amass;
@property NSString * altlsize;
@property NSString * recipe;
@property NSString * under_order;
@property NSString * is_new;

@end

RLM_ARRAY_TYPE(NITFavoriteItem)

@class NITCatalogItemModel;

@interface NITFavoriteItem (Extension)

#pragma mark - Create
+ (NITFavoriteItem *)createFavoriteItemByModel:(NITCatalogItemModel *)model;

#pragma mark - Update
- (void)updateFavoriteItems;

#pragma mark - Get
+ (NSArray <NITFavoriteItem *> *)allFavoriteItems;
+ (NITFavoriteItem *)favoriteItemByID:(NSString *)identifier;

#pragma mark - Delete
+ (void)deleteAllFavoriteItems;
+ (void)deleteFavoriteItemByID:(NSString *)identifier;

@end
