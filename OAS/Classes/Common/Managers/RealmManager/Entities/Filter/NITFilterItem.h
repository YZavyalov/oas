//
//  NITFilterItem.h
//  OAS
//
//  Created by Yaroslav on 07.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>

@interface NITFilterItem : RLMObject

@property NSString * identifier;
@property NSString * name;
@property NSString * nameFromDocuments;
@property NSDate * timeCreate;

@end

RLM_ARRAY_TYPE(NITFilterItem)

@class SLInfosystem;

@interface NITFilterItem (Extension)

#pragma mark - Create
+ (NITFilterItem *)createFilterItemByModel:(SLInfosystem *)model;

#pragma mark - Update
- (void)updateFilterItems;

#pragma mark - Get
+ (NSArray <NITFilterItem *> *)allFilterItem;
+ (NITFilterItem *)filterItemByID:(NSString *)identifier;

#pragma mark - Delete
+ (void)deleteAllFilterItems;
+ (void)deleteFilterItemByID:(NSString *)identifier;

@end

