//
//  NITFilterItem.m
//  OAS
//
//  Created by Yaroslav on 07.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterItem.h"
#import "SLInfosystemsApi.h"
#import "NITMobileConstantsModel.h"
#import "RLMResults+NSArray.h"
#import "NSArray+BlocksKit.h"
#import "NITFilterCityLoader.h"
#import "NITFilterGenformLoader.h"

@implementation NITFilterItem

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier"          : @"",
             @"name"                : @"",
             @"nameFromDocuments"   : @"",
             @"timeCreate"          : [NSDate date]
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

@implementation NITFilterItem (Extension)

#pragma mark - Create
+ (NITFilterItem *)createFilterItemByModel:(SLInfosystem *)model
{
    if(!model)
    {
        return nil;
    }
    
    NITFilterItem * filterItem = [[NITFilterItem alloc] init];
    
    filterItem.identifier   = model._id;
    filterItem.name         = model.name;
    filterItem.timeCreate   = [NSDate date];
    
    if([model.name isEqualToString:@"country"]) filterItem.nameFromDocuments = [[NITMobileConstantsModel new] county_filter_desc];
    if([model.name isEqualToString:@"firm"]) filterItem.nameFromDocuments = [[NITMobileConstantsModel new] manufacturer_filter_desc];
    if([model.name isEqualToString:@"genform"]) filterItem.nameFromDocuments = [[NITMobileConstantsModel new] form_filter_desc];
    
    // save
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    [NITFilterItem createOrUpdateInRealm:realm withValue:filterItem];
    [realm commitWriteTransaction];
    
    return filterItem;
}

#pragma mark - Update
- (void)updateFilterItems
{
    [NITFilterItem deleteAllFilterItems];
    
    [self getFiltersWithHandler:^(NSArray <SLInfosystem *> * result) {
        NSArray <SLInfosystem *> * filters = [result bk_select:^BOOL(SLInfosystem *obj) {
            return ([obj.name isEqualToString:@"country"] || [obj.name isEqualToString:@"firm"] || [obj.name isEqualToString:@"genform"]);
        }];
        [filters bk_each:^(SLInfosystem* obj) {
            [NITFilterItem createFilterItemByModel:obj];
        }];
        [[NSNotificationCenter defaultCenter] postNotificationName:kCreateFilterNotification object:nil];
        
        [[NITFilterItem allFilterItem] bk_each:^(NITFilterItem *obj) {
            if([obj.name isEqualToString:@"country"]) [[NITFilterCityLoader new] updateFilterCityItemsWithID:obj.identifier];
            if([obj.name isEqualToString:@"genform"]) [[NITFilterGenformLoader new] updateFilterGenformItemsWithID:obj.identifier];
        }];
    }];
}

#pragma mark - Get
+ (NSArray <NITFilterItem *> *)allFilterItem
{
    return [[NITFilterItem allObjects] array];
}

+ (NITFilterItem *)filterItemByID:(NSString *)identifier
{
    return [NITFilterItem objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", identifier]].firstObject;
}

#pragma mark - Delete
+ (void)deleteAllFilterItems
{
    RLMResults * results = [NITFilterItem allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
    }
}

+ (void)deleteFilterItemByID:(NSString *)identifier
{
    NITFilterItem * filter = [NITFilterItem filterItemByID:identifier];
    
    if (filter)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObject:filter];
        [realm commitWriteTransaction];
    }
}

#pragma mark - Private
- (void)getFiltersWithHandler:(void(^)(NSArray<SLInfosystem *> *result))handler
{
    [HELPER startLoading];
    [[SLInfosystemsApi new] getStaticInfosystemsWithCompletionHandler:^(NSArray<SLInfosystem> *output, NSError *error) {
        [HELPER stopLoading];
        if (error)
        {
            [ERROR_MESSAGE errorMessage:error_service_unavailable];
            [HELPER logError:error method:METHOD_NAME];
        }
        if (handler) handler (output);
    }];
}

@end
