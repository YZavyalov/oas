//
//  NITCityItem.h
//  OAS
//
//  Created by Yaroslav on 19.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>

@interface NITCityItem : RLMObject

@property NSString * identifier;
@property NSString * name;
@property NSString * parent;
@property NSString * defaultTemplateID;
//TODO: Infosystem properties
@property NSString * lat;
@property NSString * lon;
@property NSString * delivery;

@end

RLM_ARRAY_TYPE(NITCityItem)

@class SLInfoItemWithRelations;

@interface NITCityItem (Extension)

#pragma mark - Create
+ (NITCityItem *)createCityItemByModel:(SLGroup *)model withInfosystems:(NSArray <SLInfoItemWithRelations *> *)info;

#pragma mark - Update
- (void)updateCityItems;

#pragma mark - Get
+ (NSArray <NITCityItem *> *)allCityItems;
+ (NITCityItem *)cityItemByID:(NSString *)identifier;

#pragma mark - Delete
+ (void)deleteAllCityItems;
+ (void)deleteCityItemByID:(NSString *)identifier;

@end


