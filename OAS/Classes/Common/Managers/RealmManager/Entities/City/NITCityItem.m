//
//  NITCityItem.m
//  OAS
//
//  Created by Yaroslav on 19.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITCityItem.h"
#import "RLMResults+NSArray.h"
#import "NSArray+BlocksKit.h"
#import "SLShopsApi.h"
#import "SLInfosystemsApi.h"

@implementation NITCityItem

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier"          : @"",
             @"name"                : @"",
             @"parent"              : @"",
             @"defaultTemplateID"   : @"",
             @"lat"                 : @"",
             @"lon"                 : @"",
             @"delivery"            : @""
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

@implementation NITCityItem (Extension)

#pragma mark - Create
+ (NITCityItem *)createCityItemByModel:(SLGroup *)model withInfosystems:(NSArray <SLInfoItemWithRelations *> *)info
{
    if (!model)
    {
        return nil;
    }
    
    if([NITCityItem cityItemByID:model._id])
    {
        [NITCityItem deleteCityItemByID:model._id];
    }
    
    NITCityItem * cityItem = [[NITCityItem alloc] init];
    
    cityItem.identifier         = model._id;
    cityItem.name               = model.name;
    cityItem.parent             = model.parent;
    cityItem.defaultTemplateID  = model.defaultTemplateId;
    //properties
    for(SLInfoItemWithRelations * i in info)
    {
        if([i.name isEqualToString:model.name])
        {
            [i.properties bk_each:^(SLIPropertyValue * p) {
                if ([p.type isEqualToString:@"ipString"] && [p.key isEqualToString:@"geoposition"])
                {
                    NSString * coordinate = p.value.firstObject;
                    coordinate = [coordinate stringByReplacingOccurrencesOfString:@"(" withString:@""];
                    coordinate = [coordinate stringByReplacingOccurrencesOfString:@")" withString:@""];
                    NSArray <NSString *> * coordinates = [coordinate componentsSeparatedByString:@", "];
                    
                    cityItem.lat = [coordinates objectAtIndex:0];
                    cityItem.lon = [coordinates objectAtIndex:1];
                }
                if ([p.type isEqualToString:@"ipString"] && [p.key isEqualToString:@"delivery"])
                {
                    cityItem.delivery = p.value.firstObject;
                }
            }];

        }
    }
    
    
    // save
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    [NITCityItem createOrUpdateInRealm:realm withValue:cityItem];
    [realm commitWriteTransaction];
    
    return cityItem;
}

#pragma mark - Update
- (void)updateCityItems
{    
    [self getCitiesWithHandler:^(NSArray<SLGroup *> *result) {
        [self getCityDetailsWithHandler:^(NSArray<SLInfoItemWithRelations *> *output) {
            [result bk_map:^id(SLGroup *obj) {
                return [NITCityItem createCityItemByModel:obj withInfosystems:output];
            }];

            [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateCityNotification object:nil];
        }];
    }];
    
    
}

#pragma mark - Get
+ (NSArray <NITCityItem *> *)allCityItems
{
    return [[NITCityItem allObjects] array];
}

+ (NITCityItem *)cityItemByID:(NSString *)identifier
{
    return [NITCityItem objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", identifier]].firstObject;
}

#pragma mark - Delete
+ (void)deleteAllCityItems
{
    RLMResults * results = [NITCityItem allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
    }
}

+ (void)deleteCityItemByID:(NSString *)identifier
{
    NITCityItem * city = [NITCityItem cityItemByID:identifier];
    
    if (city)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObject:city];
        [realm commitWriteTransaction];
    }
}

#pragma mark - Private
- (void)getCitiesWithHandler:(void(^)(NSArray<SLGroup *> *result))handler
{
    [[SLShopsApi new] getStaticShopsGroupsWithCompletionHandler:^(SLShopsResponse *output, NSError *error) {
        if(error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler (output.groups);
    }];
}

- (void)getCityDetailsWithHandler:(void(^)(NSArray <SLInfoItemWithRelations *> *output))handler
{
    SLInfosystemQuery * query = [SLInfosystemQuery new];
    query.limit = @(200);
    [[SLInfosystemsApi new] postStaticInfosystemsByInfosystemidQuerywithrelationsWithInfosystemId:[API infosysytemCitiesID] body:query completionHandler:^(NSArray<SLInfoItemWithRelations> *output, NSError *error) {
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler (output);
    }];
}

- (void)updateCityProperties:(SLInfoItemWithRelations *)model
{
    NITCityItem * cityItem = [[NITCityItem allCityItems] bk_select:^BOOL(NITCityItem *obj) {
        return [obj.name isEqualToString:model.name];
    }].firstObject;

    NITCityItem * city = [NITCityItem cityItemByID:cityItem.identifier];
    
    // save
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    [model.properties bk_each:^(SLIPropertyValue * p) {
        if ([p.type isEqualToString:@"ipString"] && [p.key isEqualToString:@"geoposition"])
        {
            NSString * coordinate = p.value.firstObject;
            coordinate = [coordinate stringByReplacingOccurrencesOfString:@"(" withString:@""];
            coordinate = [coordinate stringByReplacingOccurrencesOfString:@")" withString:@""];
            NSArray <NSString *> * coordinates = [coordinate componentsSeparatedByString:@", "];

            city.lat = [coordinates objectAtIndex:0];
            city.lon = [coordinates objectAtIndex:1];
        }
        if ([p.type isEqualToString:@"ipString"] && [p.key isEqualToString:@"delivery"])
        {
            city.delivery = p.value.firstObject;
        }
    }];
    //
    [NITCityItem createOrUpdateInRealm:realm withValue:city];
    [realm commitWriteTransaction];
}

@end
