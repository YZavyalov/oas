//
//  NITStringObject.h
//  OAS
//
//  Created by Yaroslav on 24.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>

@interface NITStringObject : RLMObject
@property NSString * value;
@end
RLM_ARRAY_TYPE(NITStringObject)
