//
//  NITBasketItem.h
//  OAS
//
//  Created by Yaroslav on 04.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>
#import "NITCatalogItemModel.h"
#import "NITStringObject.h"

@interface NITBasketItem : RLMObject

@property NSString * parentID;
@property NSString * identifier;
@property NSString * name;
@property NSString * body;
@property NSString * creator;
@property NSString * descriptionText;
@property NSString * image;
@property NSNumber <RLMFloat> * price;
@property NSDate * createdAt;
@property NSDate * updatedAt;
@property RLMArray <NITStringObject *> <NITStringObject> * groups;
@property NSNumber <RLMInt> * count;
@property NSNumber <RLMInt> * maxCount;

// properties
@property NSString * prodform_id;
@property NSString * inn_id;
@property NSString * gentrade_id;
@property NSString * drugform_id;
@property NSString * Firm_ID;
@property NSString * Country_ID;
@property NSString * wsize;
@property NSString * vmass;
@property NSString * lsize;
@property NSString * genprodform_name;
@property NSString * cubeunit;
@property NSString * concunit;
@property NSString * amass;
@property NSString * altlsize;
@property NSString * recipe;
@property NSString * under_order;
@property NSString * is_new;

@end

RLM_ARRAY_TYPE(NITBasketItem)

@class NITCatalogItemModel;

@interface NITBasketItem (Extension)

#pragma mark - Create
+ (NITBasketItem *)createBasketItemByModel:(NITCatalogItemModel *)model;

#pragma mark - Update
- (void)updateBasketItems;
- (void)getMaxCountByItemID:(NSString *)itemID;

#pragma mark - Edit
+ (NITBasketItem *)increaseBasketItemCountByModel:(NITCatalogItemModel *)model;
+ (NITBasketItem *)dereaseBasketItemCountByModel:(NITCatalogItemModel *)model;

#pragma mark - Get
+ (NSArray <NITBasketItem *> *)allBasketItems;
+ (NITBasketItem *)basketItemByID:(NSString *)identifier;

#pragma mark - Delete
+ (void)deleteAllBasketItems;
+ (void)deleteBasketItemByID:(NSString *)identifier;

@end
