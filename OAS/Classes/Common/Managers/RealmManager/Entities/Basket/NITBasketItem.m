//
//  NITBasketItem.m
//  OAS
//
//  Created by Yaroslav on 04.05.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITBasketItem.h"
#import "RLMResults+NSArray.h"
#import "NSArray+BlocksKit.h"
#import "SLCatalogsApi.h"

#import "SLShopsApi.h"
#import "SLCatalogsApi.h"
#import "SLShopsResponse.h"
#import "SLCatalogItemInShop.h"
#import "SLCatalogItemInShopQuery.h"

@implementation NITBasketItem

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"parentID"        : @"",
             @"identifier"      : @"",
             @"name"            : @"",
             @"body"            : @"",
             @"creator"         : @"",
             @"descriptionText" : @"",
             @"image"           : @"",
             @"price"           : @(0),
             @"createdAt"       : [NSDate date],
             @"updatedAt"       : [NSDate date],
             @"groups"          : [[RLMArray alloc] initWithObjectClassName:NSStringFromClass([NITStringObject class])],
             @"count"           : @(0),
             @"maxCount"        : @(0),
             
             //properties
             @"prodform_id"     : @"",
             @"inn_id"          : @"",
             @"gentrade_id"     : @"",
             @"drugform_id"     : @"",
             @"Firm_ID"         : @"",
             @"Country_ID"      : @"",
             @"wsize"           : @"",
             @"vmass"           : @"",
             @"lsize"           : @"",
             @"genprodform_name": @"",
             @"cubeunit"        : @"",
             @"concunit"        : @"",
             @"amass"           : @"",
             @"altlsize"        : @"",
             @"recipe"          : @"",
             @"under_order"     : @"",
             @"is_new"          : @"",
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

@implementation NITBasketItem (Extension)

#pragma mark - Create
+ (NITBasketItem *)createBasketItemByModel:(NITCatalogItemModel *)model
{
    if(!model)
    {
        return nil;
    }
    
    NITBasketItem * basketItem = [[NITBasketItem alloc] init];
    
    basketItem.parentID           = model.catalogCategoryID;
    basketItem.identifier         = model.identifier;
    basketItem.name               = model.name;
    basketItem.body               = model.body;
    basketItem.creator            = model.creator;
    basketItem.descriptionText    = model.descriptionText;
    basketItem.image              = model.image;
    basketItem.price              = model.price;
    basketItem.createdAt          = model.createdAt;
    basketItem.updatedAt          = model.updatedAt;
    basketItem.count              = @(1);
    [[NITBasketItem new] getMaxCountByItemID:model.identifier];
    for(NSString * group in model.groups)
    {
        NITStringObject * stringObject = [[NITStringObject alloc] init];
        stringObject.value = group;
        [basketItem.groups addObject:stringObject];
    }
    
    
    //properties
    basketItem.prodform_id        = model.prodform_id;
    basketItem.inn_id             = model.inn_id;
    basketItem.gentrade_id        = model.gentrade_id;
    basketItem.drugform_id        = model.drugform_id;
    basketItem.Firm_ID            = model.Firm_ID;
    basketItem.Country_ID         = model.Country_ID;
    basketItem.wsize              = model.wsize;
    basketItem.vmass              = model.vmass;
    basketItem.lsize              = model.lsize;
    basketItem.genprodform_name   = model.genprodform_name;
    basketItem.cubeunit           = model.cubeunit;
    basketItem.concunit           = model.concunit;
    basketItem.amass              = model.amass;
    basketItem.altlsize           = model.altlsize;
    basketItem.recipe             = model.recipe;
    basketItem.under_order        = model.under_order;
    basketItem.is_new             = model.is_new;
    
    // save
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    [NITBasketItem createOrUpdateInRealm:realm withValue:basketItem];
    [realm commitWriteTransaction];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSaveBasketItemNotification object:model.identifier];
    
    return basketItem;
}

#pragma mark - Update
- (void)updateBasketItems
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSArray <NITBasketItem *> * basketItems = [NITBasketItem allBasketItems];
        [basketItems bk_each:^(NITBasketItem * obj) {
            [self getCatalogItemDetailsByID:obj.identifier withHandler:^(SLCatalogItemWithRelations *result) {
                if(result)
                {
                    [NITBasketItem updateBasketByModel:[[NITCatalogItemModel alloc] initWithDetailModel:result withGroupID:obj.parentID]];
                }
                else
                {
                    [NITBasketItem deleteBasketItemByID:obj.identifier];
                    [ERROR_MESSAGE errorMessage:notification_pushcart_updated];
                }
            }];
        }];
    });
}

#pragma mark - Edit
+ (NITBasketItem *)increaseBasketItemCountByModel:(NITCatalogItemModel *)model
{
    if (!model)
    {
        return nil;
    }
    
    NITBasketItem * basketItem = [NITBasketItem basketItemByID:model.identifier];
    
    if (basketItem)
    {
        // save
        RLMRealm * realm = REALM.database;
        [realm beginWriteTransaction];
        if(basketItem.count.integerValue < basketItem.maxCount.integerValue)
            basketItem.count = @(basketItem.count.integerValue + 1);
        [NITBasketItem createOrUpdateInRealm:realm withValue:basketItem];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveBasketItemNotification object:model.identifier];
    }
    else
    {
        basketItem = [NITBasketItem createBasketItemByModel:model];
    }
    
    [NOTIFICATION showNotificationByItem:[NITBasketItem basketItemByID:model.identifier]];
    
    if (model.count == 1)
    {
        [TRACKER trackEvent:TrackerEventAddToBasket];
    }
    
    return basketItem;
}

+ (NITBasketItem *)dereaseBasketItemCountByModel:(NITCatalogItemModel *)model
{
    if (!model)
    {
        return nil;
    }
    
    NITBasketItem * basketItem = [NITBasketItem basketItemByID:model.identifier];
    
    if (basketItem && basketItem.count.integerValue > 1)
    {
        // save
        RLMRealm * realm = REALM.database;
        [realm beginWriteTransaction];
        basketItem.count = @(basketItem.count.integerValue - 1);
        [NITBasketItem createOrUpdateInRealm:realm withValue:basketItem];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveBasketItemNotification object:model.identifier];
    }
    else if (basketItem && basketItem.count.integerValue == 1)
    {
        [NITBasketItem deleteBasketItemByID:model.identifier];
    }
    
    return basketItem;
}

#pragma mark - Get
+ (NSArray <NITBasketItem *> *)allBasketItems
{
    return [[NITBasketItem allObjects] array];
}

+ (NITBasketItem *)basketItemByID:(NSString *)identifier
{
    return [NITBasketItem objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", identifier]].firstObject;
}

#pragma mark - Delete
+ (void)deleteAllBasketItems
{
    RLMResults * results = [NITBasketItem allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveBasketItemNotification object:nil];
    }
}

+ (void)deleteBasketItemByID:(NSString *)identifier
{
    NITBasketItem * basket = [NITBasketItem basketItemByID:identifier];
    
    if (basket)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObject:basket];
        [realm commitWriteTransaction];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveBasketItemNotification object:identifier];
    }
}

#pragma mark - Private
+ (NITBasketItem *)updateBasketByModel:(NITCatalogItemModel *)model
{
    NITBasketItem * basketItem = [NITBasketItem basketItemByID:model.identifier];
    
    // save
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    //main
    basketItem.name               = model.name;
    basketItem.body               = model.body;
    basketItem.creator            = model.creator;
    basketItem.descriptionText    = model.descriptionText;
    basketItem.image              = model.image;
    basketItem.price              = model.price;
    basketItem.createdAt          = model.createdAt;
    basketItem.updatedAt          = model.updatedAt;
    //properties
    basketItem.prodform_id        = model.prodform_id;
    basketItem.inn_id             = model.inn_id;
    basketItem.gentrade_id        = model.gentrade_id;
    basketItem.drugform_id        = model.drugform_id;
    basketItem.Firm_ID            = model.Firm_ID;
    basketItem.Country_ID         = model.Country_ID;
    basketItem.wsize              = model.wsize;
    basketItem.vmass              = model.vmass;
    basketItem.lsize              = model.lsize;
    basketItem.genprodform_name   = model.genprodform_name;
    basketItem.cubeunit           = model.cubeunit;
    basketItem.concunit           = model.concunit;
    basketItem.amass              = model.amass;
    basketItem.altlsize           = model.altlsize;
    basketItem.recipe             = model.recipe;
    basketItem.under_order        = model.under_order;
    basketItem.is_new             = model.is_new;
    //
    [NITBasketItem createOrUpdateInRealm:realm withValue:basketItem];
    [realm commitWriteTransaction];
    
    return basketItem;
}

- (void)getCatalogItemDetailsByID:(NSString *)itemID withHandler:(void(^)(SLCatalogItemWithRelations *result))handler
{
    [[SLCatalogsApi new] getStaticCatalogItemsByItemidWithItemId:itemID completionHandler:^(SLCatalogItemWithRelations *output, NSError *error) {
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (handler) handler (output);
    }];
}

#pragma mark - Get max count
- (void)getMaxCountByItemID:(NSString *)itemID
{
    [self getListOfShopsWthHandler:^(SLShopsResponse *output) {
        NSMutableArray <NSString *> * mutableShopIds = [NSMutableArray new];
        for(SLGroup * shop in output.items) [mutableShopIds addObject:shop._id];
        [self getItemsCountWithItemID:itemID withShops:[mutableShopIds mutableCopy] withhandler:^(NSArray<SLCatalogItemInShop *> *output) {
            int maxCount = 0;
            for(SLCatalogItemInShop * price in output)
            {
                if ([price.count intValue] > maxCount) maxCount = [price.count intValue];
            }
            
            if(maxCount > 0)
            {
                NITBasketItem * basketItem = [NITBasketItem basketItemByID:itemID];
                RLMRealm * realm = REALM.database;
                [realm beginWriteTransaction];
                basketItem.maxCount = @(maxCount);

                if([basketItem.count intValue] > [basketItem.maxCount intValue])
                {
                    basketItem.count = basketItem.maxCount;
                    [[NSNotificationCenter defaultCenter] postNotificationName:kChangeBasketNotification object:nil];
                }
                [NITBasketItem createOrUpdateInRealm:realm withValue:basketItem];
                [realm commitWriteTransaction];
            }
            else
            {
                NITBasketItem * basketItem = [NITBasketItem basketItemByID:itemID];
                [NITBasketItem deleteBasketItemByID:basketItem.identifier];
                [[NSNotificationCenter defaultCenter] postNotificationName:kChangeBasketNotification object:nil];
            }
        }];
    }];
}

- (void)getListOfShopsWthHandler:(void(^)(SLShopsResponse *output))handler
{
    if(USER.cityID)
    {
        [[SLShopsApi new] getStaticShopsGroupsByGroupidWithGroupId:USER.cityID completionHandler:^(SLShopsResponse *output, NSError *error) {
            [HELPER stopLoading];
            if (error) [HELPER logError:error method:METHOD_NAME];
            if (output) handler (output);
        }];
    }
}

- (void)getItemsCountWithItemID:(NSString *)itemID withShops:(NSArray <NSString *> *)shopIDs withhandler:(void(^)(NSArray <SLCatalogItemInShop *> *output))handler
{
    SLCatalogItemInShopQuery * query = [SLCatalogItemInShopQuery new];
    query.shopIds = shopIDs;
    [[SLCatalogsApi new] postStaticCatalogItemsByItemidShopsWithItemId:itemID body:query completionHandler:^(NSArray<SLCatalogItemInShop> *output, NSError *error) {
        if (error) [HELPER logError:error method:METHOD_NAME];
        if (output) handler (output);
    }];
}

@end
