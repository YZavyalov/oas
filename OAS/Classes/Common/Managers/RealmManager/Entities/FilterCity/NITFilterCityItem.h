//
//  NITFilterCityItem.h
//  OAS
//
//  Created by Yaroslav on 07.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Realm/Realm.h>

@interface NITFilterCityItem : RLMObject

@property NSString * identifier;
@property NSString * name;
@property NSNumber <RLMInt> * weight;
@property NSDate * timeCreate;

@end

RLM_ARRAY_TYPE(NITFilterCityItem)

@class SLInfoItemWithRelations;

@interface NITFilterCityItem (Extension)

#pragma mark - Create
+ (NITFilterCityItem *)createFilterCityItemByModel:(SLInfoItemWithRelations *)model;

#pragma mark - Get
+ (NSArray <NITFilterCityItem *> *)allFilterCityItem;
+ (NITFilterCityItem *)filterCityItemByID:(NSString *)identifier;

#pragma mark - Delete
+ (void)deleteAllFilterCityItems;
+ (void)deleteFilterCityItemByID:(NSString *)identifier;

@end

