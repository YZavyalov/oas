//
//  NITFilterCityLoader.h
//  OAS
//
//  Created by Yaroslav on 07.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NITFilterCityLoader : NSObject

- (void)updateFilterCityItemsWithID:(NSString *)identifier;

@end
