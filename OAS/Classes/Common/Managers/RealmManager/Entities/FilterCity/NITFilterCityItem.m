//
//  NITFilterCityItem.m
//  OAS
//
//  Created by Yaroslav on 07.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterCityItem.h"
#import "SLInfosystemsApi.h"
#import "RLMResults+NSArray.h"
#import "NSArray+BlocksKit.h"

@implementation NITFilterCityItem

+ (NSDictionary *)defaultPropertyValues
{
    return @{
             @"identifier"          : @"",
             @"name"                : @"",
             @"weight"              : @(0),
             @"timeCreate"          : [NSDate date]
             };
}

+ (NSString *)primaryKey
{
    return @"identifier";
}

@end

@implementation NITFilterCityItem (Extension)

#pragma mark - Create
+ (NITFilterCityItem *)createFilterCityItemByModel:(SLInfoItemWithRelations *)model
{
    if(!model)
    {
        return nil;
    }
    
    NITFilterCityItem * filterCityItem = [[NITFilterCityItem alloc] init];
    
    filterCityItem.identifier   = model._id;
    filterCityItem.name         = model.name;
    filterCityItem.timeCreate   = [NSDate date];

    [model.properties bk_each:^(SLIPropertyValue *obj) {
        if([obj.key isEqualToString:@"weight"]) filterCityItem.weight = @([obj.value.firstObject integerValue]);
    }];
    
    // save
    RLMRealm * realm = REALM.database;
    [realm beginWriteTransaction];
    [NITFilterCityItem createOrUpdateInRealm:realm withValue:filterCityItem];
    [realm commitWriteTransaction];
    
    return filterCityItem;
}

#pragma mark - Get
+ (NSArray <NITFilterCityItem *> *)allFilterCityItem
{
    return [[NITFilterCityItem allObjects] array];
}

+ (NITFilterCityItem *)filterCityItemByID:(NSString *)identifier
{
    return [NITFilterCityItem objectsWithPredicate:[NSPredicate predicateWithFormat:@"identifier == %@", identifier]].firstObject;
}

#pragma mark - Delete
+ (void)deleteAllFilterCityItems
{
    RLMResults * results = [NITFilterCityItem allObjects];
    
    if (results.count > 0)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObjects:results];
        [realm commitWriteTransaction];
    }
}

+ (void)deleteFilterCityItemByID:(NSString *)identifier
{
    NITFilterCityItem * filter = [NITFilterCityItem filterCityItemByID:identifier];
    
    if (filter)
    {
        RLMRealm * realm = REALM.database;
        
        [realm beginWriteTransaction];
        [realm deleteObject:filter];
        [realm commitWriteTransaction];
    }
}

@end
