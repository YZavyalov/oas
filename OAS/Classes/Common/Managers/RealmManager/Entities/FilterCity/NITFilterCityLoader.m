//
//  NITFilterCityLoader.m
//  OAS
//
//  Created by Yaroslav on 07.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITFilterCityLoader.h"
#import "SLInfosystemsApi.h"
#import "NITFilterCityItem.h"

@interface NITFilterCityLoader ()

@property (nonatomic) NSString * cityID;
@property (nonatomic) NSArray <SLInfoItemWithRelations *> * models;

@end

@implementation NITFilterCityLoader

#pragma mark - Public
- (void)updateFilterCityItemsWithID:(NSString *)identifier
{
    NSDate * currentDay = [NSDate date];
    NSDate * createDay = [[NITFilterCityItem allFilterCityItem].firstObject timeCreate];
    
    if(!([NITFilterCityItem allFilterCityItem].count > 0) || (currentDay.day != createDay.day))
    {
        [NITFilterCityItem deleteAllFilterCityItems];
    
        self.cityID = identifier;
        [self updateDataByOffset:nil];
    }
}

#pragma mark - Private
- (void)getFilterDescriptionWithID:(NSString *)infoID offset:(NSNumber *)offset withHandler:(void(^)(NSArray <SLInfoItemWithRelations *> *result))handler
{
    SLInfosystemQuery * query = [SLInfosystemQuery new];
    query.offset = offset;
    [[SLInfosystemsApi new] postStaticInfosystemsByInfosystemidQuerywithrelationsWithInfosystemId:infoID body:query completionHandler:^(NSArray<SLInfoItemWithRelations> *output, NSError *error) {
        if (error)
        {
            [ERROR_MESSAGE errorMessage:error_service_unavailable];
            [HELPER logError:error method:METHOD_NAME];
        }
        if (handler) handler (output);
    }];
}

- (void)getFilterOptionByGroupID:(NSString *)groupID
{
    [self getFilterDescriptionWithID:groupID offset:nil withHandler:^(NSArray<SLInfoItemWithRelations *> * result) {
        [self insertDataByModels:result];
    }];
}

- (void)getNextFilterOptionBygroupID:(NSString *)groupID offset:(NSNumber *)offset
{
    [self getFilterDescriptionWithID:groupID offset:offset withHandler:^(NSArray<SLInfoItemWithRelations *> * result) {
        [self insertDataByModels:result];
    }];
}

- (void)insertDataByModels:(NSArray<SLInfoItemWithRelations *> *)models
{
    if (models.count > 0)
    {
        NSMutableArray * allItems = [NSMutableArray arrayWithArray:self.models];
        [allItems addObjectsFromArray:models];
        
        self.models = allItems;
        
        [self checkLoadNextDataFromOffset:self.models.count];
    }
    else
    {
        [self filterCityFromEntitiesWithrelation:self.models];
        [[NSNotificationCenter defaultCenter] postNotificationName:kCreateFilterCityNotification object:nil];
        NSLog(@"create");
    }
}

- (void)checkLoadNextDataFromOffset:(NSInteger)offset
{
    [self updateDataByOffset:@(offset)];
}

- (void)updateDataByOffset:(NSNumber *)offset
{
    if (offset)
    {
        [self getNextFilterOptionBygroupID:self.cityID offset:offset];
    }
    else
    {
        [self getFilterOptionByGroupID:self.cityID];
    }
}

- (void)filterCityFromEntitiesWithrelation:(NSArray<SLInfoItemWithRelations *> *)entites
{
    [entites bk_map:^id(SLInfoItemWithRelations *obj) {
        return [NITFilterCityItem createFilterCityItemByModel:obj];
    }];
}

@end
