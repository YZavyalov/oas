//
//  NITRealmManager.h
//  OAS
//
//  Created by Eugene Parafiynyk on 10/19/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

#define REALM [NITRealmManager sharedInstance]

static NSString * kSaveFavoriteItemNotification = @"save_favorite_item_notification";
static NSString * kSaveBasketItemNotification   = @"save_basket_item_notification";
static NSString * kSaveOrderItemNotification    = @"save_order_item_notification";

@interface NITRealmManager : NSObject

@property (nonatomic, strong) RLMRealm * database;

+ (NITRealmManager *)sharedInstance;

@end
