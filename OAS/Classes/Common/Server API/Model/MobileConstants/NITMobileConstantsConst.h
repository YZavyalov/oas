//
//  NITMobileConstantsConst.h
//  OAS
//
//  Created by Yaroslav on 19.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#ifndef NITMobileConstantsConst_h
#define NITMobileConstantsConst_h

static NSString * kMobileConstanceID        = @"kMobileConstanceID";
static NSString * kTimeLimit                = @"kTimeLimit";
static NSString * kPickUpNow                = @"kPickUpNow";
static NSString * kPickUpToday              = @"kPickUpToday";
static NSString * kPickUpTomorrow           = @"kPickUpTomorrow";

static NSString * kFormFilterDesc           = @"kFormFilterDesc";
static NSString * kCountryFilterDesc        = @"kCountryFilterDesc";
static NSString * kManufacturerFilterDesc   = @"kManufacturerFilterDesc";

static NSString * kDeliveryHours1           = @"kDeliveryHours1";
static NSString * kDeliveryHours2           = @"kDeliveryHours2";
static NSString * kDeliveryHours3           = @"kDeliveryHours3";

static NSString * kContactMail              = @"kContactMail";
static NSString * kContactPhone             = @"kContactPhone";

static NSString * kDiscountLevel            = @"kDiscountLevel";

static NSString * kDocFreeDelivery          = @"kDocFreeDelivery";
static NSString * kDocMinSumPrice           = @"kDocMinSumPrice";

static NSString * kBonusInfo                = @"kBonusInfo";

#endif /* NITMobileConstantsConst_h */
