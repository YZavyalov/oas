//
//  NITMobileConstantsModel.m
//  OAS
//
//  Created by Yaroslav on 13.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITMobileConstantsModel.h"
#import "NITShopItemModel.h"

@implementation NITMobileConstantsModel

#pragma mark - Life cycle
- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    if(self = [super init])
    {
        self.time_order_limit = [dict objectForKey:@"time_order_limit"];
        self.pick_up_now =      [dict objectForKey:@"pick_up_now"];
        self.pick_up_today =    [dict objectForKey:@"pick_up_today"];
        self.pick_up_tomorrow = [dict objectForKey:@"pick_up_tomorrow"];
        
        self.form_filter_desc =         [dict objectForKey:@"form_filter_desc"];
        self.county_filter_desc =       [dict objectForKey:@"county_filter_desc"];
        self.manufacturer_filter_desc = [dict objectForKey:@"manufacturer_filter_desc"];
        
        self.delivery_hours_1 = [dict objectForKey:@"delivery_hours_1"];
        self.delivery_hours_2 = [dict objectForKey:@"delivery_hours_2"];
        self.delivery_hours_3 = [dict objectForKey:@"delivery_hours_3"];
        
        self.contact_mail = [dict objectForKey:@"contact_mail"];
        self.contact_phone = [dict objectForKey:@"contact_phone"];
        
        self.discount_level = [dict objectForKey:@"discount_level"];
        
        self.free_delivery = [dict objectForKey:@"free_delivery"];
        
        self.min_sum_of_order = [dict objectForKey:@"min_sum_of_order"];
        
        self.bonus_info = [dict objectForKey:@"bonus_info"];
    }

    return self;
}

#pragma mark - Custom accessors
//Documents ID
- (NSString *)mobileCosntantsID
{
    return [self stringForKey:kMobileConstanceID];
}

- (void)setMobileCosntantsID:(NSString *)mobileCosntantsID
{
    [self setString:mobileCosntantsID forKey:kMobileConstanceID];
}

//Available time
- (NSString *)time_order_limit
{
    return [self stringForKey:kTimeLimit];
}

- (void)setTime_order_limit:(NSString *)time_order_limit
{
    [self setString:time_order_limit forKey:kTimeLimit];
}

- (NSString *)pick_up_now
{
    return [self stringForKey:kPickUpNow];
}

- (void)setPick_up_now:(NSString *)pick_up_now
{
    [self setString:pick_up_now forKey:kPickUpNow];
}

- (NSString *)pick_up_today
{
    return [self stringForKey:kPickUpToday];
}

- (void)setPick_up_today:(NSString *)pick_up_today
{
    [self setString:pick_up_today forKey:kPickUpToday];
}

- (NSString *)pick_up_tomorrow
{
    return [self stringForKey:kPickUpTomorrow];
}

- (void)setPick_up_tomorrow:(NSString *)pick_up_tomorrow
{
    [self setString:pick_up_tomorrow forKey:kPickUpTomorrow];
}

//Delivery time
- (NSString *)delivery_hours_1
{
    return [self stringForKey:kDeliveryHours1];
}

- (void)setDelivery_hours_1:(NSString *)delivery_hours_1
{
    [self setString:delivery_hours_1 forKey:kDeliveryHours1];
}

- (NSString *)delivery_hours_2
{
    return [self stringForKey:kDeliveryHours2];
}

- (void)setDelivery_hours_2:(NSString *)delivery_hours_2
{
    [self setString:delivery_hours_2 forKey:kDeliveryHours2];
}

- (NSString *)delivery_hours_3
{
    return [self stringForKey:kDeliveryHours3];
}

- (void)setDelivery_hours_3:(NSString *)delivery_hours_3
{
    [self setString:delivery_hours_3 forKey:kDeliveryHours3];
}

//Desc
- (NSString *)county_filter_desc
{
    return [self stringForKey:kCountryFilterDesc];
}

- (void)setCounty_filter_desc:(NSString *)county_filter_desc
{
    [self setString:county_filter_desc forKey:kCountryFilterDesc];
}

- (NSString *)form_filter_desc
{
    return [self stringForKey:kFormFilterDesc];
}

- (void)setForm_filter_desc:(NSString *)form_filter_desc
{
    [self setString:form_filter_desc forKey:kFormFilterDesc];
}

- (NSString *)manufacturer_filter_desc
{
    return [self stringForKey:kManufacturerFilterDesc];
}

- (void)setManufacturer_filter_desc:(NSString *)manufacturer_filter_desc
{
    [self setString:manufacturer_filter_desc forKey:kManufacturerFilterDesc];
}

//Contact
- (NSString *)contact_mail
{
    return [self stringForKey:kContactMail];
}

- (void)setContact_mail:(NSString *)contact_mail
{
    [self setString:contact_mail forKey:kContactMail];
}

- (NSString *)contact_phone
{
    return [self stringForKey:kContactPhone];
}

- (void)setContact_phone:(NSString *)contact_phone
{
    [self setString:contact_phone forKey:kContactPhone];
}

//Discount and bonus
- (NSArray *)discount_level
{
    return [self arrayForKey:kDiscountLevel];
}

- (void)setDiscount_level:(NSArray *)discount_level
{
    [self setArray:discount_level forKey:kDiscountLevel];
}

//Free delivery
- (NSString *)free_delivery
{
    return [self stringForKey:kDocFreeDelivery];
}

- (void)setFree_delivery:(NSString *)free_delivery
{
    [self setString:free_delivery forKey:kDocFreeDelivery];
}

- (NSString *)min_sum_of_order
{
    return [self stringForKey:kDocMinSumPrice];
}

- (void)setMin_sum_of_order:(NSString *)min_sum_of_order
{
    [self setString:min_sum_of_order forKey:kDocMinSumPrice];
}

//Bonus Link
- (NSString *)bonus_info
{
    return [self stringForKey:kBonusInfo];
}

- (void)setBonus_info:(NSString *)bonus_info
{
    [self setString:bonus_info forKey:kBonusInfo];
}

#pragma mark - Public
- (NSString *)deliveryStringByModel:(NITShopItemModel *)model
{
    if([model.is_hub isEqualToString:@"true"])
    {
        USER.shopHubID = model.identifier;
        return self.pick_up_now;
    }
    else
    {
        NSInteger currentHour = [NSDate date].hour;
        NSInteger orderTime = [self.time_order_limit componentsSeparatedByString:@":"].firstObject.integerValue;
        if((int)currentHour < (int)orderTime)   return self.pick_up_today;
        else                                    return self.pick_up_tomorrow;
    }
}

@end
