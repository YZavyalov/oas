//
//  NITMobileConstantsModel.h
//  OAS
//
//  Created by Yaroslav on 13.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NITMobileConstantsConst.h"

@class NITShopItemModel;

@interface NITMobileConstantsModel : NITLocalSettings

@property (nonatomic) NSString * mobileCosntantsID;

@property (nonatomic) NSString * time_order_limit;
@property (nonatomic) NSString * pick_up_now;
@property (nonatomic) NSString * pick_up_today;
@property (nonatomic) NSString * pick_up_tomorrow;

@property (nonatomic) NSString * form_filter_desc;
@property (nonatomic) NSString * county_filter_desc;
@property (nonatomic) NSString * manufacturer_filter_desc;

@property (nonatomic) NSString * delivery_hours_1;
@property (nonatomic) NSString * delivery_hours_2;
@property (nonatomic) NSString * delivery_hours_3;

@property (nonatomic) NSString * contact_mail;
@property (nonatomic) NSString * contact_phone;

@property (nonatomic) NSArray * discount_level;

@property (nonatomic) NSString * free_delivery;
@property (nonatomic) NSString * min_sum_of_order;

@property (nonatomic) NSString * bonus_info;

- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSString *)deliveryStringByModel:(NITShopItemModel *)model;

@end
