//
//  NITInfosystems.m
//  OAS
//
//  Created by Yaroslav on 16.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITInfosystems.h"
#import "SLInfosystem.h"

@implementation NITInfosystems

#pragma mark - Life cycle
- (instancetype)initWithModel:(NSArray<SLInfosystem *> *)model
{
    if (self = [super init])
    {
        [model bk_each:^(SLInfosystem * p) {
            if ([self.allPropertyNames containsObject:p.name])
            {
                id value = p._id ? : @"";
                [self setValue:value forKey:p.name];
            }
            if ([p.name isEqualToString:@"Акции"])
                [self setValue:p._id forKey:@"stock"];
        }];
    }
    
    return self;
}

#pragma mark - Custom accessors
- (NSString *)stock
{
    return [self stringForKey:kInfosystemStock] ? [self stringForKey:kInfosystemStock]
    : @"41b1d498-bd73-45b6-8c69-8aafc5d5aaaa";
}

- (void)setStock:(NSString *)stock
{
    [self setString:stock forKey:kInfosystemStock];
}

- (NSString *)cities
{
    return [self stringForKey:kInfosystemCities];
}

- (void)setCities:(NSString *)cities
{
    [self setString:cities forKey:kInfosystemCities];
}

- (NSString *)banners
{
    return [self stringForKey:KinfosystemBanners];
}

- (void)setBanners:(NSString *)banners
{
    [self setString:banners forKey:KinfosystemBanners];
}

@end
