//
//  NITInfosystems.h
//  OAS
//
//  Created by Yaroslav on 16.06.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITInfosystemsConstants.h"
#import <Foundation/Foundation.h>

@class SLInfosystem;

@interface NITInfosystems : NITLocalSettings

@property (nonatomic) NSString * stock;
@property (nonatomic) NSString * cities;
@property (nonatomic) NSString * banners;

- (instancetype)initWithModel:(NSArray <SLInfosystem *> *)model;

@end
