//
//  NITInfosystemsConstants.h
//  OAS
//
//  Created by Yaroslav on 19.07.17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#ifndef NITInfosystemsConstants_h
#define NITInfosystemsConstants_h

static NSString * const kInfosystemStock    = @"kInfosystemStock";
static NSString * const kInfosystemCities   = @"kInfosystemCities";
static NSString * const KinfosystemBanners  = @"KinfosystemBanners";

#endif /* NITInfosystemsConstants_h */
