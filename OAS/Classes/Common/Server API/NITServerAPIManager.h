//
//  NITServerAPIManager.h
//  OAS
//
//  Created by Eugene Parafiynyk on 10/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITServerAPIConstants.h"

#define API [NITServerAPIManager sharedInstance]

static NSString * const kResetSessionNotification   = @"reset_session_notification";
static NSString * const kNetworkConnectionRestored  = @"network_connection_restored";
static NSString * const kUpdateViewNotification     = @"update_view_notification";

@interface NITServerAPIManager : NSObject

+ (NITServerAPIManager *)sharedInstance;

- (void)updateCurrentSession;
- (void)checkSessionError:(NSError *)error;
- (void)imageByPath:(NSString *)path complition:(void(^)(UIImage *image))complition;

#pragma mark - API data
- (void)syncUserData;
- (NSString *)infosystemStockID;
- (NSString *)infosysytemCitiesID;
- (NSString *)infosystemBannersID;
- (NSString *)bonusURL;
- (NSString *)mobileConstantID;

@end
