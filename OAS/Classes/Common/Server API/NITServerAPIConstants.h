//
//  NITServerAPIConstants.h
//  OAS
//
//  Created by Eugene Parafiynyk on 10/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

/**
 *  API type
 */
#define API_TYPE kAPITypeDevelopment

#define kAPITypeProduction  0
#define kAPITypeDevelopment 1

/**
 *  API version
 */
#define API_VERSION @"v1"

/**
 *  API url
 */
#if API_TYPE == kAPITypeProduction
static NSString * const BASE_SERVER_URL = [@"" stringByAppendingString:API_VERSION];
#else
static NSString * const BASE_SERVER_URL = @"";
#endif

/**
 *  Request logs
 */
static BOOL const API_LOG_ENABLED = false;

#pragma mark - Keychain
static NSString * const kKeychainServiceKey = @"IOS_OAS_SERVICE";
static NSString * const kKeychainAccountKey = @"IOS_OAS_ACCOUNT";
