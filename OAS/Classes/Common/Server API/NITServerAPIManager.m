//
//  NITServerAPIManager.m
//  OAS
//
//  Created by Eugene Parafiynyk on 10/21/16.
//  Copyright © 2016 Eugene Parafiynyk. All rights reserved.
//

#import "NITServerAPIManager.h"
#import "Reachability.h"
#import "UIImageView+WebCache.h"
#import "NITInfosystems.h"
#import "NITBasketItem.h"
#import "NITFavoriteItem.h"
#import "NITOrderItem.h"
#import "NITCityItem.h"
#import "NITDiscountCardModel.h"
#import "NITPharmacyItem.h"
#import "NITMobileConstantsModel.h"
#import "NITFilterItem.h"

@interface NITServerAPIManager ()

@property (nonatomic) Reachability * reachability;
@property (nonatomic) BOOL isOffline;

@end

@implementation NITServerAPIManager

+ (NITServerAPIManager *)sharedInstance
{
    static dispatch_once_t	once;
    static NITServerAPIManager * shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id)init
{
    if (self = [super init])
    {
        [self setup];
    }
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setup
{
    [self setupCache];
    [self addObservers];
    [self setupReachability];
}

#pragma mark - Custom accessors

#pragma mark - Puplic
- (void)updateCurrentSession
{
    
}

- (void)checkSessionError:(NSError *)error
{
    if ([USER isAutorize] && error.swaggerCode == 401)
    {
        if (HELPER.interfaceLoaded)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kResetSessionNotification object:nil];
        }
        else
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self checkSessionError:error];
            });
        }
    }
    else if (error.swaggerCode == kCFURLErrorNotConnectedToInternet && !self.isOffline)
    {
        if (HELPER.interfaceLoaded)
        {
            self.isOffline = true;
        }
        else
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self checkSessionError:error];
            });
        }
    }
}

#pragma mark - Image
- (void)imageByPath:(NSString *)path complition:(void(^)(UIImage *image))complition
{
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:path done:^(UIImage *image, SDImageCacheType cacheType) {
        
        if (!image)
        {
            if (complition) complition(nil);
            
            [self downloadAndSaveImage:path withCompletion:^(UIImage *image){
                 if (complition) complition(image);
             }];
        }
        else
        {
            if (complition) complition(image);
        }
    }];
}

- (void)downloadAndSaveImage:(NSString *)urlString withCompletion:(void(^)(UIImage *image))completion
{
    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:urlString]
                                                    options:SDWebImageContinueInBackground
                                                   progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                       // progression tracking code
                                                   }
                                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                                      
                                                      if (image)
                                                      {
                                                          if (completion) completion(image);
                                                          [[SDImageCache sharedImageCache] storeImage:image forKey:urlString];
                                                      }
                                                      else
                                                      {
                                                          UIImage * placeholder;
                                                          if (completion) completion(placeholder);
                                                      }
                                                  }];
}

#pragma mark - Private
- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
}

- (void)setupCache
{
    NSURLCache * URLCache = [[NSURLCache alloc] initWithMemoryCapacity:5 * 1024 * 1024
                                                          diskCapacity:50 * 1024 * 1024
                                                              diskPath:nil];
    [NSURLCache setSharedURLCache:URLCache];
}

- (void)setupReachability
{
    if (self.reachability)
    {
        [self.reachability stopNotifier];
        self.reachability = nil;
    }
    self.reachability = [Reachability reachabilityWithHostname:@"http://www.google.com"];
    [self.reachability startNotifier];
}

- (void)reachabilityChanged:(NSNotification *)notification
{
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus] == NotReachable)
    {
        self.isOffline = true;
        [HELPER isFirstLanch] ? [ERROR_MESSAGE errorMessage:error_connectivity_unavailable] : [ERROR_MESSAGE errorMessage:error_connectivity_unavailable_on_splash];
    }
    else
    {
        if(self.isOffline)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateInternetConnect object:nil];
        }
        self.isOffline = false;
        [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkConnectionRestored object:nil];
    }
    /*if ([self.reachability currentReachabilityStatus] != NotReachable)
    {
        self.isOffline = false;
        [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkConnectionRestored object:nil];
    }*/
}

#pragma mark - Static API data
- (void)syncUserData
{
    [[NITCityItem new] updateCityItems];
    [[NITPharmacyItem new] updatePharmacyItems];
    [[NITBasketItem new] updateBasketItems];
    [[NITFavoriteItem new] updateFavoriteItems];
    [[NITOrderItem new] updateOrderItems];
    [[NITDiscountCardModel new] updateDiscountCard];
    [[NITFilterItem new] updateFilterItems];
}

- (NSString *)infosystemStockID
{
    NITInfosystems * infosystems = [NITInfosystems new];
    return infosystems.stock;
}

- (NSString *)infosystemBannersID
{
    NITInfosystems * infosystems = [NITInfosystems new];
    return infosystems.banners;
}

- (NSString *)infosysytemCitiesID
{
    NITInfosystems * infosystems = [NITInfosystems new];
    return infosystems.cities;
}

- (NSString *)bonusURL
{
    return @"http://it.oac74.ru/Calc/Calculator.svc/xml/BonusSum?id=";
}

- (NSString *)mobileConstantID
{
    NITMobileConstantsModel * mobileConstant = [NITMobileConstantsModel new];
    return mobileConstant.mobileCosntantsID;
}

@end
