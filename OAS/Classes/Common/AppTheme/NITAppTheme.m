//
//  NITAppTheme.m
//  OAS
//
//  Created by Eugene Parafiynyk on 4/12/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "NITAppTheme.h"

@implementation NITAppTheme

+ (void)applyTheme
{
    // nav bar
    [[UINavigationBar appearance] setTintColor:RGB(0, 0, 0)];
    [[UINavigationBar appearance] setBarTintColor:RGB(245, 248, 248)];
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIFont boldSystemFontOfSize:17.0], NSFontAttributeName,nil]];
    
    // tab bar
    [[UITabBar appearance] setTintColor:RGB(58, 160, 144)];
    if ([[UITabBar appearance] respondsToSelector:@selector(setUnselectedItemTintColor:)])
    {
        [[UITabBar appearance] setUnselectedItemTintColor:[UIColor redColor]];
    }
}

@end
