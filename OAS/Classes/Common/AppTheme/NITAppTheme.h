//
//  NITAppTheme.h
//  OAS
//
//  Created by Eugene Parafiynyk on 4/12/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NITAppTheme : NSObject

+ (void)applyTheme;

@end
