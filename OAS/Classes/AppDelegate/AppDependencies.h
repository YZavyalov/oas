//
//  AppDependencies.h
//  OAS
//
//  Created by Eugene Parafiynyk on 4/12/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppDependencies : NSObject

- (void)installRootViewControllerIntoWindow:(UIWindow *)window;
- (void)deallocDependencies;

- (void)handleOpenFromUrl:(NSURL *)url;

+ (void)registerPushNotificationsWithOptions:(NSDictionary *)launchOptions;
- (void)initPushNotificationsWithOptions:(NSDictionary *)launchOptions;

- (void)sendPushToken;

@end
