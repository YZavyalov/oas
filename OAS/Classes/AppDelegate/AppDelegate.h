//
//  AppDelegate.h
//  OAS
//
//  Created by Eugene Parafiynyk on 4/12/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "AppDependencies.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;
@property (nonatomic, strong) AppDependencies * appDependencies;

@end

