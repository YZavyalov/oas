//
//  AppDependencies.m
//  OAS
//
//  Created by Eugene Parafiynyk on 4/12/17.
//  Copyright © 2017 Eugene Parafiynyk. All rights reserved.
//

#import "AppDependencies.h"
#import "NITAppTheme.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "IQKeyboardManager.h"
#import <OneSignal/OneSignal.h>
#import "NITNotificationView.h"
#import "NITLaunchScreenWireFrame.h"
#import "SLUsersApi.h"

static NSString * ONESIGNAL_APPID = @"4c742869-55ac-4fdd-8673-1ad6393b4ee6";

// Log levels: off, error, warn, info, verbose
__unused static const int ddLogLevel = LOG_LEVEL_INFO;

@interface AppDependencies () <CrashlyticsDelegate>

@property (nonatomic, strong) NITLaunchScreenWireFrame * rootWireframe;

@end

@implementation AppDependencies

#pragma mark - LifeCycle
- (instancetype)init
{
    if ((self = [super init]))
    {
        [self configureDependencies];
    }
    
    return self;
}

- (void)configureDependencies
{
//    [SLDefaultConfiguration sharedConfig].host = @"http://dev.sl.itnap.ru";
    //[SLDefaultConfiguration sharedConfig].debug = true;
    SLDefaultConfiguration *apiConfig = [SLDefaultConfiguration sharedConfig];
    [apiConfig setApiKey:USER.company forApiKeyIdentifier:@"x-cid"];
    [apiConfig setApiKey:USER.accessToken forApiKeyIdentifier:@"x-token"];
    
    // crashes
    [Fabric with:@[[Crashlytics class]]];
    
    // logs
    [self loadLogging];
    [API updateCurrentSession];
    
    // init DB
    [NITRealmManager sharedInstance];
    
    // init managers
    [UserData sharedInstance];
    [NITHelperManager sharedInstance];
    
    // register Google maps id
    [GMSServices provideAPIKey:kGoogleAPIKey];
    [TRACKER initGoogleAnalytics];
    
    // keyboard
    [IQKeyboardManager sharedManager].enable = true;
    [IQKeyboardManager sharedManager].toolbarDoneBarButtonItemText = NSLocalizedString(@"Готово", nil);
    
    // ui
    [NITAppTheme applyTheme];
    self.rootWireframe = [[NITLaunchScreenWireFrame alloc] init];
}

- (void)deallocDependencies
{
    //
}

- (void)installRootViewControllerIntoWindow:(UIWindow *)window
{
    [self.rootWireframe presentNITLaunchScreenModuleFromWindow:window];
}

#pragma mark - Crashlytics
- (void) loadCrashlytics
{
    CrashlyticsKit.delegate = self;
    [Fabric with:@[[Crashlytics class]]];
}

#pragma mark - CrashlyticsDelegate
- (BOOL)crashlyticsCanUseBackgroundSessions:(Crashlytics *)crashlytics
{
    return true;
}

#pragma mark - Logs
- (void)loadLogging
{
    for (DDAbstractLogger * logger in @[[DDTTYLogger sharedInstance]])
    {
        [DDLog addLogger:logger];
    }
}

#pragma mark - Notification
+ (void)registerPushNotificationsWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    
    [OneSignal initWithLaunchOptions:launchOptions appId:ONESIGNAL_APPID handleNotificationReceived:^(OSNotification *notification) {
        
        OSNotificationPayload * payload = notification.payload;
        
        NSString * messageTitle = @"";
        NSString * fullMessage = [payload.body copy];
        
        if (payload.title.length > 0)
        {
            messageTitle = [NSString stringWithFormat:@"%@. ", payload.title];
        }
        
        [NITNotificationView showWithText:[messageTitle stringByAppendingString:fullMessage] tapHandler:^{}];
        
    } handleNotificationAction:nil settings:@{kOSSettingsKeyInAppAlerts:@false}];
    
    [OneSignal IdsAvailable:^(NSString *userId, NSString *pushToken) {
        [HELPER logString:[NSString stringWithFormat:@"OneSignal pushToken: %@", pushToken]];
        [HELPER logString:[NSString stringWithFormat:@"OneSignal userId: %@", userId]];
    }];
}

- (void)initPushNotificationsWithOptions:(NSDictionary *)launchOptions
{
    if((USER.pushTokenString.length > 0)) //&& ![HELPER isSendPushToken])
    {
        [self sendPushToken];
    }
    
    if ([HELPER isFirstLanch])
    {
        [AppDependencies registerPushNotificationsWithOptions:launchOptions];
    }
}

#pragma mark - Handle open from url
- (void)handleOpenFromUrl:(NSURL *)url
{
    NSString * query = [url query];
    NSArray * queryPairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary * pairs = [NSMutableDictionary dictionary];
    
    for (NSString * queryPair in queryPairs)
    {
        NSArray * bits = [queryPair componentsSeparatedByString:@"="];
        
        if ([bits count] != 2)
        {
            continue;
        }
        
        NSString * key = [[bits objectAtIndex:0] stringByRemovingPercentEncoding];
        NSString * value = [[bits objectAtIndex:1] stringByRemovingPercentEncoding];
        
        [pairs setObject:value forKey:key];
    }
    
    DDLogInfo(@"%@ %@", THIS_METHOD, pairs);
}

#pragma mark - Push token
- (void)sendPushToken
{
    SLUserWithRelations * query = [SLUserWithRelations new];
    query.uid = USER.identifier;
    SLCommunication * communication = [SLCommunication new];
    communication.type = @"iosPushToken";
    communication.value = USER.pushTokenString;
    
    query.communications = @[communication];
    
    [[SLUsersApi new] postDynamicUserByUseridWithUserId:USER.identifier body:query completionHandler:^(SLUserWithRelations *output, NSError *error) {
        if(error)
        {
            [HELPER logError:error method:METHOD_NAME];
        }
    }];
    
    //[HELPER setSendPushToken];
}

@end
