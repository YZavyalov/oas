#import <Foundation/Foundation.h>
#import "SLErrorModel.h"
#import "SLTuple.h"
#import "SLTupleQuery.h"
#import "SLUserSpace.h"
#import "SLUserSpaceInfo.h"
#import "SLApi.h"

/**
* Napoleon S&L API
* ### Static Data ### Приложение статичной информации (Static Data App) Текущий документ является описанием протокола микросервиса статического контента.   Сервис обеспечивает хранение и модификацию информации не изменяемой пользователями мобильного приложения.   ### Синтаксис выборки объектов из каталога (catalogQuery)  Для выборки объектов из каталога, помимо основных параметров запроса, имеется поле query, целью которого является покрытие всех возможных вариантов выборок необходимых разработчику с учетом специфики дополнительных свойств товаров. Значением данного параметра является JSON объект, который передается строкой в base64 формате.  #### Правила формирования JSON Query для запросов по объектам  Значения ключей:    * n - название свойства объекта. Доступны как основные поля объекта, так и дополнительные свойства   * v - значение или значения свойства, указанного в поле \"n\". На данный момент описаны три варианта значения данного поля.      - Объект с подключами min и max, для чисел, указывающих максимально и минимально выбираемые значения. Пример: {\"min\":25, \"max\":50} = \"25 < property AND property < 50\"     - Массив значений. Пример: [25, 30, 50] = \"property = 25 OR property = 30 OR property = 50\"     - Конкретное значение. Пример 25 = \"property = 25\"   * op - указывает, что данный объект является группой. Значение указывает, какую нужно применять операцию к объектам в массиве \"pr\". Возможные значения \"&\"(и) или \"|\"(или).    * pr - массив объектов, которые требуется обработать в соответствии с правилом указанным в поле \"op\". В массиве могут содержаться объекты как с \"op\" и \"pr\", так и с \"n\" и \"v\", описывающими работу уже с конкретными полями.      ```JSON Query: {   \"op\":\"&\",   \"pr\": [     {       \"n\":\"property1Name\",       \"v\":{\"min\":10, \"max\":50}     },     {       \"n\":\"property2Name\",       \"v\":[\"value1\", \"value2\", \"value3\"]     },     {       \"n\":\"property3Name\",       \"v\":\"value6\"     },     {       \"op\":\"|\",       \"pr\":[         {           \"n\":\"property7Name\",           \"v\":\"value10\"         },         {           \"n\":\"property13Name\",           \"v\":\"value54\",           \"cmp\":\"LIKE\"         }       ]     }   ] }  SQL WHERE: (10 =< \"property1Name\" OR \"property1Name\" =< 50) AND (\"property2Name\" = \"value1\" OR \"property2Name\" = \"value2\" OR \"property2Name\" = \"value3\") AND \"property3Name\" = \"value6\" AND (\"property7Name\" = \"value10\" OR \"property13Name\" LIKE \"value54%\") ``` ### Синтаксис выборки объектов из каталога (catalogElasticQuery) ```JSON Query: {   \"op\": \"opMust\",   \"pr\": [     {       // Товары у которых название = очки       \"n\": \"name\",       \"v\": \"очки\",       \"cmp\": \"term\"     },     {       // Диапазон цен от 0 до 300       \"n\": \"price\",       \"v\": [          null,         300       ]     },     {       \"op\": \"opMust\",       \"pr\": [         {           // Ключ свойства = is__country           \"n\": \"properties.key\",           \"v\": \"is__country\"         },         {           // Ключ значение свойства = 042f0b8c-f985-40e1-87b8-7c56f75a8666           \"n\": \"properties.value\",           \"v\": \"042f0b8c-f985-40e1-87b8-7c56f75a8666\"         }       ]     },     {       // Идентификатор родительской группы == 042f0b8c-f985-40e1-87b8-7c56f75a8534       \"n\": \"groupId\",       \"v\": \"042f0b8c-f985-40e1-87b8-7c56f75a8534\"     }   ] } ``` ### Dynamic Data ### Приложение пользовательских данных (Dynamic Data App) Текущий документ является описанием интерфейса системы хранения динамических пользовательских данных. Данная система используется для хранения и модификации таких данных как: данные профиля, избранное, сохраненные поиски и другие. ### Signin Signup Сервис регистрации и авторизации ### Support Сервис обратной связи и поддержки
*
* OpenAPI spec version: 1.1.36
* Contact: ag@napoleonit.ru
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/



@interface SLSpaceApi: NSObject <SLApi>

extern NSString* kSLSpaceApiErrorDomain;
extern NSInteger kSLSpaceApiMissingParamErrorCode;

-(instancetype) initWithApiClient:(SLApiClient *)apiClient NS_DESIGNATED_INITIALIZER;

/// 
/// Удаление пространства
///
/// @param spaceKey Ключ пространства пользователя
/// @param spaceVersion Версия пространства пользователя (optional)
/// 
///  code:200 message:"Пространство удалено",
///  code:0 message:"Ошибка"
///
/// @return 
-(NSURLSessionTask*) deleteDynamicSpaceBySpacekeyWithSpaceKey: (NSString*) spaceKey
    spaceVersion: (NSString*) spaceVersion
    completionHandler: (void (^)(NSError* error)) handler;


/// 
/// Удаление кортежа
///
/// @param spaceKey Ключ пространства пользователя
/// @param tupleId Идентификатор кортежа
/// @param spaceVersion Версия пространства пользователя (optional)
/// 
///  code:200 message:"Кортеж удален",
///  code:0 message:"Ошибка"
///
/// @return 
-(NSURLSessionTask*) deleteDynamicSpaceBySpacekeyTuplesByTupleidWithSpaceKey: (NSString*) spaceKey
    tupleId: (NSString*) tupleId
    spaceVersion: (NSString*) spaceVersion
    completionHandler: (void (^)(NSError* error)) handler;


/// 
/// Запрос информации о текущем пространстве по ключу
///
/// @param spaceKey Ключ пространства пользователя
/// @param spaceVersion Версия пространства пользователя (optional)
/// 
///  code:200 message:"Информация о пространстве",
///  code:0 message:"Ошибка"
///
/// @return SLUserSpaceInfo*
-(NSURLSessionTask*) getDynamicSpaceBySpacekeyWithSpaceKey: (NSString*) spaceKey
    spaceVersion: (NSString*) spaceVersion
    completionHandler: (void (^)(SLUserSpaceInfo* output, NSError* error)) handler;


/// 
/// Запрос кортежа пространства
///
/// @param spaceKey Ключ пространства пользователя
/// @param tupleId Идентификатор кортежа
/// @param spaceVersion Версия пространства пользователя (optional)
/// 
///  code:200 message:"Выбранный кортеж пространства",
///  code:0 message:"Ошибка"
///
/// @return SLTuple*
-(NSURLSessionTask*) getDynamicSpaceBySpacekeyTuplesByTupleidWithSpaceKey: (NSString*) spaceKey
    tupleId: (NSString*) tupleId
    spaceVersion: (NSString*) spaceVersion
    completionHandler: (void (^)(SLTuple* output, NSError* error)) handler;


/// 
/// Создание пространства в зависимости от spaceKey и версии схемы
///
/// @param body 
/// 
///  code:201 message:"Схема созданна",
///  code:0 message:"Ошибка"
///
/// @return SLUserSpace*
-(NSURLSessionTask*) postDynamicSpaceWithBody: (SLUserSpace*) body
    completionHandler: (void (^)(SLUserSpace* output, NSError* error)) handler;


/// 
/// Запрос кортежей пространства
///
/// @param spaceKey Ключ пространства пользователя
/// @param body 
/// 
///  code:200 message:"Выбранные кортежи пространства",
///  code:0 message:"Ошибка"
///
/// @return NSArray<SLTuple>*
-(NSURLSessionTask*) postDynamicSpaceBySpacekeyQueryWithSpaceKey: (NSString*) spaceKey
    body: (SLTupleQuery*) body
    completionHandler: (void (^)(NSArray<SLTuple>* output, NSError* error)) handler;


/// 
/// Вставка кортежа
///
/// @param spaceKey Ключ пространства пользователя
/// @param body 
/// @param spaceVersion Версия пространства пользователя (optional)
/// 
///  code:200 message:"Вставленный кортеж",
///  code:0 message:"Ошибка"
///
/// @return SLTuple*
-(NSURLSessionTask*) postDynamicSpaceBySpacekeyTuplesWithSpaceKey: (NSString*) spaceKey
    body: (SLTuple*) body
    spaceVersion: (NSString*) spaceVersion
    completionHandler: (void (^)(SLTuple* output, NSError* error)) handler;


/// 
/// Обновление кортежа
///
/// @param spaceKey Ключ пространства пользователя
/// @param tupleId Идентификатор кортежа
/// @param body 
/// @param spaceVersion Версия пространства пользователя (optional)
/// 
///  code:200 message:"Обновленный кортеж",
///  code:0 message:"Ошибка"
///
/// @return SLTuple*
-(NSURLSessionTask*) postDynamicSpaceBySpacekeyTuplesByTupleidWithSpaceKey: (NSString*) spaceKey
    tupleId: (NSString*) tupleId
    body: (SLTuple*) body
    spaceVersion: (NSString*) spaceVersion
    completionHandler: (void (^)(SLTuple* output, NSError* error)) handler;



@end
