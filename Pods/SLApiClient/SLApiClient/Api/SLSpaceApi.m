#import "SLSpaceApi.h"
#import "SLQueryParamCollection.h"
#import "SLApiClient.h"
#import "SLErrorModel.h"
#import "SLTuple.h"
#import "SLTupleQuery.h"
#import "SLUserSpace.h"
#import "SLUserSpaceInfo.h"


@interface SLSpaceApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SLSpaceApi

NSString* kSLSpaceApiErrorDomain = @"SLSpaceApiErrorDomain";
NSInteger kSLSpaceApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SLApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SLApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// 
/// Удаление пространства
///  @param spaceKey Ключ пространства пользователя 
///
///  @param spaceVersion Версия пространства пользователя (optional)
///
///  @returns void
///
-(NSURLSessionTask*) deleteDynamicSpaceBySpacekeyWithSpaceKey: (NSString*) spaceKey
    spaceVersion: (NSString*) spaceVersion
    completionHandler: (void (^)(NSError* error)) handler {
    // verify the required parameter 'spaceKey' is set
    if (spaceKey == nil) {
        NSParameterAssert(spaceKey);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"spaceKey"] };
            NSError* error = [NSError errorWithDomain:kSLSpaceApiErrorDomain code:kSLSpaceApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/dynamic/space/{spaceKey}/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (spaceKey != nil) {
        pathParams[@"spaceKey"] = spaceKey;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (spaceVersion != nil) {
        queryParams[@"spaceVersion"] = spaceVersion;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"DELETE"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: nil
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler(error);
                                }
                            }];
}

///
/// 
/// Удаление кортежа
///  @param spaceKey Ключ пространства пользователя 
///
///  @param tupleId Идентификатор кортежа 
///
///  @param spaceVersion Версия пространства пользователя (optional)
///
///  @returns void
///
-(NSURLSessionTask*) deleteDynamicSpaceBySpacekeyTuplesByTupleidWithSpaceKey: (NSString*) spaceKey
    tupleId: (NSString*) tupleId
    spaceVersion: (NSString*) spaceVersion
    completionHandler: (void (^)(NSError* error)) handler {
    // verify the required parameter 'spaceKey' is set
    if (spaceKey == nil) {
        NSParameterAssert(spaceKey);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"spaceKey"] };
            NSError* error = [NSError errorWithDomain:kSLSpaceApiErrorDomain code:kSLSpaceApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    // verify the required parameter 'tupleId' is set
    if (tupleId == nil) {
        NSParameterAssert(tupleId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"tupleId"] };
            NSError* error = [NSError errorWithDomain:kSLSpaceApiErrorDomain code:kSLSpaceApiMissingParamErrorCode userInfo:userInfo];
            handler(error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/dynamic/space/{spaceKey}/tuples/{tupleId}/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (spaceKey != nil) {
        pathParams[@"spaceKey"] = spaceKey;
    }
    if (tupleId != nil) {
        pathParams[@"tupleId"] = tupleId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (spaceVersion != nil) {
        queryParams[@"spaceVersion"] = spaceVersion;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"DELETE"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: nil
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler(error);
                                }
                            }];
}

///
/// 
/// Запрос информации о текущем пространстве по ключу
///  @param spaceKey Ключ пространства пользователя 
///
///  @param spaceVersion Версия пространства пользователя (optional)
///
///  @returns SLUserSpaceInfo*
///
-(NSURLSessionTask*) getDynamicSpaceBySpacekeyWithSpaceKey: (NSString*) spaceKey
    spaceVersion: (NSString*) spaceVersion
    completionHandler: (void (^)(SLUserSpaceInfo* output, NSError* error)) handler {
    // verify the required parameter 'spaceKey' is set
    if (spaceKey == nil) {
        NSParameterAssert(spaceKey);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"spaceKey"] };
            NSError* error = [NSError errorWithDomain:kSLSpaceApiErrorDomain code:kSLSpaceApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/dynamic/space/{spaceKey}/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (spaceKey != nil) {
        pathParams[@"spaceKey"] = spaceKey;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (spaceVersion != nil) {
        queryParams[@"spaceVersion"] = spaceVersion;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SLUserSpaceInfo*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SLUserSpaceInfo*)data, error);
                                }
                            }];
}

///
/// 
/// Запрос кортежа пространства
///  @param spaceKey Ключ пространства пользователя 
///
///  @param tupleId Идентификатор кортежа 
///
///  @param spaceVersion Версия пространства пользователя (optional)
///
///  @returns SLTuple*
///
-(NSURLSessionTask*) getDynamicSpaceBySpacekeyTuplesByTupleidWithSpaceKey: (NSString*) spaceKey
    tupleId: (NSString*) tupleId
    spaceVersion: (NSString*) spaceVersion
    completionHandler: (void (^)(SLTuple* output, NSError* error)) handler {
    // verify the required parameter 'spaceKey' is set
    if (spaceKey == nil) {
        NSParameterAssert(spaceKey);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"spaceKey"] };
            NSError* error = [NSError errorWithDomain:kSLSpaceApiErrorDomain code:kSLSpaceApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'tupleId' is set
    if (tupleId == nil) {
        NSParameterAssert(tupleId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"tupleId"] };
            NSError* error = [NSError errorWithDomain:kSLSpaceApiErrorDomain code:kSLSpaceApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/dynamic/space/{spaceKey}/tuples/{tupleId}/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (spaceKey != nil) {
        pathParams[@"spaceKey"] = spaceKey;
    }
    if (tupleId != nil) {
        pathParams[@"tupleId"] = tupleId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (spaceVersion != nil) {
        queryParams[@"spaceVersion"] = spaceVersion;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SLTuple*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SLTuple*)data, error);
                                }
                            }];
}

///
/// 
/// Создание пространства в зависимости от spaceKey и версии схемы
///  @param body  
///
///  @returns SLUserSpace*
///
-(NSURLSessionTask*) postDynamicSpaceWithBody: (SLUserSpace*) body
    completionHandler: (void (^)(SLUserSpace* output, NSError* error)) handler {
    // verify the required parameter 'body' is set
    if (body == nil) {
        NSParameterAssert(body);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"body"] };
            NSError* error = [NSError errorWithDomain:kSLSpaceApiErrorDomain code:kSLSpaceApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/dynamic/space/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = body;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SLUserSpace*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SLUserSpace*)data, error);
                                }
                            }];
}

///
/// 
/// Запрос кортежей пространства
///  @param spaceKey Ключ пространства пользователя 
///
///  @param body  
///
///  @returns NSArray<SLTuple>*
///
-(NSURLSessionTask*) postDynamicSpaceBySpacekeyQueryWithSpaceKey: (NSString*) spaceKey
    body: (SLTupleQuery*) body
    completionHandler: (void (^)(NSArray<SLTuple>* output, NSError* error)) handler {
    // verify the required parameter 'spaceKey' is set
    if (spaceKey == nil) {
        NSParameterAssert(spaceKey);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"spaceKey"] };
            NSError* error = [NSError errorWithDomain:kSLSpaceApiErrorDomain code:kSLSpaceApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'body' is set
    if (body == nil) {
        NSParameterAssert(body);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"body"] };
            NSError* error = [NSError errorWithDomain:kSLSpaceApiErrorDomain code:kSLSpaceApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/dynamic/space/{spaceKey}/query/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (spaceKey != nil) {
        pathParams[@"spaceKey"] = spaceKey;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = body;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SLTuple>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SLTuple>*)data, error);
                                }
                            }];
}

///
/// 
/// Вставка кортежа
///  @param spaceKey Ключ пространства пользователя 
///
///  @param body  
///
///  @param spaceVersion Версия пространства пользователя (optional)
///
///  @returns SLTuple*
///
-(NSURLSessionTask*) postDynamicSpaceBySpacekeyTuplesWithSpaceKey: (NSString*) spaceKey
    body: (SLTuple*) body
    spaceVersion: (NSString*) spaceVersion
    completionHandler: (void (^)(SLTuple* output, NSError* error)) handler {
    // verify the required parameter 'spaceKey' is set
    if (spaceKey == nil) {
        NSParameterAssert(spaceKey);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"spaceKey"] };
            NSError* error = [NSError errorWithDomain:kSLSpaceApiErrorDomain code:kSLSpaceApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'body' is set
    if (body == nil) {
        NSParameterAssert(body);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"body"] };
            NSError* error = [NSError errorWithDomain:kSLSpaceApiErrorDomain code:kSLSpaceApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/dynamic/space/{spaceKey}/tuples/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (spaceKey != nil) {
        pathParams[@"spaceKey"] = spaceKey;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (spaceVersion != nil) {
        queryParams[@"spaceVersion"] = spaceVersion;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = body;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SLTuple*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SLTuple*)data, error);
                                }
                            }];
}

///
/// 
/// Обновление кортежа
///  @param spaceKey Ключ пространства пользователя 
///
///  @param tupleId Идентификатор кортежа 
///
///  @param body  
///
///  @param spaceVersion Версия пространства пользователя (optional)
///
///  @returns SLTuple*
///
-(NSURLSessionTask*) postDynamicSpaceBySpacekeyTuplesByTupleidWithSpaceKey: (NSString*) spaceKey
    tupleId: (NSString*) tupleId
    body: (SLTuple*) body
    spaceVersion: (NSString*) spaceVersion
    completionHandler: (void (^)(SLTuple* output, NSError* error)) handler {
    // verify the required parameter 'spaceKey' is set
    if (spaceKey == nil) {
        NSParameterAssert(spaceKey);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"spaceKey"] };
            NSError* error = [NSError errorWithDomain:kSLSpaceApiErrorDomain code:kSLSpaceApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'tupleId' is set
    if (tupleId == nil) {
        NSParameterAssert(tupleId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"tupleId"] };
            NSError* error = [NSError errorWithDomain:kSLSpaceApiErrorDomain code:kSLSpaceApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'body' is set
    if (body == nil) {
        NSParameterAssert(body);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"body"] };
            NSError* error = [NSError errorWithDomain:kSLSpaceApiErrorDomain code:kSLSpaceApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/dynamic/space/{spaceKey}/tuples/{tupleId}/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (spaceKey != nil) {
        pathParams[@"spaceKey"] = spaceKey;
    }
    if (tupleId != nil) {
        pathParams[@"tupleId"] = tupleId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (spaceVersion != nil) {
        queryParams[@"spaceVersion"] = spaceVersion;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = body;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SLTuple*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SLTuple*)data, error);
                                }
                            }];
}



@end
