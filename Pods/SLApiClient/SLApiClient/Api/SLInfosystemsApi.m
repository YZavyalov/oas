#import "SLInfosystemsApi.h"
#import "SLQueryParamCollection.h"
#import "SLApiClient.h"
#import "SLErrorModel.h"
#import "SLIProperty.h"
#import "SLInfoItem.h"
#import "SLInfoItemWithRelations.h"
#import "SLInfosystem.h"
#import "SLInfosystemQuery.h"
#import "SLTag.h"


@interface SLInfosystemsApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SLInfosystemsApi

NSString* kSLInfosystemsApiErrorDomain = @"SLInfosystemsApiErrorDomain";
NSInteger kSLInfosystemsApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SLApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SLApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// 
/// Запрос списка информационных систем
///  @returns NSArray<SLInfosystem>*
///
-(NSURLSessionTask*) getStaticInfosystemsWithCompletionHandler: 
    (void (^)(NSArray<SLInfosystem>* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/static/infosystems/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SLInfosystem>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SLInfosystem>*)data, error);
                                }
                            }];
}

///
/// 
/// Запрос информации об информационной системе
///  @param infosystemId Идентификатор информационной системы 
///
///  @returns SLInfosystem*
///
-(NSURLSessionTask*) getStaticInfosystemsByInfosystemidWithInfosystemId: (NSString*) infosystemId
    completionHandler: (void (^)(SLInfosystem* output, NSError* error)) handler {
    // verify the required parameter 'infosystemId' is set
    if (infosystemId == nil) {
        NSParameterAssert(infosystemId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"infosystemId"] };
            NSError* error = [NSError errorWithDomain:kSLInfosystemsApiErrorDomain code:kSLInfosystemsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/static/infosystems/{infosystemId}/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (infosystemId != nil) {
        pathParams[@"infosystemId"] = infosystemId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SLInfosystem*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SLInfosystem*)data, error);
                                }
                            }];
}

///
/// 
/// Запрос элемента информационной системы
///  @param infosystemId Идентификатор информационной системы 
///
///  @param itemId Идентификатор элемента 
///
///  @returns SLInfoItemWithRelations*
///
-(NSURLSessionTask*) getStaticInfosystemsByInfosystemidItemsByItemidWithInfosystemId: (NSString*) infosystemId
    itemId: (NSString*) itemId
    completionHandler: (void (^)(SLInfoItemWithRelations* output, NSError* error)) handler {
    // verify the required parameter 'infosystemId' is set
    if (infosystemId == nil) {
        NSParameterAssert(infosystemId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"infosystemId"] };
            NSError* error = [NSError errorWithDomain:kSLInfosystemsApiErrorDomain code:kSLInfosystemsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'itemId' is set
    if (itemId == nil) {
        NSParameterAssert(itemId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"itemId"] };
            NSError* error = [NSError errorWithDomain:kSLInfosystemsApiErrorDomain code:kSLInfosystemsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/static/infosystems/{infosystemId}/items/{itemId}/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (infosystemId != nil) {
        pathParams[@"infosystemId"] = infosystemId;
    }
    if (itemId != nil) {
        pathParams[@"itemId"] = itemId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SLInfoItemWithRelations*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SLInfoItemWithRelations*)data, error);
                                }
                            }];
}

///
/// 
/// Запрос тегов элемента
///  @param infosystemId Идентификатор информационной системы 
///
///  @param itemId Идентификатор элемента 
///
///  @returns NSArray<SLTag>*
///
-(NSURLSessionTask*) getStaticInfosystemsByInfosystemidItemsByItemidTagsWithInfosystemId: (NSString*) infosystemId
    itemId: (NSString*) itemId
    completionHandler: (void (^)(NSArray<SLTag>* output, NSError* error)) handler {
    // verify the required parameter 'infosystemId' is set
    if (infosystemId == nil) {
        NSParameterAssert(infosystemId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"infosystemId"] };
            NSError* error = [NSError errorWithDomain:kSLInfosystemsApiErrorDomain code:kSLInfosystemsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'itemId' is set
    if (itemId == nil) {
        NSParameterAssert(itemId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"itemId"] };
            NSError* error = [NSError errorWithDomain:kSLInfosystemsApiErrorDomain code:kSLInfosystemsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/static/infosystems/{infosystemId}/items/{itemId}/tags/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (infosystemId != nil) {
        pathParams[@"infosystemId"] = infosystemId;
    }
    if (itemId != nil) {
        pathParams[@"itemId"] = itemId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SLTag>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SLTag>*)data, error);
                                }
                            }];
}

///
/// 
/// Запрос списка свойств информационных элементов
///  @param infosystemId Идентификатор информационной системы 
///
///  @returns NSArray<SLIProperty>*
///
-(NSURLSessionTask*) getStaticInfosystemsByInfosystemidPropertiesWithInfosystemId: (NSString*) infosystemId
    completionHandler: (void (^)(NSArray<SLIProperty>* output, NSError* error)) handler {
    // verify the required parameter 'infosystemId' is set
    if (infosystemId == nil) {
        NSParameterAssert(infosystemId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"infosystemId"] };
            NSError* error = [NSError errorWithDomain:kSLInfosystemsApiErrorDomain code:kSLInfosystemsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/static/infosystems/{infosystemId}/properties/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (infosystemId != nil) {
        pathParams[@"infosystemId"] = infosystemId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SLIProperty>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SLIProperty>*)data, error);
                                }
                            }];
}

///
/// 
/// Запрос свойства
///  @param infosystemId Идентификатор информационной системы 
///
///  @param propertyKey Идентификатор свойства элемента 
///
///  @returns SLIProperty*
///
-(NSURLSessionTask*) getStaticInfosystemsByInfosystemidPropertiesByPropertykeyWithInfosystemId: (NSString*) infosystemId
    propertyKey: (NSString*) propertyKey
    completionHandler: (void (^)(SLIProperty* output, NSError* error)) handler {
    // verify the required parameter 'infosystemId' is set
    if (infosystemId == nil) {
        NSParameterAssert(infosystemId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"infosystemId"] };
            NSError* error = [NSError errorWithDomain:kSLInfosystemsApiErrorDomain code:kSLInfosystemsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'propertyKey' is set
    if (propertyKey == nil) {
        NSParameterAssert(propertyKey);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"propertyKey"] };
            NSError* error = [NSError errorWithDomain:kSLInfosystemsApiErrorDomain code:kSLInfosystemsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/static/infosystems/{infosystemId}/properties/{propertyKey}/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (infosystemId != nil) {
        pathParams[@"infosystemId"] = infosystemId;
    }
    if (propertyKey != nil) {
        pathParams[@"propertyKey"] = propertyKey;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SLIProperty*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SLIProperty*)data, error);
                                }
                            }];
}

///
/// 
/// Запрос списков тегов
///  @param infosystemId Идентификатор информационной системы 
///
///  @returns NSArray<SLTag>*
///
-(NSURLSessionTask*) getStaticInfosystemsByInfosystemidTagsWithInfosystemId: (NSString*) infosystemId
    completionHandler: (void (^)(NSArray<SLTag>* output, NSError* error)) handler {
    // verify the required parameter 'infosystemId' is set
    if (infosystemId == nil) {
        NSParameterAssert(infosystemId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"infosystemId"] };
            NSError* error = [NSError errorWithDomain:kSLInfosystemsApiErrorDomain code:kSLInfosystemsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/static/infosystems/{infosystemId}/tags/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (infosystemId != nil) {
        pathParams[@"infosystemId"] = infosystemId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SLTag>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SLTag>*)data, error);
                                }
                            }];
}

///
/// 
/// Запрос информации о теге
///  @param tagId Идентификатор тега 
///
///  @returns SLTag*
///
-(NSURLSessionTask*) getStaticInfosystemsTagsByTagidWithTagId: (NSString*) tagId
    completionHandler: (void (^)(SLTag* output, NSError* error)) handler {
    // verify the required parameter 'tagId' is set
    if (tagId == nil) {
        NSParameterAssert(tagId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"tagId"] };
            NSError* error = [NSError errorWithDomain:kSLInfosystemsApiErrorDomain code:kSLInfosystemsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/static/infosystems/tags/{tagId}/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (tagId != nil) {
        pathParams[@"tagId"] = tagId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SLTag*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SLTag*)data, error);
                                }
                            }];
}

///
/// 
/// Получить все инфоэлементы для тега
///  @param tagId Идентификатор тега 
///
///  @returns NSArray<SLInfoItem>*
///
-(NSURLSessionTask*) getStaticInfosystemsTagsByTagidItemsWithTagId: (NSString*) tagId
    completionHandler: (void (^)(NSArray<SLInfoItem>* output, NSError* error)) handler {
    // verify the required parameter 'tagId' is set
    if (tagId == nil) {
        NSParameterAssert(tagId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"tagId"] };
            NSError* error = [NSError errorWithDomain:kSLInfosystemsApiErrorDomain code:kSLInfosystemsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/static/infosystems/tags/{tagId}/items/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (tagId != nil) {
        pathParams[@"tagId"] = tagId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SLInfoItem>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SLInfoItem>*)data, error);
                                }
                            }];
}

///
/// 
/// Запрос списка элементов
///  @param infosystemId Идентификатор информационной системы 
///
///  @param body  
///
///  @returns NSArray<SLInfoItem>*
///
-(NSURLSessionTask*) postStaticInfosystemsByInfosystemidQueryWithInfosystemId: (NSString*) infosystemId
    body: (SLInfosystemQuery*) body
    completionHandler: (void (^)(NSArray<SLInfoItem>* output, NSError* error)) handler {
    // verify the required parameter 'infosystemId' is set
    if (infosystemId == nil) {
        NSParameterAssert(infosystemId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"infosystemId"] };
            NSError* error = [NSError errorWithDomain:kSLInfosystemsApiErrorDomain code:kSLInfosystemsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'body' is set
    if (body == nil) {
        NSParameterAssert(body);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"body"] };
            NSError* error = [NSError errorWithDomain:kSLInfosystemsApiErrorDomain code:kSLInfosystemsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/static/infosystems/{infosystemId}/query/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (infosystemId != nil) {
        pathParams[@"infosystemId"] = infosystemId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = body;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SLInfoItem>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SLInfoItem>*)data, error);
                                }
                            }];
}

///
/// 
/// Запрос списка элементов
///  @param infosystemId Идентификатор информационной системы 
///
///  @param body  
///
///  @returns NSArray<SLInfoItemWithRelations>*
///
-(NSURLSessionTask*) postStaticInfosystemsByInfosystemidQuerywithrelationsWithInfosystemId: (NSString*) infosystemId
    body: (SLInfosystemQuery*) body
    completionHandler: (void (^)(NSArray<SLInfoItemWithRelations>* output, NSError* error)) handler {
    // verify the required parameter 'infosystemId' is set
    if (infosystemId == nil) {
        NSParameterAssert(infosystemId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"infosystemId"] };
            NSError* error = [NSError errorWithDomain:kSLInfosystemsApiErrorDomain code:kSLInfosystemsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'body' is set
    if (body == nil) {
        NSParameterAssert(body);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"body"] };
            NSError* error = [NSError errorWithDomain:kSLInfosystemsApiErrorDomain code:kSLInfosystemsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/static/infosystems/{infosystemId}/queryWithRelations/"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (infosystemId != nil) {
        pathParams[@"infosystemId"] = infosystemId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"x-cid", @"x-token"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = body;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<SLInfoItemWithRelations>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<SLInfoItemWithRelations>*)data, error);
                                }
                            }];
}



@end
