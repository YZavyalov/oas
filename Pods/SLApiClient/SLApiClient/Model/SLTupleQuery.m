#import "SLTupleQuery.h"

@implementation SLTupleQuery

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.limit = @50;
    self.offset = @0;
    self.order = @"DESC";
    self.orderBy = @"created_at";
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"limit": @"limit", @"offset": @"offset", @"order": @"order", @"orderBy": @"orderBy", @"version": @"version" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"limit", @"offset", @"order", @"orderBy", @"version"];
  return [optionalProperties containsObject:propertyName];
}

@end
