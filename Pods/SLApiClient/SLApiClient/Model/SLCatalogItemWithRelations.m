#import "SLCatalogItemWithRelations.h"

@implementation SLCatalogItemWithRelations

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"body": @"body", @"cid": @"cid", @"createdAt": @"created_at", @"creator": @"creator", @"_description": @"description", @"_id": @"id", @"image": @"image", @"name": @"name", @"price": @"price", @"templateId": @"templateId", @"updatedAt": @"updated_at", @"groups": @"groups", @"properties": @"properties" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"body", @"cid", @"createdAt", @"creator", @"_description", @"_id", @"image", @"price", @"updatedAt", @"groups", @"properties"];
  return [optionalProperties containsObject:propertyName];
}

@end
