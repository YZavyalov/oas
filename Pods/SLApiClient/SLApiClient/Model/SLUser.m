#import "SLUser.h"

@implementation SLUser

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"birthday": @"birthday", @"firstName": @"firstName", @"lastName": @"lastName", @"login": @"login", @"middleName": @"middleName", @"profilePicture": @"profilePicture", @"uid": @"uid" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"birthday", @"firstName", @"lastName", @"login", @"middleName", @"profilePicture", ];
  return [optionalProperties containsObject:propertyName];
}

@end
