#import "SLCatalogGroupQuery.h"

@implementation SLCatalogGroupQuery

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.limit = @50;
    self.offset = @0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"limit": @"limit", @"name": @"name", @"offset": @"offset" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"limit", @"name", @"offset"];
  return [optionalProperties containsObject:propertyName];
}

@end
