#import "SLUserQuery.h"

@implementation SLUserQuery

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.limit = @50;
    self.offset = @0;
    self.order = @"DESC";
    self.orderBy = @"created_at";
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"ids": @"ids", @"limit": @"limit", @"offset": @"offset", @"order": @"order", @"orderBy": @"orderBy", @"query": @"query" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"ids", @"limit", @"offset", @"order", @"orderBy", @"query"];
  return [optionalProperties containsObject:propertyName];
}

@end
