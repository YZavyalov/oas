#import "SLCatalogQuery.h"

@implementation SLCatalogQuery

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.limit = @50;
    self.offset = @0;
    self.order = @"DESC";
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"groupId": @"groupId", @"limit": @"limit", @"offset": @"offset", @"order": @"order", @"orderBy": @"orderBy", @"query": @"query", @"shopIdsIn": @"shopIdsIn", @"shopIdsOr": @"shopIdsOr" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"groupId", @"limit", @"offset", @"order", @"orderBy", @"query", @"shopIdsIn", @"shopIdsOr"];
  return [optionalProperties containsObject:propertyName];
}

@end
