#import "SLInfoItem.h"

@implementation SLInfoItem

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"body": @"body", @"createdAt": @"created_at", @"creator": @"creator", @"_description": @"description", @"_id": @"id", @"image": @"image", @"name": @"name", @"updatedAt": @"updated_at" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"body", @"createdAt", @"creator", @"_description", @"_id", @"image", @"updatedAt"];
  return [optionalProperties containsObject:propertyName];
}

@end
