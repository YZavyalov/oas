#import "SLQueryShopsSearch.h"

@implementation SLQueryShopsSearch

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.limit = @50;
    self.offset = @0;
    self.order = @"DESC";
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"catalogItemIds": @"catalogItemIds", @"group": @"group", @"limit": @"limit", @"offset": @"offset", @"order": @"order", @"orderBy": @"orderBy", @"query": @"query", @"rectangle": @"rectangle" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"catalogItemIds", @"group", @"limit", @"offset", @"order", @"orderBy", @"query", @"rectangle"];
  return [optionalProperties containsObject:propertyName];
}

@end
