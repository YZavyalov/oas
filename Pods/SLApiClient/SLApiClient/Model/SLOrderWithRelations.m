#import "SLOrderWithRelations.h"

@implementation SLOrderWithRelations

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"createdAt": @"created_at", @"_id": @"id", @"status": @"status", @"updatedAt": @"updated_at", @"userId": @"userId", @"items": @"items", @"properties": @"properties" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"createdAt", @"_id", @"status", @"updatedAt", @"userId", @"items", @"properties"];
  return [optionalProperties containsObject:propertyName];
}

@end
