#import "SLAddress.h"

@implementation SLAddress

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"apartment": @"apartment", @"area": @"area", @"block": @"block", @"city": @"city", @"country": @"country", @"house": @"house", @"region": @"region", @"street": @"street" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"apartment", @"area", @"block", @"city", @"country", @"house", @"region", @"street"];
  return [optionalProperties containsObject:propertyName];
}

@end
