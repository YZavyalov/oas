//
//  NITLocalSettings.h
//  Talan_realtor
//
//  Created by Alexey Shagaleev on 07/05/17.
//  Copyright © 2017 Alexey Shagaleev. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, NITLocalSettingsCryptoAlgorithm) {
    NITLocalSettingsCryptoAlgorithmNone,
    NITLocalSettingsCryptoAlgorithmAES256
};

NS_ASSUME_NONNULL_BEGIN

@interface NITLocalSettings : NSObject

+ (nullable NITLocalSettings *)sharedInstance;

- (void)setObject:(nullable id)value forKey:(nonnull NSString *)key;
- (id)objectForKey:(nonnull NSString *)key;

- (void)setData:(NSData *)value forKey:(NSString *)key;
- (NSData *)dataForKey:(NSString *)key;

- (void)setInteger:(NSInteger)value forKey:(nonnull NSString *)key;
- (NSInteger)integerForKey:(nonnull NSString *)key;

- (void)setFloat:(float)value forKey:(NSString *)key;
- (float)floatForKey:(NSString *)key;

- (void)setBool:(BOOL)value forKey:(nonnull NSString *)key;
- (BOOL)boolForKey:(nonnull NSString *)key;

- (void)setString:(NSString *)value forKey:(nonnull NSString *)key;
- (NSString *)stringForKey:(nonnull NSString *)key;

- (void)setString:(NSString *)value forKey:(nonnull NSString *)key withCryptoAlgorithm:(NITLocalSettingsCryptoAlgorithm)algorithm;
- (NSString *)stringForKey:(nonnull NSString *)key withCryptoAlgorithm:(NITLocalSettingsCryptoAlgorithm)algorithm;

- (void)setArray:(NSArray *)value forKey:(nonnull NSString *)key;
- (NSArray *)arrayForKey:(nonnull NSString *)key;

- (void)setDictionary:(NSDictionary *)value forKey:(NSString *)key;
- (NSDictionary *)dictionaryForKey:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
