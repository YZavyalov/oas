NITLocalSettings - NSUserDefaulr wrapper

Setters/Getters for:
    - Object
    - Data
    - String
    - String with encryption (AES256 only)
    - Integer
    - Float
    - BOOL