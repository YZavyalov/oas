//
//  NITLocalSettings.h
//  Talan_realtor
//
//  Created by Alexey Shagaleev on 07/05/17.
//  Copyright © 2017 Alexey Shagaleev. All rights reserved.
//

#import "NITLocalSettings.h"
#import <FBEncryptor/FBEncryptorAES.h>
#import <UICKeyChainStore/UICKeyChainStore.h>

#define UserDefaults [NSUserDefaults standardUserDefaults]

NSString * const keyChainKey = @"Talan_realtor_nit";

@implementation NITLocalSettings

+ (NITLocalSettings *)sharedInstance {
    static dispatch_once_t	once;
    static NITLocalSettings *shared;
    
    dispatch_once(&once, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}


- (id)init {
    self = [super init];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup {
    
}

#pragma mark - Public

- (void)setObject:(nullable id)value forKey:(nonnull NSString *)key {
    if (value == nil) {
        [UserDefaults removeObjectForKey:key];
    }
    else {
        [UserDefaults setObject:value forKey:key];
    }
    
    [UserDefaults synchronize];
}

- (id)objectForKey:(nonnull NSString *)key {
    return [UserDefaults objectForKey:key];
}


- (void)setData:(NSData *)value forKey:(NSString *)key {
    [self setObject:value forKey:key];
}

- (NSData *)dataForKey:(NSString *)key {
    return (NSData *)[self objectForKey:key];
}


- (void)setInteger:(NSInteger)value forKey:(nonnull NSString *)key {
    [UserDefaults setInteger:value forKey:key];
    [UserDefaults synchronize];
}

- (NSInteger)integerForKey:(nonnull NSString *)key {
    return [UserDefaults integerForKey:key];
}


- (void)setFloat:(float)value forKey:(NSString *)key {
    [UserDefaults setFloat:value forKey:key];
    [UserDefaults synchronize];
}

- (float)floatForKey:(NSString *)key {
    return [UserDefaults floatForKey:key];
}


- (void)setBool:(BOOL)value forKey:(nonnull NSString *)key  {
    [UserDefaults setBool:value forKey:key];
    [UserDefaults synchronize];
}

- (BOOL)boolForKey:(nonnull NSString *)key  {
    return [UserDefaults boolForKey:key];
}


- (void)setString:(NSString *)value forKey:(nonnull NSString *)key {
    [self setObject:value forKey:key];
}

- (NSString *)stringForKey:(nonnull NSString *)key {
    return (NSString *)[self objectForKey:key];
}


- (void)setString:(NSString *)value forKey:(nonnull NSString *)key withCryptoAlgorithm:(NITLocalSettingsCryptoAlgorithm)algorithm {
    NSString *string = value;
    
    if ([value isKindOfClass:[NSString class]]) {
        switch (algorithm) {
            case NITLocalSettingsCryptoAlgorithmAES256:
                string = [FBEncryptorAES encryptBase64String:value keyString:[self keyForCryptoAlgorithm] separateLines:false];
                break;
                
            default:
                break;
        }
    }
    
    [self setString:string forKey:key];
}

- (NSString *)stringForKey:(nonnull NSString *)key withCryptoAlgorithm:(NITLocalSettingsCryptoAlgorithm)algorithm {
    NSString *string = [self stringForKey:key];
    
    if ([string isKindOfClass:[NSString class]]) {
        switch (algorithm) {
            case NITLocalSettingsCryptoAlgorithmAES256:
                string = [FBEncryptorAES decryptBase64String:string keyString:[self keyForCryptoAlgorithm]];
                break;
                
            default:
                break;
        }
    }
    
    return string;
}


- (void)setArray:(NSArray *)value forKey:(nonnull NSString *)key {
    [self setObject:value forKey:key];
}

- (NSArray *)arrayForKey:(nonnull NSString *)key {
    return (NSArray *)[self objectForKey:key];
}


- (void)setDictionary:(NSDictionary *)value forKey:(NSString *)key {
    [self setObject:value forKey:key];
}

- (NSDictionary *)dictionaryForKey:(NSString *)key {
    return (NSDictionary *)[self objectForKey:key];
}

#pragma mark - Private

- (NSString *)vendorUDID {
    return [[UIDevice currentDevice] identifierForVendor].UUIDString;
}

- (NSString *)vendorFormatedUDID {
    NSString *notFormatedUDID = [self vendorUDID];
    NSString *formatedUDID = [notFormatedUDID stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    return formatedUDID;
}

- (NSString *)keyForCryptoAlgorithm {
    NSString *key = [UICKeyChainStore stringForKey:keyChainKey];

    if (key == nil || key.length == 0) {
        key = [self vendorFormatedUDID];
        [UICKeyChainStore setString:key forKey:keyChainKey];
    }
    
    return key;
}

@end
